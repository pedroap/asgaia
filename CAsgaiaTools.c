#include <python2.7/Python.h>
#include <numpy/arrayobject.h>
#include <math.h>
#include "AsgaiaConstants.h"
#include "MyAsgaiaTools_Salpeter.h"
#include "MyAsgaiaTools_Kroupa.h"
#include "MyAsgaiaTools_Bimodal.h"
#include "MyAsgaiaDistr_Salpeter.h"
#include "MyAsgaiaDistr_Kroupa.h"
#include "MyAsgaiaDistr_Bimodal.h"

#define ARRAYF(p) ((float *) (((PyArrayObject *)p)->data))//
#define ARRAYV(p) ((void *) (PyArray_DATA((PyArrayObject*)p)))
#define ARRAYI(p) ((int *) (PyArray_DATA((PyArrayObject*)p)))
#define ARRAYC(p) ((char *) (PyArray_DATA((PyArrayObject*)p)))


// Version 2.5: April, 9, 2018
// spectroscopic tools implemented
// 
//
//+++++++++++  THIS LIBRARY INCLUDES: +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//		*IMFdistr
//		*IMF
//		*StellarAbsMagnitude
//		*StellarParameters
//
//-------------- extinction tools ---------------------------------------
//		*ColourExcessMap
//		*ColourExcess3DMap
//		*SharmaCorrection
//		*GeometricCorrection
//		*GetAV
//
//-------------- astrometric tools ---------------------------------------
//		*SigmaPiGaia
//		*GetSigmaPiGaia
//		*ConversionSigmas
//
//-------------- photometric tools ---------------------------------------
//		*PhotometryGaia
//		*GetPhotometryGaia
//
//--------------- spectroscopic tools ------------------------------------
//		*SigmaVrGaia
//		*GetSigmaVrGaia
//
//-------------- distribution tools ---------------------------------------
//		*GetSigmaPiDistr
//		*GetGaiaIMGhist
//		*GetMagnitude
//		*CMD
//		*HR





//
//----------------------------- IMFdistr -----------------------------------------------------// 
static PyObject *CAsgaiaTools_IMFdistr(PyObject *self,PyObject *args){

	Py_Initialize();//needed command
	import_array();// needed command

	float *cv_masses;
	int nrows;//Length of the arrays
	char IMF_type='s'; //S,s mean Salpeter IMF. K, k mean Kroupa IMF. B, b mean Bimodal
	float IMF_slope = 2.35; // It is used only when IMF_type = 'b'

	//Output issue
	float *cv_Pmi; //Its dimensions must be nrows
	void *cv_void_Pmi;
	npy_intp dims_out[1];

	PyArrayObject *pv_Pmi;
	PyObject *pv_masses; //Prefix pv means Python Vector
	Py_ssize_t Nargs = PyTuple_Size(args); //Number of the input arguments received

	//Check the number of input arguments
	if(Nargs>=1 && Nargs<=3){
		PyArg_ParseTuple(args,"O|cf", &pv_masses, &IMF_type, &IMF_slope); //Python input arguments
	}else{
		fprintf(stderr, " *** Warning: IMFdistr only needs one input argument\n");
		return Py_BuildValue("i", 0);
	};

	//Analyse the IMF_type:
	IMF_type = IMF_type*(IMF_type>=65 && IMF_type<=122);
	IMF_type = IMF_type + 32*(IMF_type<97) + 65*(IMF_type==0);
	if(IMF_type==115){
		fprintf(stderr,"   INFO: Salpeter IMF will be used as IMF\n");
	}else if(IMF_type==107){
		fprintf(stderr,"   INFO: Kroupa IMF will be used as IMF\n");
	}else if(IMF_type==98){
		fprintf(stderr,"   INFO: Bimodal IMF will be used as IMF\n");
	}else{
		fprintf(stderr,"***WARNING: unknown IMF. Salpeter will be used by IMF\n");
		IMF_type = 115;
	};

		
	//Get number of particles:
	nrows = (int) ((PyArrayObject*)pv_masses)->dimensions[0];

	//Create output python vector using nrows
	dims_out[0] = nrows;
	pv_Pmi  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); //Python vector (of floats)
	

	//Let's link two types of pointers
	cv_masses = ARRAYF(pv_masses);

	cv_void_Pmi = ARRAYV(pv_Pmi);
	cv_Pmi = (float*)cv_void_Pmi;

	//Calling to the IMF function:
	if(IMF_type == 107){
		KroupaIMFdistr( cv_masses, nrows, cv_Pmi);
	}else if(IMF_type == 98){
		BimodalIMFdistr( cv_masses, IMF_slope, nrows, cv_Pmi);
	}else{
		SalpeterIMFdistr( cv_masses, nrows, cv_Pmi);
	};

	//Copy it to a new direction
	memcpy(cv_void_Pmi, cv_Pmi, PyArray_ITEMSIZE((PyArrayObject*) pv_Pmi)*nrows);
//	printf("Free was executed\n");

	return Py_BuildValue("O", pv_Pmi);

}; // End of IMFdistr
//------------------------------------------------------------------------------------------------------------//






//----------------------------- IMF -----------------------------------------------------// 
static PyObject *CAsgaiaTools_IMF(PyObject *self,PyObject *args){

	Py_Initialize();//needed command
	import_array();// needed command

	float *cv_masses;
	int nrows;//Length of the arrays
	char IMF_type='s'; //S,s mean Salpeter IMF. K, k mean Kroupa IMF. B, b mean Bimodal
	float IMF_slope = 2.35; // It is used only when IMF_type = 'b'
	float cutoff = INFINITY; // Upper mass limit of the IMF

	//Output issue
	float *cv_Nmi; //Its dimensions must be nrows
	void *cv_void_Nmi;
	npy_intp dims_out[1];

	PyArrayObject *pv_Nmi;
	PyObject *pv_masses; //Prefix pv means Python Vector
	Py_ssize_t Nargs = PyTuple_Size(args); //Number of the input arguments received

	//Check the number of input arguments
	if(Nargs>=1 && Nargs<=4){
		PyArg_ParseTuple(args,"O|cff", &pv_masses, &IMF_type, &cutoff, &IMF_slope); //Python input arguments
	}else{
		fprintf(stderr, " *** Warning: IMF only needs one input argument\n");
		return Py_BuildValue("i", 0);
	};

	//Analyse the IMF_type:
	IMF_type = IMF_type*(IMF_type>=65 && IMF_type<=122);
	IMF_type = IMF_type + 32*(IMF_type<97) + 65*(IMF_type==0);
	if(IMF_type==115){
		fprintf(stderr,"   INFO: Salpeter IMF selected\n");
	}else if(IMF_type==107){
		fprintf(stderr,"   INFO: Kroupa IMF selected\n");
	}else if(IMF_type==98){
		fprintf(stderr,"   INFO: Bimodal IMF with slope %.3f selected\n", -IMF_slope);
	}else{
		fprintf(stderr,"***WARNING: unknown IMF. Salpeter will be used by IMF\n");
		IMF_type = 115;
	};

		
	//Get number of particles:
	nrows = (int) ((PyArrayObject*)pv_masses)->dimensions[0];

	//Create output python vector using nrows
	dims_out[0] = nrows;
	pv_Nmi  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); //Python vector (of floats)
	

	//Let's link two types of pointers
	cv_masses = ARRAYF(pv_masses);

	cv_void_Nmi = ARRAYV(pv_Nmi);
	cv_Nmi = (float*)cv_void_Nmi;

	//Calling to the IMF function:
	if(IMF_type == 107){
		KroupaIMF( cv_masses, nrows, cutoff, cv_Nmi);
	}else if(IMF_type == 98){
		BimodalIMF( cv_masses, IMF_slope, nrows, cutoff, cv_Nmi);
	}else{
		SalpeterIMF( cv_masses, nrows, cutoff, cv_Nmi);
	};

	//Copy it to a new direction
	memcpy(cv_void_Nmi, cv_Nmi, PyArray_ITEMSIZE((PyArrayObject*) pv_Nmi)*nrows);
//	printf("Free was executed\n");

	return Py_BuildValue("O", pv_Nmi);

}; // End of IMF
//------------------------------------------------------------------------------------------------------------//





//----------------------------- StellarParameters -----------------------------------------------------------// 
static PyObject *CAsgaiaTools_StellarParameters(PyObject *self,PyObject *args){

	Py_Initialize();//needed command
	import_array();// needed command

	float metZ, logAge;
	int logmass_flag = 0;

	//Output issue
	float *cv_masses, *cv_logTe, *cv_logG, *cv_mbol; //Its dimensions must be nrows
	void *cv_void_masses, *cv_void_logTe, *cv_void_logG, *cv_void_mbol;
	npy_intp dims_out[1];

	PyArrayObject *pv_masses, *pv_logTe, *pv_logG, *pv_mbol; //Prefix pv means Python Vector
	Py_ssize_t Nargs = PyTuple_Size(args); //Number of the input arguments received (used here for first time)

	if(Nargs>=2 && Nargs<=3){
		//We have received two or three arguments: metZ, logAge, *IMF_type
		PyArg_ParseTuple(args,"ff|i", &metZ, &logAge, &logmass_flag); //Python input arguments

		//Create output python vector using nrows
		dims_out[0] = Max_Number_of_masses_per_isochrone*(logmass_flag==0) + Max_Number_of_logmasses_per_isochrone*(logmass_flag!=0);

		pv_masses  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); //Python vector (of floats)
		pv_logTe  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); //Python vector (of floats)
		pv_logG  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); //Python vector (of floats)
		pv_mbol  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); //Python vector (of floats)
	

		//Let's link two types of pointers
		cv_void_masses = ARRAYV(pv_masses);
		cv_masses = (float*)cv_void_masses;

		cv_void_logTe = ARRAYV(pv_logTe);
		cv_logTe = (float*)cv_void_logTe;

		cv_void_logG = ARRAYV(pv_logG);
		cv_logG = (float*)cv_void_logG;

		cv_void_mbol = ARRAYV(pv_mbol);
		cv_mbol = (float*)cv_void_mbol;


		// Call to StellarParameters or to StellarParameters_logm
		if(!logmass_flag){
			StellarParameters_linm(metZ, logAge, cv_masses, cv_logTe, cv_logG, cv_mbol);// Uniform distr. masses

		}else{
			StellarParameters_logm(metZ, logAge, cv_masses, cv_logTe, cv_logG, cv_mbol);// Uniform distr. logmasses
		};

		//Copy it to a new direction
		memcpy(cv_void_masses, cv_masses, PyArray_ITEMSIZE((PyArrayObject*) pv_masses)*dims_out[0]);
		memcpy(cv_void_logTe, cv_logTe, PyArray_ITEMSIZE((PyArrayObject*) pv_logTe)*dims_out[0]);
		memcpy(cv_void_logG, cv_logG, PyArray_ITEMSIZE((PyArrayObject*) pv_logG)*dims_out[0]);
		memcpy(cv_void_mbol, cv_mbol, PyArray_ITEMSIZE((PyArrayObject*) pv_mbol)*dims_out[0]);

		return Py_BuildValue("OOOO", pv_masses, pv_logTe, pv_logG, pv_mbol);
	
	}else{
		fprintf(stderr, " *** Warning: StellarParameters must have al least 2 input arguments\n");
		fprintf(stderr, " Zero value will be returned\n");
		return Py_BuildValue("i",0);
	}


}; // End of StellarParameters
//--------------------------------------------------------------------------------------------------------------------------------------//




//----------------------------- StellarAbsMagnitude -----------------------------------------------------------// 
static PyObject *CAsgaiaTools_StellarAbsMagnitude(PyObject *self,PyObject *args){

	Py_Initialize();//needed command
	import_array();// needed command

	float metZ, logAge;
	int logmass_flag = 0;
	int band_id=2; // V as default

	//Output issue
	float *cv_masses, *cv_Mag; //Its dimensions must be nrows
	void *cv_void_masses, *cv_void_Mag;
	npy_intp dims_out[1];

	PyArrayObject *pv_masses, *pv_Mag; //Prefix pv means Python Vector
	Py_ssize_t Nargs = PyTuple_Size(args); //Number of the input arguments received (used here for first time)

	if(Nargs>=2 && Nargs<=4){
		//We have received two or three arguments: metZ, logAge, *IMF_type
		PyArg_ParseTuple(args,"ff|ii", &metZ, &logAge, &logmass_flag, &band_id); //Python input arguments

		//Create output python vector using nrows
		dims_out[0] = Max_Number_of_masses_per_isochrone*(logmass_flag==0) + Max_Number_of_logmasses_per_isochrone*(logmass_flag!=0);

		pv_masses  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); //Python vector (of floats)
		pv_Mag  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); //Python vector (of floats)
	

		//Let's link two types of pointers
		cv_void_masses = ARRAYV(pv_masses);
		cv_masses = (float*)cv_void_masses;

		cv_void_Mag = ARRAYV(pv_Mag);
		cv_Mag = (float*)cv_void_Mag;


		// Call to StellarAbsMagnitude or to StellarAbsMagnitude_logm
		if(!logmass_flag){
			StellarAbsMagnitude_linm(metZ, logAge, cv_masses, cv_Mag, band_id);// Uniform distr. masses

		}else{
			StellarAbsMagnitude_logm(metZ, logAge, cv_masses, cv_Mag, band_id);// Uniform distr. logmasses
		};

		//Copy it to a new direction
		memcpy(cv_void_masses, cv_masses, PyArray_ITEMSIZE((PyArrayObject*) pv_masses)*dims_out[0]);
		memcpy(cv_void_Mag, cv_Mag, PyArray_ITEMSIZE((PyArrayObject*) pv_Mag)*dims_out[0]);

		return Py_BuildValue("OO", pv_masses, pv_Mag);
	
	}else{
		fprintf(stderr, " *** Warning: StellarAbsMagnitude must have al least 2 input arguments\n");
		fprintf(stderr, " Zero value will be returned\n");
		return Py_BuildValue("i",0);
	}


}; // End of StellarAbsMagnitude
//--------------------------------------------------------------------------------------------------------------------------------------//





//----------------------------- ColourExcessMap -----------------------------------------------------// 
static PyObject *CAsgaiaTools_ColourExcessMap(PyObject *self,PyObject *args){

	Py_Initialize();//needed command
	import_array();// needed command

	float *cv_lon, *cv_lat;
	int nrows;//Length of the arrays
	int n_lon, n_lat; //Check if the dimensions are correct

	//Output issue
	float *cv_exS; //Its dimensions must be nrows
	void *cv_void_exS;
	npy_intp dims_out[1];

	PyArrayObject *pv_exS;
	PyObject *pv_lon,*pv_lat; //Prefix pv means Python Vector
	Py_ssize_t Nargs = PyTuple_Size(args); //Number of the input arguments received

	//Check the number of input arguments
	if(Nargs==2){
		PyArg_ParseTuple(args,"OO", &pv_lon, &pv_lat); //Python input arguments
	}else{
		fprintf(stderr, " *** Warning: ColourExcessMap always needs two input arguments\n");
		return Py_BuildValue("i", 0);
	};
		
	//Get number of particles:
	nrows = (int) ((PyArrayObject*)pv_lon)->dimensions[0];

	// Check the dimensions:
	n_lon = nrows;
	n_lat = ((PyArrayObject*)pv_lat)->dimensions[0];


	if(n_lon!=n_lat){
		fprintf(stderr, " *** Warning: dimensions in ColourExcessMap mismatch\n");
		return Py_BuildValue("i", 0);
	};


	//Create output python vector using nrows
	dims_out[0] = nrows;
	pv_exS  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); //Python vector (of floats)
	
//	printf("Read Python Data\n");

	//Let's link two types of pointers
	cv_lon = ARRAYF(pv_lon);
	cv_lat= ARRAYF(pv_lat);

	cv_void_exS = ARRAYV(pv_exS);
	cv_exS = (float*)cv_void_exS;

	//Calling to ColourExcessMap...
	ColourExcessMap( cv_lon, cv_lat, nrows, cv_exS);
	//printf("Fin de ColourExcessMap\n");

	//Copy it to a new direction
	memcpy(cv_void_exS, cv_exS, PyArray_ITEMSIZE((PyArrayObject*) pv_exS)*nrows);
//	printf("Free was executed\n");

	return Py_BuildValue("O", pv_exS);

}; // End of ColourExcessMap
//------------------------------------------------------------------------------------------------------------//



//
//----------------------------- ColourExcess3DMap -----------------------------------------------------------// 
static PyObject *CAsgaiaTools_ColourExcess3DMap(PyObject *self,PyObject *args){

	Py_Initialize();//needed command
	import_array();// needed command

	float *cv_lon, *cv_lat, *cv_dist;
	int nrows;//Length of the arrays
	int n_lon, n_lat, n_dist; //Check if the dimensions are correct
	int pass;

	//Output issue
	float *cv_ex3; //Its dimensions must be nrows
	void *cv_void_ex3;
	npy_intp dims_out[1];

	PyArrayObject *pv_ex3;
	PyObject *pv_lon,*pv_lat, *pv_dist; //Prefix pv means Python Vector
	Py_ssize_t Nargs = PyTuple_Size(args); //Number of the input arguments received

	//Check the number of input arguments
	if(Nargs==3){
		PyArg_ParseTuple(args,"OOO", &pv_lon, &pv_lat, &pv_dist); //Python input arguments
	}else{
		fprintf(stderr, " *** Warning: ColourExcess3DMap always needs three input arguments\n");
		return Py_BuildValue("i",0);
	};
	//Get number of particles:
	nrows = (int) ((PyArrayObject*)pv_lon)->dimensions[0];

	// Check the dimensions:
	n_lon = nrows;
	n_lat = ((PyArrayObject*)pv_lat)->dimensions[0];
	n_dist = ((PyArrayObject*)pv_dist)->dimensions[0];

	pass = (n_lon==n_lat)*(n_lat==n_dist);

	if(!pass){
		fprintf(stderr, " *** Warning: dimensions in ColourExcess3DMap mismatch\n");
		return Py_BuildValue("i",0);
	};


	//Create output python vector using nrows
	dims_out[0] = nrows;
	pv_ex3  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); //Python vector (of floats)
	
//	printf("Read Python Data\n");

	//Let's link two types of pointers
	cv_lon = ARRAYF(pv_lon);
	cv_lat= ARRAYF(pv_lat);
	cv_dist= ARRAYF(pv_dist);

	cv_void_ex3 = ARRAYV(pv_ex3);
	cv_ex3 = (float*)cv_void_ex3;

	//Calling to ColourExcess3DMap...
	ColourExcess3DMap( cv_lon, cv_lat, cv_dist, nrows, cv_ex3);
	//printf("Fin de ColourExcessMap\n");

	//Copy it to a new direction
	memcpy(cv_void_ex3, cv_ex3, PyArray_ITEMSIZE((PyArrayObject*) pv_ex3)*nrows);
//	printf("Free was executed\n");

	return Py_BuildValue("O", pv_ex3);

}; // End of ColourExcess3DMap
//--------------------------------------------------------------------------------------------------------------------------------------//



//----------------------------- SharmaCorrection -----------------------------------------------------------// 
static PyObject *CAsgaiaTools_SharmaCorrection(PyObject *self,PyObject *args){

	Py_Initialize();//needed command
	import_array();// needed command

	float *cv_exS, *cv_ex3;
	int nrows;//Length of the arrays
	int n_exS, n_ex3; //Check if the dimensions are correct
	int k; // Counter

	//Output issue
	float *cv_ex; //Its dimensions must be nrows
	void *cv_void_ex;
	npy_intp dims_out[1];

	PyArrayObject *pv_ex;
	PyObject *pv_exS,*pv_ex3; //Prefix pv means Python Vector
	Py_ssize_t Nargs = PyTuple_Size(args); //Number of the input arguments received (used here for first time)

	if(Nargs == 1){
		//We have received one argument: ex
		PyArg_ParseTuple(args,"O", &pv_exS); //we name it as exS because ex is the output

		//Get number of particles:
		nrows = (int) ((PyArrayObject*)pv_exS)->dimensions[0];

		//Create output python vector using nrows
		dims_out[0] = nrows;
		pv_ex  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); //Python vector (of floats)

		//Let's link two types of pointers
		cv_exS = ARRAYF(pv_exS);

		cv_void_ex = ARRAYV(pv_ex);
		cv_ex = (float*)cv_void_ex;

		// Apply the Sharma correction:
		for(k=0; k<nrows; k++){
			cv_ex[k] = cv_exS[k]*(0.8-0.2*tanh(10.*cv_exS[k]-1.5));
		}

		//Copy it to a new direction
		memcpy(cv_void_ex, cv_ex, PyArray_ITEMSIZE((PyArrayObject*) pv_ex)*nrows);

		return Py_BuildValue("O", pv_ex);
		
	}else if(Nargs==2){
		//We have receive two arguments: exS and ex3
		PyArg_ParseTuple(args,"OO", &pv_exS, &pv_ex3); //Python input arguments

		//Get number of particles:
		nrows = (int) ((PyArrayObject*)pv_exS)->dimensions[0];

		// Check the dimensions:
		n_exS = nrows;
		n_ex3 = ((PyArrayObject*)pv_ex3)->dimensions[0];
	
		if(n_exS!=n_ex3){
			fprintf(stderr, " *** Warning: dimensions in SharmaCorrection mismatch\n");
			fprintf(stderr, "\tZero value will be returned\n");
			return Py_BuildValue("i",0);
		};

		//Create output python vector using nrows
		dims_out[0] = nrows;
		pv_ex  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); //Python vector (of floats)
	
	//	printf("Read Python Data\n");

		//Let's link two types of pointers
		cv_exS = ARRAYF(pv_exS);
		cv_ex3= ARRAYF(pv_ex3);

		cv_void_ex = ARRAYV(pv_ex);
		cv_ex = (float*)cv_void_ex;

		// Apply the Sharma correction:
		for(k=0; k<nrows; k++){
			cv_ex[k] = cv_exS[k]*cv_ex3[k];
			cv_ex[k] = cv_ex[k]*(0.8-0.2*tanh(10.*cv_ex[k]-1.5));
		}

		//Copy it to a new direction
		memcpy(cv_void_ex, cv_ex, PyArray_ITEMSIZE((PyArrayObject*) pv_ex)*nrows);
	//	printf("Free was executed\n");

		return Py_BuildValue("O", pv_ex);
	}else{
		fprintf(stderr, " *** Warning: SharmaCorrection must have 1 or 2 input arguments\n");
		fprintf(stderr, " Zero value will be returned\n");
		return Py_BuildValue("i",0);
	}


}; // End of SharmaCorrection
//--------------------------------------------------------------------------------------------------------------------------------------//



//----------------------------- GeometricCorrection -----------------------------------------------------------// 
static PyObject *CAsgaiaTools_GeometricCorrection(PyObject *self,PyObject *args){

	Py_Initialize();//needed command
	import_array();// needed command

	float *cv_lon, *cv_lat;
	int n_lon, n_lat; //Check if the dimensions are correct
	int flag=-1; // 1 or -1 (default value)	

	//Output issue
	float *cv_correction; //Its dimensions must be nrows
	void *cv_void_correction;
	npy_intp dims_out[1];

	PyArrayObject *pv_correction;
	PyObject *pv_lon,*pv_lat; //Prefix pv means Python Vector
	Py_ssize_t Nargs = PyTuple_Size(args); //Number of the input arguments received (used here for first time)

	if(Nargs==2 || Nargs==3){
		//We have received two or three arguments: lon, lat and flag
		PyArg_ParseTuple(args,"OO|i", &pv_lon, &pv_lat, &flag); //Python input arguments

		//Get number of particles:
		n_lon = (int) ((PyArrayObject*)pv_lon)->dimensions[0];

		// Check the dimensions:
		n_lat = ((PyArrayObject*)pv_lat)->dimensions[0];
	
		if(n_lon!=n_lat){
			fprintf(stderr, " *** Warning: dimensions in GeometricCorrection mismatch\n");
			fprintf(stderr, "\tZero value will be returned\n");
			return Py_BuildValue("i",0);
		};

		//Create output python vector using nrows
		dims_out[0] = n_lon;
		pv_correction  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); //Python vector (of floats)
	
	//	printf("Read Python Data\n");

		//Let's link two types of pointers
		cv_lon = ARRAYF(pv_lon);
		cv_lat= ARRAYF(pv_lat);

		cv_void_correction = ARRAYV(pv_correction);
		cv_correction = (float*)cv_void_correction;

		// Call to geometric_correction
		geometric_correction( cv_lon, cv_lat, n_lon, flag, cv_correction );
		//Copy it to a new direction
		memcpy(cv_void_correction, cv_correction, PyArray_ITEMSIZE((PyArrayObject*) pv_correction)*n_lon);
	//	printf("Free was executed\n");

		return Py_BuildValue("O", pv_correction);
	}else{
		fprintf(stderr, " *** Warning: GeometricCorrection must have at least 2 input arguments\n");
		fprintf(stderr, " Zero value will be returned\n");
		return Py_BuildValue("i",0);
	}


}; // End of GeometricCorrection
//--------------------------------------------------------------------------------------------------------------------------------------//





//----------------------------- GetAV -----------------------------------------------------------------------------------------// 
static PyObject *CAsgaiaTools_GetAV(PyObject *self,PyObject *args){

	Py_Initialize();//needed command
	import_array();// needed command
	int k; // Counter

	//Inputs
	float *cv_lon, *cv_lat, *cv_dist;
	
	// Intermediate quantities:
	float *cv_aux; // It will be the correction due to the Gaia' scanning law

	//Output issue
	float *cv_ex31;
	void *cv_void_ex31;

	// Checks
	int n_lon, n_lat, n_dist, pass; //Check if the dimensions are correct
	npy_intp dims_out[1];

	// Python structures
	PyArrayObject *pv_ex31; // Outputs
	PyArrayObject *pv_aux; //Intermediate
	PyObject *pv_lon, *pv_lat, *pv_dist; // Inputs


	Py_ssize_t Nargs = PyTuple_Size(args); // Number of the input arguments received (used here for first time)

	if(Nargs==3){
		//We have receive five arguments: metZ, logAge, dist(kpc), ex31=A_V, correction
		PyArg_ParseTuple(args,"OOO", &pv_lon, &pv_lat, &pv_dist); //Python input arguments

		//Get number of particles:
		n_dist = (int) ((PyArrayObject*)pv_dist)->dimensions[0];

		// Check the dimensions:
		n_lon = ((PyArrayObject*)pv_lon)->dimensions[0];
		n_lat = ((PyArrayObject*)pv_lat)->dimensions[0];

		// Condition to continue:
		pass = (n_dist==n_lon)*(n_lon==n_lat);
	
		if(!pass){
			fprintf(stderr, " *** Warning: dimensions in GetAV mismatch\n");
			fprintf(stderr, "\tZero value will be returned\n");
			return Py_BuildValue("i",0);
		};

		//Create output python vector using nrows
		dims_out[0] = n_lon;
		pv_ex31  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); //Python vector (of floats)

		//Create the intermediate python vector
		pv_aux  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); //Python vector (of floats)
	

		//Let's link two types of pointers
		cv_lon = ARRAYF(pv_lon);
		cv_lat = ARRAYF(pv_lat);
		cv_dist = ARRAYF(pv_dist);

		cv_aux = ARRAYF(pv_aux);

		cv_void_ex31 = ARRAYV(pv_ex31);
		cv_ex31 = (float*)cv_void_ex31;

		//First calculate the Schlegel extinction
		pass = ColourExcessMap(cv_lon, cv_lat, n_lon, cv_ex31); //cv_ex31: only the Schlegel values

		if(!pass){
			fprintf(stderr, " *** Warning: problems with the calling to ColourExcessMap with GetAV\n");
			fprintf(stderr, "\tZero value will be returned\n");
			return Py_BuildValue("i",0);
		};
		
		// Then, the 3D correction
		pass = ColourExcess3DMap(cv_lon, cv_lat, cv_dist, n_lon, cv_aux);// cv_aux: the 3d correction

		if(!pass){
			fprintf(stderr, " *** Warning: problems with the calling to ColourExcess3DMap with GetAV\n");
			fprintf(stderr, "\tZero value will be returned\n");
			return Py_BuildValue("i",0);
		};
		
		// Now apply the SharmaCorrection and multiply by 3.24 to get the extinction in the V-band
		for(k=0;k<n_dist;k++){
			cv_ex31[k] = cv_aux[k]*cv_ex31[k];
			cv_ex31[k] = 3.24*cv_ex31[k]*(0.8-0.2*tanh(10.*cv_ex31[k]-1.5));

		}; // End of the Sharma Correction
		// The A_V extinction is calculated!

		//Copy the output to a new direction
		memcpy(cv_void_ex31, cv_ex31, PyArray_ITEMSIZE((PyArrayObject*) pv_ex31)*n_dist);

		return Py_BuildValue("O", pv_ex31);
	}else{
		fprintf(stderr, " *** Warning: GetAV must have 3 input arguments\n");
		fprintf(stderr, " Zero value will be returned\n");
		return Py_BuildValue("i",0);
	}

}; // End of GetAV
//--------------------------------------------------------------------------------------------------------------------------------------//



//----------------------------- SigmaPiGaia -----------------------------------------------------------// 
static PyObject *CAsgaiaTools_SigmaPiGaia(PyObject *self,PyObject *args){

	Py_Initialize();//needed command
	import_array();// needed command

	int k; // Counter

	float *cv_metZ, *cv_logAge, *cv_mass;
	float *cv_dist, *cv_ex31, *cv_correction; // Correction must include the correction due to the Gaia' scanning law, the calibration factor CAfactor and the epoch correction
	int n_metZ, n_logAge, n_mass, n_dist, n_ex31, pass; //Check if the dimensions are correct
	float Cfactor = 1.2; //Correction factor = CAfactor (calibration)*epoch correction (sqrt(60./month))
	// Range of the G mag.
	float G_limits_min = 3.;
	float G_limits_max = 20.;
	float G_limits[2]; 

	// Flags
	int interpolation = 0;
	char IMF_type='s'; //S,s mean Salpeter IMF. K, k mean Kroupa IMF. B,b mean Bimodal IMF

	// Other optional inputs:
	float rspicoff = INFINITY; //rspicoff: Relative Sigma_PI CutOFF = Max of sigma_pi/parallax
	float IMF_slope = 2.35;
	float float_oargs[2];

	//Output issue
	float *cv_errorpi, *cv_nobstars; //Its dimensions must be nrows
	void *cv_void_errorpi, *cv_void_nobstars;
	npy_intp dims_out[1];

	PyArrayObject *pv_errorpi, *pv_nobstars;
	PyObject *pv_metZ,*pv_logAge, *pv_mass; //Prefix pv means Python Vector
	PyObject *pv_dist,*pv_ex31,*pv_correction; //Prefix pv means Python Vector
	Py_ssize_t Nargs = PyTuple_Size(args); //Number of the input arguments received (used here for first time)


	if(Nargs==6 || Nargs==13){
		//We have received six or thirteen arguments: metZ, logAge, mass, dist(kpc), ex31=A_V, correction, *IMF_type, *Cfactor, *interpolation, *G_limits_min, *G_limits_max, *IMF_slope, *rspicoff
		PyArg_ParseTuple(args,"OOOOOO|cfiffff", &pv_metZ, &pv_logAge, &pv_mass, &pv_dist, &pv_ex31, &pv_correction, &IMF_type, &Cfactor, &interpolation, &G_limits_min, &G_limits_max, &IMF_slope, &rspicoff); //Python input arguments

		// G limits:
		G_limits[0] = G_limits_min;
		G_limits[1] = G_limits_max;

		// Float options
		float_oargs[0] = rspicoff;
		float_oargs[1] = IMF_slope;

		//Analyse the IMF_type:
		IMF_type = IMF_type*(IMF_type>=65 && IMF_type<=122);
		IMF_type = IMF_type + 32*(IMF_type<97) + 65*(IMF_type==0);
		if(IMF_type==115){
			fprintf(stderr,"   INFO: Salpeter IMF will be used as IMF\n");
		}else if(IMF_type==107){
			fprintf(stderr,"   INFO: Kroupa IMF will be used as IMF\n");
		}else if(IMF_type==98){
			fprintf(stderr,"   INFO: Bimodal IMF will be used as IMF\n");
		}else{
			fprintf(stderr,"***WARNING: unknown IMF. Salpeter will be used by IMF\n");
			IMF_type = 115;
		};

		//Get number of particles:
		n_metZ = (int) ((PyArrayObject*)pv_metZ)->dimensions[0];

		// Check the dimensions:
		n_logAge = ((PyArrayObject*)pv_logAge)->dimensions[0];
		n_mass = ((PyArrayObject*)pv_mass)->dimensions[0];
		n_dist = ((PyArrayObject*)pv_dist)->dimensions[0];
		n_ex31 = ((PyArrayObject*)pv_ex31)->dimensions[0];

		pass = (n_metZ==n_logAge)*(n_logAge==n_mass)*(n_mass==n_dist)*(n_dist==n_ex31);
	
		if(!pass){
			fprintf(stderr, " *** Warning: dimensions in SigmaPiGaia mismatch\n");
			fprintf(stderr, "\tZero value will be returned\n");
			return Py_BuildValue("i",0);
		};

		//Create output python vector using nrows
		dims_out[0] = n_metZ;
		pv_errorpi  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); //Python vector (of floats)
		pv_nobstars  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); //Python vector (of floats)
	

		//Let's link two types of pointers
		cv_metZ = ARRAYF(pv_metZ);
		cv_logAge= ARRAYF(pv_logAge);
		cv_mass= ARRAYF(pv_mass);
		cv_dist = ARRAYF(pv_dist);
		cv_ex31= ARRAYF(pv_ex31);
		cv_correction= ARRAYF(pv_correction);

		cv_void_errorpi = ARRAYV(pv_errorpi);
		cv_errorpi = (float*)cv_void_errorpi;

		cv_void_nobstars = ARRAYV(pv_nobstars);
		cv_nobstars = (float*)cv_void_nobstars;

		//Now calculate the Gaia errors in pi:
		pass = 10*(IMF_type==107)+20*(IMF_type==98)+1*(interpolation==1); //pass is a bitmask now

		//Accounts for the CAfactor and epoch correction:
		for(k=0;k<n_metZ;k++){
			cv_correction[k]*=Cfactor;
		}

		switch(pass){
			case 21:
				fprintf(stderr, "\t Calling to SigmaPiGaiaBimodal_4Points (%i)...",pass);
				SigmaPiGaiaBimodal_4Points( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_ex31, cv_correction, cv_errorpi, cv_nobstars, n_metZ, G_limits, float_oargs);
				break;
			case 20:
				fprintf(stderr, "\t Calling to SigmaPiGaia_Bimodal (%i)...",pass);
				SigmaPiGaia_Bimodal( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_ex31, cv_correction, cv_errorpi, cv_nobstars, n_metZ, G_limits, float_oargs);
				break;
			case 11:
				fprintf(stderr, "\t Calling to SigmaPiGaiaKroupa_4Points (%i)...",pass);
				SigmaPiGaiaKroupa_4Points( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_ex31, cv_correction, cv_errorpi, cv_nobstars, n_metZ, G_limits, float_oargs);
				break;
			case 10:
				fprintf(stderr, "\t Calling to SigmaPiGaia_Kroupa (%i)...",pass);
				SigmaPiGaia_Kroupa( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_ex31, cv_correction, cv_errorpi, cv_nobstars, n_metZ, G_limits, float_oargs);
				break;
			case 1:
				fprintf(stderr, "\t Calling to SigmaPiGaiaSalpeter_4Points (%i)...",pass);
				SigmaPiGaiaSalpeter_4Points( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_ex31, cv_correction, cv_errorpi, cv_nobstars, n_metZ, G_limits, float_oargs);
				break;
			case 0:
				fprintf(stderr, "\t Calling to SigmaPiGaia_Salpeter (%i)...",pass);
				SigmaPiGaia_Salpeter( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_ex31, cv_correction, cv_errorpi, cv_nobstars, n_metZ, G_limits, float_oargs);
				break;
			default:
				fprintf(stderr, "\tCalling to SigmaPiGaia_Salpeter (%i)...",pass);
				SigmaPiGaia_Salpeter( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_ex31, cv_correction, cv_errorpi, cv_nobstars, n_metZ, G_limits, float_oargs);
		};
		fprintf(stderr, "finished.\n");	
		//Copy it to a new direction
		memcpy(cv_void_errorpi, cv_errorpi, PyArray_ITEMSIZE((PyArrayObject*) pv_errorpi)*n_metZ);
		memcpy(cv_void_nobstars, cv_nobstars, PyArray_ITEMSIZE((PyArrayObject*) pv_nobstars)*n_metZ);
	//	printf("Free was executed\n");

		return Py_BuildValue("OO", pv_errorpi, pv_nobstars);
	}else{
		fprintf(stderr, " *** Warning: SigmaPiGaia must have either 6 or 13 input arguments\n");
		fprintf(stderr, " Zero value will be returned\n");
		return Py_BuildValue("i",0);
	}


}; // End of SigmaPiGaia
//--------------------------------------------------------------------------------------------------------------------------------------//







//----------------------------- GetSigmaPiGaia -----------------------------------------------------------------------------------------// 
static PyObject *CAsgaiaTools_GetSigmaPiGaia(PyObject *self,PyObject *args){

	Py_Initialize();//needed command
	import_array();// needed command
	int k; // Counter

	//Inputs
	float *cv_lon, *cv_lat, *cv_dist;
	float *cv_metZ, *cv_logAge, *cv_mass;
	float Cfactor = 1.2; //Correction factor = CAfactor (calibration)*epoch correction (sqrt(60./month))
	int correction_flag = -1;
	int interpolation = 0;
	char IMF_type='s'; //S,s mean Salpeter IMF. K, k mean Kroupa IMF. B, b mean Bimodal

	// Other optional inputs:
	float rspicoff = INFINITY; //rspicoff: Relative Sigma_PI CutOFF = Max of sigma_pi/parallax
	float IMF_slope = 2.35;
	float float_oargs[2];

	// Range of the G mag.
	float G_limits_min = 3.;
	float G_limits_max = 20.;
	float G_limits[2]; 
	
	// Intermediate quantities:
	float *cv_aux; // It will be the correction due to the Gaia' scanning law

	//Output issue
	float *cv_errorpi, *cv_ex31, *cv_nobstars;
	void *cv_void_errorpi, *cv_void_ex31, *cv_void_nobstars;

	// Checks
	int n_lon, n_lat, n_mass, n_dist, n_metZ, n_logAge, pass; //Check if the dimensions are correct
	npy_intp dims_out[1];

	// Python structures
	PyArrayObject *pv_errorpi, *pv_ex31, *pv_nobstars; // Outputs
	PyArrayObject *pv_aux; //Intermediate
	PyObject *pv_lon, *pv_lat, *pv_dist; // Inputs
	PyObject *pv_metZ,*pv_logAge, *pv_mass; // Inputs


	Py_ssize_t Nargs = PyTuple_Size(args); // Number of the input arguments received (used here for first time)



	if(Nargs==6 || Nargs==14){
		//We can receive either six or thirteen arguments: lon, lat, dist(kpc), metZ, logAge, *IMF_type, *Cfactor, *correction_flag, *interpolation, *G_limits_min, *G_limits_max, *IMF_slope
		PyArg_ParseTuple(args,"OOOOOO|cfiiffff", &pv_lon, &pv_lat, &pv_dist, &pv_metZ, &pv_logAge, &pv_mass, &IMF_type, &Cfactor, &correction_flag, &interpolation, &G_limits_min, &G_limits_max, &IMF_slope, &rspicoff); //Python input arguments

		// G limits:
		G_limits[0] = G_limits_min;
		G_limits[1] = G_limits_max;

		// Float options
		float_oargs[0] = rspicoff;
		float_oargs[1] = IMF_slope;

		//Analyse the IMF_type:
		IMF_type = IMF_type*(IMF_type>=65 && IMF_type<=122);
		IMF_type = IMF_type + 32*(IMF_type<97) + 65*(IMF_type==0);
		if(IMF_type==115){
			fprintf(stderr,"   INFO: Salpeter IMF will be used as IMF\n");
		}else if(IMF_type==107){
			fprintf(stderr,"   INFO: Kroupa IMF will be used as IMF\n");
		}else if(IMF_type==98){
			fprintf(stderr,"   INFO: Bimodal IMF will be used as IMF\n");
		}else{
			fprintf(stderr,"***WARNING: unknown IMF. Salpeter will be used by IMF\n");
			IMF_type = 115;
		};

		//Get number of particles:
		n_metZ = (int) ((PyArrayObject*)pv_metZ)->dimensions[0];

		// Check the dimensions:
		n_logAge = ((PyArrayObject*)pv_logAge)->dimensions[0];
		n_mass = ((PyArrayObject*)pv_mass)->dimensions[0];
		n_lon = ((PyArrayObject*)pv_lon)->dimensions[0];
		n_lat = ((PyArrayObject*)pv_lat)->dimensions[0];
		n_dist = ((PyArrayObject*)pv_dist)->dimensions[0];

		// Condition to continue:
		pass = (n_metZ==n_logAge)*(n_logAge==n_mass)*(n_mass==n_dist)*(n_dist==n_lon)*(n_lon==n_lat);
	
		if(!pass){
			fprintf(stderr, " *** Warning: dimensions in GetSigmaPiGaia mismatch\n");
			fprintf(stderr, "\tZero value will be returned\n");
			return Py_BuildValue("i",0);
		};

		//Create output python vector using nrows
		dims_out[0] = n_metZ;
		pv_errorpi  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); //Python vector (of floats)
		pv_nobstars  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); //Python vector (of floats)
		pv_ex31  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); //Python vector (of floats)

		//Create the intermediate python vector
		pv_aux  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); //Python vector (of floats)
	

		//Let's link two types of pointers
		cv_lon = ARRAYF(pv_lon);
		cv_lat = ARRAYF(pv_lat);
		cv_dist = ARRAYF(pv_dist);
		cv_metZ = ARRAYF(pv_metZ);
		cv_logAge = ARRAYF(pv_logAge);
		cv_mass = ARRAYF(pv_mass);

		cv_aux = ARRAYF(pv_aux);

		cv_void_errorpi = ARRAYV(pv_errorpi);
		cv_errorpi = (float*)cv_void_errorpi;

		cv_void_nobstars = ARRAYV(pv_nobstars);
		cv_nobstars = (float*)cv_void_nobstars;

		cv_void_ex31 = ARRAYV(pv_ex31);
		cv_ex31 = (float*)cv_void_ex31;

		//First calculate the Schlegel extinction
		pass = ColourExcessMap(cv_lon, cv_lat, n_lon, cv_ex31); //cv_ex31: only the Schlegel values

		if(!pass){
			fprintf(stderr, " *** Warning: problems with the calling to ColourExcessMap with GetSigmaPiGaia\n");
			fprintf(stderr, "\tZero value will be returned\n");
			return Py_BuildValue("i",0);
		};
		
		// Then, the 3D correction
		pass = ColourExcess3DMap(cv_lon, cv_lat, cv_dist, n_lon, cv_aux);// cv_aux: the 3d correction
		if(!pass){
			fprintf(stderr, " *** Warning: problems with the calling to ColourExcess3DMap with GetSigmaPiGaia\n");
			fprintf(stderr, "\tZero value will be returned\n");
			return Py_BuildValue("i",0);
		};
		
		// Now apply the SharmaCorrection and multiply by 3.24 to get the extinction in the V-band
		for(k=0;k<n_lon;k++){
			cv_ex31[k] = cv_aux[k]*cv_ex31[k];
			cv_ex31[k] = 3.24*cv_ex31[k]*(0.8-0.2*tanh(10.*cv_ex31[k]-1.5));

		}; // End of the Sharma Correction
		// The A_V extinction is calculated!

		// Now calculate the scanning law correction:
		geometric_correction( cv_lon, cv_lat, n_lon, correction_flag, cv_aux ); //Now aux is the geom. correction
		// The geometric correction was calculated!
		//Now include the Calibration factor and the epoch correction (sqrt(60/month))
		for(k=0;k<n_lon;k++){
			cv_aux[k]*=Cfactor;
		};

		//Now calculate the Gaia errors in pi:
		pass = 10*(IMF_type==107)+20*(IMF_type==98)+1*(interpolation==1); //pass is a bitmask now

		switch(pass){
			case 21:
				fprintf(stderr, "Calling to SigmaPiGaiaBimodal_4Points...");
				SigmaPiGaiaBimodal_4Points( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_ex31, cv_aux, cv_errorpi, cv_nobstars, n_metZ, G_limits, float_oargs);
				break;
			case 20:
				fprintf(stderr, "Calling to SigmaPiGaia_Bimodal...");
				SigmaPiGaia_Bimodal( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_ex31, cv_aux, cv_errorpi, cv_nobstars, n_metZ, G_limits, float_oargs);
				break;
			case 11:
				fprintf(stderr, "Calling to SigmaPiGaiaKroupa_4Points...");
				SigmaPiGaiaKroupa_4Points( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_ex31, cv_aux, cv_errorpi, cv_nobstars, n_metZ, G_limits, float_oargs);
				break;
			case 10:
				fprintf(stderr, "Calling to SigmaPiGaia_Kroupa...");
				SigmaPiGaia_Kroupa( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_ex31, cv_aux, cv_errorpi, cv_nobstars, n_metZ, G_limits, float_oargs);
				break;
			case 1:
				fprintf(stderr, "\tCalling to SigmaPiGaiaSalpeter_4Points...");
				SigmaPiGaiaSalpeter_4Points( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_ex31, cv_aux, cv_errorpi, cv_nobstars, n_metZ, G_limits, float_oargs);
				break;
			case 0:
				fprintf(stderr, "\tCalling to SigmaPiGaia_Salpeter...");
				SigmaPiGaia_Salpeter( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_ex31, cv_aux, cv_errorpi, cv_nobstars, n_metZ, G_limits, float_oargs);
				break;
			default:
				fprintf(stderr, "\tCalling to SigmaPiGaia_Salpeter as default...");
				SigmaPiGaia_Salpeter( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_ex31, cv_aux, cv_errorpi, cv_nobstars, n_metZ, G_limits, float_oargs);
		};
		fprintf(stderr, "finished.\n");	

		//Copy the output to a new direction
		memcpy(cv_void_errorpi, cv_errorpi, PyArray_ITEMSIZE((PyArrayObject*) pv_errorpi)*n_metZ);
		memcpy(cv_void_nobstars, cv_nobstars, PyArray_ITEMSIZE((PyArrayObject*) pv_nobstars)*n_metZ);
		memcpy(cv_void_ex31, cv_ex31, PyArray_ITEMSIZE((PyArrayObject*) pv_ex31)*n_metZ);

		return Py_BuildValue("OOO", pv_errorpi, pv_nobstars, pv_ex31);
	}else{
		fprintf(stderr, " *** Warning: GetSigmaPiGaia must have at least 6 input arguments\n");
		fprintf(stderr, " Zero value will be returned\n");
		return Py_BuildValue("i",0);
	}

}; // End of GetSigmaPiGaia
//--------------------------------------------------------------------------------------------------------------------------------------//



//------------------------------------------------------ ConversionSigmas -----------------------------------------------------------------// 
static PyObject *CAsgaiaTools_ConversionSigmas(PyObject *self,PyObject *args){

	Py_Initialize();//needed command
	import_array();// needed command

	float *cv_lon, *cv_lat, *cv_sigmapi;
	int n_lon, n_lat, n_sigmapi; //Check if the dimensions are correct
	int flag=-1; // 1 or -1 (default value)	
	int pass;

	//Output issue (their dimensions must be n_sigmapi)
	float *cv_alphastar;
	float *cv_delta; 
	float *cv_muas; 
	float *cv_mudelta;

	void *cv_void_alphastar;
	void *cv_void_delta;
	void *cv_void_muas;
	void *cv_void_mudelta;

	npy_intp dims_out[1];

	PyArrayObject *pv_alphastar, *pv_delta, *pv_muas, *pv_mudelta;
	PyObject *pv_lon,*pv_lat, *pv_sigmapi; //Prefix pv means Python Vector
	Py_ssize_t Nargs = PyTuple_Size(args); //Number of the input arguments received (used here for first time)

	if(Nargs==3 || Nargs==4){
		//We have receive three or four arguments: lon, lat, sigma_pi and flag
		PyArg_ParseTuple(args,"OOO|i", &pv_lon, &pv_lat, &pv_sigmapi, &flag); //Python input arguments

		//Get number of particles:
		n_lon = (int) ((PyArrayObject*)pv_lon)->dimensions[0];

		// Check the dimensions:
		n_lat = ((PyArrayObject*)pv_lat)->dimensions[0];
		n_sigmapi = ((PyArrayObject*)pv_sigmapi)->dimensions[0];
		pass = (n_lon==n_lat)*(n_lat==n_sigmapi);
	
		if(!pass){
			fprintf(stderr, " *** Warning: dimensions in ConversionSigmas mismatch\n");
			fprintf(stderr, "\tZero value will be returned\n");
			return Py_BuildValue("i",0);
		};

		//Create output python vector using nrows
		dims_out[0] = n_lon;
		pv_alphastar  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); //Python vector (of floats)
		pv_delta  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); //Python vector (of floats)
		pv_muas  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); //Python vector (of floats)
		pv_mudelta  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); //Python vector (of floats)
	

		//Let's link two types of pointers
		cv_lon = ARRAYF(pv_lon);
		cv_lat= ARRAYF(pv_lat);
		cv_sigmapi= ARRAYF(pv_sigmapi);

		cv_void_alphastar = ARRAYV(pv_alphastar);
		cv_alphastar = (float*)cv_void_alphastar;

		cv_void_delta = ARRAYV(pv_delta);
		cv_delta = (float*)cv_void_delta;

		cv_void_muas = ARRAYV(pv_muas);
		cv_muas = (float*)cv_void_muas;

		cv_void_mudelta = ARRAYV(pv_mudelta);
		cv_mudelta = (float*)cv_void_mudelta;

		// Call to ConversionSigmas
		ConversionSigmas( cv_lon, cv_lat, cv_sigmapi, n_sigmapi, flag, cv_alphastar, cv_delta, cv_muas, cv_mudelta);

		//Copy it to a new direction
		memcpy(cv_void_alphastar, cv_alphastar, PyArray_ITEMSIZE((PyArrayObject*) pv_alphastar)*n_sigmapi);
		memcpy(cv_void_delta, cv_delta, PyArray_ITEMSIZE((PyArrayObject*) pv_delta)*n_sigmapi);
		memcpy(cv_void_muas, cv_muas, PyArray_ITEMSIZE((PyArrayObject*) pv_muas)*n_sigmapi);
		memcpy(cv_void_mudelta, cv_mudelta, PyArray_ITEMSIZE((PyArrayObject*) pv_mudelta)*n_sigmapi);


		return Py_BuildValue("OOOO", pv_alphastar, pv_delta, pv_muas, pv_mudelta);
	}else if(Nargs==1){
		// Assume flag=-1, lon and lat are not needed

		//We have receive three or four arguments: lon, lat, sigma_pi and flag
		PyArg_ParseTuple(args,"O", &pv_sigmapi); //Python input arguments
		// Get the array length:
		n_sigmapi = ((PyArrayObject*)pv_sigmapi)->dimensions[0];

		//Create output python vector using nrows
		dims_out[0] = n_sigmapi;
		pv_alphastar  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); //Python vector (of floats)
		pv_delta  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); //Python vector (of floats)
		pv_muas  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); //Python vector (of floats)
		pv_mudelta  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); //Python vector (of floats)

		//Let's link two types of pointers
		cv_lon= ARRAYF(pv_sigmapi);// they are initialized to avoid a compiler warning
		cv_lat= ARRAYF(pv_sigmapi);// they are initialized to avoid a compiler warning
		cv_sigmapi= ARRAYF(pv_sigmapi);

		cv_void_alphastar = ARRAYV(pv_alphastar);
		cv_alphastar = (float*)cv_void_alphastar;

		cv_void_delta = ARRAYV(pv_delta);
		cv_delta = (float*)cv_void_delta;

		cv_void_muas = ARRAYV(pv_muas);
		cv_muas = (float*)cv_void_muas;

		cv_void_mudelta = ARRAYV(pv_mudelta);
		cv_mudelta = (float*)cv_void_mudelta;

		// Call to ConversionSigmas
		ConversionSigmas( cv_lon, cv_lat, cv_sigmapi, n_sigmapi, 1, cv_alphastar, cv_delta, cv_muas, cv_mudelta);

		//Copy it to a new direction
		memcpy(cv_void_alphastar, cv_alphastar, PyArray_ITEMSIZE((PyArrayObject*) pv_alphastar)*n_sigmapi);
		memcpy(cv_void_delta, cv_delta, PyArray_ITEMSIZE((PyArrayObject*) pv_delta)*n_sigmapi);
		memcpy(cv_void_muas, cv_muas, PyArray_ITEMSIZE((PyArrayObject*) pv_muas)*n_sigmapi);
		memcpy(cv_void_mudelta, cv_mudelta, PyArray_ITEMSIZE((PyArrayObject*) pv_mudelta)*n_sigmapi);


		return Py_BuildValue("OOOO", pv_alphastar, pv_delta, pv_muas, pv_mudelta);

	}else{
		fprintf(stderr, " *** Warning: GeometricCorrection must have at least 1 input argument\n");
		fprintf(stderr, " Zero value will be returned\n");
		return Py_BuildValue("i",0);
	}


}; // End of ConversionSigmas
//--------------------------------------------------------------------------------------------------------------------------------------//




//----------------------------- PhotometryGaia -----------------------------------------------------------// 
static PyObject *CAsgaiaTools_PhotometryGaia(PyObject *self,PyObject *args){

	Py_Initialize();//needed command
	import_array();// needed command

	float *cv_metZ, *cv_logAge, *cv_mass;
	float *cv_dist, *cv_ex31;
	int n_metZ, n_logAge, n_mass, n_dist, n_ex31, pass; //Check if the dimensions are correct

	// Range of the G mag.
	float G_limits_min = 3.;
	float G_limits_max = 20.;
	float G_limits[2]; 

	// FlagsPhotometryGaia
	int interpolation = 0;
	char IMF_type='s'; //S,s mean Salpeter IMF. K, k mean Kroupa IMF. B,b mean Bimodal IMF

	// Other optional inputs:
	float IMF_slope = 2.35;

	//Output issue
	float *cv_errorG, *cv_errorBP, *cv_errorRP, *cv_meanG, *cv_meanBP, *cv_meanRP,*cv_nobstars; //Its dimensions must be nrows
	void *cv_void_meanG, *cv_void_meanBP, *cv_void_meanRP, *cv_void_nobstars;
	void *cv_void_errorG, *cv_void_errorBP, *cv_void_errorRP;
	npy_intp dims_out[1];

	PyArrayObject *pv_meanG, *pv_meanBP, *pv_meanRP, *pv_errorG, *pv_errorBP, *pv_errorRP, *pv_nobstars;
	PyObject *pv_metZ,*pv_logAge, *pv_mass; //Prefix pv means Python Vector
	PyObject *pv_dist,*pv_ex31; //Prefix pv means Python Vector
	Py_ssize_t Nargs = PyTuple_Size(args); //Number of the input arguments received (used here for first time)


	if(Nargs==5 || Nargs==10){
		//We have received five or ten arguments: metZ, logAge, mass, dist(kpc), ex31=A_V, *IMF_type, *interpolation, *G_limits_min, *G_limits_max, *IMF_slope
		PyArg_ParseTuple(args,"OOOOO|cifff", &pv_metZ, &pv_logAge, &pv_mass, &pv_dist, &pv_ex31, &IMF_type, &interpolation, &G_limits_min, &G_limits_max, &IMF_slope); //Python input arguments

		// G limits:
		G_limits[0] = G_limits_min;
		G_limits[1] = G_limits_max;

		//Analyse the IMF_type:
		IMF_type = IMF_type*(IMF_type>=65 && IMF_type<=122);
		IMF_type = IMF_type + 32*(IMF_type<97) + 65*(IMF_type==0);
		if(IMF_type==115){
			fprintf(stderr,"   INFO: Salpeter IMF will be used as IMF\n");
		}else if(IMF_type==107){
			fprintf(stderr,"   INFO: Kroupa IMF will be used as IMF\n");
		}else if(IMF_type==98){
			fprintf(stderr,"   INFO: Bimodal IMF will be used as IMF\n");
		}else{
			fprintf(stderr,"***WARNING: unknown IMF. Salpeter will be used by IMF\n");
			IMF_type = 115;
		};

		//Get number of particles:
		n_metZ = (int) ((PyArrayObject*)pv_metZ)->dimensions[0];

		// Check the dimensions:
		n_logAge = ((PyArrayObject*)pv_logAge)->dimensions[0];
		n_mass = ((PyArrayObject*)pv_mass)->dimensions[0];
		n_dist = ((PyArrayObject*)pv_dist)->dimensions[0];
		n_ex31 = ((PyArrayObject*)pv_ex31)->dimensions[0];

		pass = (n_metZ==n_logAge)*(n_logAge==n_mass)*(n_mass==n_dist)*(n_dist==n_ex31);
	
		if(!pass){
			fprintf(stderr, " *** Warning: dimensions in PhotometryGaia mismatch\n");
			fprintf(stderr, "\tZero value will be returned\n");
			return Py_BuildValue("i",0);
		};

		//Create output python vector using nrows
		dims_out[0] = n_metZ;
		pv_meanG  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); //Python vector (of floats)
		pv_meanBP  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); //Python vector (of floats)
		pv_meanRP  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); //Python vector (of floats)
		pv_errorG  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); //Python vector (of floats)
		pv_errorBP  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); //Python vector (of floats)
		pv_errorRP  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); //Python vector (of floats)

		pv_nobstars  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); //Python vector (of floats)
	

		//Let's link two types of pointers
		cv_metZ = ARRAYF(pv_metZ);
		cv_logAge= ARRAYF(pv_logAge);
		cv_mass= ARRAYF(pv_mass);
		cv_dist = ARRAYF(pv_dist);
		cv_ex31= ARRAYF(pv_ex31);

		cv_void_meanG = ARRAYV(pv_meanG);
		cv_meanG = (float*)cv_void_meanG;

		cv_void_meanBP = ARRAYV(pv_meanBP);
		cv_meanBP = (float*)cv_void_meanBP;

		cv_void_meanRP = ARRAYV(pv_meanRP);
		cv_meanRP = (float*)cv_void_meanRP;

		cv_void_errorG = ARRAYV(pv_errorG);
		cv_errorG = (float*)cv_void_errorG;

		cv_void_errorBP = ARRAYV(pv_errorBP);
		cv_errorBP = (float*)cv_void_errorBP;

		cv_void_errorRP = ARRAYV(pv_errorRP);
		cv_errorRP = (float*)cv_void_errorRP;

		cv_void_nobstars = ARRAYV(pv_nobstars);
		cv_nobstars = (float*)cv_void_nobstars;

		//Now calculate the Gaia errors in pi:
		pass = 10*(IMF_type==107)+20*(IMF_type==98)+1*(interpolation==1); //pass is a bitmask now

		switch(pass){
			case 21:
				fprintf(stderr, "\t Calling to PhotometryGaiaBimodal_4Points (%i)...",pass);
				PhotometryGaiaBimodal_4Points( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_ex31, cv_meanG, cv_meanBP, cv_meanRP, cv_errorG, cv_errorBP, cv_errorRP, cv_nobstars, n_metZ, G_limits, IMF_slope);
				break;
			case 20:
				fprintf(stderr, "\t Calling to PhotometryGaia_Bimodal (%i)...",pass);
				PhotometryGaia_Bimodal( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_ex31, cv_meanG, cv_meanBP, cv_meanRP, cv_errorG, cv_errorBP, cv_errorRP, cv_nobstars, n_metZ, G_limits, IMF_slope);
				break;
			case 11:
				fprintf(stderr, "\t Calling to PhotometryGaiaKroupa_4Points (%i)...",pass);
				PhotometryGaiaKroupa_4Points( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_ex31, cv_meanG, cv_meanBP, cv_meanRP, cv_errorG, cv_errorBP, cv_errorRP, cv_nobstars, n_metZ, G_limits);
				break;
			case 10:
				fprintf(stderr, "\t Calling to PhotometryGaia_Kroupa (%i)...",pass);
				PhotometryGaia_Kroupa( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_ex31, cv_meanG, cv_meanBP, cv_meanRP, cv_errorG, cv_errorBP, cv_errorRP, cv_nobstars, n_metZ, G_limits);
				break;
			case 1:
				fprintf(stderr, "\t Calling to PhotometryGaiaSalpeter_4Points (%i)...",pass);
				PhotometryGaiaSalpeter_4Points( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_ex31, cv_meanG, cv_meanBP, cv_meanRP, cv_errorG, cv_errorBP, cv_errorRP, cv_nobstars, n_metZ, G_limits);
				break;
			case 0:
				fprintf(stderr, "\t Calling to PhotometryGaia_Salpeter (%i)...",pass);
				PhotometryGaia_Salpeter( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_ex31, cv_meanG, cv_meanBP, cv_meanRP, cv_errorG, cv_errorBP, cv_errorRP, cv_nobstars, n_metZ, G_limits);
				break;
			default:
				fprintf(stderr, "\tCalling to PhotometryGaia_Salpeter (%i)...",pass);
				PhotometryGaia_Salpeter( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_ex31, cv_meanG, cv_meanBP, cv_meanRP, cv_errorG, cv_errorBP, cv_errorRP, cv_nobstars, n_metZ, G_limits);
		};

		//Copy it to a new direction
		memcpy(cv_void_meanG, cv_meanG, PyArray_ITEMSIZE((PyArrayObject*) pv_meanG)*n_metZ);
		memcpy(cv_void_meanBP, cv_meanBP, PyArray_ITEMSIZE((PyArrayObject*) pv_meanBP)*n_metZ);
		memcpy(cv_void_meanRP, cv_meanRP, PyArray_ITEMSIZE((PyArrayObject*) pv_meanRP)*n_metZ);

		memcpy(cv_void_errorG, cv_errorG, PyArray_ITEMSIZE((PyArrayObject*) pv_errorG)*n_metZ);
		memcpy(cv_void_errorBP, cv_errorBP, PyArray_ITEMSIZE((PyArrayObject*) pv_errorBP)*n_metZ);
		memcpy(cv_void_errorRP, cv_errorRP, PyArray_ITEMSIZE((PyArrayObject*) pv_errorRP)*n_metZ);

		memcpy(cv_void_nobstars, cv_nobstars, PyArray_ITEMSIZE((PyArrayObject*) pv_nobstars)*n_metZ);


		return Py_BuildValue("OOOOOOO", pv_meanG, pv_meanBP, pv_meanRP, pv_errorG, pv_errorBP, pv_errorRP, pv_nobstars);
	}else{
		fprintf(stderr, " *** Warning: PhotometryGaia must have either 5 or 10 input arguments\n");
		fprintf(stderr, " Zero value will be returned\n");
		return Py_BuildValue("i",0);
	}


}; // End of PhotometryGaia
//--------------------------------------------------------------------------------------------------------------------------------------//




//----------------------------- GetPhotometryGaia -----------------------------------------------------------------------------------------// 
static PyObject *CAsgaiaTools_GetPhotometryGaia(PyObject *self,PyObject *args){

	Py_Initialize();//needed command
	import_array();// needed command
	int k; // Counter

	//Inputs
	float *cv_lon, *cv_lat, *cv_dist;
	float *cv_metZ, *cv_logAge, *cv_mass;
	int interpolation = 0;
	char IMF_type='s'; //S,s mean Salpeter IMF. K, k mean Kroupa IMF. B, b mean Bimodal
	float IMF_slope = 2.35; // Only important when Bimodal IMF is selected

	// Range of the G mag.
	float G_limits_min = 3.;
	float G_limits_max = 20.;
	float G_limits[2]; 
	
	// Intermediate quantities:
	float *cv_aux;

	//Intermediate
	PyArrayObject *pv_aux;


	//Output issue
	float *cv_meanG, *cv_meanBP, *cv_meanRP, *cv_errorG, *cv_errorBP, *cv_errorRP, *cv_nobstars, *cv_ex31; //Its dimensions must be nrows
	void *cv_void_meanG, *cv_void_meanBP, *cv_void_meanRP;
	void *cv_void_errorG, *cv_void_errorBP, *cv_void_errorRP, *cv_void_nobstars, *cv_void_ex31;
	npy_intp dims_out[1];

	// Checks
	int n_lon, n_lat, n_mass, n_dist, n_metZ, n_logAge, pass; //Check if the dimensions are correct

	// Python structures
	PyArrayObject *pv_ex31, *pv_nobstars; // Outputs
	PyArrayObject *pv_meanG, *pv_meanBP, *pv_meanRP, *pv_errorG, *pv_errorBP, *pv_errorRP;
	PyObject *pv_lon, *pv_lat, *pv_dist; // Inputs
	PyObject *pv_metZ,*pv_logAge, *pv_mass; // Inputs


	Py_ssize_t Nargs = PyTuple_Size(args); // Number of the input arguments received (used here for first time)

	if(Nargs==6 || Nargs==11){
		//We can receive either six or eleven arguments: lon, lat, dist(kpc), metZ, logAge, *IMF_type, *interpolation, *G_limits_min, *G_limits_max, *IMF_slope
		PyArg_ParseTuple(args,"OOOOOO|cifff", &pv_lon, &pv_lat, &pv_dist, &pv_metZ, &pv_logAge, &pv_mass, &IMF_type, &interpolation, &G_limits_min, &G_limits_max, &IMF_slope); //Python input arguments

		// G limits:
		G_limits[0] = G_limits_min;
		G_limits[1] = G_limits_max;

		//Analyse the IMF_type:
		IMF_type = IMF_type*(IMF_type>=65 && IMF_type<=122);
		IMF_type = IMF_type + 32*(IMF_type<97) + 65*(IMF_type==0);
		if(IMF_type==115){
			fprintf(stderr,"   INFO: Salpeter IMF will be used as IMF\n");
		}else if(IMF_type==107){
			fprintf(stderr,"   INFO: Kroupa IMF will be used as IMF\n");
		}else if(IMF_type==98){
			fprintf(stderr,"   INFO: Bimodal IMF will be used as IMF\n");
		}else{
			fprintf(stderr,"***WARNING: unknown IMF. Salpeter will be used by IMF\n");
			IMF_type = 115;
		};

		//Get number of particles:
		n_metZ = (int) ((PyArrayObject*)pv_metZ)->dimensions[0];

		// Check the dimensions:
		n_logAge = ((PyArrayObject*)pv_logAge)->dimensions[0];
		n_mass = ((PyArrayObject*)pv_mass)->dimensions[0];
		n_lon = ((PyArrayObject*)pv_lon)->dimensions[0];
		n_lat = ((PyArrayObject*)pv_lat)->dimensions[0];
		n_dist = ((PyArrayObject*)pv_dist)->dimensions[0];

		// Condition to continue:
		pass = (n_metZ==n_logAge)*(n_logAge==n_mass)*(n_mass==n_dist)*(n_dist==n_lon)*(n_lon==n_lat);
	
		if(!pass){
			fprintf(stderr, " *** Warning: dimensions in GetPhotometryGaia mismatch\n");
			fprintf(stderr, "\tZero value will be returned\n");
			return Py_BuildValue("i",0);
		};

		//Create output python vector using nrows
		dims_out[0] = n_metZ;
		pv_meanG  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); //Python vector (of floats)
		pv_meanBP  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); //Python vector (of floats)
		pv_meanRP  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); //Python vector (of floats)
		pv_errorG  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); //Python vector (of floats)
		pv_errorBP  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); //Python vector (of floats)
		pv_errorRP  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); //Python vector (of floats)
		pv_nobstars  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); //Python vector (of floats)
		pv_ex31  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); //Python vector (of floats)

		//Create the intermediate python vector
		pv_aux  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); //Python vector (of floats)


		//Let's link two types of pointers
		cv_lon = ARRAYF(pv_lon);
		cv_lat = ARRAYF(pv_lat);
		cv_dist = ARRAYF(pv_dist);
		cv_metZ = ARRAYF(pv_metZ);
		cv_logAge = ARRAYF(pv_logAge);
		cv_mass = ARRAYF(pv_mass);

		cv_void_meanG = ARRAYV(pv_meanG);
		cv_meanG = (float*)cv_void_meanG;

		cv_void_meanBP = ARRAYV(pv_meanBP);
		cv_meanBP = (float*)cv_void_meanBP;

		cv_void_meanRP = ARRAYV(pv_meanRP);
		cv_meanRP = (float*)cv_void_meanRP;

		cv_void_errorG = ARRAYV(pv_errorG);
		cv_errorG = (float*)cv_void_errorG;

		cv_void_errorBP = ARRAYV(pv_errorBP);
		cv_errorBP = (float*)cv_void_errorBP;

		cv_void_errorRP = ARRAYV(pv_errorRP);
		cv_errorRP = (float*)cv_void_errorRP;

		cv_void_nobstars = ARRAYV(pv_nobstars);
		cv_nobstars = (float*)cv_void_nobstars;

		cv_void_ex31 = ARRAYV(pv_ex31);
		cv_ex31 = (float*)cv_void_ex31;

		cv_aux = ARRAYF(pv_aux);

		//First calculate the Schlegel extinction
		pass = ColourExcessMap(cv_lon, cv_lat, n_lon, cv_ex31); //cv_ex31: only the Schlegel values

		if(!pass){
			fprintf(stderr, " *** Warning: problems with the calling to ColourExcessMap with GetPhotometryGaia\n");
			fprintf(stderr, "\tZero value will be returned\n");
			return Py_BuildValue("i",0);
		};
		
		// Then, the 3D correction
		pass = ColourExcess3DMap(cv_lon, cv_lat, cv_dist, n_lon, cv_aux);// cv_aux: the 3d correction
		if(!pass){
			fprintf(stderr, " *** Warning: problems with the calling to ColourExcess3DMap with GetPhotometryGaia\n");
			fprintf(stderr, "\tZero value will be returned\n");
			return Py_BuildValue("i",0);
		};
		
		// Now apply the SharmaCorrection and multiply by 3.24 to get the extinction in the V-band
		for(k=0;k<n_lon;k++){
			cv_ex31[k] = cv_aux[k]*cv_ex31[k];
			cv_ex31[k] = 3.24*cv_ex31[k]*(0.8-0.2*tanh(10.*cv_ex31[k]-1.5));

		}; // End of the Sharma Correction
		// The A_V extinction is calculated!


		//Now calculate the Gaia errors in pi:
		pass = 10*(IMF_type==107)+20*(IMF_type==98)+1*(interpolation==1); //pass is a bitmask now

		switch(pass){
			case 21:
				fprintf(stderr, "\t Calling to PhotometryGaiaBimodal_4Points (%i)...",pass);
				PhotometryGaiaBimodal_4Points( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_ex31, cv_meanG, cv_meanBP, cv_meanRP, cv_errorG, cv_errorBP, cv_errorRP, cv_nobstars, n_metZ, G_limits, IMF_slope);
				break;
			case 20:
				fprintf(stderr, "\t Calling to PhotometryGaia_Bimodal (%i)...",pass);
				PhotometryGaia_Bimodal( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_ex31, cv_meanG, cv_meanBP, cv_meanRP, cv_errorG, cv_errorBP, cv_errorRP, cv_nobstars, n_metZ, G_limits, IMF_slope);
				break;
			case 11:
				fprintf(stderr, "\t Calling to PhotometryGaiaKroupa_4Points (%i)...",pass);
				PhotometryGaiaKroupa_4Points( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_ex31, cv_meanG, cv_meanBP, cv_meanRP, cv_errorG, cv_errorBP, cv_errorRP, cv_nobstars, n_metZ, G_limits);
				break;
			case 10:
				fprintf(stderr, "\t Calling to PhotometryGaia_Kroupa (%i)...",pass);
				PhotometryGaia_Kroupa( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_ex31, cv_meanG, cv_meanBP, cv_meanRP, cv_errorG, cv_errorBP, cv_errorRP, cv_nobstars, n_metZ, G_limits);
				break;
			case 1:
				fprintf(stderr, "\t Calling to PhotometryGaiaSalpeter_4Points (%i)...",pass);
				PhotometryGaiaSalpeter_4Points( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_ex31, cv_meanG, cv_meanBP, cv_meanRP, cv_errorG, cv_errorBP, cv_errorRP, cv_nobstars, n_metZ, G_limits);
				break;
			case 0:
				fprintf(stderr, "\t Calling to PhotometryGaia_Salpeter (%i)...",pass);
				PhotometryGaia_Salpeter( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_ex31, cv_meanG, cv_meanBP, cv_meanRP, cv_errorG, cv_errorBP, cv_errorRP, cv_nobstars, n_metZ, G_limits);
				break;
			default:
				fprintf(stderr, "\tCalling to PhotometryGaia_Salpeter (%i)...",pass);
				PhotometryGaia_Salpeter( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_ex31, cv_meanG, cv_meanBP, cv_meanRP, cv_errorG, cv_errorBP, cv_errorRP, cv_nobstars, n_metZ, G_limits);
		};

		//Copy the output to a new direction
		memcpy(cv_void_meanG, cv_meanG, PyArray_ITEMSIZE((PyArrayObject*) pv_meanG)*n_metZ);
		memcpy(cv_void_meanBP, cv_meanBP, PyArray_ITEMSIZE((PyArrayObject*) pv_meanBP)*n_metZ);
		memcpy(cv_void_meanRP, cv_meanRP, PyArray_ITEMSIZE((PyArrayObject*) pv_meanRP)*n_metZ);

		memcpy(cv_void_errorG, cv_errorG, PyArray_ITEMSIZE((PyArrayObject*) pv_errorG)*n_metZ);
		memcpy(cv_void_errorBP, cv_errorBP, PyArray_ITEMSIZE((PyArrayObject*) pv_errorBP)*n_metZ);
		memcpy(cv_void_errorRP, cv_errorRP, PyArray_ITEMSIZE((PyArrayObject*) pv_errorRP)*n_metZ);

		memcpy(cv_void_nobstars, cv_nobstars, PyArray_ITEMSIZE((PyArrayObject*) pv_nobstars)*n_metZ);
		memcpy(cv_void_ex31, cv_ex31, PyArray_ITEMSIZE((PyArrayObject*) pv_ex31)*n_metZ);

		return Py_BuildValue("OOOOOOOO", pv_meanG, pv_meanBP, pv_meanRP, pv_errorG, pv_errorBP, pv_errorRP, pv_nobstars, pv_ex31);
	}else{
		fprintf(stderr, " *** Warning: GetPhotometryGaia must have at least 5 input arguments \n");
		fprintf(stderr, " Zero value will be returned instead\n");
		return Py_BuildValue("i",0);
	}

}; // End of GetPhotometryGaia
//--------------------------------------------------------------------------------------------------------------------------------------//



//----------------------------- SigmaVrGaia -----------------------------------------------------------// 
static PyObject *CAsgaiaTools_SigmaVrGaia(PyObject *self,PyObject *args){

	Py_Initialize();//needed command
	import_array();// needed command

	float *cv_metZ, *cv_logAge, *cv_mass;
	float *cv_dist, *cv_ex31; // Distance and extinction
	int n_metZ, n_logAge, n_mass, n_dist, n_ex31, pass; //Check if the dimensions are correct

	// Range of the G mag.
	float G_limits_min = 3.;
	float G_limits_max = 20.;
	float G_RVS_limits_min = -INFINITY;
	float G_RVS_limits_max = 16.;
	float G_limits[4]; 

	// Flags
	int interpolation = 0;
	char IMF_type='s'; //S,s mean Salpeter IMF. K, k mean Kroupa IMF. B,b mean Bimodal IMF

	// Other optional inputs:
	float IMF_slope = 2.35;

	//Output issue
	float *cv_errorVr, *cv_nobstars; //Its dimensions must be nrows
	void *cv_void_errorVr, *cv_void_nobstars;
	npy_intp dims_out[1];

	PyArrayObject *pv_errorVr, *pv_nobstars;
	PyObject *pv_metZ,*pv_logAge, *pv_mass; //Prefix pv means Python Vector
	PyObject *pv_dist,*pv_ex31; //Prefix pv means Python Vector
	Py_ssize_t Nargs = PyTuple_Size(args); //Number of the input arguments received (used here for first time)


	if(Nargs==5 || Nargs==12){
		//We have received five or twelve arguments: metZ, logAge, mass, dist(kpc), ex31=A_V, *IMF_type, *interpolation, *G_limits_min, *G_limits_max, *G_RVS_limits_min, *G_RVS_limits_max, *IMF_slope
		PyArg_ParseTuple(args,"OOOOO|cifffff", &pv_metZ, &pv_logAge, &pv_mass, &pv_dist, &pv_ex31, &IMF_type, &interpolation, &G_limits_min, &G_limits_max, &G_RVS_limits_min, &G_RVS_limits_max, &IMF_slope); //Python input arguments

		// G limits:
		G_limits[0] = G_limits_min;
		G_limits[1] = G_limits_max;
		G_limits[2] = G_RVS_limits_min;
		G_limits[3] = G_RVS_limits_max;

		//Analyse the IMF_type:
		IMF_type = IMF_type*(IMF_type>=65 && IMF_type<=122);
		IMF_type = IMF_type + 32*(IMF_type<97) + 65*(IMF_type==0);
		if(IMF_type==115){
			fprintf(stderr,"   INFO: Salpeter IMF will be used as IMF\n");
		}else if(IMF_type==107){
			fprintf(stderr,"   INFO: Kroupa IMF will be used as IMF\n");
		}else if(IMF_type==98){
			fprintf(stderr,"   INFO: Bimodal IMF will be used as IMF\n");
		}else{
			fprintf(stderr,"***WARNING: unknown IMF. Salpeter will be used by IMF\n");
			IMF_type = 115;
		};

		//Get number of particles:
		n_metZ = (int) ((PyArrayObject*)pv_metZ)->dimensions[0];

		// Check the dimensions:
		n_logAge = ((PyArrayObject*)pv_logAge)->dimensions[0];
		n_mass = ((PyArrayObject*)pv_mass)->dimensions[0];
		n_dist = ((PyArrayObject*)pv_dist)->dimensions[0];
		n_ex31 = ((PyArrayObject*)pv_ex31)->dimensions[0];

		pass = (n_metZ==n_logAge)*(n_logAge==n_mass)*(n_mass==n_dist)*(n_dist==n_ex31);
	
		if(!pass){
			fprintf(stderr, " *** Warning: dimensions in SigmaVrGaia mismatch\n");
			fprintf(stderr, "\tZero value will be returned\n");
			return Py_BuildValue("i",0);
		};

		//Create output python vector using nrows
		dims_out[0] = n_metZ;
		pv_errorVr  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); //Python vector (of floats)
		pv_nobstars  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); //Python vector (of floats)
	

		//Let's link two types of pointers
		cv_metZ = ARRAYF(pv_metZ);
		cv_logAge= ARRAYF(pv_logAge);
		cv_mass= ARRAYF(pv_mass);
		cv_dist = ARRAYF(pv_dist);
		cv_ex31= ARRAYF(pv_ex31);

		cv_void_errorVr = ARRAYV(pv_errorVr);
		cv_errorVr = (float*)cv_void_errorVr;

		cv_void_nobstars = ARRAYV(pv_nobstars);
		cv_nobstars = (float*)cv_void_nobstars;

		//Now calculate the Gaia errors in pi:
		pass = 10*(IMF_type==107)+20*(IMF_type==98)+1*(interpolation==1); //pass is a bitmask now

		switch(pass){
			case 21:
				fprintf(stderr, "\t Calling to SigmaVrGaiaBimodal_4Points (%i)...",pass);
				SigmaVrGaiaBimodal_4Points( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_ex31, cv_errorVr, cv_nobstars, n_metZ, G_limits, IMF_slope);
				break;
			case 20:
				fprintf(stderr, "\t Calling to SigmaVrGaia_Bimodal (%i)...",pass);
				SigmaVrGaia_Bimodal( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_ex31, cv_errorVr, cv_nobstars, n_metZ, G_limits, IMF_slope);
				break;
			case 11:
				fprintf(stderr, "\t Calling to SigmaVrGaiaKroupa_4Points (%i)...",pass);
				SigmaVrGaiaKroupa_4Points( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_ex31, cv_errorVr, cv_nobstars, n_metZ, G_limits);
				break;
			case 10:
				fprintf(stderr, "\t Calling to SigmaVrGaia_Kroupa (%i)...",pass);
				SigmaVrGaia_Kroupa( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_ex31, cv_errorVr, cv_nobstars, n_metZ, G_limits);
				break;
			case 1:
				fprintf(stderr, "\t Calling to SigmaVrGaiaSalpeter_4Points (%i)...",pass);
				SigmaVrGaia_Salpeter( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_ex31, cv_errorVr, cv_nobstars, n_metZ, G_limits);
				break;
			case 0:
				fprintf(stderr, "\t Calling to SigmaVrGaia_Salpeter (%i)...",pass);
				SigmaVrGaia_Salpeter( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_ex31, cv_errorVr, cv_nobstars, n_metZ, G_limits);
				break;
			default:
				fprintf(stderr, "\tCalling to SigmaVrGaia_Salpeter (%i)...",pass);
				SigmaVrGaia_Salpeter( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_ex31, cv_errorVr, cv_nobstars, n_metZ, G_limits);
		};


		fprintf(stderr, "finished.\n");	
		//Copy it to a new direction
		memcpy(cv_void_errorVr, cv_errorVr, PyArray_ITEMSIZE((PyArrayObject*) pv_errorVr)*n_metZ);
		memcpy(cv_void_nobstars, cv_nobstars, PyArray_ITEMSIZE((PyArrayObject*) pv_nobstars)*n_metZ);

		return Py_BuildValue("OO", pv_errorVr, pv_nobstars);
	}else{
		fprintf(stderr, " *** Warning: SigmaVrGaia must have either 5 or 12 input arguments\n");
		fprintf(stderr, " Zero value will be returned\n");
		return Py_BuildValue("i",0);
	}


}; // End of SigmaVrGaia
//--------------------------------------------------------------------------------------------------------------------------------------//





//----------------------------- GetSigmaVrGaia -----------------------------------------------------------------------------------------// 
static PyObject *CAsgaiaTools_GetSigmaVrGaia(PyObject *self,PyObject *args){

	Py_Initialize();//needed command
	import_array();// needed command
	int k; // Counter

	//Inputs
	float *cv_lon, *cv_lat, *cv_dist;
	float *cv_metZ, *cv_logAge, *cv_mass;
	int interpolation = 0;
	char IMF_type='s'; //S,s mean Salpeter IMF. K, k mean Kroupa IMF. B, b mean Bimodal
	float IMF_slope = 2.35; // Only important when Bimodal IMF is selected

	// Range of the G mag.
	float G_limits_min = 3.;
	float G_limits_max = 20.;
	float G_RVS_limits_min = -INFINITY;
	float G_RVS_limits_max = 16.;
	float G_limits[4]; 
	
	// Intermediate quantities:
	float *cv_aux; // It will be the correction due to the Gaia' scanning law

	//Output issue
	float *cv_errorVr, *cv_ex31, *cv_nobstars;
	void *cv_void_errorVr, *cv_void_ex31, *cv_void_nobstars;

	// Checks
	int n_lon, n_lat, n_mass, n_dist, n_metZ, n_logAge, pass; //Check if the dimensions are correct
	npy_intp dims_out[1];

	// Python structures
	PyArrayObject *pv_errorVr, *pv_ex31, *pv_nobstars; // Outputs
	PyArrayObject *pv_aux; //Intermediate
	PyObject *pv_lon, *pv_lat, *pv_dist; // Inputs
	PyObject *pv_metZ,*pv_logAge, *pv_mass; // Inputs


	Py_ssize_t Nargs = PyTuple_Size(args); // Number of the input arguments received (used here for first time)



	if(Nargs==6 || Nargs==13){
		//We can receive either six or thirteen arguments: lon, lat, dist(kpc), metZ, logAge, *IMF_type, *interpolation, *G_limits_min, *G_limits_max, &G_RVS_limits_min, &G_RVS_limits_max, *IMF_slope
		PyArg_ParseTuple(args,"OOOOOO|cifffff", &pv_lon, &pv_lat, &pv_dist, &pv_metZ, &pv_logAge, &pv_mass, &IMF_type, &interpolation, &G_limits_min, &G_limits_max, &G_RVS_limits_min, &G_RVS_limits_max, &IMF_slope); //Python input arguments
		
		// G limits:
		G_limits[0] = G_limits_min;
		G_limits[1] = G_limits_max;
		G_limits[2] = G_RVS_limits_min;
		G_limits[3] = G_RVS_limits_max;

		//Analyse the IMF_type:
		IMF_type = IMF_type*(IMF_type>=65 && IMF_type<=122);
		IMF_type = IMF_type + 32*(IMF_type<97) + 65*(IMF_type==0);
		if(IMF_type==115){
			fprintf(stderr,"   INFO: Salpeter IMF will be used as IMF\n");
		}else if(IMF_type==107){
			fprintf(stderr,"   INFO: Kroupa IMF will be used as IMF\n");
		}else if(IMF_type==98){
			fprintf(stderr,"   INFO: Bimodal IMF will be used as IMF\n");
		}else{
			fprintf(stderr,"***WARNING: unknown IMF. Salpeter will be used by IMF\n");
			IMF_type = 115;
		};

		//Get number of particles:
		n_metZ = (int) ((PyArrayObject*)pv_metZ)->dimensions[0];

		// Check the dimensions:
		n_logAge = ((PyArrayObject*)pv_logAge)->dimensions[0];
		n_mass = ((PyArrayObject*)pv_mass)->dimensions[0];
		n_lon = ((PyArrayObject*)pv_lon)->dimensions[0];
		n_lat = ((PyArrayObject*)pv_lat)->dimensions[0];
		n_dist = ((PyArrayObject*)pv_dist)->dimensions[0];

		// Condition to continue:
		pass = (n_metZ==n_logAge)*(n_logAge==n_mass)*(n_mass==n_dist)*(n_dist==n_lon)*(n_lon==n_lat);
	
		if(!pass){
			fprintf(stderr, " *** Warning: dimensions in GetSigmaVrGaia mismatch\n");
			fprintf(stderr, "\tZero value will be returned\n");
			return Py_BuildValue("i",0);
		};

		//Create output python vector using nrows
		dims_out[0] = n_metZ;
		pv_errorVr  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); //Python vector (of floats)
		pv_nobstars  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); //Python vector (of floats)
		pv_ex31  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); //Python vector (of floats)

		//Create the intermediate python vector
		pv_aux  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); //Python vector (of floats)
	

		//Let's link two types of pointers
		cv_lon = ARRAYF(pv_lon);
		cv_lat = ARRAYF(pv_lat);
		cv_dist = ARRAYF(pv_dist);
		cv_metZ = ARRAYF(pv_metZ);
		cv_logAge = ARRAYF(pv_logAge);
		cv_mass = ARRAYF(pv_mass);

		cv_aux = ARRAYF(pv_aux);

		cv_void_errorVr = ARRAYV(pv_errorVr);
		cv_errorVr = (float*)cv_void_errorVr;

		cv_void_nobstars = ARRAYV(pv_nobstars);
		cv_nobstars = (float*)cv_void_nobstars;

		cv_void_ex31 = ARRAYV(pv_ex31);
		cv_ex31 = (float*)cv_void_ex31;

		//First calculate the Schlegel extinction
		pass = ColourExcessMap(cv_lon, cv_lat, n_lon, cv_ex31); //cv_ex31: only the Schlegel values

		if(!pass){
			fprintf(stderr, " *** Warning: problems with the calling to ColourExcessMap with GetSigmaVrGaia\n");
			fprintf(stderr, "\tZero value will be returned\n");
			return Py_BuildValue("i",0);
		};
		
		// Then, the 3D correction
		pass = ColourExcess3DMap(cv_lon, cv_lat, cv_dist, n_lon, cv_aux);// cv_aux: the 3d correction
		if(!pass){
			fprintf(stderr, " *** Warning: problems with the calling to ColourExcess3DMap with GetSigmaVrGaia\n");
			fprintf(stderr, "\tZero value will be returned\n");
			return Py_BuildValue("i",0);
		};
		
		// Now apply the SharmaCorrection and multiply by 3.24 to get the extinction in the V-band
		for(k=0;k<n_lon;k++){
			cv_ex31[k] = cv_aux[k]*cv_ex31[k];
			cv_ex31[k] = 3.24*cv_ex31[k]*(0.8-0.2*tanh(10.*cv_ex31[k]-1.5));

		}; // End of the Sharma Correction
		// The A_V extinction is calculated!


		//Now calculate the Gaia errors in pi:
		pass = 10*(IMF_type==107)+20*(IMF_type==98)+1*(interpolation==1); //pass is a bitmask now

		switch(pass){
			case 21:
				fprintf(stderr, "\t Calling to SigmaVrGaiaBimodal_4Points (%i)...",pass);
				SigmaVrGaiaBimodal_4Points( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_ex31, cv_errorVr, cv_nobstars, n_metZ, G_limits, IMF_slope);
				break;
			case 20:
				fprintf(stderr, "\t Calling to SigmaVrGaia_Bimodal (%i)...",pass);
				SigmaVrGaia_Bimodal( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_ex31, cv_errorVr, cv_nobstars, n_metZ, G_limits, IMF_slope);
				break;
			case 11:
				fprintf(stderr, "\t Calling to SigmaVrGaiaKroupa_4Points (%i)...",pass);
				SigmaVrGaiaKroupa_4Points( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_ex31, cv_errorVr, cv_nobstars, n_metZ, G_limits);
				break;
			case 10:
				fprintf(stderr, "\t Calling to SigmaVrGaia_Kroupa (%i)...",pass);
				SigmaVrGaia_Kroupa( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_ex31, cv_errorVr, cv_nobstars, n_metZ, G_limits);
				break;
			case 1:
				fprintf(stderr, "\t Calling to SigmaVrGaiaSalpeter_4Points (%i)...",pass);
				SigmaVrGaia_Salpeter( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_ex31, cv_errorVr, cv_nobstars, n_metZ, G_limits);
				break;
			case 0:
				fprintf(stderr, "\t Calling to SigmaVrGaia_Salpeter (%i)...",pass);
				SigmaVrGaia_Salpeter( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_ex31, cv_errorVr, cv_nobstars, n_metZ, G_limits);
				break;
			default:
				fprintf(stderr, "\tCalling to SigmaVrGaia_Salpeter (%i)...",pass);
				SigmaVrGaia_Salpeter( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_ex31, cv_errorVr, cv_nobstars, n_metZ, G_limits);
		};
		fprintf(stderr, "finished.\n");	

		//Copy the output to a new direction
		memcpy(cv_void_errorVr, cv_errorVr, PyArray_ITEMSIZE((PyArrayObject*) pv_errorVr)*n_metZ);
		memcpy(cv_void_nobstars, cv_nobstars, PyArray_ITEMSIZE((PyArrayObject*) pv_nobstars)*n_metZ);
		memcpy(cv_void_ex31, cv_ex31, PyArray_ITEMSIZE((PyArrayObject*) pv_ex31)*n_metZ);

		return Py_BuildValue("OOO", pv_errorVr, pv_nobstars, pv_ex31);
	}else{
		fprintf(stderr, " *** Warning: GetSigmaVrGaia must have at least 6 input arguments\n");
		fprintf(stderr, " Zero value will be returned\n");
		return Py_BuildValue("i",0);
	}

}; // End of GetSigmaVrGaia
//--------------------------------------------------------------------------------------------------------------------------------------//






//----------------------------- GetSigmaPiDistr -----------------------------------------------------------------------------------------// 
static PyObject *CAsgaiaTools_GetSigmaPiDistr(PyObject *self,PyObject *args){

	Py_Initialize();//needed command
	import_array();// needed command
	int k; // Counter

	//Inputs
	float *cv_lon, *cv_lat, *cv_dist;
	float *cv_metZ, *cv_logAge, *cv_mass;
	float Cfactor = 1.2; //Correction factor = CAfactor (calibration)*epoch correction (sqrt(60./month))
	int correction_flag = -1;
	char IMF_type='s'; //S,s mean Salpeter IMF. K, k mean Kroupa IMF. B, b mean Bimodal
	float IMF_slope = 2.35; //Only used when IMF_type = 'b'
	// Sigma pi histogram
	int Nbins = 136;
	float histo_min = 3.;
	float histo_max = 20.;
	float histo_axis[2];
	
	// Intermediate quantities:
	float *cv_aux; // It will be the correction due to the Gaia' scanning law

	//Output issue
	float *cv_errorpi, *cv_errorpi2, *cv_ex31;
	void *cv_void_errorpi, *cv_void_errorpi2, *cv_void_ex31;
	float *cv_sigmapi_histogram;
	void *cv_void_sigmapi_histogram;
	float *cv_nobstars;
	void *cv_void_nobstars;

	// Checks
	int n_lon, n_lat, n_dist, n_metZ, n_logAge, pass; //Check if the dimensions are correct
	npy_intp dims_out[1], dims_histogram[1];

	// Python structures
	PyArrayObject *pv_errorpi, *pv_errorpi2,*pv_ex31; // Outputs
	PyArrayObject *pv_sigmapi_histogram; // Histogram (output)
	PyArrayObject *pv_nobstars; // Number of stars (output)
	PyArrayObject *pv_aux; //Intermediate
	PyObject *pv_lon, *pv_lat, *pv_dist; // Inputs
	PyObject *pv_metZ,*pv_logAge, *pv_mass; // Inputs


	Py_ssize_t Nargs = PyTuple_Size(args); // Number of the input arguments received (used here for first time)



	if(Nargs==6 || Nargs==13){
		//We can receive either six or thirteen arguments: lon, lat, dist(kpc), metZ, logAge, *IMF_type, *Cfactor, *correction_flag, *Nbins, *histo_min, *histo_max, *IMF_slope
		PyArg_ParseTuple(args,"OOOOOO|cfiifff", &pv_lon, &pv_lat, &pv_dist, &pv_metZ, &pv_logAge, &pv_mass, &IMF_type, &Cfactor, &correction_flag, &Nbins, &histo_min, &histo_max, &IMF_slope); //Python input arguments

		if(histo_min<0) fprintf(stderr," NOTE: sigma_pi cannot be positive. You can increase the lower limit.  \n");
		histo_axis[0] = histo_min;
		histo_axis[1] = histo_max;

		//Analyse the IMF_type:
		IMF_type = IMF_type*(IMF_type>=65 && IMF_type<=122);
		IMF_type = IMF_type + 32*(IMF_type<97) + 65*(IMF_type==0);
		if(IMF_type==115){
			fprintf(stderr,"   INFO: Salpeter IMF will be used by GetSigmaPiDistr\n");
		}else if(IMF_type==107){
			fprintf(stderr,"   INFO: Kroupa IMF will be used by GetSigmaPiDistr\n");
		}else if(IMF_type==98){
			fprintf(stderr,"   INFO: Bimodal IMF will be used by GetSigmaPiDistr\n");
		}else{
			fprintf(stderr,"***WARNING: unknown IMF. Salpeter will be used by GetSigmaPiDistr\n");
			IMF_type = 115;
		};

		//Get number of particles:
		n_metZ = (int) ((PyArrayObject*)pv_metZ)->dimensions[0];

		// Check the dimensions:
		n_logAge = ((PyArrayObject*)pv_logAge)->dimensions[0];
		n_lon = ((PyArrayObject*)pv_lon)->dimensions[0];
		n_lat = ((PyArrayObject*)pv_lat)->dimensions[0];
		n_dist = ((PyArrayObject*)pv_dist)->dimensions[0];

		// Condition to continue:
		pass = (n_metZ==n_logAge)*(n_logAge==n_dist)*(n_dist==n_lon)*(n_lon==n_lat);
	
		if(!pass){
			fprintf(stderr, " *** Warning: dimensions in GetSigmaPiDistr mismatch\n");
			fprintf(stderr, "\tZero value will be returned\n");
			return Py_BuildValue("i",0);
		};

		//Create output python vector (particle's sigmapi)
		dims_out[0] = n_metZ;
		pv_errorpi  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); // Python vector (of floats)
		pv_errorpi2  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); // Python vector (of floats)
		pv_nobstars  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); // Python vector (of floats)
		pv_ex31  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); // Python vector (of floats)


		//Create output python histogram (for the photometry - G band)
		dims_histogram[0] = Nbins;// Now dims_histogram is an ancilliary variable, only used for type conversion
		pv_sigmapi_histogram = (PyArrayObject*)PyArray_SimpleNew(1, dims_histogram, PyArray_FLOAT32 ); // Python vector (of floats)


		//Create the intermediate python vector
		pv_aux  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); // Python vector (of floats)
	

		//Let's link two types of pointers
		cv_lon = ARRAYF(pv_lon);
		cv_lat = ARRAYF(pv_lat);
		cv_dist = ARRAYF(pv_dist);
		cv_metZ = ARRAYF(pv_metZ);
		cv_logAge = ARRAYF(pv_logAge);
		cv_mass = ARRAYF(pv_mass);

		cv_aux = ARRAYF(pv_aux);

		cv_void_ex31 = ARRAYV(pv_ex31);
		cv_ex31 = (float*)cv_void_ex31;

		// Output
		cv_void_errorpi = ARRAYV(pv_errorpi);
		cv_errorpi = (float*)cv_void_errorpi;
		cv_void_errorpi2 = ARRAYV(pv_errorpi2);
		cv_errorpi2 = (float*)cv_void_errorpi2;

		// G band Histogram
		cv_void_sigmapi_histogram = ARRAYV(pv_sigmapi_histogram);
		cv_sigmapi_histogram = (float*)cv_void_sigmapi_histogram;

		// Number of stars
		cv_void_nobstars = ARRAYV(pv_nobstars);
		cv_nobstars = (float*)cv_void_nobstars;

		//First calculate the Schlegel extinction
		pass = ColourExcessMap(cv_lon, cv_lat, n_lon, cv_ex31); //cv_ex31: only the Schlegel values

		if(!pass){
			fprintf(stderr, " *** Warning: problems with the calling to ColourExcessMap with GetSigmaPiDistr\n");
			fprintf(stderr, "\tZero value will be returned\n");
			return Py_BuildValue("i",0);
		};
		
		// Then, the 3D correction
		pass = ColourExcess3DMap(cv_lon, cv_lat, cv_dist, n_lon, cv_aux); // cv_aux: the 3d correction
		if(!pass){
			fprintf(stderr, " *** Warning: problems with the calling to ColourExcess3DMap with GetSigmaPiDistr\n");
			fprintf(stderr, "\tZero value will be returned\n");
			return Py_BuildValue("i",0);
		};
		
		// Now apply the SharmaCorrection and multiply by 3.24 to get the extinction in the V-band
		for(k=0;k<n_lon;k++){
			cv_ex31[k] = cv_aux[k]*cv_ex31[k];
			cv_ex31[k] = 3.24*cv_ex31[k]*(0.8-0.2*tanh(10.*cv_ex31[k]-1.5));

		}; // End of the Sharma Correction
		// The A_V extinction is calculated!

		// Now calculate the scanning law correction:
		geometric_correction( cv_lon, cv_lat, n_lon, correction_flag, cv_aux ); //Now aux is the geom. correction
		// The geometric correction was calculated!
		//Now include the Calibration factor and the epoch correction (sqrt(60/month))
		for(k=0;k<n_lon;k++){
			cv_aux[k]*=Cfactor;
		};

		//Now calculate the Gaia errors in pi:
		pass = 1*(IMF_type==107)+2*(IMF_type==98); // pass is a bitmask now
		switch(pass){
			case 2:
				fprintf(stderr, "\tSigmaPiGaia_distr_Bimodal (%i)...",pass);
				SigmaPiGaia_distr_Bimodal( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_ex31, cv_aux, cv_errorpi, cv_errorpi2, cv_sigmapi_histogram, cv_nobstars, n_metZ, Nbins, histo_axis, IMF_slope);
				break;
			case 1:
				fprintf(stderr, "\tSigmaPiGaia_distr_Kroupa (%i)...",pass);
				SigmaPiGaia_distr_Kroupa( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_ex31, cv_aux, cv_errorpi, cv_errorpi2, cv_sigmapi_histogram, cv_nobstars, n_metZ, Nbins, histo_axis);
				break;
			case  0:
				fprintf(stderr, "\tSigmaPiGaia_distr_Salpeter (%i)...",pass);
				SigmaPiGaia_distr_Salpeter( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_ex31, cv_aux, cv_errorpi, cv_errorpi2, cv_sigmapi_histogram, cv_nobstars, n_metZ, Nbins, histo_axis);
				break;
			default:
				fprintf(stderr, "\tdefault case: SigmaPiGaia_distr_Salpeter_logm...");
				SigmaPiGaia_distr_Salpeter( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_ex31, cv_aux, cv_errorpi, cv_errorpi2, cv_sigmapi_histogram, cv_nobstars, n_metZ, Nbins, histo_axis);

		};// End of switch
		fprintf(stderr, "finished.\n");	

		//Copy the output to a new direction
		memcpy(cv_void_errorpi, cv_errorpi, PyArray_ITEMSIZE((PyArrayObject*) pv_errorpi)*dims_out[0]);
		memcpy(cv_void_errorpi2, cv_errorpi2, PyArray_ITEMSIZE((PyArrayObject*) pv_errorpi2)*dims_out[0]);
		memcpy(cv_void_nobstars, cv_nobstars, PyArray_ITEMSIZE((PyArrayObject*) pv_nobstars)*dims_out[0]);
		memcpy(cv_void_ex31, cv_ex31, PyArray_ITEMSIZE((PyArrayObject*) pv_ex31)*dims_out[0]);

		memcpy(cv_void_sigmapi_histogram, cv_sigmapi_histogram, PyArray_ITEMSIZE((PyArrayObject*) pv_sigmapi_histogram)*Nbins);
		return Py_BuildValue("OOOOO", pv_errorpi, pv_errorpi2, pv_nobstars, pv_ex31, pv_sigmapi_histogram);



	}else{
		fprintf(stderr, " *** Warning: GetSigmaPiDistr must have at least 6 input arguments\n");
		fprintf(stderr, " Zero value will be returned\n");
		return Py_BuildValue("i",0);
	}

}; // End of GetSigmaPiDistr
//--------------------------------------------------------------------------------------------------------------------------------------//






//----------------------------- GetGaiaIMGhist -----------------------------------------------------------------------------------------// 
static PyObject *CAsgaiaTools_GetGaiaIMGhist(PyObject *self,PyObject *args){

	Py_Initialize();//needed command
	import_array();// needed command
	int k; // Counter

	//Inputs
	float *cv_lon, *cv_lat, *cv_dist;
	float *cv_metZ, *cv_logAge, *cv_mass;
	float Cfactor = 1.2; //Correction factor = CAfactor (calibration)*epoch correction (sqrt(60./month))
	int correction_flag = -1;
	char IMF_type='s'; //S,s mean Salpeter IMF. K, k mean Kroupa IMF. B, b mean Bimodal
	float IMF_slope = 2.35; //Only used when IMF_type = 'b'
	// G histogram
	int Nbins_G = 136;
	float G_histo_min = 3.;
	float G_histo_max = 20.;
	float G_histo_axis[2];
	
	// Intermediate quantities:
	float *cv_aux; // It will be the correction due to the Gaia' scanning law

	//Output issue
	float *cv_errorpi, *cv_ex31;
	void *cv_void_errorpi, *cv_void_ex31;
	float *cv_ini_mass_histogram;
	void *cv_void_ini_mass_histogram;
	float *cv_Gband_histogram;
	void *cv_void_Gband_histogram;
	float *cv_nobstars;
	void *cv_void_nobstars;

	// Checks
	int n_lon, n_lat, n_dist, n_metZ, n_logAge, pass; //Check if the dimensions are correct
	npy_intp dims_out[1], dims_histogram[1];

	// Python structures
	PyArrayObject *pv_errorpi, *pv_ex31; // Outputs
	PyArrayObject *pv_ini_mass_histogram, *pv_Gband_histogram; // Histogram (output)
	PyArrayObject *pv_nobstars; // Number of stars (output)
	PyArrayObject *pv_aux; //Intermediate
	PyObject *pv_lon, *pv_lat, *pv_dist; // Inputs
	PyObject *pv_metZ,*pv_logAge, *pv_mass; // Inputs


	Py_ssize_t Nargs = PyTuple_Size(args); // Number of the input arguments received (used here for first time)



	if(Nargs==6 || Nargs==13){
		//We can receive either six or thirteen arguments: lon, lat, dist(kpc), metZ, logAge, *IMF_type, *Cfactor, *correction_flag
		PyArg_ParseTuple(args,"OOOOOO|cfiifff", &pv_lon, &pv_lat, &pv_dist, &pv_metZ, &pv_logAge, &pv_mass, &IMF_type, &Cfactor, &correction_flag, &Nbins_G, &G_histo_min, &G_histo_max, &IMF_slope); //Python input arguments

		// Always in the Gaia limit:
		if(G_histo_axis[0]<Gmin) fprintf(stderr,"   WARN: G-band lower limit modified to %.2f\n",Gmin);
		if(G_histo_axis[1]>Gmax) fprintf(stderr,"   WARN: G-band upper limit modified to %.2f\n",Gmax);
		G_histo_axis[0] = max(G_histo_min, Gmin);
		G_histo_axis[1] = min(G_histo_max, Gmax);

		//Analyse the IMF_type:
		IMF_type = IMF_type*(IMF_type>=65 && IMF_type<=122);
		IMF_type = IMF_type + 32*(IMF_type<97) + 65*(IMF_type==0);
		if(IMF_type==115){
			fprintf(stderr,"   INFO: Salpeter IMF will be used by GetGaiaIMGhist\n");
		}else if(IMF_type==107){
			fprintf(stderr,"   INFO: Kroupa IMF will be used by GetGaiaIMGhist\n");
		}else if(IMF_type==98){
			fprintf(stderr,"   INFO: Bimodal IMF will be used by GetGaiaIMGhist\n");
		}else{
			fprintf(stderr,"***WARNING: unknown IMF. Salpeter will be used by GetGaiaIMGhist\n");
			IMF_type = 115;
		};

		//Get number of particles:
		n_metZ = (int) ((PyArrayObject*)pv_metZ)->dimensions[0];

		// Check the dimensions:
		n_logAge = ((PyArrayObject*)pv_logAge)->dimensions[0];
		n_lon = ((PyArrayObject*)pv_lon)->dimensions[0];
		n_lat = ((PyArrayObject*)pv_lat)->dimensions[0];
		n_dist = ((PyArrayObject*)pv_dist)->dimensions[0];

		// Condition to continue:
		pass = (n_metZ==n_logAge)*(n_logAge==n_dist)*(n_dist==n_lon)*(n_lon==n_lat);
	
		if(!pass){
			fprintf(stderr, " *** Warning: dimensions in GetGaiaIMGhist mismatch\n");
			fprintf(stderr, "\tZero value will be returned\n");
			return Py_BuildValue("i",0);
		};

		//Create output python vector (particle's sigmapi)
		dims_out[0] = n_metZ;
		pv_errorpi  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); // Python vector (of floats)
		pv_ex31  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); // Python vector (of floats)
		pv_nobstars  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); // Python vector (of floats)

		//Create output python histogram (for the photometry - G band)
		dims_histogram[0] = Nbins_G;// Now dims_histogram is an ancilliary variable, only used for type conversion
		pv_Gband_histogram = (PyArrayObject*)PyArray_SimpleNew(1, dims_histogram, PyArray_FLOAT32 ); // Python vector (of floats)

		//Create output python histogram (for the initial masses)
		dims_histogram[0] = Max_Number_of_logmasses_per_isochrone;// Dimensions
		pv_ini_mass_histogram = (PyArrayObject*)PyArray_SimpleNew(1, dims_histogram, PyArray_FLOAT32 ); // Python vector (of floats)


		//Create the intermediate python vector
		pv_aux  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); // Python vector (of floats)
	

		//Let's link two types of pointers
		cv_lon = ARRAYF(pv_lon);
		cv_lat = ARRAYF(pv_lat);
		cv_dist = ARRAYF(pv_dist);
		cv_metZ = ARRAYF(pv_metZ);
		cv_logAge = ARRAYF(pv_logAge);
		cv_mass = ARRAYF(pv_mass);

		cv_aux = ARRAYF(pv_aux);

		cv_void_ex31 = ARRAYV(pv_ex31);
		cv_ex31 = (float*)cv_void_ex31;

		// Output
		cv_void_errorpi = ARRAYV(pv_errorpi);
		cv_errorpi = (float*)cv_void_errorpi;

		// Initial Mass Histogram
		cv_void_ini_mass_histogram = ARRAYV(pv_ini_mass_histogram);
		cv_ini_mass_histogram = (float*)cv_void_ini_mass_histogram;

		// G band Histogram
		cv_void_Gband_histogram = ARRAYV(pv_Gband_histogram);
		cv_Gband_histogram = (float*)cv_void_Gband_histogram;

		// Number of stars
		cv_void_nobstars = ARRAYV(pv_nobstars);
		cv_nobstars = (float*)cv_void_nobstars;

		//First calculate the Schlegel extinction
		pass = ColourExcessMap(cv_lon, cv_lat, n_lon, cv_ex31); //cv_ex31: only the Schlegel values

		if(!pass){
			fprintf(stderr, " *** Warning: problems with the calling to ColourExcessMap with GetGaiaIMGhist\n");
			fprintf(stderr, "\tZero value will be returned\n");
			return Py_BuildValue("i",0);
		};
		
		// Then, the 3D correction
		pass = ColourExcess3DMap(cv_lon, cv_lat, cv_dist, n_lon, cv_aux); // cv_aux: the 3d correction
		if(!pass){
			fprintf(stderr, " *** Warning: problems with the calling to ColourExcess3DMap with GetGaiaIMGhist\n");
			fprintf(stderr, "\tZero value will be returned\n");
			return Py_BuildValue("i",0);
		};
		
		// Now apply the SharmaCorrection and multiply by 3.24 to get the extinction in the V-band
		for(k=0;k<n_lon;k++){
			cv_ex31[k] = cv_aux[k]*cv_ex31[k];
			cv_ex31[k] = 3.24*cv_ex31[k]*(0.8-0.2*tanh(10.*cv_ex31[k]-1.5));

		}; // End of the Sharma Correction
		// The A_V extinction is calculated!

		// Now calculate the scanning law correction:
		geometric_correction( cv_lon, cv_lat, n_lon, correction_flag, cv_aux ); //Now aux is the geom. correction
		// The geometric correction was calculated!
		//Now include the Calibration factor and the epoch correction (sqrt(60/month))
		for(k=0;k<n_lon;k++){
			cv_aux[k]*=Cfactor;
		};

		//Now calculate the Gaia errors in pi:
		pass = 10*(IMF_type==107)+20*(IMF_type==98); // pass is a bitmask now
		switch(pass){
			case 2:
				fprintf(stderr, "\tSigmaPiGaia_IMGhist_Bimodal (%i)...",pass);
				SigmaPiGaia_IMGhist_Bimodal( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_ex31, cv_aux, cv_errorpi, cv_ini_mass_histogram, cv_Gband_histogram, cv_nobstars, n_metZ, Nbins_G, G_histo_axis, IMF_slope);
				break;
			case 1:
				fprintf(stderr, "\tSigmaPiGaia_IMGhist_Kroupa (%i)...",pass);
				SigmaPiGaia_IMGhist_Kroupa( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_ex31, cv_aux, cv_errorpi, cv_ini_mass_histogram, cv_Gband_histogram, cv_nobstars, n_metZ, Nbins_G, G_histo_axis);
				break;
			case  0:
				fprintf(stderr, "\tSigmaPiGaia_IMGhist_Salpeter (%i)...",pass);
				SigmaPiGaia_IMGhist_Salpeter( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_ex31, cv_aux, cv_errorpi, cv_ini_mass_histogram, cv_Gband_histogram, cv_nobstars, n_metZ, Nbins_G, G_histo_axis);
				break;
			default:
				fprintf(stderr, "\tdefault case: SigmaPiGaia_IMGhist_Salpeter_logm...");
				SigmaPiGaia_IMGhist_Salpeter( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_ex31, cv_aux, cv_errorpi, cv_ini_mass_histogram, cv_Gband_histogram, cv_nobstars, n_metZ, Nbins_G, G_histo_axis);
		};// End of switch
		fprintf(stderr, "finished.\n");	

		//Copy the output to a new direction
		memcpy(cv_void_errorpi, cv_errorpi, PyArray_ITEMSIZE((PyArrayObject*) pv_errorpi)*dims_out[0]);
		memcpy(cv_void_nobstars, cv_nobstars, PyArray_ITEMSIZE((PyArrayObject*) pv_nobstars)*dims_out[0]);
		memcpy(cv_void_ex31, cv_ex31, PyArray_ITEMSIZE((PyArrayObject*) pv_ex31)*dims_out[0]);

		memcpy(cv_void_ini_mass_histogram, cv_ini_mass_histogram, PyArray_ITEMSIZE((PyArrayObject*) pv_ini_mass_histogram)*dims_histogram[0]);
		memcpy(cv_void_Gband_histogram, cv_Gband_histogram, PyArray_ITEMSIZE((PyArrayObject*) pv_Gband_histogram)*Nbins_G);
		return Py_BuildValue("OOOOO", pv_errorpi, pv_nobstars, pv_ex31, pv_ini_mass_histogram, pv_Gband_histogram);



	}else{
		fprintf(stderr, " *** Warning: GetGaiaIMGhist must have at least 6 input arguments (Correction factor is optional)\n");
		fprintf(stderr, " Zero value will be returned\n");
		return Py_BuildValue("i",0);
	}

}; // End of GetGaiaIMGhist
//--------------------------------------------------------------------------------------------------------------------------------------//




/* INDIVIDUAL MAGNITUDES */
//----------------------------- GetMagnitude -----------------------------------------------------------------------------------------// 
static PyObject *CAsgaiaTools_GetMagnitude(PyObject *self,PyObject *args){

	Py_Initialize();//needed command
	import_array();// needed command

	//Inputs
	float *cv_exmag, *cv_dist;
	float *cv_metZ, *cv_logAge, *cv_mass;
	char IMF_type='s'; //S, s mean Salpeter IMF. K, k mean Kroupa IMF. B, b mean Bimodal IMF
	float IMF_slope = 2.35; // Used only for the Bimodal IMF
	int band_id = 0;
	float mag_min, mag_max;
	float mag_axis[2] = {0.,10.};
	
	//Output issue
	int Nbins;
	float *cv_Mfcis;
	void *cv_void_Mfcis;
	float *cv_nobstars;
	void *cv_void_nobstars;
	float *cv_magnitude_histogram;
	void *cv_void_magnitude_histogram;

	// Checks
	int n_metZ, n_logAge, n_dist, n_mass, n_exmag, pass; //Check if the dimensions are correct
	int isGaia; // 1 when the considered band is G, G_BP, G_RP or G_RVS
	npy_intp dims_out[1], dims_histogram[1];

	// Python structures
	PyArrayObject *pv_Mfcis, *pv_nobstars; // Outputs
	PyArrayObject *pv_magnitude_histogram; // Histogram output
	PyObject *pv_dist, *pv_exmag; // Inputs
	PyObject *pv_metZ, *pv_logAge, *pv_mass; // Inputs


	Py_ssize_t Nargs = PyTuple_Size(args); // Number of the input arguments received (used here for first time)


	if(Nargs>=5){
	//If we receive 3 arrays :[Z, logAge, Mass] -> calculate absolute magnitudes		
	//If we receive 5 arrays :[Z, logAge, Mass, dist_kpc, band_extinction] -> calculate apparent magnitudes
		PyArg_ParseTuple(args,"OOOOO|iffcif", &pv_metZ, &pv_logAge, &pv_mass, &pv_dist, &pv_exmag, &Nbins, &mag_min, &mag_max, &IMF_type, &band_id, &IMF_slope);
		
		// Set the axis limits
		mag_axis[0] = mag_min;
		mag_axis[1] = mag_max;

		//Analyse the IMF_type:
		IMF_type = IMF_type*(IMF_type>=65 && IMF_type<=122);
		IMF_type = IMF_type + 32*(IMF_type<97) + 65*(IMF_type==0);
		if(IMF_type==115){
			fprintf(stderr,"   INFO: Salpeter IMF will be used by GetMagnitude\n");
		}else if(IMF_type==107){
			fprintf(stderr,"   INFO: Kroupa IMF will be used by GetMagnitude\n");
		}else if(IMF_type==98){
			fprintf(stderr,"   INFO: Bimodal IMF will be used by GetMagnitude\n");
		}else{
			fprintf(stderr,"***WARNING: unknown IMF. Salpeter will be used by GetMagnitude\n");
			IMF_type = 115;
		};

		//Get number of particles:
		n_metZ = (int) ((PyArrayObject*)pv_metZ)->dimensions[0];

		// Check the dimensions:
		n_logAge = ((PyArrayObject*)pv_logAge)->dimensions[0];
		n_mass = ((PyArrayObject*)pv_mass)->dimensions[0];
		n_dist = ((PyArrayObject*)pv_dist)->dimensions[0];
		n_exmag = ((PyArrayObject*)pv_exmag)->dimensions[0];


		// Condition to continue:
		pass = (n_metZ==n_logAge)*(n_logAge==n_mass)*(n_mass==n_dist)*(n_dist==n_exmag);
	
		if(!pass){
			fprintf(stderr, " *** Warning: dimensions in GetMagnitude mismatch\n");
			fprintf(stderr, "\tZero value will be returned\n");
			return Py_BuildValue("i",0);
		};

		//Create output python vector (particle's Mass fraction converted into stars and stars observed in the particle)
		dims_out[0] = n_metZ;
		pv_Mfcis  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); // Python vector (of floats)
		pv_nobstars  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); // Python vector (of floats)

		//Create output python histogram (for the photometry)
		dims_histogram[0] = Nbins;// Now dims_histogram is an ancilliary variable, only used for type conversion
		pv_magnitude_histogram = (PyArrayObject*)PyArray_SimpleNew(1, dims_histogram, PyArray_FLOAT32 ); // Python vector (of floats)

	
		//Let's link two types of pointers
		cv_exmag = ARRAYF(pv_exmag);
		cv_dist = ARRAYF(pv_dist);
		cv_metZ = ARRAYF(pv_metZ);
		cv_logAge = ARRAYF(pv_logAge);
		cv_mass = ARRAYF(pv_mass);

		// Output
		cv_void_nobstars = ARRAYV(pv_nobstars);
		cv_nobstars = (float*)cv_void_nobstars;
		cv_void_Mfcis = ARRAYV(pv_Mfcis);
		cv_Mfcis = (float*)cv_void_Mfcis;

		// Magnitude histogram
		cv_void_magnitude_histogram = ARRAYV(pv_magnitude_histogram);
		cv_magnitude_histogram = (float*)cv_void_magnitude_histogram;

		// Is a gaia band?
		isGaia = (band_id==2) + (band_id==4) + (band_id==6) + (band_id==8);

		//Now calculate the Magnitudes in mags:
		pass = 1*(IMF_type==107)+2*(IMF_type==98); // pass is a bitmask now

		if(!isGaia){
			switch(pass){
				case 2:
					fprintf(stderr, "\t%i case: Mag_distr_Bimodal...\n",pass);
					Mag_distr_Bimodal( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_exmag, cv_magnitude_histogram, cv_nobstars, cv_Mfcis, n_metZ, Nbins, mag_axis, band_id, IMF_slope);
					break;
				case 1:
					fprintf(stderr, "\t%i case: Mag_distr_Kroupa...\n",pass);
					Mag_distr_Kroupa( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_exmag, cv_magnitude_histogram, cv_nobstars, cv_Mfcis, n_metZ, Nbins, mag_axis, band_id);
					break;
				default:
					fprintf(stderr, "\tdefault case: Mag_distr_Salpeter...\n");
					Mag_distr_Salpeter( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_exmag, cv_magnitude_histogram, cv_nobstars, cv_Mfcis, n_metZ, Nbins, mag_axis, band_id);	
			};// End of switch
		}else{
			switch(pass){
				case 2:
					fprintf(stderr, "\t%i case: MagGaia_distr_Bimodal...\n",pass);
					MagGaia_distr_Bimodal( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_exmag, cv_magnitude_histogram, cv_nobstars, cv_Mfcis, n_metZ, Nbins, mag_axis, band_id, IMF_slope);
					break;
				case 1:
					fprintf(stderr, "\t%i case: MagGaia_distr_Kroupa...\n",pass);
					MagGaia_distr_Kroupa( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_exmag, cv_magnitude_histogram, cv_nobstars, cv_Mfcis, n_metZ, Nbins, mag_axis, band_id);
					break;
				default:
					fprintf(stderr, "\tdefault case: MagGaia_distr_Salpeter...\n");
					MagGaia_distr_Salpeter( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_exmag, cv_magnitude_histogram, cv_nobstars, cv_Mfcis, n_metZ, Nbins, mag_axis, band_id);	
			};// End of switch
		}; // End of the if loop


		//Copy the output to a new direction
		memcpy(cv_void_Mfcis, cv_Mfcis, PyArray_ITEMSIZE((PyArrayObject*) pv_Mfcis)*n_metZ);
		memcpy(cv_void_nobstars, cv_nobstars, PyArray_ITEMSIZE((PyArrayObject*) pv_nobstars)*n_metZ);
		memcpy(cv_void_magnitude_histogram, cv_magnitude_histogram, PyArray_ITEMSIZE((PyArrayObject*) pv_magnitude_histogram)*Nbins);

		return Py_BuildValue("OOO", pv_magnitude_histogram, pv_nobstars, pv_Mfcis);
			


	}else{
		fprintf(stderr, " *** Warning: GetMagnitude must have at least 5 input arguments\n");
		fprintf(stderr, " Zero value will be returned\n");
		return Py_BuildValue("i",0);
	}

}; // End of GetMagnitude
//--------------------------------------------------------------------------------------------------------------------------------------//




//----------------------------- CMD -----------------------------------------------------------------------------------------// 
static PyObject *CAsgaiaTools_CMD(PyObject *self,PyObject *args){

	Py_Initialize();//needed command
	import_array();// needed command

	//Inputs
	float *cv_AV, *cv_dist;
	float *cv_metZ, *cv_logAge, *cv_mass;
	int interpolation = 0;
	char IMF_type='s'; //S,s mean Salpeter IMF. K, k mean Kroupa IMF. B,b mean Bimodal
	float IMF_slope= 2.35; // Used when the bimodal IMF is selected

	float mag_axis[4] = {0.,10.,-1.,10.};//Diagram axis limits
	float mag_min_x, mag_max_x, mag_min_y, mag_max_y;// Diagram axis limits

	int Nbins_x = 10;//Number of bins in x
	int Nbins_y = 10;//Number of bins in y
	int CMD_bins[2]; //Number of bins (CMD size)

	int band_id_x = 1;
	int band_id_y = 2;
	int band_id_z = 2;
	int bands_id[3]; //[band_id_x, band_id_y, band_id_z] CMD: band_id_z vs (band_id_x-band_id_y)

	//Output issue
	float *cv_nobstars;
	void *cv_void_nobstars;
	float *cv_Mfcis;
	void *cv_void_Mfcis;
	float *cv_cmd;
	void *cv_void_cmd;

	// Checks
	int n_metZ, n_logAge, n_dist, n_mass, n_AV, pass; //Check if the dimensions are correct
	npy_intp dims_cmd[2];
	npy_intp dims_out[2];

	// Python structures
	PyArrayObject *pv_Mfcis, *pv_nobstars; // Outputs
	PyArrayObject *pv_cmd; // Histogram output
	PyObject *pv_dist, *pv_AV; // Inputs
	PyObject *pv_metZ, *pv_logAge, *pv_mass; // Inputs


	Py_ssize_t Nargs = PyTuple_Size(args); // Number of the input arguments received (used here for first time)


	if(Nargs>=5){
	//If we receive 3 arrays :[Z, logAge, Mass] -> calculate absolute magnitudes		
	//If we receive 5 arrays :[Z, logAge, Mass, dist_kpc, band_extinction] -> calculate apparent magnitudes
		PyArg_ParseTuple(args,"OOOOO|iiffffciiiif", &pv_metZ, &pv_logAge, &pv_mass, &pv_dist, &pv_AV, &Nbins_x, &Nbins_y, &mag_min_x, &mag_max_x, &mag_min_y, &mag_max_y, &IMF_type, &interpolation, &band_id_x, &band_id_y, &band_id_z, &IMF_slope);
		
		// Set the axis limits
		mag_axis[0] = mag_min_x;
		mag_axis[1] = mag_max_x;
		mag_axis[2] = mag_min_y;
		mag_axis[3] = mag_max_y;

		// Set number of bins
		CMD_bins[0] = Nbins_x;
		CMD_bins[1] = Nbins_y;

		// Set bands:
		bands_id[0] = band_id_x;
		bands_id[1] = band_id_y;
		bands_id[2] = band_id_z;

		//Analyse the IMF_type:
		IMF_type = IMF_type*(IMF_type>=65 && IMF_type<=122);
		IMF_type = IMF_type + 32*(IMF_type<97) + 65*(IMF_type==0);
		if(IMF_type==115){
			fprintf(stderr,"   INFO: Salpeter IMF will be used by CMD\n");
		}else if(IMF_type==107){
			fprintf(stderr,"   INFO: Kroupa IMF will be used by CMD\n");
		}else if(IMF_type==98){
			fprintf(stderr,"   INFO: Bimodal IMF will be used by CMD\n");
		}else{
			fprintf(stderr,"***WARNING: unknown IMF. Salpeter will be used by CMD\n");
			IMF_type = 115;
		};

		//Get number of particles:
		n_metZ = (int) ((PyArrayObject*)pv_metZ)->dimensions[0];

		// Check the dimensions:
		n_logAge = ((PyArrayObject*)pv_logAge)->dimensions[0];
		n_mass = ((PyArrayObject*)pv_mass)->dimensions[0];
		n_dist = ((PyArrayObject*)pv_dist)->dimensions[0];
		n_AV = ((PyArrayObject*)pv_AV)->dimensions[0];


		// Condition to continue:
		pass = (n_metZ==n_logAge)*(n_logAge==n_mass)*(n_mass==n_dist)*(n_dist==n_AV);
	
		if(!pass){
			fprintf(stderr, " *** Warning: dimensions in CMD mismatch:\n");
			fprintf(stderr, "      %i %i %i %i %i \n",n_metZ, n_logAge, n_mass, n_dist, n_AV);
			fprintf(stderr, "\tZero value will be returned\n");
			return Py_BuildValue("i",0);
		};

		//Create output python vector (particle's Mass fraction converted into stars and stars observed in the particle)
		dims_out[0] = n_metZ;
		pv_Mfcis  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); // Python vector (of floats)
		pv_nobstars  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); // Python vector (of floats)

		//Create output python map (for the photometry)
		dims_cmd[0] = Nbins_y;
		dims_cmd[1] = Nbins_x;
		pv_cmd = (PyArrayObject*)PyArray_SimpleNew(2, dims_cmd, PyArray_FLOAT32 ); // Python vector (of floats)

	
		//Let's link two types of pointers
		cv_AV = ARRAYF(pv_AV);
		cv_dist = ARRAYF(pv_dist);
		cv_metZ = ARRAYF(pv_metZ);
		cv_logAge = ARRAYF(pv_logAge);
		cv_mass = ARRAYF(pv_mass);

		// Output
		cv_void_nobstars = ARRAYV(pv_nobstars);
		cv_nobstars = (float*)cv_void_nobstars;
		cv_void_Mfcis = ARRAYV(pv_Mfcis);
		cv_Mfcis = (float*)cv_void_Mfcis;

		// Magnitude histogram
		cv_void_cmd = ARRAYV(pv_cmd);
		cv_cmd = (float*)cv_void_cmd;


		//Now calculate the Magnitudes in mags:
		pass = 10*(IMF_type==107)+20*(IMF_type==98)+1*(interpolation==1); // pass is a bitmask now


		switch(pass){
			case 21:
				fprintf(stderr, "\t%i case: CMD_Bimodal_4Points...\n",pass);
				CMD_Bimodal_4Points( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_AV, cv_cmd, cv_nobstars, cv_Mfcis, n_metZ, CMD_bins, mag_axis, bands_id, IMF_slope);
				break;
			case 20:
				fprintf(stderr, "\t%i case: CMD_Bimodal...\n",pass);
				CMD_Bimodal( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_AV, cv_cmd, cv_nobstars, cv_Mfcis, n_metZ, CMD_bins, mag_axis, bands_id, IMF_slope);
				break;
			case 11:
				fprintf(stderr, "\t%i case: CMD_Kroupa_4Points...\n",pass);
				CMD_Kroupa_4Points( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_AV, cv_cmd, cv_nobstars, cv_Mfcis, n_metZ, CMD_bins, mag_axis, bands_id);
				break;
			case 10:
				fprintf(stderr, "\t%i case: CMD_Kroupa...\n",pass);
				CMD_Kroupa( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_AV, cv_cmd, cv_nobstars, cv_Mfcis, n_metZ, CMD_bins, mag_axis, bands_id);
				break;
			case 1:
				fprintf(stderr, "\t%i case: CMD_Salpeter_4Points...\n",pass);
				CMD_Salpeter_4Points( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_AV, cv_cmd, cv_nobstars, cv_Mfcis, n_metZ, CMD_bins, mag_axis, bands_id);
				break;
			default:
				fprintf(stderr, "\tdefault case: CMD_Salpeter...\n");
				CMD_Salpeter( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_AV, cv_cmd, cv_nobstars, cv_Mfcis, n_metZ, CMD_bins, mag_axis, bands_id);
		};

		//Copy the output to a new direction
		memcpy(cv_void_nobstars, cv_nobstars, PyArray_ITEMSIZE((PyArrayObject*) pv_nobstars)*n_metZ);
		memcpy(cv_void_Mfcis, cv_Mfcis, PyArray_ITEMSIZE((PyArrayObject*) pv_Mfcis)*n_metZ);
		memcpy(cv_void_cmd, cv_cmd, PyArray_ITEMSIZE((PyArrayObject*) pv_cmd)*dims_cmd[0]*dims_cmd[1]);

		return Py_BuildValue("OOO", pv_cmd, pv_nobstars, pv_Mfcis);
			


	}else{
		fprintf(stderr, " *** Warning: CMD must have at least 5 input arguments\n");
		fprintf(stderr, " Zero value will be returned\n");
		return Py_BuildValue("i",0);
	}

}; // End of CMD
//--------------------------------------------------------------------------------------------------------------------------------------//




//----------------------------- HR -----------------------------------------------------------------------------------------// 
static PyObject *CAsgaiaTools_HR(PyObject *self,PyObject *args){

	Py_Initialize();//needed command
	import_array();// needed command

	//Inputs
	float *cv_AV, *cv_dist;
	float *cv_metZ, *cv_logAge, *cv_mass;
	int interpolation = 0;
	char IMF_type='s'; //S,s mean Salpeter IMF. K, k mean Kroupa IMF. B,b means bimodal IMF
	float IMF_slope = 2.35; // Used only with Bimodal IMFs

	float hr_axis[4] = {3.4,4.7,-1.,10.};//Diagram axis limits
	float hr_min_x, hr_max_x, hr_min_y, hr_max_y;// Diagram axis limits

	int Nbins_x = 10;//Number of bins in x
	int Nbins_y = 10;//Number of bins in y
	int HR_bins[2]; //Number of bins (HR size)

	int band_id_x = -1;
	int band_id_y = 2;
	int bands_id[2]; //[band_id_x, band_id_y]

	//Output issue
	float *cv_nobstars;
	void *cv_void_nobstars;
	float *cv_Mfcis;
	void *cv_void_Mfcis;
	float *cv_hr;
	void *cv_void_hr;

	// Checks
	int n_metZ, n_logAge, n_dist, n_mass, n_AV, pass; //Check if the dimensions are correct
	npy_intp dims_hr[2];
	npy_intp dims_out[2];

	// Python structures
	PyArrayObject *pv_Mfcis, *pv_nobstars; // Outputs
	PyArrayObject *pv_hr; // Histogram output
	PyObject *pv_dist, *pv_AV; // Inputs
	PyObject *pv_metZ, *pv_logAge, *pv_mass; // Inputs


	Py_ssize_t Nargs = PyTuple_Size(args); // Number of the input arguments received (used here for first time)


	if(Nargs>=5){
	//If we receive 3 arrays :[Z, logAge, Mass] -> calculate absolute magnitudes		
	//If we receive 5 arrays :[Z, logAge, Mass, dist_kpc, band_extinction] -> calculate apparent magnitudes
		PyArg_ParseTuple(args,"OOOOO|iiffffciiif", &pv_metZ, &pv_logAge, &pv_mass, &pv_dist, &pv_AV, &Nbins_x, &Nbins_y, &hr_min_x, &hr_max_x, &hr_min_y, &hr_max_y, &IMF_type, &interpolation, &band_id_x, &band_id_y, &IMF_slope);
	
		// Set the axis limits
		hr_axis[0] = hr_min_x;
		hr_axis[1] = hr_max_x;
		hr_axis[2] = hr_min_y;
		hr_axis[3] = hr_max_y;

		// Set number of bins
		HR_bins[0] = Nbins_x;
		HR_bins[1] = Nbins_y;

		// Set bands:
		bands_id[0] = band_id_x;
		bands_id[1] = band_id_y;

		//Analyse the IMF_type:
		IMF_type = IMF_type*(IMF_type>=65 && IMF_type<=122);
		IMF_type = IMF_type + 32*(IMF_type<97) + 65*(IMF_type==0);
		if(IMF_type==115){
			fprintf(stderr,"   INFO: Salpeter IMF will be used by HR\n");
		}else if(IMF_type==107){
			fprintf(stderr,"   INFO: Kroupa IMF will be used by HR\n");
		}else if(IMF_type==98){
			fprintf(stderr,"   INFO: Bimodal IMF will be used by HR\n");
		}else{
			fprintf(stderr,"***WARNING: unknown IMF. Salpeter will be used by HR\n");
			IMF_type = 115;
		};

		//Get number of particles:
		n_metZ = (int) ((PyArrayObject*)pv_metZ)->dimensions[0];

		// Check the dimensions:
		n_logAge = ((PyArrayObject*)pv_logAge)->dimensions[0];
		n_mass = ((PyArrayObject*)pv_mass)->dimensions[0];
		n_dist = ((PyArrayObject*)pv_dist)->dimensions[0];
		n_AV = ((PyArrayObject*)pv_AV)->dimensions[0];


		// Condition to continue:
		pass = (n_metZ==n_logAge)*(n_logAge==n_mass)*(n_mass==n_dist)*(n_dist==n_AV);
	
		if(!pass){
			fprintf(stderr, " *** Warning: dimensions in HR mismatch:\n");
			fprintf(stderr, "      %i %i %i %i %i \n",n_metZ, n_logAge, n_mass, n_dist, n_AV);
			fprintf(stderr, "\tZero value will be returned\n");
			return Py_BuildValue("i",0);
		};

		//Create output python vector (particle's Mass fraction converted into stars and stars observed in the particle)
		dims_out[0] = n_metZ;
		pv_Mfcis  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); // Python vector (of floats)
		pv_nobstars  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); // Python vector (of floats)

		//Create output python map (for the photometry)
		dims_hr[0] = Nbins_y;
		dims_hr[1] = Nbins_x;
		pv_hr = (PyArrayObject*)PyArray_SimpleNew(2, dims_hr, PyArray_FLOAT32 ); // Python vector (of floats)

	
		//Let's link two types of pointers
		cv_AV = ARRAYF(pv_AV);
		cv_dist = ARRAYF(pv_dist);
		cv_metZ = ARRAYF(pv_metZ);
		cv_logAge = ARRAYF(pv_logAge);
		cv_mass = ARRAYF(pv_mass);

		// Output
		cv_void_nobstars = ARRAYV(pv_nobstars);
		cv_nobstars = (float*)cv_void_nobstars;
		cv_void_Mfcis = ARRAYV(pv_Mfcis);
		cv_Mfcis = (float*)cv_void_Mfcis;

		// Magnitude histogram
		cv_void_hr = ARRAYV(pv_hr);
		cv_hr = (float*)cv_void_hr;


		//Now calculate the Magnitudes in mags:
		pass = 10*(IMF_type==107)+20*(IMF_type==98)+1*(interpolation==1); // pass is a bitmask now
		switch(pass){
			case 21:
				fprintf(stderr, "\t%i case: HR_Bimodal_4Points...\n",pass);
				HR_Bimodal_4Points( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_AV, cv_hr, cv_nobstars, cv_Mfcis, n_metZ, HR_bins, hr_axis, bands_id, IMF_slope);
				break;
			case 20:
				fprintf(stderr, "\t%i case: HR_Bimodal...\n",pass);
				HR_Bimodal( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_AV, cv_hr, cv_nobstars, cv_Mfcis, n_metZ, HR_bins, hr_axis, bands_id, IMF_slope);
				break;
			case 11:
				fprintf(stderr, "\t%i case: HR_Kroupa_4Points...\n",pass);
				HR_Kroupa_4Points( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_AV, cv_hr, cv_nobstars, cv_Mfcis, n_metZ, HR_bins, hr_axis, bands_id);
				break;
			case 10:
				fprintf(stderr, "\t%i case: HR_Kroupa...\n",pass);
				HR_Kroupa( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_AV, cv_hr, cv_nobstars, cv_Mfcis, n_metZ, HR_bins, hr_axis, bands_id);
				break;
			case 1:
				fprintf(stderr, "\t%i case: HR_Salpeter_4Points...\n",pass);
				HR_Salpeter_4Points( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_AV, cv_hr, cv_nobstars, cv_Mfcis, n_metZ, HR_bins, hr_axis, bands_id);
				break;
			default:
				fprintf(stderr, "\tdefault case: HR_Salpeter...\n");
				HR_Salpeter( cv_metZ, cv_logAge, cv_mass, cv_dist, cv_AV, cv_hr, cv_nobstars, cv_Mfcis, n_metZ, HR_bins, hr_axis, bands_id);

		};
		//Copy the output to a new direction
		memcpy(cv_void_nobstars, cv_nobstars, PyArray_ITEMSIZE((PyArrayObject*) pv_nobstars)*n_metZ);
		memcpy(cv_void_Mfcis, cv_Mfcis, PyArray_ITEMSIZE((PyArrayObject*) pv_Mfcis)*n_metZ);
		memcpy(cv_void_hr, cv_hr, PyArray_ITEMSIZE((PyArrayObject*) pv_hr)*dims_hr[0]*dims_hr[1]);

		return Py_BuildValue("OOO", pv_hr, pv_nobstars, pv_Mfcis);
			


	}else{
		fprintf(stderr, " *** Warning: HR must have at least 5 input arguments\n");
		fprintf(stderr, " Zero value will be returned\n");
		return Py_BuildValue("i",0);
	}

}; // End of HR
//--------------------------------------------------------------------------------------------------------------------------------------//








//----------------------------- GetAPOGEEdistances -----------------------------------------------------------// 
static PyObject *CAsgaiaTools_GetAPOGEEdistances(PyObject *self,PyObject *args){

	Py_Initialize();//needed command
	import_array();// needed command

	float *cv_lon, *cv_lat, *cv_metZ, *cv_logTe, *cv_logg;
	float *cv_eZ, *cv_elogTe, *cv_elogg;
	float *cv_J, *cv_H, *cv_K;
	float *cv_eJ, *cv_eH, *cv_eK;
	int n_metZ, n_logTe, n_logg, n_lon, n_lat, n_elogg, n_elogTe, n_eZ, pass; //Check if the dimensions are correct
	int n_J, n_H, n_K, n_eJ, n_eH, n_eK;

	//Output issue
	float *cv_distestim;
	int *cv_infoflag;
	void *cv_void_distestim, *cv_void_infoflag;
	npy_intp dims_out[1];

	PyArrayObject *pv_distestim, *pv_infoflag;
	PyObject *pv_lon,*pv_lat; //Prefix pv means Python Vector
	PyObject *pv_metZ,*pv_logTe, *pv_logg; //Prefix pv means Python Vector
	PyObject *pv_J,*pv_H, *pv_K; //Prefix pv means Python Vector
	PyObject *pv_eJ,*pv_eH, *pv_eK; //Prefix pv means Python Vector
	PyObject *pv_eZ,*pv_elogTe, *pv_elogg; //Prefix pv means Python Vector
	Py_ssize_t Nargs = PyTuple_Size(args); //Number of the input arguments received (used here for first time)

	
	if(Nargs==14){
		//We have received 14 arguments: 
		PyArg_ParseTuple(args,"OOOOOOOOOOOOOO", &pv_lon, &pv_lat, &pv_metZ, &pv_logTe, &pv_logg, &pv_J, &pv_H, &pv_K, &pv_eZ, &pv_elogTe, &pv_elogg, &pv_eJ, &pv_eH, &pv_eK); //Python input arguments

		//Get number of particles:
		n_metZ = (int) ((PyArrayObject*)pv_metZ)->dimensions[0];

		// Check the dimensions:
		n_lon = ((PyArrayObject*)pv_lon)->dimensions[0];
		n_lat = ((PyArrayObject*)pv_lat)->dimensions[0];

		n_logTe = ((PyArrayObject*)pv_logTe)->dimensions[0];
		n_logg = ((PyArrayObject*)pv_logg)->dimensions[0];

		n_J = ((PyArrayObject*)pv_J)->dimensions[0];
		n_H = ((PyArrayObject*)pv_H)->dimensions[0];
		n_K = ((PyArrayObject*)pv_K)->dimensions[0];

		n_eZ = ((PyArrayObject*)pv_eZ)->dimensions[0];
		n_elogTe = ((PyArrayObject*)pv_elogTe)->dimensions[0];
		n_elogg = ((PyArrayObject*)pv_elogg)->dimensions[0];

		n_eJ = ((PyArrayObject*)pv_eJ)->dimensions[0];
		n_eH = ((PyArrayObject*)pv_eH)->dimensions[0];
		n_eK = ((PyArrayObject*)pv_eK)->dimensions[0];

		pass = (n_lon==n_lat)*(n_lat==n_logTe)*(n_logTe==n_logg)*(n_logg==n_J);
		pass *= (n_J==n_H)*(n_H==n_K)*(n_K==n_eJ)*(n_eJ==n_eH)*(n_eH==n_eK);
		pass *= (n_eK==n_metZ)*(n_metZ==n_eZ)*(n_eZ==n_elogg)*(n_elogg==n_elogTe);

	
		if(!pass){
			fprintf(stderr, " *** Warning: dimensions in GetAPOGEEdistances mismatch\n");
			fprintf(stderr, "\tZero value will be returned\n");
			return Py_BuildValue("i",0);
		};

		//Create output python vector using nrows
		dims_out[0] = n_metZ;
		pv_distestim  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_FLOAT32 ); //Python vector (of floats)
		pv_infoflag  = (PyArrayObject*)PyArray_SimpleNew(1, dims_out, PyArray_INT32 ); //Python vector (of floats)


		//Let's link two types of pointers
		cv_lon = ARRAYF(pv_lon);
		cv_lat = ARRAYF(pv_lat);
		cv_metZ = ARRAYF(pv_metZ);
		cv_logTe = ARRAYF(pv_logTe);
		cv_logg = ARRAYF(pv_logg);
		cv_J = ARRAYF(pv_J);
		cv_H = ARRAYF(pv_H);
		cv_K = ARRAYF(pv_K);

		cv_eZ = ARRAYF(pv_eZ);
		cv_elogTe = ARRAYF(pv_elogTe);
		cv_elogg = ARRAYF(pv_elogg);
		cv_eJ = ARRAYF(pv_eJ);
		cv_eH = ARRAYF(pv_eH);
		cv_eK = ARRAYF(pv_eK);


		cv_void_distestim = ARRAYV(pv_distestim);
		cv_distestim = (float*)cv_void_distestim;

		cv_void_infoflag = ARRAYV(pv_infoflag);
		cv_infoflag = (int*)cv_void_infoflag;

		// Call to the C function
		GetAPOGEEdistances(cv_lon, cv_lat, cv_metZ, cv_logTe, cv_logg, cv_J, cv_H, cv_K, cv_eZ, cv_elogTe, cv_elogg, cv_eJ, cv_eH, cv_eK, cv_distestim, cv_infoflag, n_metZ);

		//Copy it to a new direction
		memcpy(cv_void_distestim, cv_distestim, PyArray_ITEMSIZE((PyArrayObject*) pv_distestim)*n_metZ);
		memcpy(cv_void_infoflag, cv_infoflag, PyArray_ITEMSIZE((PyArrayObject*) pv_infoflag)*n_metZ);

	//	printf("Free was executed\n");

		return Py_BuildValue("OO", pv_distestim, pv_infoflag);
	}else{
		fprintf(stderr, " *** Warning: GetAPOGEEdistances must have 14 input arguments\n");
		fprintf(stderr, " Zero value will be returned\n");
		return Py_BuildValue("i",0);
	}


}; // End of GetAPOGEEdistances
//--------------------------------------------------------------------------------------------------------------------------------------//




//--------------------------------------------------------------------------------------------------------------------------------------//
static PyMethodDef CAsgaiaToolsMethods[] = {
	{"IMFdistr", CAsgaiaTools_IMFdistr, METH_VARARGS, "P = CAsgaiaTools.IMFdistr( masses, *type ):\n  Returns the probability of having a mass centered in masses[n]. Border effects are taken into account. type = 's' for Salpeter (default), 'k' for Kroupa or 'b' for the bimodal IMF.\n "},
	{"IMF", CAsgaiaTools_IMF, METH_VARARGS, "N = CAsgaiaTools.IMF( masses, *type, *cutoff, *IMFslope ):\n  Returns the number of stars in the bin centered at masses[n] for a 1 M0 population (just multiply by the total mass). Border effects are taken into account.\n type = 's' for Salpeter (default), 'k' for Kroupa or 'b' for Bimodal.\n cutoff: Upper mass limit. \n IMFslope (def. value 2.35): free parameter of the bimodal IMF (see Vazdekis et al. 1996).\n "},
	{"StellarParameters", CAsgaiaTools_StellarParameters, METH_VARARGS, "[masses, logTe, logg, mbol] = CAsgaiaTools.StellarParameters(Zmet, logAge, *logmass_flag):\n  Returns the logTe (log. of effective Temperature), logg (gravity) and the bolometric magnitude (mbol) for one particle with Z and logAge (in years). masses are given in M_sun and magnitudes in mag. When logmass_flag == True, log10(masses) are uniformly distributed."},
	{"StellarAbsMagnitude", CAsgaiaTools_StellarAbsMagnitude, METH_VARARGS, "[masses, M_V] = CAsgaiaTools.StellarAbsMagnitude(Zmet, logAge, *logmass_flag, *band_id):\n  Returns the absolute magnitudes in the band given by band_id for one particle with Z and logAge (in years). masses are given in M_sun and magnitudes in mag. When logmass_flag == True, log10(masses) are uniformly distributed. The default value is False \n. As default, band_id = 2 (V-band)\n"},
	{"ColourExcessMap", CAsgaiaTools_ColourExcessMap, METH_VARARGS, "exS = CAsgaiaTools.ColourExcessMap(lon, lat):\n Returns the Schlegel extinction in the region given by (l,b).\n \tl and b are np.float32 arrays.\n\t (l,b) in radians, exS in mag\n \tIMPORTANT: We make use of the binary file:\n /home/pedroap/pedroap_scratch/Snapdragons/SforCarlos/FromFortranToC/ScEx1.dat\n"},
	{"ColourExcess3DMap", CAsgaiaTools_ColourExcess3DMap, METH_VARARGS, "ex3 = CAsgaiaTools.ColourExcess3DMap(lon, lat, dist):\n Returns the 3D correction to the extinction in the region given by (l,b,d).\n\t l, b and d are np.float32 arrays.\n\t (l,b) in radians, d in kpc, ex3 in mag\n\t IMPORTANT: We make use of the binary file:\n /home/pedroap/pedroap_scratch/Snapdragons/SforCarlos/FromFortranToC/3dEx1.dat\n"},
	{"SharmaCorrection", CAsgaiaTools_SharmaCorrection, METH_VARARGS, "ex = CAsgaiaTools.SharmaCorrection(exS, ex3):\n  Applies the correction to the extinction given by Sharma et al. +14\n  exS and ex3 are outputs of ColourExcessMap and ColourExcess3DMap respectively, but their positions in the input are not important:\n ex = exS*ex3*( 0.8 - 0.2*tanh(10*exS*ex3 - 1.5) )\n  Both exS and ex3 are np.float32 arrays.\n "},
	{"GeometricCorrection", CAsgaiaTools_GeometricCorrection, METH_VARARGS, "correction = CAsgaiaTools.GeometricCorrection(lon, lat, *flag):\n  Applies the correction due to the scanning law. \n If flag==-1 use the interpolated values, otherwise use the mean values.\n This method makes use of the Table gfactor-Jun2013.dat\n  Both lon and lat are np.float32 arrays.\n "},
	{"SigmaPiGaia", CAsgaiaTools_SigmaPiGaia, METH_VARARGS, "[error_pi, nobstars] = CAsgaiaTools.SigmaPiGaia(metZ, logAge, mass, dist, A_V, correction, *IMF_type, *Cfactor, *Interpolation, *G_min=3, *G_max=20, *IMF_slope=2.35, *rspicoff=np.inf):\n  Returns the parallax error using the (metZ, logAge[in yrs]) isochrones, with A_V as the extinction (in mag) and correction as a factor due to the scanning law of Gaia (usually obtained using GeometricCorrection). \n All the inputs but the optional ones are np.float32 arrays. The distances are given in kpc. \n IMF_type = 's' (def.), 'k' or 'b'\n Cfactor = Calibration factor*sqrt(60/month) (float).\n Interpolation = True/False (False is the default value).\n rspicoff: Relative SigmaPI CutOFF (max. parallax error/parallax).\n error_pi is in mas.\n This method uses some PARSEC isochrones stored in the binary file:\n All_the_isochrones.bin\n as well as two .txt files.\n Trick:\n error_pi = CAsgaiaTools.SigmaPiGaia(metZ, logAge, mass, dist, A_V, correction)\n is the same as\n error_pi = CAsgaiaTools.SigmaPiGaia(metZ, logAge, mass, dist, A_V, correction, 's', 1.2, False, 3,20, 2.35)"},	
	{"GetSigmaPiGaia", CAsgaiaTools_GetSigmaPiGaia, METH_VARARGS, "[error_pi, nobstars, A_V] = CAsgaiaTools.GetSigmaPiGaia(lon, lat, dist_kpc, Zmet, logAge, mass, IMF_type, Cfactor, correction_flag, Interpolation, *G_min=3, *G_max=20, *IMF_slope=2.35, *rspicoff=np.inf):\n  Returns sigma(parallax) and A_V. The first six parameters are np.float32 arrays.\n IMF_type = 's' (Salpeter, default), 'k' (Kroupa) or 'b' (bimodal)\n Cfactor = Calibration factor*sqrt(60/month) (float).\n correction_flag = -1/+1 (default value = -1).\n Interpolation = True/False (False is the default value). \n rspicoff: Relative SigmaPI CutOFF (max. parallax error/parallax). When Interpolation = 1, it uses SigmaPiGaia4Points\n A_V is in mags and error_pi in mas.\n\tTrick:\n\t[error_pi, nobstars, A_V] = GetSigmaPiGaia(lon, lat, dist_kpc, Zmet, logAge, mass)\n is the same as\n [error_pi, A_V] = GetSigmaPiGaia(lon, lat, dist_kpc, Zmet, logAge, mass, 's', 1.2,  -1, False, 3, 20, 2.35)"},
	{"GetAV", CAsgaiaTools_GetAV, METH_VARARGS, "A_V = CAsgaiaTools.GetAV(lon, lat, dist_kpc):\n  Calculates the extinction in the V band. It is the same as use:\n exS = CAsgaiaTools.ColourExcessMap(lon, lat);\n ex3 = CAsgaiaTools.ColourExcess3DMap(lon, lat, dist); \n A_V = 3.24*CAsgaiaTools.SharmaCorrection(exS, ex3); \n lon, lat (in radians) and dist_kpc are np.float32 arrays. A_V is given in magnitudes\n "},
	{"ConversionSigmas", CAsgaiaTools_ConversionSigmas, METH_VARARGS, "[s_alphas, s_delta, s_muas, s_mud] = CAsgaiaTools.ConversionSigmas(lon, lat, sigma_pi, *flag):\n  Applies the correction due to the scanning law to the error in alpha* (s_alphas), declination (s_delta), the proper motion in right ascension mu_a* (s_muas) and in declination (s_mud). The error in parallax has to be known (sigma_pi)\n If flag==-1 (def. value) use the interpolated values, otherwise use the mean values.\n This method makes use of the Table gfactor-Jun2013.dat\n  lon, lat (in radians) and sigma_pi are np.float32 arrays.\n Trick:\n\t [s_alphas, s_delta, s_muas, s_mud] = CAsgaiaTools.ConversionSigmas(sigma_pi) assumes flag=1 because neither lon nor lat are required.\n "},
	{"PhotometryGaia", CAsgaiaTools_PhotometryGaia, METH_VARARGS, "error_pi = CAsgaiaTools.PhotometryGaia(metZ, logAge, mass, dist, A_V, *IMF_type, *Interpolation, *G_min=3, *G_max=20, *IMF_slope=2.35):\n  Returns the parallax error using the (metZ, logAge[in yrs]) isochrones, with A_V as the extinction (in mag) and correction as a factor due to the scanning law of Gaia (usually obtained using GeometricCorrection). \n All the inputs but the optional ones are np.float32 arrays. The distances are given in kpc. \n IMF_type = 's' (def.), 'k' or 'b'\n Interpolation = True/False (False is the default value).\n error_pi is in mas.\n This method uses some PARSEC isochrones stored in the binary file:\n All_the_isochrones.bin\n as well as two .txt files.\n Trick:\n error_pi = CAsgaiaTools.SigmaPiGaia(metZ, logAge, mass, dist, A_V, correction)\n is the same as\n error_pi = CAsgaiaTools.PhotometryGaia(metZ, logAge, mass, dist, A_V, 's', False, 3,20, 2.35)"},
	{"GetPhotometryGaia", CAsgaiaTools_GetPhotometryGaia, METH_VARARGS, "output = CAsgaiaTools.GetPhotometryGaia(lon, lat, dist_kpc, Zmet, logAge, mass, IMF_type, Interpolation, *G_min=3, *G_max=20, *IMF_slope=2.35):\n  Returns a list of eight elements: the mean values of G, G_BP, G_R, their errors in the same order, the number of stars and the A_V extincion\n. The first six parameters are np.float32 arrays.\n IMF_type = 's' (Salpeter, default), 'k' (Kroupa) or 'b' (bimodal)\n. When Interpolation = 1, it uses PhotometryGaia4Points\n A_V is in mags and error_pi in mas.\n\tTrick:\n\t[error_pi, A_V] = GetPhotometryGaia(lon, lat, dist_kpc, Zmet, logAge, mass)\n is the same as\n [mean_G, mean_BP, mean_RP, sigma_G, sigma_BP, sigma_RP, nobstars, A_V] = GetPhotometryGaia(lon, lat, dist_kpc, Zmet, logAge, mass)"},
	{"SigmaVrGaia", CAsgaiaTools_SigmaVrGaia, METH_VARARGS, "[error_Vr, nobstars] = CAsgaiaTools.SigmaVrGaia(metZ, logAge, mass, dist, A_V, *IMF_type, *Interpolation, *G_min=3, *G_max=20, *G_RVS_min, *G_RVS_max, *IMF_slope=2.35):\n  Returns the radial velocity error using the (metZ, logAge[in yrs]) isochrones, with A_V as the extinction (in mag). \n All the inputs but the optional ones are np.float32 arrays. The distances are given in kpc. \n IMF_type = 's' (def.), 'k' or 'b'\n Interpolation = True/False (False is the default value).\n error_Vr is in km/s.\n This method uses some PARSEC isochrones stored in the binary file:\n All_the_isochrones3.bin\n as well as two .txt files.\n Trick:\n error_Vr = CAsgaiaTools.SigmaVrGaia(metZ, logAge, mass, dist, A_V)\n is the same as\n error_pi = CAsgaiaTools.SigmaVrGaia(metZ, logAge, mass, dist, A_V, 's', 1.2, False, 3,20, -infty, infty, 2.35)"},
	{"GetSigmaVrGaia", CAsgaiaTools_GetSigmaVrGaia, METH_VARARGS, "[error_pi, nobstars, A_V] = CAsgaiaTools.GetSigmaVrGaia(lon, lat, dist_kpc, Zmet, logAge, mass, IMF_type, Interpolation, *G_min=3, *G_max=20, *GRVS_min, *GRVS_max, *IMF_slope=2.35):\n  Returns sigma(Vr) and A_V. The first six parameters are np.float32 arrays.\n IMF_type = 's' (Salpeter, default), 'k' (Kroupa) or 'b' (bimodal)\n Interpolation = True/False (False is the default value). When Interpolation = 1, it uses SigmaVrGaia4Points\n A_V is in mags and error_Vr in km/s"},
	{"GetSigmaPiDistr", CAsgaiaTools_GetSigmaPiDistr, METH_VARARGS, "[error_pi, error_pi2,nobstars, A_V, sigmapi_hist] = CAsgaiaTools.GetSigmaPiDistr(lon, lat, dist_kpc, Zmet, logAge, mass, IMF_type, Cfactor, correction_flag, *IMF_slope=2.35):\n  Returns the mean value of sigma(parallax) and sigma(parallax)**2, the number of observed stars, A_V and the histogram of sigmapi. The first six parameters are np.float32 arrays.\n IMF_type = 's' (Salpeter, default), 'k' (Kroupa) or 'b' (bimodal)\n Cfactor = Calibration factor*sqrt(60/month) (float).\n correction_flag = -1/+1 (default value = -1).\n A_V is in mags and error_pi in mas.\n"},
	{"GetGaiaIMGhist", CAsgaiaTools_GetGaiaIMGhist, METH_VARARGS, "[error_pi, nobstars, A_V, im_hist, G_hist] = CAsgaiaTools.GetGaiaIMGhist(lon, lat, dist_kpc, Zmet, logAge, mass, IMF_type, Cfactor, correction_flag, *IMF_slope=2.35):\n  Returns sigma(parallax), the number of observed stars, A_V and the histograms of the initial masses and the G magnitude. The first six parameters are np.float32 arrays.\n IMF_type = 's' (Salpeter, default), 'k' (Kroupa) or 'b' (bimodal)\n Cfactor = Calibration factor*sqrt(60/month) (float).\n correction_flag = -1/+1 (default value = -1).\n A_V is in mags and error_pi in mas.\n\tTrick:\n\tGetGaiaIMGhist(lon, lat, dist_kpc, Zmet, logAge, mass)\n is the same as\n GetGaiaIMGhist(lon, lat, dist_kpc, Zmet, logAge, mass, 's', 1.2, False, -1, False)."},
	{"GetMagnitude", CAsgaiaTools_GetMagnitude, METH_VARARGS, "[magnitude_histogram, nobstars, Mfcis] = CAsgaiaTools.GetMagnitude(Zmet, logAge, mass, dist_kpc, extinction, Nbins, mag_min, mag_max,  IMF_type, interpolation, band_id, IMF_slope):\n Returns the distribution of the magnitudes for the stars contained in the particles. The number of observed stars in each particle.\n Mfcis means: fraction of mass to be converted into stars."},
	{"CMD", CAsgaiaTools_CMD, METH_VARARGS, "[magnitude_histogram, nobstars, Mfcis] = CAsgaiaTools.CMD(Zmet, logAge, mass, dist_kpc, A_V, Nbins_x, Nbins_y, mag_min_x, mag_max_x, mag_min_y, mag_max_y, IMF_type, interpolation, band_id_x, band_id_y, *IMF_slope=2.35):\n Returns the Color-Magnitude diagram for the stars contained in the particles, the number of observed stars in each particle and Mfcis.The extinctions must be given for the Johnson V band band.\n Mfcis means: fraction of mass to be converted into stars."},
	{"HR", CAsgaiaTools_HR, METH_VARARGS, "[magnitude_histogram, nobstars, Mfcis] = CAsgaiaTools.HR(Zmet, logAge, mass, dist_kpc, A_V, Nbins_x, Nbins_y, hr_min_x, hr_max_x, hr_min_y, hr_max_y, IMF_type, interpolation, paramX, band_id, *IMF_slope=2.35):\n Returns the Hertzsprung-Russell Diagram for the stars contained in the particles, the number of observed stars in each particle and Mfcis.The extinctions must be given for the Johnson V band band.\n Mfcis means: fraction of mass to be converted into stars."},
	{"GetAPOGEEdistances", CAsgaiaTools_GetAPOGEEdistances, METH_VARARGS, "[distestim, infoflag] = GetAPOGEEdistances(lon, lat, metZ, logTe, logg, J, H, K, eZ, elogTe, elogg, eJ, eH, eK);"},
	{NULL, NULL, 0, NULL}
};


void initCAsgaiaTools(void){
	(void) Py_InitModule("CAsgaiaTools", CAsgaiaToolsMethods);
}

