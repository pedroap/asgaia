import numpy as np
import CAsgaiaTools
import matplotlib.pyplot as plt


# IMFdistr( masses, IMFtype = 's', IMF_slope=2.35)
# StellarParameters( metZ, logAge, logmass_flag = False)
# StellarAbsMagnitude( metZ, logAge, logmass_flag = False, band='V')

# ColourExcessMap(lon, lat, degrees = False)
# ColourExcess3DMap(lon, lat, dist, degrees=False)
# ExtinctionConversion( band, band_ini = 'V')
# SharmaCorrection(exS, ex3, band = 'V')
# GetAV(lon, lat, dkpc)

# GeometricCorrection(lon, lat, interp_flag=-1)
# SigmaPiGaia(metZ, logAge, dist, A_V, correction, Mass = None, IMF_type = 's', Cfactor = 1.2, Interp = False, G_limits = [3,20], IMF_slope = 2.35)
# GetSigmaPiGaia(lon, lat, dkpc, metZ, logAge, Mass = None, IMF_type='s', Cfactor=1.2, correction_flag=-1, Interp=False, G_limits = [3,20], IMF_slope = 2.35)
# ConversionSigmas(lon=None, lat=None, sigma_pi=None, flag=-1)

# PhotometryGaia(metZ, logAge, dist, A_V, Mass = None, IMF_type = 's', Interp = False, G_limits = [3,20], IMF_slope = 2.35)
# GetPhotometryGaia(lon, lat, dkpc, metZ, logAge, Mass = None, IMF_type='s', Interp=False, G_limits = [3,20], IMF_slope = 2.35)

# SigmaVrGaia(metZ, logAge, dist, A_V, Mass = None, IMF_type = 's', Interp = False, G_limits = [3,20], GRVS_limits = None, IMF_slope = 2.35)
# GetSigmaVrGaia(lon, lat, dkpc, metZ, logAge, Mass = None, IMF_type='s', Interp=False, G_limits = [3,20], GRVS_limits = None, IMF_slope = 2.35)

# GetSigmaPiDistr(lon, lat, dkpc, metZ, logAge, mass, IMF_type='s', Cfactor=1.2, correction_flag=-1, normalize = False, histo_limits = [0.,1.], Nbins = 136, IMF_slope = 2.35)
# GetGaiaIMGhist(lon, lat, dkpc, metZ, logAge, mass, IMF_type='s', Cfactor=1.2, correction_flag=-1, Interp=False, normalize = False, G_limits = [3.,20], Nbins_G = 136)
# GetMagnitude_hist(lon=None, lat=None, dkpc=None, metZ=None, logAge=None, mass=None,  Nbins = 10, IMF_type='s', Interp=False, band = 'V', degrees = False, normalize = False, mag_min = 0., mag_max = 10.)
# GetCMD(lon=None, lat=None, dkpc=None, metZ=None, logAge=None, mass=None,  Nbins = [10,10], IMF_type='s', Interp=False, bands = ['B','V'], degrees = False, normalize = False, mag_axis=[0., 10., -1. ,10], #	plotflag = False, IMF_slope = 2.35)
# GetHR(lon=None, lat=None, dkpc=None, metZ=None, logAge=None, mass=None,  Nbins = [25,25], IMF_type='s', Interp=False, band = 'V', degrees = False, normalize = False, hr_axis=[3.4,4.7, -15.,10], plotflag = #	False, logoutput = False, parameterX='logTe', IMF_slope = 2.35)

# ----------------------- IMFdistr --------------------------------------
def IMFdistr( masses, IMFtype = 's', IMF_slope = 2.35):
	'''P = AsgaiaTools.IMFdistr( masses, IMFtype='s' ):
	Returns the probability of having a mass centered in masses[n].
	Border effects are taken into account.
	IMFtype = 's' for Salpeter (default), 'k' for Kroupa, 'b' for bimodal.
	IMF_slope: free parameter of the bimodal IMF (default 2.35). Unused when IMFtype != 'b'.

	CAsgaia function associated:
		CAsgaiaTools.IMFdistr( masses, *type, *IMFslope );'''

	masses = np.float32(1.*masses); # Must be a np.float32 array

	# Check the IMFtype
	IMFtype = IMFtype.lower();
	#IMF_type must be a char:
	IMFtype = IMFtype[0];

	if(IMFtype in ['s','k','b']):
		# Call to CAsgaiaTools
		P = CAsgaiaTools.IMFdistr(masses, IMFtype, IMF_slope);
	else:
		P = 0*masses;

	return P;
# ----------------------------------------------------------------------


# ----------------------- IMF --------------------------------------
def IMF( masses, IMFtype = 's', cutoff = np.inf, IMF_slope = 2.35, PopMass = 1.):
	'''N = AsgaiaTools.IMFdistr( masses, IMFtype='s', cutoff = inf, IMF_slope = 2.35 ):
	Returns the number of stars in the mass bin centered at masses[n] for a Pop. of total mass "PopMass".
	Border effects are taken into account.
	IMFtype = 's' for Salpeter (default), 'k' for Kroupa, 'b' for bimodal.
	cutoff = inf. Upper limit of the mass bins.
	IMF_slope: free parameter of the bimodal IMF (default 2.35)
	PopMass: Total solar masses of the Stellar Population. If omitted, 1 is assumed.

	CAsgaia function associated:
		CAsgaiaTools.IMF( masses, *type , *cutoff, *IMFslope);'''

	masses = np.float32(1.*masses); # Must be a np.float32 array

	# Check the IMFtype
	IMFtype = IMFtype.lower();
	#IMF_type must be a char:
	IMFtype = IMFtype[0];

	if(IMFtype in ['s','k','b']):
		# Call to CAsgaiaTools
		N = CAsgaiaTools.IMF(masses, IMFtype, cutoff, IMF_slope);
	else:
		N = 0*masses;

	N = N*PopMass;
	return N;
# ----------------------------------------------------------------------



# ---------------------- StellarParameters -----------------------------
def StellarParameters( metZ, logAge, logmass_flag = False):
	''' [masses, logTe, logg, mbol] = AsgaiaTools.StellarParameters(metZ, logAge, logmass_flag=False):
	Returns the log(Te), log(g) and the bolometric magnitude for one particle with Z and logAge (in years). 
	masses are given in M_sun.
	When logmass_flag == True, log10(masses) are uniformly distributed.
	The default value is False.


	CAsgaia function associated:
		CAsgaiaTools.StellarParameters(metZ, logAge, *logmass_flag);'''


	# Conversion:
	metZ = np.float32(1.*metZ);
	logAge = np.float32(1.*logAge);

	# Call to CAsgaiaTools
	[masses, logTe, logg, mbol] = CAsgaiaTools.StellarParameters(metZ, logAge, logmass_flag);

	return [masses, logTe, logg, mbol];
			
# ----------------------------------------------------------------------




# ---------------------- StellarAbsMagnitude -----------------------------
def StellarAbsMagnitude( metZ, logAge, logmass_flag = False, band = 'V'):
	''' [masses, Mag] = AsgaiaTools.StellarAbsMagnitude(metZ, logAge, logmass_flag=False, band = 'V'):
	Returns the Mag magnitude for one particle with Z and logAge (in years). 
	masses are given in M_sun and M in mag.
	When logmass_flag == True, log10(masses) are uniformly distributed.
	The default value is False.
	band must be included in [U, B, V, R, I, J, H, K].

	CAsgaia function associated:
		CAsgaiaTools.StellarAbsMagnitude(metZ, logAge, *logmass_flag);'''

	band_list = ['U','B','V','R','I','J','H','K','Ks'];
	# band_list += ['B_T','V_T','u','g','r','i','z','Z','Y','Vista_J','Vista_H','Vista_Ks']; Not implemented yet

	# Conversion:
	metZ = np.float32(1.*metZ);
	logAge = np.float32(1.*logAge);

	band.replace('Ic','I');

	assert(band in band_list),'Unknown band parameter. Please see StellarAbsMagnitude? for the available filters.'
	band_id = band_list.index(band); # Now band_id is a number
	# Call to CAsgaiaTools
	masses, Mag = CAsgaiaTools.StellarAbsMagnitude(metZ, logAge, logmass_flag, band_id);

	return([masses, Mag]);
# ----------------------------------------------------------------------


# ---------------------- ColourExcessMap -----------------------------
def ColourExcessMap(lon, lat, degrees = False):
	''' exS = AsgaiaTools.ColourExcessMap(lon, lat):
	Returns the Schlegel extinction in the region given by (l,b).
		l and b are np.float32 arrays.
		(l,b) in radians, exS in mag.
	IMPORTANT: We make use of the binary file:
		/home/pedroap/pedroap_scratch/Snapdragons/SforCarlos/FromFortranToC/ScEx1.dat

	CAsgaia function associated:
		CAsgaiaTools.ColourExcessMap(lon, lat);'''	

	# Unit Conversion:
	if(degrees):
		lon = np.deg2rad(1.*lon);
		lat = np.deg2rad(1.*lat);

	# Type conversion:
	lon = np.float32(lon);
	lat = np.float32(lat);

	# Call to CAsgaiaTools
	exS = CAsgaiaTools.ColourExcessMap(lon, lat);

	return exS;
# ------------------------------------------------------------------------


# ---------------------- ColourExcess3DMap -------------------------------
def ColourExcess3DMap(lon, lat, dist, degrees=False):
	''' ex3 = AsgaiaTools.ColourExcess3DMap(lon, lat, dist):
	Returns the 3D correction to the extinction in the region given by (l,b,d).
		l, b and d are np.float32 arrays.
		(l,b) in radians
		d in kpc, ex3 in mag
	IMPORTANT: We make use of the binary file:
		/home/pedroap/pedroap_scratch/Snapdragons/SforCarlos/FromFortranToC/3dEx1.dat

	Associated CAsgaiaTools function:
		CAsgaiaTools.ColourExcess3DMap(lon, lat, dist);'''

	# Unit Conversion:
	if(degrees):
		lon = np.deg2rad(1.*lon);
		lat = np.deg2rad(1.*lat);

	# Type conversion:
	lon = np.float32(1.*lon);
	lat = np.float32(1.*lat);
	dist = np.float32(1.*dist);

	# Call to CAsgaiaTools
	ex3 = CAsgaiaTools.ColourExcess3DMap(lon, lat, dist);

	return ex3;
# ------------------------------------------------------------------------


# ---------------------- ExtinctionConversion ----------------------------
def ExtinctionConversion( band, band_ini = 'V'):
	'''Applies the correction to the absorption given for band_ini to get the absorption in the band "band".
	band must be included in [U, B, V, R, I, J, H, K, G]
	Values obtained from Schlegel, Princeton 16 April 1999'''

	# Mulpliple names:
	band = band*(band!='Ic')+'I'*(band=='Ic');
	band_ini = band_ini*(band_ini!='Ic')+'I'*(band_ini=='Ic');

	band_list = ['U','B','G_BP','V','G','R','G_RP','I','G_RVS','J','H','K','Ks'];
	# band_list += ['B_T','V_T','u','g','r','i','z','Z','Y','Vista_J','Vista_H','Vista_Ks']; Not implemented yet

	if (not(band in band_list) or not(band_ini in band_list)):
		num = 0.;
		den = 1.;
	else:

		Conversion_table = {}; # Stores the ratios A(X)/E(B-V)
		# Sharma
		Conversion_table['U'] = 4.968;
		Conversion_table['B'] = 4.325;
		Conversion_table['V'] = 3.24;
		Conversion_table['R'] = 2.634;
		Conversion_table['I'] = 1.962;
		Conversion_table['K'] = 0.367; # UKIRT

		# 2MASS values (from Padova [not UKIRT])
		Conversion_table['J'] = 0.9536616;
		Conversion_table['H'] = 0.5873472;
		Conversion_table['Ks'] = 0.3835512;

		# Gaia (from Padova)
		Conversion_table['G'] = 2.7887652;
		Conversion_table['G_BP'] = 3.4423056;
		Conversion_table['G_RP'] = 2.0730816;
		Conversion_table['G_RVS'] = 2.0730816; #The same as G_RP (overestimated). No value found

		# Tycho (from Padova)
		Conversion_table['B_T'] = 4.3918848;
		Conversion_table['V_T'] = 3.4035228;

		# SDSS ugriz (from Padova)
		Conversion_table['u'] = 5.0837544;
		Conversion_table['g'] = 3.906954;
		Conversion_table['r'] = 2.8227528;
		Conversion_table['i'] = 2.2135356;
		Conversion_table['z'] = 1.5955704;

		# Vista (from Padova):
		Conversion_table['Z'] = 1.6495488;
		Conversion_table['Y'] = 1.2675528;
		Conversion_table['Vista_J'] = 0.9315324;
		Conversion_table['Vista_H'] = 0.5895828;
		Conversion_table['Vista_Ks'] = 0.3898368;

		num = Conversion_table[band];
		den = Conversion_table[band_ini];

	return num/den;
# -----------------------------------------------------------------------


# ---------------------- SharmaCorrection -------------------------------
def SharmaCorrection(exS, ex3, band = 'V'):
	'''ex = AsgaiaTools.SharmaCorrection(exS, ex3, band='V');

	Applies the correction to the extinction given by Sharma et al. +14 
	-------------------------------------------------------------------------------------------
	exS and ex3 are outputs of ColourExcessMap and ColourExcess3DMap respectively, but their positions in the input are not important:
	 ex = exS*ex3*( 0.8 - 0.2*tanh(10*exS*ex3 - 1.5) )
	Both exS and ex3 are np.float32 arrays.

	-------------------------------------------------------------------------------------------
	Associated CAsgaiaTools function (only for the V band):
		CAsgaiaTools.SharmaCorrection(exS, ex3);'''

	# Type conversion:
	exS = np.float32(1.*exS);
	ex3 = np.float32(1.*ex3);

	# Call to CAsgaiaTools
	ex3 = CAsgaiaTools.SharmaCorrection(exS, ex3);

	# Band conversion:
	ex3 = ex3*ExtinctionConversion(band);

	return ex3;
# -----------------------------------------------------------------------


# --------------------------- GetAV ---------------------------
def GetAV(lon, lat, dkpc):
	'''A_V = AsgaiaTools.GetAV(lon, lat, dkpc)
	Calculates the extinction in the V band. It is the same as use:
		exS = CAsgaiaTools.ColourExcessMap(lon, lat);
		ex3 = CAsgaiaTools.ColourExcess3DMap(lon, lat, dist);
		A_V = 3.24*CAsgaiaTools.SharmaCorrection(exS, ex3);
	-------------------------------------------------------------------------------------------
	lon, lat (in radians) and dkpc are np.float32 arrays.
	A_V is given in magnitudes
	-------------------------------------------------------------------------------------------
	Associated CAsgaiaTools function:
		A_V = CAsgaiaTools.GetAV(lon, lat, dkpc)

	For other bands, see ExtinctionConversion.'''

	# Type conversion:
	lon = np.float32(1.*lon);
	lat = np.float32(1.*lat);
	dkpc = np.float32(1.*dkpc);

	# Call to CAsgaiaTools
	A_V = CAsgaiaTools.GetAV(lon, lat, dkpc);
	
	return(A_V);
# -------------------------------------------------------------




# ---------------------- GeometricCorrection ----------------------------
def GeometricCorrection(lon, lat, interp_flag=True):
	'''correction = AsgaiaTools.GeometricCorrection(lon, lat, interp_flag==True):
	Applies the correction due to the scanning law.
	If flag==True use the interpolated values, otherwise use the mean values.
	This method makes use of the Table gfactor-Jun2013.dat
	Both lon and lat are np.float32 arrays.
	-------------------------------------------------------------------------------------------
	Associated CAsgaiaTools function:
		CAsgaiaTools.GeometricCorrection(lon, lat, *interp_flag);'''


	# Type conversion:
	lon = np.float32(1.*lon);	
	lat = np.float32(1.*lat);

	if(type(interp_flag)==type(True)):
		interp_flag = -1*interp_flag + 1*(not(interp_flag));

	
	# Call to CAsgaiaTools
	correction = CAsgaiaTools.GeometricCorrection(lon, lat, interp_flag);
	return correction
# -----------------------------------------------------------------------




# --------------------------- SigmaPiGaia -------------------------------
def SigmaPiGaia(metZ, logAge, dist, A_V, correction, Mass = None, IMF_type = 's', Cfactor = 1.2, Interp = False, G_limits = [3,20], IMF_slope = 2.35, rspicoff = np.inf):
	'''error_pi = AsgaiaTools.SigmaPiGaia(metZ, logAge, dist, A_V, correction, Mass = None, IMF_type='s', Cfactor=1.2, Interp = False, G_limits = [3,20], IMF_slope = 2.35);
	-------------------------------------------------------------------------------------------
	Returns the parallax error using the (metZ, logAge[in yrs]) isochrones, with A_V as the extinction (in mag) and correction as a factor due to the scanning law of Gaia (usually obtained using GeometricCorrection). When Mass is provided, return [error_pi, nobstars] (nobstars = Number of OBSERVED stars for each particle)
	-------------------------------------------------------------------------------------------
	All the inputs but the optional ones are np.float32 arrays. The distances are given in kpc.
	IMF_type = 's' (Salpeter, default), 'k' (Kroupa) or 'b' (Bimodal).
	Cfactor = Calibration factor*sqrt(60/month) (float). Default value: 1.2
	Interpolation = True/False (False is the default value).
	G_limits = Two element list, tuple or array of floats
	IMF_slope = Free parameter of the bimodal IMF. Default value: 2.35
	rspicoff = Relative SigmaPI CutOFF = max. of sigma_parallax/parallax. Infinity by default.
	error_pi is in mas.
	-------------------------------------------------------------------------------------------
	This method uses some PARSEC isochrones stored in the binary file:
		All_the_isochrones.bin
	as well as two .txt files.
	-------------------------------------------------------------------------------------------
	Associated CAsgaiaTools functions:
		CAsgaiaTools.SigmaPiGaia(metZ, logAge, Mass, dist, A_V, correction, *IMF_type, *Cfactor, *Interpolation, *G_min, *G_max, *IMF_slope, *rspicoff);'''

	#IMF_type must be a char:
	IMF_type = IMF_type[0];

	# Type conversion:
	metZ = np.float32(1.*metZ);	
	logAge = np.float32(1.*logAge);
	dist = np.float32(1.*dist);
	A_V = np.float32(1.*A_V);
	correction = np.float32(1.*correction);

	Cfactor = np.float(Cfactor);

	G_limits = np.float32(G_limits);

	IMF_slope = np.float32(IMF_slope);
	rspicoff = np.float32(rspicoff);

	# Call to CAsgaiaTools
	if(type(Mass)==type(None)):
		Mass = np.ones(len(lon), dtype = np.float32);
		[error_pi, nobstars] = CAsgaiaTools.SigmaPiGaia(metZ, logAge, Mass, dist, A_V, correction, IMF_type, Cfactor, Interp, G_limits[0], G_limits[1], IMF_slope, rspicoff);
		return error_pi;
	else:
		Mass = np.float32(Mass);
		[error_pi, nobstars] = CAsgaiaTools.SigmaPiGaia(metZ, logAge, Mass, dist, A_V, correction, IMF_type, Cfactor, Interp, G_limits[0], G_limits[1], IMF_slope, rspicoff);
		return [error_pi, nobstars];

# -----------------------------------------------------------------------



# --------------------------- GetSigmaPiGaia ----------------------------
def GetSigmaPiGaia(lon, lat, dkpc, metZ, logAge, Mass = None, IMF_type='s', Cfactor=1.2, correction_flag=True, Interp=False, G_limits = [3,20], IMF_slope = 2.35, rspicoff = np.inf):
	''' [error_pi, nobstars, A_V] = AsgaiaTools.GetSigmaPiGaia(lon, lat, dkpc, metZ, logAge,  Mass = None, IMF_type='s', Cfactor=1.2, correction_flag=-1, Interp=False, G_limits = [3,20], IMF_slope = 2.35):
	Returns sigma(parallax) and A_V. The first five parameters are np.float32 arrays.
	-------------------------------------------------------------------------------------------
	When Mass is provided, return [error_pi, nobstars, A_V] (nobstars = Number of observed stars for each particle)
	-------------------------------------------------------------------------------------------
	IMF_type = 's' (Salpeter, defaulf), 'k' (Kroupa), 'b' (Bimodal)
	Cfactor = Calibration factor*sqrt(60/month) (float).
	correction_flag = True/False (default value = True). If True (or -1), interpolates in the Geometric Correction
	Interpolation = True/False (False is the default value). When Interpolation = True, it uses SigmaPiGaia4Points
	G_limits = Two element list, tuple or array of floats
	IMF_slope: free parameter of the bimodal IMF (default 2.35)
	rspicoff = Relative SigmaPI CutOFF = max. of sigma_parallax/parallax. Infinity by default.

	A_V is in mags and error_pi in mas.
	-------------------------------------------------------------------------------------------
	Associated CAsgaiaTools functions:
		CAsgaiaTools.GetSigmaPiGaia(lon, lat, dkpc, metZ, logAge, Mass, IMF_type, Cfactor, correction_flag, Interpolation, *G_min, *G_max, *IMF_slope)'''

	#IMF_type must be a char:
	IMF_type = IMF_type[0];

	# Type conversion:
	lon = np.float32(1.*lon);
	lat = np.float32(1.*lat);
	dkpc = np.float32(1.*dkpc);
	metZ = np.float32(1.*metZ);
	logAge = np.float32(1.*logAge);

	if(type(correction_flag)==type(True)):
		correction_flag = -1*correction_flag + 1*(not(correction_flag));

	Cfactor = np.float(Cfactor);
	G_limits = np.float32(G_limits);

	IMF_slope = np.float32(IMF_slope);
	rspicoff = np.float32(rspicoff);

	# Call to CAsgaiaTools
	if(type(Mass)==type(None)):
		Mass = np.ones(len(lon),dtype = np.float32);
		[error_pi, nobstars, A_V] = CAsgaiaTools.GetSigmaPiGaia(lon, lat, dkpc, metZ, logAge, Mass, IMF_type, Cfactor, correction_flag, Interp, G_limits[0], G_limits[1], IMF_slope, rspicoff);
		return([error_pi, A_V]);
	else:
		Mass = np.float32(Mass);
		[error_pi, nobstars, A_V] = CAsgaiaTools.GetSigmaPiGaia(lon, lat, dkpc, metZ, logAge, Mass, IMF_type, Cfactor, correction_flag, Interp, G_limits[0], G_limits[1], IMF_slope, rspicoff);
		return([error_pi, nobstars, A_V]);		

# -------------------------------------------------------------



# --------------------------- ConversionSigmas ----------------
def ConversionSigmas(lon=None, lat=None, sigma_pi=None, flag=-1):
	'''[s_alphas, s_delta, s_muas, s_mud] = AsgaiaTools.ConversionSigmas(lon, lat, sigma_pi, flag=-1)
	-------------------------------------------------------------------------------------------
	Applies the correction due to the scanning law to the error in alpha* (s_alphas), declination (s_delta),
	 the proper motion in right ascension mu_a* (s_muas) and in declination (s_mud).
	The error in parallax has to be known (sigma_pi)
	-------------------------------------------------------------------------------------------
	If flag==-1 (def. value) use the interpolated values, otherwise use the mean values.
	This method makes use of the Table gfactor-Jun2013.dat
	lon, lat (in radians) and sigma_pi are np.float32 arrays.
	Trick:
		[s_alphas, s_delta, s_muas, s_mud] = CAsgaiaTools.ConversionSigmas(sigma_pi) assumes flag=1 because neither lon nor lat are required.

	-------------------------------------------------------------------------------------------
	Associated CAsgaiaTools function:
		[s_alphas, s_delta, s_muas, s_mud] = CAsgaiaTools.ConversionSigmas(lon, lat, sigma_pi, *flag);
	'''

	short_version = (type(lon)==type(None))*(type(lon)==type(lat))*(type(sigma_pi)!=type(None));
	short_version = short_version or (type(lon)!=type(None))*(type(lat)==type(sigma_pi))*(type(sigma_pi)==type(None))

	if(short_version):
		# Only sigma_pi was given as input, but stored in lon:
		sigma_pi = np.float32(1.*lon);

		# Call to CAsgaiaTools
		[s_alphas, s_delta, s_muas, s_mud] = CAsgaiaTools.AllSigmas(sigma_pi);
		success = True;
	else:
		# Only sigma_pi was given as input, but stored in lon:
		lon = np.float32(1.*lon);
		lat = np.float32(1.*lat);
		sigma_pi = np.float32(1.*sigma_pi);

		# Call to CAsgaiaTools
		[s_alphas, s_delta, s_muas, s_mud] = CAsgaiaTools.ConversionSigmas(lon, lat, sigma_pi, flag);
		success = True;

	assert success,'Wrong input mode. See AllSigmas? to know haw to call this function properly.'




	return([s_alphas, s_delta, s_muas, s_mud]);
# -------------------------------------------------------------


# --------------------------- TimeConversionSigmas ----------------
def TimeConversionSigma( sigma, month = 60, Sigma_type = 'parallax' ):
	''' sigma_t = AsgaiaTools.TimeConversionSigma(sigma, month=60, Sigma_type = parallax);
	-------------------------------------------------------------------------------------------
	Accounts for the partial time corrections to the end-of-file astrometric errors.
	month: should be 22<=month<=60, but any non-zero value will give no error.
	-------------------------------------------------------------------------------------------
	Sigma_type:  'parallax', 'position' or 'pm' (for proper motions)
	      for 'parallax' and 'position', the correction scales as month^-0.5, for 'pm' as t^-1.5.
	       See GAIA-C9-TN-UB-RMC-001-1 for more info.
	'''
	
	# Obtain the type of the error
	Sigma_type = Sigma_type.lower();
	
	assert(Sigma_type in ['position','parallax','pm']), 'TimeConversionSigma Error: Unknown Sigma_type'

	sigma = np.float32(np.sqrt(60./month)*sigma); # This is common for all the cases
	
	if(Sigma_type == 'pm'):
		sigma = sigma*60./month; # See GAIA-C9-TN-UB-RMC-001-1 for more info

	return sigma;
# --------------------------------------------------------------------------




# --------------------------- PhotometryGaia -------------------------------
def PhotometryGaia(metZ, logAge, dist, A_V, Mass = None, IMF_type = 's',Interp = False, G_limits = [3,20], IMF_slope = 2.35):
	'''[mean_G, mean_GBP, mean_GRP, error_G, error_GBP, error_GRP, nobstars] = AsgaiaTools.PhotometryGaia(metZ, logAge, dist, A_V, Mass = None, IMF_type='s', Interp = False, G_limits = [3,20], IMF_slope = 2.35);
	-------------------------------------------------------------------------------------------
	Returns the photometric mean values and errors using the (metZ, logAge[in yrs]) isochrones, with A_V as the extinction (in mag) and correction as a factor due to the scanning law of Gaia (usually obtained using GeometricCorrection). When Mass is provided, also returns nobstars (nobstars = Number of OBSERVED stars for each particle)
	-------------------------------------------------------------------------------------------
	All the inputs but the optional ones are np.float32 arrays. The distances are given in kpc.
	IMF_type = 's' (def.) or 'k'.
	Interpolation = True/False (False is the default value).
	G_limits = Two element list, tuple or array of floats
	errors and magnitudes are in mag.
	-------------------------------------------------------------------------------------------
	This method uses some PARSEC isochrones stored in the binary file:
		All_the_isochrones.bin
	as well as two .txt files.
	-------------------------------------------------------------------------------------------
	Associated CAsgaiaTools functions:
		CAsgaiaTools.PhotometryGaia(metZ, logAge, Mass, dist, A_V, *IMF_type, *Interpolation, *G_min, *G_max, *IMF_slope);'''

	#IMF_type must be a char:
	IMF_type = IMF_type[0];

	# Type conversion:
	metZ = np.float32(1.*metZ);	
	logAge = np.float32(1.*logAge);
	dist = np.float32(1.*dist);
	A_V = np.float32(1.*A_V);

	G_limits = np.float32(G_limits);

	# Call to CAsgaiaTools
	if(type(Mass)==type(None)):
		Mass = np.ones(len(lon), dtype = np.float32);
		output = CAsgaiaTools.PhotometryGaia(metZ, logAge, Mass, dist, A_V, IMF_type, Interp, G_limits[0], G_limits[1], IMF_slope);
		return output[0:7];
	else:
		Mass = np.float32(Mass);
		output = CAsgaiaTools.PhotometryGaia(metZ, logAge, Mass, dist, A_V, IMF_type, Interp, G_limits[0], G_limits[1], IMF_slope);
		return output;

# -----------------------------------------------------------------------



# --------------------------- GetPhotometryGaia ----------------------------
def GetPhotometryGaia(lon, lat, dkpc, metZ, logAge, Mass = None, IMF_type='s', Interp=False, G_limits = [3,20], IMF_slope = 2.35):
	''' output = AsgaiaTools.GetPhotometryGaia(lon, lat, dkpc, metZ, logAge,  Mass = None, IMF_type='s', Cfactor=1.2, correction_flag=-1, Interp=False, G_limits = [3,20], IMF_slope = 2.35):
	Returns photometric mean values and errors in mag, as well as nobstars and A_V. The first five parameters are np.float32 arrays.
	-------------------------------------------------------------------------------------------
	output[0] = mean magnitude in the G band
	output[1] = mean magnitude in the G_BP band
	output[2] = mean magnitude in the G_RP band

	output[3] = mean error in the G band
	output[4] = mean error in the G_BP band
	output[5] = mean error in the G_RP band

	output[6] = nobstars or A_V (see the note below)
	output[7] = A_V extinction (only if nobstars was also returned)

	When Mass is provided, it also return nobstars = Number of observed stars for each particle.
	-------------------------------------------------------------------------------------------
	IMF_type = 's' (Salpeter, defaulf), 'k' (Kroupa), 'b' (Bimodal)
	Interpolation = True/False (False is the default value). When Interpolation = True, it uses PhotometryGaia4Points
	G_limits = Two element list, tuple or array of floats
	IMF_slope: free parameter of the bimodal IMF (default 2.35)

	-------------------------------------------------------------------------------------------
	Associated CAsgaiaTools functions:
		CAsgaiaTools.GetPhotometryGaia(lon, lat, dkpc, metZ, logAge, Mass, IMF_type, Cfactor, correction_flag, Interpolation, *G_min, *G_max, *IMF_slope)'''

	#IMF_type must be a char:
	IMF_type = IMF_type[0];

	# Type conversion:
	lon = np.float32(1.*lon);
	lat = np.float32(1.*lat);
	dkpc = np.float32(1.*dkpc);
	metZ = np.float32(1.*metZ);
	logAge = np.float32(1.*logAge);


	G_limits = np.float32(G_limits);

	# Call to CAsgaiaTools
	if(type(Mass)==type(None)):
		Mass = np.ones(len(lon),dtype = np.float32);
		output = CAsgaiaTools.GetPhotometryGaia(lon, lat, dkpc, metZ, logAge, Mass, IMF_type, Interp, G_limits[0], G_limits[1], IMF_slope);
		output = list(output);
		output.pop(6);
		return(output);
	else:
		Mass = np.float32(Mass);
		output = CAsgaiaTools.GetPhotometryGaia(lon, lat, dkpc, metZ, logAge, Mass, IMF_type, Interp, G_limits[0], G_limits[1], IMF_slope);
		return(output);		

# -------------------------------------------------------------




# --------------------------- SigmaVrGaia -------------------------------
def SigmaVrGaia(metZ, logAge, dist, A_V, Mass = None, IMF_type = 's', Interp = False, G_limits = [3.,20.], GRVS_limits=[-np.inf,16.], IMF_slope = 2.35):
	'''error_Vr = AsgaiaTools.SigmaVrGaia(metZ, logAge, dist, A_V, Mass = None, IMF_type='s', Interp = False, G_limits = [3,20], GRVS_limits=None, IMF_slope = 2.35);
	-------------------------------------------------------------------------------------------
	Returns the line-of-sight velocity error using the (metZ, logAge[in yrs]) isochrones, with A_V as the extinction (in mag). 
	When Mass is provided, return [error_Vr, nobstars] (nobstars = Number of OBSERVED stars for each particle)
	-------------------------------------------------------------------------------------------
	All the inputs but the optional ones are np.float32 arrays. The distances are given in kpc.
	IMF_type = 's' (def.) or 'k'.
	Interpolation = True/False (False is the default value).
	G_limits = Two element list, tuple or array of floats. Default values: [-inf,16.]
	GRVS_limits = Two element list, tuple or array of floats

	error_Vr is in km/s.
	-------------------------------------------------------------------------------------------
	This method uses some PARSEC isochrones stored in the binary file:
		All_the_isochrones.bin
	as well as two .txt files.
	-------------------------------------------------------------------------------------------
	Associated CAsgaiaTools functions:
		CAsgaiaTools.SigmaVrGaia(metZ, logAge, Mass, dist, A_V, *IMF_type, *Interpolation, *G_min, *G_max, *GRVS_min, *GRVS_max);'''

	#IMF_type must be a char:
	IMF_type = IMF_type[0];

	# Type conversion:
	metZ = np.float32(1.*metZ);	
	logAge = np.float32(1.*logAge);
	dist = np.float32(1.*dist);
	A_V = np.float32(1.*A_V);

	G_limits = np.float32(G_limits);
	GRVS_limits = np.float32(GRVS_limits);


	# Call to CAsgaiaTools
	if(type(Mass)==type(None)):
		Mass = np.ones(len(metZ), dtype = np.float32);
		[error_Vr, nobstars] = CAsgaiaTools.SigmaVrGaia(metZ, logAge, Mass, dist, A_V, IMF_type, Interp, G_limits[0], G_limits[1], GRVS_limits[0], GRVS_limits[1], IMF_slope);
		return error_Vr;
	else:
		Mass = np.float32(Mass);
		[error_Vr, nobstars] = CAsgaiaTools.SigmaVrGaia(metZ, logAge, Mass, dist, A_V, IMF_type, Interp, G_limits[0], G_limits[1], GRVS_limits[0], GRVS_limits[1], IMF_slope);
		return [error_Vr, nobstars];

# -----------------------------------------------------------------------



# --------------------------- GetSigmaVrGaia ----------------------------
def GetSigmaVrGaia(lon, lat, dkpc, metZ, logAge, Mass = None, IMF_type='s', Interp=False, G_limits = [3.,20.], GRVS_limits=[-np.inf,16.], IMF_slope = 2.35):
	''' [error_Vr, nobstars, A_V] = AsgaiaTools.GetSigmaVrGaia(lon, lat, dkpc, metZ, logAge,  Mass = None, IMF_type='s', Interp=False, G_limits = [3,20], GRVS_limits=None, IMF_slope = 2.35):
	Returns sigma(Vr) and A_V. The first five parameters are np.float32 arrays.
	-------------------------------------------------------------------------------------------
	When Mass is provided, return [error_Vr, nobstars, A_V] (nobstars = Number of observed stars for each particle)
	-------------------------------------------------------------------------------------------
	IMF_type = 's' (Salpeter, defaulf), 'k' (Kroupa), 'b' (Bimodal)
	Interpolation = True/False (False is the default value). When Interpolation = True, it uses SigmaVrGaia4Points
	G_limits = Two element list, tuple or array of floats
	GRVS_limits = Two element list, tuple or array of floats
	IMF_slope: free parameter of the bimodal IMF (default 2.35)

	error_Vr in km/s.
	A_V is in mags
	-------------------------------------------------------------------------------------------
	Associated CAsgaiaTools functions:
		CAsgaiaTools.GetSigmaVrGaia(lon, lat, dkpc, metZ, logAge, Mass, IMF_type, Interpolation, *G_min, *G_max, *GRVS_min, *GRVS_max, *IMF_slope)'''

	#IMF_type must be a char:
	IMF_type = IMF_type[0];

	# Type conversion:
	lon = np.float32(1.*lon);
	lat = np.float32(1.*lat);
	dkpc = np.float32(1.*dkpc);
	metZ = np.float32(1.*metZ);
	logAge = np.float32(1.*logAge);

	G_limits = np.float32(G_limits);
	GRVS_limits = np.float32(GRVS_limits);


	# Call to CAsgaiaTools
	if(type(Mass)==type(None)):
		Mass = np.ones(len(metZ),dtype = np.float32);
		[error_Vr, nobstars, A_V] = CAsgaiaTools.GetSigmaVrGaia(lon, lat, dkpc, metZ, logAge, Mass, IMF_type, Interp, G_limits[0], G_limits[1], GRVS_limits[0], GRVS_limits[1], IMF_slope);
		return([error_Vr, A_V]);
	else:
		Mass = np.float32(Mass);
		[error_Vr, nobstars, A_V] = CAsgaiaTools.GetSigmaVrGaia(lon, lat, dkpc, metZ, logAge, Mass, IMF_type,  Interp, G_limits[0], G_limits[1], GRVS_limits[0], GRVS_limits[1], IMF_slope);
		return([error_Vr, nobstars, A_V]);		

# -------------------------------------------------------------




# --------------------- GetSigmaPiDistr ------------------------
def GetSigmaPiDistr(lon, lat, dkpc, metZ, logAge, mass, IMF_type='s', Cfactor=1.2, correction_flag=-1, normalize = False, histo_limits = [0.,1.], Nbins = 136, IMF_slope = 2.35):
	'''[sigma_pi, sigma_pi2, nobstars, A_V, sigmapi_hist] = AsgaiaTools.GetSigmaPiDistr(lon, lat, dkpc, metZ, logAge, mass, IMF_type='s', Cfactor=1.2, correction_flag=-1, histo_limits = [0.,1.], Nbins = 136, IMF_slope = 2.35)
	-------------------------------------------------------------------------------------------
	Returns sigma(parallax) and A_V. 
	The first five parameters are np.float32 arrays.
	-------------------------------------------------------------------------------------------
	IMF_type = 's' (Salpeter, default), 'k' for (Kroupa) or 'b' for bimodal
	Cfactor = Calibration factor*sqrt(60/month) (float).
	correction_flag = -1/+1 (default value = -1). This is for the geometric correction.

	For the moment, no isochrone interpolation implemented
	A_V is in mags and error_pi in mas.
	histo_limits: list, tuple or array which at least two terms.
		NOTE: ALWAYS 3<=G(mag)<20
		Out of range values will be excluded.
	IMF_slope: free parameter of the bimodal IMF (default 2.35)
	-------------------------------------------------------------------------------------------
	Associated CAsgaiaTools function:
		[sigma_pi, sigma_pi2, nobstars, A_V, sigmapi_hist] = CAsgaiaTools.GetSigmaPiDistr(lon, lat, dkpc, metZ, logAge, mass, 's', 1.2, -1);'''

	# Type conversion:
	lon = np.float32(1.*lon);
	lat = np.float32(1.*lat);
	dkpc = np.float32(1.*dkpc);
	metZ = np.float32(1.*metZ);
	logAge = np.float32(1.*logAge);
	mass = np.float32(1.*mass);

	Histomin, Histomax = histo_limits;

	#IMF_type must be a char:
	IMF_type = IMF_type[0];

	# Call to CAsgaiaTools
	[sigma_pi, sigma_pi2, nobstars, A_V, sigmapi_hist] = CAsgaiaTools.GetSigmaPiDistr(lon, lat, dkpc, metZ, logAge, mass, IMF_type, Cfactor, correction_flag, Nbins, Histomin, Histomax, IMF_slope);

	# Normalize (or not)
	if(normalize):
		Norm = np.sum(sigmapi_hist);
		Norm = Norm + 1.*(Norm==0.0);
		sigmapi_hist = np.float32(sigmapi_hist/Norm);

	return([sigma_pi, sigma_pi2, nobstars, A_V, sigmapi_hist]);
# ------------------------------------------------------------------




# --------------------- GetGaiaIMGhist ------------------------
def GetGaiaIMGhist(lon, lat, dkpc, metZ, logAge, mass, IMF_type='s', Cfactor=1.2, correction_flag=-1, normalize = False, G_limits = [3.,20], Nbins_G = 136, IMF_slope = 2.35):
	'''[error_pi, nobstars, A_V, im_hist, G_hist] = AsgaiaTools.GetGaiaIMGhist(lon, lat, dkpc, metZ, logAge, mass, IMF_type='s', Cfactor=1.2, correction_flag=-1, G_limits = [3.,20], Nbins_G = 136, IMF_slope = 2.35)
	-------------------------------------------------------------------------------------------
	Returns sigma(parallax) and A_V. 
	The first five parameters are np.float32 arrays.
	-------------------------------------------------------------------------------------------
	IMF_type = 's' (Salpeter, default), 'k' for (Kroupa) or 'b' for bimodal
	Cfactor = Calibration factor*sqrt(60/month) (float).
	correction_flag = -1/+1 (default value = -1).

	For the moment, no isochrone interpolation implemented
	A_V is in mags and error_pi in mas.
	G_limits: list, tuple or array which at least two terms.
		NOTE: ALWAYS 3<=G(mag)<20
		Out of range values will be excluded.
	IMF_slope: free parameter of the bimodal IMF (default 2.35)
	-------------------------------------------------------------------------------------------
	Associated CAsgaiaTools function:
		[error_pi, nobstars, A_V, im_hist, G_hist] = CAsgaiaTools.GetGaiaIMGhist(lon, lat, dkpc, metZ, logAge, mass, 's', 1.2, -1);'''

	# Type conversion:
	lon = np.float32(1.*lon);
	lat = np.float32(1.*lat);
	dkpc = np.float32(1.*dkpc);
	metZ = np.float32(1.*metZ);
	logAge = np.float32(1.*logAge);
	mass = np.float32(1.*mass);

	Gmin = np.float32(np.max([G_limits[0],3.]));
	Gmax = np.float32(np.min([20.,G_limits[1]]));

	#IMF_type must be a char:
	IMF_type = IMF_type[0];

	# Call to CAsgaiaTools
	[error_pi, nobstars, A_V, im_hist, G_hist] = CAsgaiaTools.GetGaiaIMGhist(lon, lat, dkpc, metZ, logAge, mass, IMF_type, Cfactor, correction_flag, Nbins_G, Gmin, Gmax, IMF_slope);

	# Normalize (or not)
	if(normalize):
		Norm = np.sum(im_hist);
		Norm = Norm + 1.*(Norm==0.0);
		im_hist = np.float32(im_hist/Norm);

		Norm = np.sum(G_hist);
		Norm = Norm + 1.*(Norm==0.0);
		G_hist = np.float32(G_hist/Norm);

	return([error_pi, nobstars, A_V, im_hist, G_hist]);
# -------------------------------------------------------------



# --------------------- GetMagnitude_hist ---------------------
def GetMagnitude_hist(lon=None, lat=None, dkpc=None, metZ=None, logAge=None, mass=None,  Nbins = 10, IMF_type='s', band = 'V', degrees = False, normalize = False, mag_min = 0., mag_max = 10., IMF_slope = 2.35):
	'''Returns the histogram of the magnitudes in the "band" band.
	Mfcis is the Mass fraction converted into stars.

	[magnitude_histogram, nobstars, Mfcis] = GetMagnitude_hist(lon, lat, dkpc, metZ, logAge, mass, Nbins = 10, IMF_type='s',  band = 'V', mag_min = 0., mag_max = 1.0, IMF_slope = 2.35 )
	-------------------------------------------------------------------------------------------

	*** Additional inputs ***
	mag_min and mag_max are the limits of the histogram
	band('V'): Filter to be used. See information below.
	IMF_type ('s'): can be 's' (Salpeter), 'k' (Kroupa), 'b' (Bimodal).
	degrees (False): Units of lon and lat
	normalize (False): magnitude_histogram gets normalized so that the sum becomes 1.
	IMF_slope: free parameter of the bimodal IMF (default 2.35)

	For the moment, no interpolation is implemented.
	-------------------------------------------------------------------------------------------
	Values for band:
	"U","V","B","R","I","K","J","H","Ks"
	'''

	# Mulpliple names:
	band = band*(band!='Ic')+'I'*(band=='Ic');

	band_list = ['U','B','G_BP','V','G','R','G_RP','I','G_RVS','J','H','K','Ks'];
	# band_list += ['B_T','V_T','u','g','r','i','z','Z','Y','Vista_J','Vista_H','Vista_Ks']; Not implemented yet

	assert(band in band_list),'Unknown band parameter. Please see GetMagnitude_hist? for the available filters.'

	band_id = band_list.index(band); # Now band_id is a number

	short_version = (type(metZ)==type(None))*(type(logAge)==type(None))*(type(mass)==type(None)); # Absolute mag. No extinction and no distances

	if(short_version):
		# (lon, lat, dkpc) contain (metZ, logAge, mass) respectively
		metZ = np.float32(1.*lon);
		logAge = np.float32(1.*lat);
		mass = np.float32(1.*dkpc);

		dkpc = 0.01 + 0.*mass; # 10 pc for the absolute magnitude
		exmag = 0.*dkpc; # No extinction

		# IMF_type must be a char
		IMF_type = IMF_type[0].lower();

		# Call to CAsgaiaTools
		[magnitude_histogram, nobstars, Mfcis] = CAsgaiaTools.GetMagnitude( metZ, logAge, mass, dkpc, exmag, Nbins, mag_min, mag_max, IMF_type, band_id, IMF_slope);
		success = True;
		
	else:
		if(degrees):
			lon = np.deg2rad(lon);
			lat = np.deg2rad(lat);

		# Type conversion:
		lon = np.float32(1.*lon);
		lat = np.float32(1.*lat);
		dkpc = np.float32(1.*dkpc);
		metZ = np.float32(1.*metZ);
		logAge = np.float32(1.*logAge);
		mass = np.float32(1.*mass);

		# Get the extinction in the V band:
		exmag = CAsgaiaTools.GetAV(lon, lat, dkpc);

		# Now change the band:
		factor = ExtinctionConversion( band );
		if(band in ['G_BP','G','G_RP','G_RVS']):
			factor = ExtinctionConversion( 'V' ); # They use an different algoritm
		exmag = factor*exmag;

		# Call to CAsgaiaTools
		[magnitude_histogram, nobstars, Mfcis] = CAsgaiaTools.GetMagnitude( metZ, logAge, mass, dkpc, exmag, Nbins, mag_min, mag_max, IMF_type, band_id, IMF_slope);
		success = True;

	assert success, 'Input error. Try one of the suggested input choices'

	del(success, short_version);

	if(normalize):
		Norm = np.sum(magnitude_histogram);
		Norm = Norm + 1.*(Norm==0.0);
		magnitude_histogram = np.float32(magnitude_histogram/Norm);

	return([magnitude_histogram, nobstars, Mfcis]);
# -------------------------------------------------------------




# -------------------------- GetCMD ---------------------------
def GetCMD(lon=None, lat=None, dkpc=None, metZ=None, logAge=None, mass=None,  Nbins = [25,25], IMF_type='s', Interp=False, bands = ['B','V','V'], degrees = False, normalize = False, mag_axis=[0., 2., -1. ,20], plotflag = False, logoutput = False, IMF_slope = 2.35, A_V = None):
	'''Returns the Colour Magnitude Diagram bands[1] vs bands[1]-bands[0].
	Mfcis is the Mass fraction converted into stars.
	[magnitude_histogram, nobstars, Mfcis] = AsgaiaTools.CMD(lon, lat, dkpc, metZ, logAge, mass )
	-------------------------------------------------------------------------------------------
	*** Additional inputs and default values ***
	Nbins = [10,10]
	mag_axis = [xmin, xmax, ymin, ymax]
	bands = ['B', 'V']: Filters to be used. See information below.
	Interp (False): Interpolates in the [metZ, logAge] grid.
	IMF_type ('s'): can be 's' (Salpeter), 'k' (Kroupa) or 'b' (Bimodal).
	degrees (False): Units of lon and lat
	normalize (False): CMD gets normalized so that the sum becomes 1.
	plotflag(False).
	logoutput(False): If true, calculates the CMD in logscale.
	IMF_slope: free parameter of the bimodal IMF (default 2.35)
	A_V (None): If provided, this extinction in the V band will be used. Otherwise, uses the Schelgel maps.
	-------------------------------------------------------------------------------------------
	Values for band:
	"U","B","V","R","I" or "Ic","J","H","K"

	Although strickly speaking a CMD does not use logTe, logg or mbol, the following quantities can be also selected for a "band" value:
	'logTe', 'logg','mbol'. In other words, you can create a logTe vs (B-V) plot (for example) with this function by putting: bands=['B','V','logTe']

	WARNING: The bolometric magnitude IS NOT corrected by the extinction.
	'''
	# Short version of bands:
	if(len(bands)<3):
		bands = [bands[0],bands[1],bands[1]];

	# Mulpliple names:
	bands[0] = bands[0]*(bands[0]!='Ic')+'I'*(bands[0]=='Ic');
	bands[1] = bands[1]*(bands[1]!='Ic')+'I'*(bands[1]=='Ic');
	bands[2] = bands[2]*(bands[2]!='Ic')+'I'*(bands[2]=='Ic');

	band_list = ['mbol', 'logg','logTe','U','B','G_BP','V','G','R','G_RP','I','G_RVS','J','H','K','Ks'];
	# band_list += ['B_T','V_T','u','g','r','i','z','Z','Y','Vista_J','Vista_H','Vista_Ks']; Not implemented yet

	assert(bands[0] in band_list),'Unknown first photometric band. Please see GetMagnitude_hist? for the available filters.'
	assert(bands[1] in band_list),'Unknown second photometric band. Please see GetMagnitude_hist? for the available filters.'
	assert(bands[2] in band_list),'Unknown third photometric band. Please see GetMagnitude_hist? for the available filters.'

	# From band name to band number
	bands_id = np.zeros(3, dtype = np.int);
	bands_id[0] = band_list.index(bands[0])-3; # Now band_id is a number
	bands_id[1] = band_list.index(bands[1])-3; # Now band_id is a number
	bands_id[2] = band_list.index(bands[2])-3; # Now band_id is a number

	# IMF_type must be a char
	IMF_type = IMF_type[0].lower();

	# Nbins must be integers
	Nbins = np.int32(Nbins);

	# Check the order of the axis:
	mag_axis[0],mag_axis[1] = np.min(mag_axis[0:2]), np.max(mag_axis[0:2]);
	mag_axis[2],mag_axis[3] = np.min(mag_axis[2:4]), np.max(mag_axis[2:4]);


	short_version = (type(metZ)==type(None))*(type(logAge)==type(None))*(type(mass)==type(None)); # Absolute mag. No extinction and no distances


	if(short_version):
		# (lon, lat, dkpc) contain (metZ, logAge, mass) respectively
		metZ = np.float32(1.*lon);
		logAge = np.float32(1.*lat);
		mass = np.float32(1.*dkpc);
		mag_axis = 1.*np.float32(mag_axis);

		dkpc = 0.01 + 0.*mass; # 10 pc for the absolute magnitude
		if(type(A_V)==type(None)):
			A_V = 0.*dkpc; # No extinction
		else:
			A_V = np.float32(1.*A_V);

		band_list = ['Mbol', 'logg','logTe','M_U','M_B','M_G_BP','M_V','M_G','M_R','M_G_RP','M_I','M_G_RVS','M_J','M_H','M_K','M_Ks'];
		# band_list += ['B_T','V_T','u','g','r','i','z','Z','Y','Vista_J','Vista_H','Vista_Ks']; Not implemented yet
		
	else:
		if(degrees):
			lon = np.deg2rad(lon);
			lat = np.deg2rad(lat);

		# Type conversion:
		lon = np.float32(1.*lon);
		lat = np.float32(1.*lat);
		dkpc = np.float32(1.*dkpc);
		metZ = np.float32(1.*metZ);
		logAge = np.float32(1.*logAge);
		mass = np.float32(1.*mass);
		mag_axis = 1.*np.float32(mag_axis);

		# Get the extinction in the V band (if necessary)
		if(type(A_V)==type(None)):
			A_V = CAsgaiaTools.GetAV(lon, lat, dkpc);
		else:
			A_V = np.float32(1.*A_V); # Otherwise, convert to np.float32



	# Call to CAsgaiaTools
	[cmd_MAP, nobstars, Mfcis] = CAsgaiaTools.CMD( metZ, logAge, mass, dkpc, A_V, Nbins[0], Nbins[1], mag_axis[0], mag_axis[1], mag_axis[2], mag_axis[3], IMF_type, Interp, bands_id[0], bands_id[1], bands_id[2], IMF_slope);
	success = True;

	assert success, 'Input error. Try one of the suggested input choices'

	del(success, short_version);

	if(normalize):
		Norm = np.sum(cmd_MAP);
		Norm = Norm + 1.*(Norm==0.0);
		cmd_MAP = np.float32(cmd_MAP/Norm);

	if(plotflag):
		bands[0] = band_list[bands_id[0]+3];
		bands[1] = band_list[bands_id[1]+3];
		bands[2] = band_list[bands_id[2]+3];
		if(logoutput):
			plt.figure('CMD %s %s (log)'%(bands[0], bands[1]));
		else:
			plt.figure('CMD %s %s'%(bands[0], bands[1]));
		cmd_MAP = _my_mask_function(cmd_MAP==0, cmd_MAP);
		if(logoutput):
			cmd_MAP = np.log10(cmd_MAP);
		eje_x = np.linspace(mag_axis[0], mag_axis[1], Nbins[0]+1, endpoint = True);
		eje_y = np.linspace(mag_axis[2], mag_axis[3], Nbins[1]+1, endpoint = True);
		plt.pcolor( eje_x, eje_y, cmd_MAP);
		plt.ylabel(bands[2]);
		if(bands_id[2]!=-1):
			plt.gca().invert_yaxis(); ## logTe is usually represented in vertical direct axis
		bands_id = bands_id[0:2];
		plt.xlabel('%s-%s'%(bands[bands_id.argmin()], bands[bands_id.argmax()]));
		plt.show();

	return([cmd_MAP, nobstars, Mfcis]);
# -------------------------------------------------------------




# -------------------------- GetHR ---------------------------
def GetHR(lon=None, lat=None, dkpc=None, metZ=None, logAge=None, mass=None,  Nbins = [25,25], IMF_type='s', Interp=False, band = 'V', degrees = False, normalize = False, hr_axis=[3.4,4.7, -15.,10], plotflag = False, logoutput = False, parameterX='logTe', IMF_slope = 2.35, A_V = None):
	'''Returns the Hertzsprung Russell Diagram (band vs logTe).
	Mfcis is the Mass fraction converted into stars.
	[magnitude_histogram, nobstars, Mfcis] = AsgaiaTools.HR(lon, lat, dkpc, metZ, logAge, mass )
	-------------------------------------------------------------------------------------------
	*** Additional inputs and default values ***
	Nbins = [10,10]
	hr_axis = [xmin, xmax, ymin, ymax]
	band = 'V': Filter to be used. See information below.
	Interp (False): Interpolates in the [metZ, logAge] grid.
	IMF_type ('s'): can be 's' (Salpeter), 'k' (Kroupa) or 'b' for Bimodal.
	degrees (False): Units of lon and lat
	normalize (False): HR gets normalized so that the sum becomes 1.
	logoutput(False): If true, calculates the HR in logscale.

	parameterX = 'logTe': Sometimes we want to use log(g), m_bol or any band instead of logTe.
	             In that cases, parameterX can be 'logg', 'mbol' or any valid band value.

	IMF_slope: free parameter of the bimodal IMF (default 2.35)
	A_V (None): If provided, this extinction in the V band will be used. Otherwise, uses the Schelgel maps.
	-------------------------------------------------------------------------------------------
	Values for band:
	"U","V","B","R","I" or "Ic","K","J","H","Ks"

	NOTE: logg, logTe and mbol can be used as band value.
	     mbol is nor extinction corrected
	'''

	band_list = ['mbol', 'logg','logTe','U','B','G_BP','V','G','R','G_RP','I','G_RVS','J','H','K','Ks'];
	# band_list += ['B_T','V_T','u','g','r','i','z','Z','Y','Vista_J','Vista_H','Vista_Ks']; Not implemented yet

	# First, evaluate parameterX:
	if(parameterX.lower()=='logte'):
		pX_id = -1;
	elif(parameterX.lower()=='logg'):
		pX_id = -2;
	elif(parameterX.lower()=='mbol'):
		pX_id = -3;
	else:
		#It must be a band
		parameterX = parameterX*(parameterX!='Ic')+'I'*(parameterX=='Ic');
		assert(band in band_list),'Unknown parameterX. Please use logTe, logg, mbol or a valid band name.'
		# From band name to band number
		pX_id = band_list.index(band)-3; # Now band_id is a number
		
	# Check the order of the axis:
	hr_axis[0],hr_axis[1] = np.min(hr_axis[0:2]), np.max(hr_axis[0:2]);
	hr_axis[2],hr_axis[3] = np.min(hr_axis[2:4]), np.max(hr_axis[2:4]);

	# Now evaluate band:
	# Mulpliple names:
	band = band*(band!='Ic')+'I'*(band=='Ic');

	assert(band in band_list),'Unknown photometric band. Please see GetMagnitude_hist? for the available filters.'

	# From band name to band number
	band_id = band_list.index(band)-3; # Now band_id is a number

	# IMF_type must be a char
	IMF_type = IMF_type[0].lower();

	# Nbins must be integers
	Nbins = np.int32(Nbins);

	short_version = (type(metZ)==type(None))*(type(logAge)==type(None))*(type(mass)==type(None)); # Absolute mag. No extinction and no distances


	if(short_version):
		# (lon, lat, dkpc) contain (metZ, logAge, mass) respectively
		metZ = np.float32(1.*lon);
		logAge = np.float32(1.*lat);
		mass = np.float32(1.*dkpc);
		hr_axis = 1.*np.float32(hr_axis);

		dkpc = 0.01 + 0.*mass; # 10 pc for the absolute magnitude
		if(type(A_V)==type(None)):
			A_V = 0.*dkpc; # No extinction
		else:
			A_V = np.float32(1.*A_V);
		
	else:
		if(degrees):
			lon = np.deg2rad(lon);
			lat = np.deg2rad(lat);

		# Type conversion:
		lon = np.float32(1.*lon);
		lat = np.float32(1.*lat);
		dkpc = np.float32(1.*dkpc);
		metZ = np.float32(1.*metZ);
		logAge = np.float32(1.*logAge);
		mass = np.float32(1.*mass);
		hr_axis = 1.*np.float32(hr_axis);

		# Get the extinction in the V band:
		if(type(A_V)==type(None)):
			A_V = CAsgaiaTools.GetAV(lon, lat, dkpc);
		else:
			A_V = np.float32(1.*A_V);


	# Call to CAsgaiaTools
	[hr_MAP, nobstars, Mfcis] = CAsgaiaTools.HR( metZ, logAge, mass, dkpc, A_V, Nbins[0], Nbins[1], hr_axis[0], hr_axis[1], hr_axis[2], hr_axis[3], IMF_type, Interp, pX_id, band_id, IMF_slope);
	success = True;

	assert success, 'Input error. Try one of the suggested input choices'

	del(success, short_version);

	if(normalize):
		Norm = np.sum(hr_MAP);
		Norm = Norm + 1.*(Norm==0.0);
		hr_MAP = np.float32(hr_MAP/Norm);

	if(plotflag):
		if(logoutput):
			plt.figure('HR %s %s (log)'%(parameterX, band));
		else:
			plt.figure('HR %s %s'%(parameterX, band));
		hr_MAP = _my_mask_function(hr_MAP==0, hr_MAP);
		if(logoutput):
			hr_MAP = np.log10(hr_MAP);
		eje_x = np.linspace(hr_axis[0], hr_axis[1], Nbins[0]+1, endpoint = True);
		eje_y = np.linspace(hr_axis[2], hr_axis[3], Nbins[1]+1, endpoint = True);
		plt.pcolor( eje_x, eje_y, hr_MAP);
		plt.ylabel(band);
		plt.xlabel(parameterX);
		plt.gca().invert_yaxis();
		plt.gca().invert_xaxis();
		plt.show();

	return([hr_MAP, nobstars, Mfcis]);
# -------------------------------------------------------------


# -------------------------- _my_mask_function --------------------------
def _my_mask_function(bool_condition, element):
	'''Similar to np.ma.masked_where( condition, element), but the mask is ALWAYS an array'''
	element = np.ma.masked_where( bool_condition, element);
	element.mask = bool_condition;
	return element;
