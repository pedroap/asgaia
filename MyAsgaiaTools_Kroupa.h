#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#ifndef Number_of_Z
#include "AsgaiaConstants.h"
#endif

//Note: M_PI is defined in math.h

/* Modified 06/04/2018
	This .h file contains functions that use the Kroupa IMF.

	Using Kroupa IMF:
		KroupaIMF(float *ini_mass, int Nmasses, float mL, float *N_mi);
		KroupaIMFdistr(float *ini_mass, int N, float *P_mi);

		SigmaPiGaia_Kroupa(float *Z, float *logAge, float *dist, float *ex31, float *correction, float *error_pi, int Nparticles, float *float_oargs);
		SigmaPiGaiaKroupa_4Points(float *Z, float *logAge, float *dist, float *ex31, float *correction, float *error_pi, int Nparticles, float *float_oargs);

		PhotometryGaia_Kroupa(float *Z, float *logAge, float *ParticleMass, float *dist, float *ex31, float *meanG, float *mean_BP, float *mean_RP, float *error_G, float *error_BP, float *error_RP, float *nobstars, int Nparticles, float *G_limits);
		PhotometryGaiaKroupa_4Points(float *Z, float *logAge, float *ParticleMass, float *dist, float *ex31, float *meanG, float *mean_BP, float *mean_RP, float *error_G, float *error_BP, float *error_RP, float *nobstars, int Nparticles, float *G_limits);

		SigmaVrGaia_Kroupa(float *Z, float *logAge, float *ParticleMass, float *dist, float *ex31, float *error_Vr, float *nobstars, int Nparticles, float *G_limits);
		SigmaVrGaiaKroupa_4Points(float *Z, float *logAge, float *ParticleMass, float *dist, float *ex31, float *error_Vr, float *nobstars, int Nparticles, float *G_limits);

*/


// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//
//					Using Kroupa IMF:
//
// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/* KroupaIMF */
void KroupaIMF(float *ini_mass, int Nmasses, float mL, float *N_mi){
// Returns the Number of stars within the limits given by ini_mass (that must be sorted)
// assuming a global mass (from kimf_m0 to mL) of 1
// For other masses, just multiply N_mi times the global mass (particle mass)
// If you normalize the result, you will obtain a probability distribution
// Nmasses is the length of ini_mass
// No extra finite mass correction is needed

	int n; // Counter

	/* IMF variables */
	float k_IMF; //(Per global mass)
	float m, mu; //mass and mass/kimf_m1
	/* Cumulative quantities */
	float CN, CN0; //Cumulative Number of stars
	float CM; //Cumulative Mass
	float fraction; // For the interpolation

	// Cumulative mass of stars:
	mu = mL/kimf_m1;
	CM = (pow(min(mu,1.), 2.-kimf_a0)-pow(kimf_m0/kimf_m1, 2.-kimf_a0))/(2.-kimf_a0)*(mL>=kimf_m0);
	CM = CM + (1.-pow(min(mu,kimf_m2/kimf_m1), 2.-kimf_a1))/(kimf_a1-2.)*(mL>=kimf_m1);
	CM = CM + (pow(kimf_m2/kimf_m1,2.-kimf_a1)-pow(kimf_m2/kimf_m1, kimf_a2-kimf_a1)*pow(mu,2.-kimf_a2))/(kimf_a2-2.)*(mL>=kimf_m2);
	CM = CM*kimf_m1*kimf_m1;


	//Calculate k_IMF
	k_IMF = 1./CM; //Normalization (for Mp=1)


	// Cumulative number of stars:
	CN = (pow(min(mu,1.), 1.-kimf_a0)-pow(kimf_m0/kimf_m1, 1.-kimf_a0))/(1.-kimf_a0)*(mL>=kimf_m0);
	CN += (1.-pow(min(mu,kimf_m2/kimf_m1), 1.-kimf_a1))/(kimf_a1-1.)*(mL>=kimf_m1);
	CN += (pow(kimf_m2/kimf_m1,1.-kimf_a1)-pow(kimf_m2/kimf_m1,kimf_a2-kimf_a1)*pow(mu,1.-kimf_a2))/(kimf_a2-1.)*(mL>=kimf_m2);
	CN = k_IMF*kimf_m1*CN;

	m = ini_mass[0];
	mu = m/kimf_m1;
	CN0 = (pow(min(mu,1.), 1.-kimf_a0)-pow(kimf_m0/kimf_m1, 1.-kimf_a0))/(1.-kimf_a0)*(m>=kimf_m0);
	CN0 += (1.-pow(min(mu,kimf_m2/kimf_m1), 1.-kimf_a1))/(kimf_a1-1.)*(m>=kimf_m1);
	CN0 += (pow(kimf_m2/kimf_m1,1.-kimf_a1)-pow(kimf_m2/kimf_m1,kimf_a2-kimf_a1)*pow(mu,1.-kimf_a2))/(kimf_a2-1.)*(m>=kimf_m2);
	CN0 = k_IMF*kimf_m1*CN0;

	//Now CN is the total number of stars to be generated:
	CN = CN-CN0;

	//The first term is different:
	N_mi[0] = CN0;

	for(n=1;n<Nmasses;n++){
		m = .5*ini_mass[n-1] + 0.5*ini_mass[n];
		mu = m/kimf_m1;
		N_mi[n] = (pow(min(mu,1.), 1.-kimf_a0)-pow(kimf_m0/kimf_m1, 1.-kimf_a0))/(1.-kimf_a0)*(m>=kimf_m0);
		N_mi[n] += (1.-pow(min(mu,kimf_m2/kimf_m1), 1.-kimf_a1))/(kimf_a1-1.)*(m>=kimf_m1);
		N_mi[n] += (pow(kimf_m2/kimf_m1,1.-kimf_a1)-pow(kimf_m2/kimf_m1,kimf_a2-kimf_a1)*pow(mu,1.-kimf_a2))/(kimf_a2-1.)*(m>=kimf_m2);
		N_mi[n] *= kimf_m1*k_IMF;
	}

	//Get the difference:
	// The first term is different:
	fraction = 2.*(mL-ini_mass[0])/(ini_mass[1]-ini_mass[0]);
	fraction = max( min(fraction,1.), 0.);
	N_mi[0] = (N_mi[1]-N_mi[0])*fraction;

	// For the rest of the masses:
	for(n=1;n<(Nmasses-1);n++){
		fraction = (2.*mL-ini_mass[n]-ini_mass[n-1])/(ini_mass[n+1]-ini_mass[n-1]);
		fraction = max( min(fraction,1.), 0.);

		N_mi[n] = (N_mi[n+1]-N_mi[n])*fraction;
	}
	//The last term is different:
	m = ini_mass[Nmasses-1];
	mu = m/kimf_m1;
	CN = (pow(min(mu,1.), 1.-kimf_a0)-pow(kimf_m0/kimf_m1, 1.-kimf_a0))/(1.-kimf_a0)*(m>=kimf_m0);
	CN += (1.-pow(min(mu,kimf_m2/kimf_m1), 1.-kimf_a1))/(kimf_a1-1.)*(m>=kimf_m1);
	CN += (pow(kimf_m2/kimf_m1,1.-kimf_a1)-pow(kimf_m2/kimf_m1,kimf_a2-kimf_a1)*pow(mu,1.-kimf_a2))/(kimf_a2-1.)*(m>=kimf_m2);
	CN *= kimf_m1*k_IMF;

	fraction = (2.*mL-m-ini_mass[Nmasses-2])/(m-ini_mass[Nmasses-2]);
	fraction = max( min(fraction,1.), 0.);
	N_mi[n] = (CN-N_mi[n])*fraction;

}//End of KroupaIMF
//-----------------------------------------------------------------------------------------------------------------//



//-----------------------------------------------------------------------------------------------------------------//
/* KroupaIMFdistr */
void KroupaIMFdistr(float *ini_mass, int N, float *P_mi){
// Returns the Probability of having a mass within the limits given by ini_mass (that must be sorted)
// N is the length of ini_mass
	int n; // Counter

	/* IMF variables */
	const float N_IMF1 = (1.-pow(kimf_m0/kimf_m1,1-kimf_a0))/(1.-kimf_a0); // Cumulative IMF (track1)
	const float N_IMF2 = (1.-pow(kimf_m2/kimf_m1,1.-kimf_a1))/(kimf_a1-1.);// Cumulative IMF (track2)
	const float N_IMF3 = pow(kimf_m2/kimf_m1,1-kimf_a1)/(kimf_a2-1.);// Cumulative IMF (track3)
	float NP_mi = 1./(N_IMF1 + N_IMF2 + N_IMF3);// Normalization of the Kroupa IMF//
	float New_Norm_of_P_mi; // True norm of the IMF for the observed masses
	float P0, P1, P2; // Temporal values of the parts of the IMF(m)
	float mi, dm;// initial mass, mass bin size

	// Cumulative probabilities:
	//	The first one is different
	mi = ini_mass[0];
	P0 = (pow(mi/kimf_m1,1-kimf_a0)-pow(kimf_m0/kimf_m1,1-kimf_a0))/(1.-kimf_a0);
	P1 = N_IMF1 + (1.-pow(mi/kimf_m1,1-kimf_a1))/(kimf_a1-1);
	P2 = N_IMF1 + N_IMF2 + pow(kimf_m2/kimf_m1,1-kimf_a1)*(1.-pow(mi/kimf_m2,1-kimf_a2))/(kimf_a2-1);

	P0 = P0*(kimf_m0<=mi && mi<kimf_m1);
	P1 = P1*(kimf_m1<=mi && mi<kimf_m2);
	P2 = P2*(kimf_m2<=mi);

	P_mi[0] = (P0 + P1 + P2)*NP_mi;  // The first one is different 

	for(n=1;n<N;n++){
		dm = ini_mass[n]-ini_mass[n-1];
		mi = ini_mass[n]-.5*dm;
		P0 = (pow(mi/kimf_m1,1-kimf_a0)-pow(kimf_m0/kimf_m1,1-kimf_a0))/(1.-kimf_a0);
		P1 = N_IMF1 + (1.-pow(mi/kimf_m1,1-kimf_a1))/(kimf_a1-1);
		P2 = N_IMF1 + N_IMF2 + pow(kimf_m2/kimf_m1,1-kimf_a1)*(1.-pow(mi/kimf_m2,1-kimf_a2))/(kimf_a2-1);

		P0 = P0*(kimf_m0<=mi && mi<kimf_m1);
		P1 = P1*(kimf_m1<=mi && mi<kimf_m2);
		P2 = P2*(kimf_m2<=mi);

		P_mi[n] = (P0 + P1 + P2)*NP_mi;
	} 
	/* P_mi is not the cumulative probability given by the IMF (yet). It will be calculated in the next loop */

	New_Norm_of_P_mi = 0.;
	for(n=0;n<(N-1);n++){
		//Calculate P[mi] for that star (Kroupa IMF)
		P_mi[n] = P_mi[n+1]-P_mi[n]; // P(m-dm/2<=m<m+dm/2).
		New_Norm_of_P_mi += P_mi[n];
	}; // Most of the stars have been read, but the last one is different. Now n = N-1

	// The last term is different
	mi = ini_mass[n];
	P0 = (pow(mi/kimf_m1,1-kimf_a0)-pow(kimf_m0/kimf_m1,1-kimf_a0))/(1.-kimf_a0);
	P1 = N_IMF1 + (1.-pow(mi/kimf_m1,1-kimf_a1))/(kimf_a1-1);
	P2 = N_IMF1 + N_IMF2 + pow(kimf_m2/kimf_m1,1-kimf_a1)*(1.-pow(mi/kimf_m2,1-kimf_a2))/(kimf_a2-1);

	P0 = P0*(kimf_m0<=mi && mi<kimf_m1);
	P1 = P1*(kimf_m1<=mi && mi<kimf_m2);
	P2 = P2*(kimf_m2<=mi);

	P_mi[n] = (P0 + P1 + P2)*NP_mi-P_mi[n];  // P(m-dm/2<=m).
	New_Norm_of_P_mi += P_mi[n];
	New_Norm_of_P_mi = 1./New_Norm_of_P_mi;

	for(n=0;n<N;n++){
		//Calculate P[mi] for that star (Kroupa IMF)
		P_mi[n] *= New_Norm_of_P_mi;
	};


}//End of KroupaIMFdistr
//-----------------------------------------------------------------------------------------------------------------//





//-----------------------------------------------------------------------------------------------------------------//
/* SigmaPiGaia_Kroupa */
void SigmaPiGaia_Kroupa(float *Z, float *logAge, float *ParticleMass, float *dist, float *ex31, float *total_correction, float *error_pi, float *nobstars, int Nparticles, float *G_limits, float *float_oargs){
/* Since ex31 is provided, lon and lat are not needed*/
// Correction includes Sky Scanning Correction + Calibration factor + epoch (sqrt(60/month))
// UNITS: Z is a fraction, Ages in yrs, distances in kpc, ex31 in mag, error_pi in mas
//
// float_oargs: arrays of optional float arguments (oargs) used to avoid large number of inputs. For THIS function: [0] = REL_SIGMA_CUTOFF

	int n, particle, star; // Counters
	float Z_set[34]; // Set of metallicities (the grid is not regular in Z)
	float aux; //auxiliary variable
	int is_selected; // 1 if the G mag is in the Gaia range, 0 otherwise

	/* Values for each particle */
	int i_Z, i_logAge, i_isochrone; //Index of the metallicity, logAge, isochrone
	float Z_p, logAge_p, Mass_p, ex31_p; // Values for one selected particle
	float distance_correction; // 10+5*log10(d_kpc)
	float rel_sigma_cutoff_p; // modified version of sigma_pi/parallax upper limit. It is the same for all the stars in a particle. The cutoff is redefined to get only the "aux" variable in the left hand side, and rel_sigma_cutoff_p = 1000/distance/total_correction*REL_SIGMA_CUTOFF (true sigma_pi/parallax)

	/* Values for each star */
	float V_star, G_star, VmIc_star; //Apparent V mag, G mag and colour difference

	/* Values for each isochrone */
//	float V_min, V_max, VmIc_min, VmIc_max;//Parameters to read in Explanation2.bin
	int starting_point_of_the_isochrone, Number_of_logmasses_in_the_isochrone;

	/* IMF */
	const float dlogm = (Logmi_MAX-Logmi_MIN)/(1.0*Max_Number_of_logmasses_per_isochrone); // bin size
	float mi[Max_Number_of_logmasses_per_isochrone], N_mi[Max_Number_of_logmasses_per_isochrone]; //Masses and global IMF
	float Cum_N_mi;
	float mL; //Maximum initial mass

	/* Float options */
	float REL_SIGMA_CUTOFF = float_oargs[0];

	// Files:
	FILE *fichero_ASCII; // ASCII file with the information about the isochrones
	FILE *fichero_stat_bin; //Binary file with the statistics of all the isochrones
	FILE *fichero_isochr_bin; //Binary file with all the isochroness

	// Always in the Gaia range
	G_limits[0] = max(G_limits[0], Gmin);
	G_limits[1] = min(G_limits[1], Gmax);

	/* Read the metallicity values of the grid */
	fichero_ASCII = fopen( ISONO,"r"); //Open IsoNo.txt
	for (n=0; n<Number_of_Z;n++){
		fscanf( fichero_ASCII, "%f\n", &Z_set[n]); // Read the decimal part of the metallicity 
		Z_set[n] = Z_set[n]*1E-6; // Convert to true Z [met]

	} // Metallicity values read!
	fclose( fichero_ASCII ); //Close the ASCII file


	fichero_isochr_bin = fopen( BIN3,"rb"); //Open All_the_isochrones3.bin
	fichero_stat_bin = fopen( EXPL3,"rb"); // Open the statistics of all the isochr.

	//Since logmi_mi and Logmi_MAX are equal for all of the isochrones, the IMF would be the same:
	/* Probability given the Kroupa IMF */
	for(star=0; star<Max_Number_of_logmasses_per_isochrone;star++){
		mi[star] = pow(10., Logmi_MIN + dlogm*star);
	};
//fprintf(stderr,"%f %f %f \n", Logmi_MIN, Logmi_MAX, dlogm);


// Loop over the particles
	for(particle=0;particle<Nparticles;particle++){

		// Store the parameters of the considered particle:
		Z_p = Z[particle];
		logAge_p = logAge[particle];
		ex31_p = ex31[particle];
		distance_correction = 10.+5.*log10(dist[particle]);
		Mass_p = ParticleMass[particle];
		rel_sigma_cutoff_p = 1000./dist[particle]/total_correction[particle]*REL_SIGMA_CUTOFF;		

		// Avoid out of range values:
		logAge_p = (logAge_p<logAge_min? logAge_min: logAge_p);
		logAge_p = (logAge_p>logAge_max? logAge_max: logAge_p);

		Z_p = max(Z_p, Z_set[0]);
		Z_p = min(Z_p,Z_set[Number_of_Z-1]);


		// Get the index of that metallicity value (the Z sampling is not homogeneous):
		i_Z = -1; //Initial value
		do{
			i_Z++;
		}while( Z_set[i_Z]<=Z_p );
		i_Z--;
		i_Z = min(i_Z, Number_of_Z-1);
		i_Z = max(i_Z, 0);

		//Assume that the logAge grid is regular (from logAge_min to logAge_max with step logAge_step)
		i_logAge = (int)((Number_of_logAge-1)*(logAge_p-logAge_min)/(logAge_max-logAge_min));
		i_isochrone = i_logAge + (Number_of_logAge)*i_Z;


		// Read the following information about the selected isochrone: [Z, logAge, m_ini.min, m_ini.max, Nasses, V.min, V.max, (V-Ic).min, (V-Ic).max]
//		fseek( fichero_stat_bin, 40*i_isochrone, SEEK_SET);// 40 bytes = 4 bytes*10 columns
//		fread(&Z_p,4,1,fichero_stat_bin); // Metallicity of the isochrone
//		fread(&logAge_p,4,1,fichero_stat_bin); // logAge of the isochrone
		
		fseek( fichero_stat_bin, 40*i_isochrone+16, SEEK_SET);// 40 bytes = 4 bytes*10 columns
//		skip logmi_*
//		fread(&Logmi_MIN,4,1,fichero_stat_bin); // min of the initial mass (of the isochrone)
//		fread(&Logmi_MAX,4,1,fichero_stat_bin); // max of the initial mass (of the isochrone)

		fread(&starting_point_of_the_isochrone,4,1,fichero_stat_bin); //Starting position (inclusive)
		fread(&Number_of_logmasses_in_the_isochrone,4,1,fichero_stat_bin);// Rows (stars) that belong to this isochrone

//		fread(&V_min,4,1,fichero_stat_bin); // min of the V band (of the isochrone)
//		fread(&V_max,4,1,fichero_stat_bin); // max of the V band (of the isochrone)
//		fread(&VmIc_min,4,1,fichero_stat_bin); // min of the V band (of the isochrone)
//		fread(&VmIc_max,4,1,fichero_stat_bin); // max of the V band (of the isochrone)

		//Call to the Kroupa IMF function
		mL = pow(10., Logmi_MIN + dlogm*(Number_of_logmasses_in_the_isochrone-1));

		KroupaIMF( mi, Max_Number_of_logmasses_per_isochrone, mL, N_mi); //N_mi means number of masses between mi and mi+dm (egde effects included)

		// Initialize the variables:
		aux = 0.; // Quantity used to estimate error_pi
		error_pi[particle] = 0.; // Sigma_pi
		Cum_N_mi = 0.; //New norm of N_mi_p
		nobstars[particle] = 0.;

		// Go to the desired isochrone
		fseek( fichero_isochr_bin, 8*starting_point_of_the_isochrone, SEEK_SET); //8=2*4 bytes = np.float32 bits*2 columns


		// Load V and V-Ic from the isochrone given by (Z, logAge), and finish P(m_i)
		for(star=0;star<Number_of_logmasses_in_the_isochrone;star++){
			// Get the absolute magnitudes
			fread(&V_star,4,1, fichero_isochr_bin); // Absolute V
			fread(&VmIc_star,4,1, fichero_isochr_bin); // Absolute V-Ic

			// From absolute to apparent
			V_star = V_star + distance_correction + ex31_p;
			VmIc_star = VmIc_star + 355./900.0*ex31_p; // 355/900 = 0.394444... = 1.962/3.24 (ratio between V and Ic extinction corrections)

			// Estimate the G band
			G_star = V_star-0.01746+0.008092*VmIc_star-0.2810*VmIc_star*VmIc_star+0.03655*VmIc_star*VmIc_star*VmIc_star; // G band (Tony Brown's formula)
		
			// Would this star be observed with Gaia?
			is_selected = GaiaSelectionFunction(G_star, G_limits[0],G_limits[1]); // 1= Yes, 0=No
			is_selected *= IndicatorFunction( VmIc_star, VmIc_cutoffmin, VmIc_cutoffmax); // 1= Yes, 0=No

			if(!is_selected) continue; // Go to the next star

			// Calculate the error for this star and add it to the particle contribution:
			aux = max(0.063095734448019303, pow(10.,0.4*G_star-6.)); // 0.06309...=10**(0.4*(12.0-15))
			// aux = z in www.cosmos.esa.int/web/gaia/science-performance#photometric performance
			// Including the is_selected in the power we avoid out-of-float range values when the extinction is extremely high
			aux =  sqrt(-1.631 + 680.766*aux + 32.732*aux*aux)*(0.986 + 0.014*VmIc_star)/1.2; // Here aux = sigma_pi for the considered star. The 20% of science margin is already included

			// Exclude stars whose relative parallax errors are above the selected threshold:
			is_selected = is_selected*(aux<=rel_sigma_cutoff_p);

			N_mi[star]*=is_selected; //Sometimes is_selected is a fraction

			Cum_N_mi += N_mi[star]; //Renormalisation of N_mi due to the out-of-G range stars. Cum_N_mi is the total number of stars observed (if Mass_p = 1)
			error_pi[particle] += aux*N_mi[star]; // Add the contribution to the average


		}; // End of the star-loop

		// Save the number of stars observed for this particle
		nobstars[particle] = Mass_p*Cum_N_mi;

		// Correction due to the G-band exclusion
		Cum_N_mi = Cum_N_mi -1.0*(Cum_N_mi<=0);//If no stars are observed, return a negative value of sigma_pi
		error_pi[particle] = total_correction[particle]*error_pi[particle]/Cum_N_mi; //Include the Calibration, the scanning law and the epoch corrections
		error_pi[particle] = error_pi[particle]*(Cum_N_mi>0.) - 99000.00*(Cum_N_mi<=0.);//If no stars are observed, return a negative value of sigma_pi
		error_pi[particle] *= 0.001; //From uas to mas

	
	}; // End of the particle

	fclose( fichero_stat_bin );
	fclose( fichero_isochr_bin );

}//End of SigmaPiGaia_Kroupa
//-----------------------------------------------------------------------------------------------------------------//




//---------------------------------------SigmaPiGaiaKroupa_4Points --------------------------------//
void SigmaPiGaiaKroupa_4Points(float *Z, float *logAge, float *ParticleMass, float *dist, float *ex31, float *total_correction, float *error_pi, float *nobstars, int Nparticles, float *G_limits, float *float_oargs){
/* Since ex31 is provided, lon and lat are not needed. Interpolates in the (Z, logAge) grid*/
// UNITS: Z (is a fraction)
//	  logAge (ages in yrs)
//	  distances in kpc
//	  ex31 in mag
//	  error_pi in mas
// Correction includes Sky Scanning Correction + Calibration factor + epoch (sqrt(60/month))
//
// float_oargs: arrays of optional float arguments (oargs) used to avoid large number of inputs. For THIS function: [0] = REL_SIGMA_CUTOFF

	int n, particle, star; // Counters
	float Z_set[34]; // Set of metallicities (the grid is not regular in Z)
	float aux; //auxiliary variable
	int is_selected; // 1 if the G mag is in the Gaia range, 0 otherwise

	/* Values for each particle */
	int i_Z, i_logAge, i_isochrone; //Index of the metallicity, logAge, isochrone
	float Z_p, logAge_p, Mass_p, ex31_p; // Values for one selected particle
	float distance_correction; // 10+5*log10(d_kpc)
	float rel_sigma_cutoff_p; // modified version of sigma_pi/parallax upper limit. It is the same for all the stars in a particle. The cutoff is redefined to get only the "aux" variable in the left hand side, and rel_sigma_cutoff_p = 1000/distance/total_correction*REL_SIGMA_CUTOFF (true sigma_pi/parallax)

	/* Values for each star */
	float V_star, G_star, VmIc_star; //Apparent V mag, G mag and colour difference

	/* Values for each isochrone */
//	float V_min, V_max, VmIc_min, VmIc_max;//Parameters to read in Explanation3.bin
	int starting_point_of_the_isochrone, Number_of_logmasses_in_the_isochrone;

	/* IMF */
	const float dlogm = (Logmi_MAX-Logmi_MIN)/(1.0*Max_Number_of_logmasses_per_isochrone); // bin size
	float mi[Max_Number_of_logmasses_per_isochrone], N_mi[Max_Number_of_logmasses_per_isochrone]; //Masses and global IMF
	float Cum_N_mi;
	float mL;

	/* Float options */
	float REL_SIGMA_CUTOFF = float_oargs[0];

	/* Weights and temporal errors */
	float nobstars_LD, nobstars_RD, nobstars_RU, nobstars_LU;
	float errorpi_LD, errorpi_RD, errorpi_RU, errorpi_LU;
	float wLR, wRL, wDU, wUD; // wLR+wRL = wDU+wUD = 1.


	// Files:
	FILE *fichero_ASCII; // ASCII file with the information about the isochrones
	FILE *fichero_stat_bin; //Binary file with the statistics of all the isochrones
	FILE *fichero_isochr_bin; //Binary file with all the isochroness

	// Always in the Gaia range
	G_limits[0] = max(G_limits[0], Gmin);
	G_limits[1] = min(G_limits[1], Gmax);


	/* Read the metallicity values of the grid */
	fichero_ASCII = fopen( ISONO,"r"); //Open ISONO
	for (n=0; n<Number_of_Z;n++){
		fscanf( fichero_ASCII, "%f\n", &Z_set[n]); // Read the decimal part of the metallicity 
		Z_set[n] = Z_set[n]*1E-6; // Convert to true Z [met]

	} // Metallicity values read!
	fclose( fichero_ASCII ); //Close the ASCII file


	fichero_isochr_bin = fopen( BIN3,"rb"); //Open All_the_isochrones2.bin
	fichero_stat_bin = fopen( EXPL3,"rb"); // Open the statistics of all the isochr.

	//Since logmi_mi and Logmi_MAX are equal for all of the isochrones, the IMF would be the same:
	/* Probability given the Kroupa IMF */
	for(star=0; star<Max_Number_of_logmasses_per_isochrone;star++){
		mi[star] = pow(10., Logmi_MIN + dlogm*star);
	};


// Loop over the particles
	for(particle=0;particle<Nparticles;particle++){

		// Store the parameters of the considered particle:
		Z_p = Z[particle];
		logAge_p = logAge[particle];
		Mass_p = ParticleMass[particle];
		ex31_p = ex31[particle];
		distance_correction = 10.+5.*log10(dist[particle]);
		rel_sigma_cutoff_p = 1000./dist[particle]/total_correction[particle]*REL_SIGMA_CUTOFF;
		

		// Avoid out of range values:
		logAge_p = (logAge_p<logAge_min? logAge_min: logAge_p);
		logAge_p = (logAge_p>logAge_max? logAge_max: logAge_p);

		Z_p = max(Z_p, Z_set[0]);
		Z_p = min(Z_p,Z_set[Number_of_Z-1]);


		// Get the index of that metallicity value (the Z sampling is not homogeneous):
		i_Z = -1; //Initial value
		do{
			i_Z++;
		}while( Z_set[i_Z]<=Z_p );
		i_Z--;
		i_Z = min(i_Z, Number_of_Z-1);
		i_Z = max(i_Z, 0);

		/* ++++++++++++++++++++++++++++++++ LEFT DOWN POINT ++++++++++++++++++++++++++++++++++++++++*/

		//Assume that the logAge grid is regular (from logAge_min to logAge_max with step logAge_step)
		i_logAge = (int)((Number_of_logAge-1)*(logAge_p-logAge_min)/(logAge_max-logAge_min));
		i_isochrone = i_logAge + Number_of_logAge*i_Z;

		//Weights:
		wLR = Z_p-Z_set[i_Z]; //For the Left-Down point of the grid


		// Read the following information about the selected isochrone		
		fseek( fichero_stat_bin, 40*i_isochrone+16, SEEK_SET);// 40 bytes = 4 bytes*10 columns
//		skip logmi_*
//		fread(&Logmi_MIN,4,1,fichero_stat_bin); // min of the initial mass (of the isochrone)
//		fread(&Logmi_MAX,4,1,fichero_stat_bin); // max of the initial mass (of the isochrone)

		fread(&starting_point_of_the_isochrone,4,1,fichero_stat_bin); //Starting position (inclusive)
		fread(&Number_of_logmasses_in_the_isochrone,4,1,fichero_stat_bin);// Rows (stars) that belong to this isochrone

		//Call to the Kroupa IMF function
		mL = pow(10., Logmi_MIN + dlogm*(Number_of_logmasses_in_the_isochrone-1));
		KroupaIMF( mi, Max_Number_of_logmasses_per_isochrone, mL, N_mi); //N_mi means number of masses between mi and mi+dm (egde effects included)

		// Initialize the variables:
		aux = 0.; // Quantity used to estimate error_pi
		// Initialize the Gaia error:
		aux = 0.; // Quantity used to estimate error_pi
		errorpi_LD = 0.; // Sigma_pi
		Cum_N_mi = 0.; //New norm of N_mi_p
		nobstars_LD = 0.;

		// Go to the desired isochrone
		fseek( fichero_isochr_bin, 8*starting_point_of_the_isochrone, SEEK_SET); //8=2*4 bytes = np.float32 bits*2 columns


		// Load V and V-Ic from the isochrone given by (Z, logAge), and finish P(m_i)
		for(star=0;star<Number_of_logmasses_in_the_isochrone;star++){
			// Get the absolute magnitudes
			fread(&V_star,4,1, fichero_isochr_bin); // Absolute V
			fread(&VmIc_star,4,1, fichero_isochr_bin); // Absolute V-Ic

			// From absolute to apparent
			V_star = V_star + distance_correction + ex31_p;
			VmIc_star = VmIc_star + 355./900.0*ex31_p; // 355/900 = 0.394444... = 1.962/3.24 (ratio between V and Ic extinction corrections)

			// Estimate the G band
			G_star = V_star-0.01746+0.008092*VmIc_star-0.2810*VmIc_star*VmIc_star+0.03655*VmIc_star*VmIc_star*VmIc_star; // G band (Tony Brown's formula)
		
			// Would this star be observed with Gaia?
			is_selected = GaiaSelectionFunction(G_star, G_limits[0],G_limits[1]); // 1= Yes, 0=No
			is_selected *= IndicatorFunction( VmIc_star, VmIc_cutoffmin, VmIc_cutoffmax); // 1= Yes, 0=No
			if(!is_selected) continue; // Go to the next star

			// Calculate the error for this star and add it to the particle contribution:
			aux = max(0.063095734448019303, pow(10.,0.4*G_star-6.)); // 0.06309...=10**(0.4*(12.0-15))
			// aux = z in www.cosmos.esa.int/web/gaia/science-performance#photometric performance
			// Including the is_selected in the power we avoid out-of-float range values when the extinction is extremely high
			aux =  sqrt(-1.631 + 680.766*aux + 32.732*aux*aux)*(0.986 + 0.014*VmIc_star)/1.2; // Here aux = sigma_pi for the considered star. The 20% of science margin is already included

			// Exclude stars whose relative parallax errors are above the selected threshold:
			is_selected = is_selected*(aux<=rel_sigma_cutoff_p);

			N_mi[star]*=is_selected; //Sometimes is_selected is a fraction

			Cum_N_mi += N_mi[star]; //Renormalisation of N_mi due to the out-of-G range stars. Cum_N_mi is the total number of stars observed (if Mass_p = 1)
			errorpi_LD += aux*N_mi[star]; // Add the contribution to the average


		}; // End of the star-loop

		// Save the number of stars observed for this particle
		nobstars_LD = Mass_p*Cum_N_mi;

		// Correction due to the G-band exclusion
		Cum_N_mi = Cum_N_mi -1.0*(Cum_N_mi<=0);//If no stars are observed, return a negative value of sigma_pi
		errorpi_LD = total_correction[particle]*errorpi_LD/Cum_N_mi; //Include the Calibration, the scanning law and the epoch corrections
		errorpi_LD = errorpi_LD*(Cum_N_mi>0.) - 99000.00*(Cum_N_mi<=0.);//If no stars are observed, return a negative value of sigma_pi

	
		/* ++++++++++++++++++++++++++++++++ RIGHT DOWN POINT ++++++++++++++++++++++++++++++++++++++++*/

		//i_logAge does not change but i_Z...
		i_Z = i_Z + 1*(i_Z!=(Number_of_Z-1));
		
		i_isochrone = i_logAge + Number_of_logAge*i_Z;

		//Weights:
		wRL = Z_set[i_Z]-Z_p; //For the Right-Down point of the grid

		// Read the following information about the selected isochrone		
		fseek( fichero_stat_bin, 40*i_isochrone+16, SEEK_SET);// 40 bytes = 4 bytes*10 columns
//		skip logmi_*
//		fread(&Logmi_MIN,4,1,fichero_stat_bin); // min of the initial mass (of the isochrone)
//		fread(&Logmi_MAX,4,1,fichero_stat_bin); // max of the initial mass (of the isochrone)

		fread(&starting_point_of_the_isochrone,4,1,fichero_stat_bin); //Starting position (inclusive)
		fread(&Number_of_logmasses_in_the_isochrone,4,1,fichero_stat_bin);// Rows (stars) that belong to this isochrone

		//Call to the Kroupa IMF function
		mL = pow(10., Logmi_MIN + dlogm*(Number_of_logmasses_in_the_isochrone-1));
		KroupaIMF( mi, Max_Number_of_logmasses_per_isochrone, mL, N_mi); //N_mi means number of masses between mi and mi+dm (egde effects included)

		// Initialize the variables:
		aux = 0.; // Quantity used to estimate error_pi
		// Initialize the Gaia error:
		aux = 0.; // Quantity used to estimate error_pi
		errorpi_RD = 0.; // Sigma_pi
		Cum_N_mi = 0.; //New norm of N_mi_p
		nobstars_RD = 0.;

		// Go to the desired isochrone
		fseek( fichero_isochr_bin, 8*starting_point_of_the_isochrone, SEEK_SET); //8=2*4 bytes = np.float32 bits*2 columns


		// Load V and V-Ic from the isochrone given by (Z, logAge), and finish P(m_i)
		for(star=0;star<Number_of_logmasses_in_the_isochrone;star++){
			// Get the absolute magnitudes
			fread(&V_star,4,1, fichero_isochr_bin); // Absolute V
			fread(&VmIc_star,4,1, fichero_isochr_bin); // Absolute V-Ic

			// From absolute to apparent
			V_star = V_star + distance_correction + ex31_p;
			VmIc_star = VmIc_star + 355./900.0*ex31_p; // 355/900 = 0.394444... = 1.962/3.24 (ratio between V and Ic extinction corrections)

			// Estimate the G band
			G_star = V_star-0.01746+0.008092*VmIc_star-0.2810*VmIc_star*VmIc_star+0.03655*VmIc_star*VmIc_star*VmIc_star; // G band (Tony Brown's formula)
		
			// Would this star be observed with Gaia?
			is_selected = GaiaSelectionFunction(G_star, G_limits[0],G_limits[1]); // 1= Yes, 0=No
			is_selected *= IndicatorFunction( VmIc_star, VmIc_cutoffmin, VmIc_cutoffmax); // 1= Yes, 0=No

			if(!is_selected) continue; // Go to the next star

			// Calculate the error for this star and add it to the particle contribution:
			aux = max(0.063095734448019303, pow(10.,0.4*G_star-6.)); // 0.06309...=10**(0.4*(12.0-15))
			// aux = z in www.cosmos.esa.int/web/gaia/science-performance#photometric performance
			// Including the is_selected in the power we avoid out-of-float range values when the extinction is extremely high
			aux =  sqrt(-1.631 + 680.766*aux + 32.732*aux*aux)*(0.986 + 0.014*VmIc_star)/1.2; // Here aux = sigma_pi for the considered star. The 20% of science margin is already included

			// Exclude stars whose relative parallax errors are above the selected threshold:
			is_selected = is_selected*(aux<=rel_sigma_cutoff_p);

			N_mi[star]*=is_selected; //Sometimes is_selected is a fraction

			Cum_N_mi += N_mi[star]; //Renormalisation of N_mi due to the out-of-G range stars. Cum_N_mi is the total number of stars observed (if Mass_p = 1)
			errorpi_RD += aux*N_mi[star]; // Add the contribution to the average


		}; // End of the star-loop

		// Save the number of stars observed for this particle
		nobstars_RD = Mass_p*Cum_N_mi;

		// Correction due to the G-band exclusion
		Cum_N_mi = Cum_N_mi -1.0*(Cum_N_mi<=0);//If no stars are observed, return a negative value of sigma_pi
		errorpi_RD = total_correction[particle]*errorpi_RD/Cum_N_mi; //Include the Calibration, the scanning law and the epoch corrections
		errorpi_RD = errorpi_RD*(Cum_N_mi>0.) - 99000.00*(Cum_N_mi<=0.);//If no stars are observed, return a negative value of sigma_pi


		/* ++++++++++++++++++++++++++++++++ RIGHT UP POINT ++++++++++++++++++++++++++++++++++++++++*/

		//i_Z as in the previous case, but i_logAge has increase (or not)...
		i_logAge = i_logAge + 1*(i_logAge!=(Number_of_logAge-1));
		i_isochrone = i_logAge + Number_of_logAge*i_Z;

		//Weights:
		wUD = logAge_min - i_logAge*logAge_step - logAge_p; //For the Right-Up point of the grid

		// Read the following information about the selected isochrone		
		fseek( fichero_stat_bin, 40*i_isochrone+16, SEEK_SET);// 40 bytes = 4 bytes*10 columns
//		skip logmi_*
//		fread(&Logmi_MIN,4,1,fichero_stat_bin); // min of the initial mass (of the isochrone)
//		fread(&Logmi_MAX,4,1,fichero_stat_bin); // max of the initial mass (of the isochrone)

		fread(&starting_point_of_the_isochrone,4,1,fichero_stat_bin); //Starting position (inclusive)
		fread(&Number_of_logmasses_in_the_isochrone,4,1,fichero_stat_bin);// Rows (stars) that belong to this isochrone

		//Call to the Kroupa IMF function
		mL = pow(10., Logmi_MIN + dlogm*(Number_of_logmasses_in_the_isochrone-1));
		KroupaIMF( mi, Max_Number_of_logmasses_per_isochrone, mL, N_mi); //N_mi means number of masses between mi and mi+dm (egde effects included)

		// Initialize the variables:
		aux = 0.; // Quantity used to estimate error_pi
		// Initialize the Gaia error:
		aux = 0.; // Quantity used to estimate error_pi
		errorpi_RU = 0.; // Sigma_pi
		Cum_N_mi = 0.; //New norm of N_mi_p
		nobstars_RU = 0.;

		// Go to the desired isochrone
		fseek( fichero_isochr_bin, 8*starting_point_of_the_isochrone, SEEK_SET); //8=2*4 bytes = np.float32 bits*2 columns


		// Load V and V-Ic from the isochrone given by (Z, logAge), and finish P(m_i)
		for(star=0;star<Number_of_logmasses_in_the_isochrone;star++){
			// Get the absolute magnitudes
			fread(&V_star,4,1, fichero_isochr_bin); // Absolute V
			fread(&VmIc_star,4,1, fichero_isochr_bin); // Absolute V-Ic

			// From absolute to apparent
			V_star = V_star + distance_correction + ex31_p;
			VmIc_star = VmIc_star + 355./900.0*ex31_p; // 355/900 = 0.394444... = 1.962/3.24 (ratio between V and Ic extinction corrections)

			// Estimate the G band
			G_star = V_star-0.01746+0.008092*VmIc_star-0.2810*VmIc_star*VmIc_star+0.03655*VmIc_star*VmIc_star*VmIc_star; // G band (Tony Brown's formula)
		
			// Would this star be observed with Gaia?
			is_selected = GaiaSelectionFunction(G_star, G_limits[0],G_limits[1]); // 1= Yes, 0=No
			is_selected *= IndicatorFunction( VmIc_star, VmIc_cutoffmin, VmIc_cutoffmax); // 1= Yes, 0=No
			if(!is_selected) continue; // Go to the next star

			// Calculate the error for this star and add it to the particle contribution:
			aux = max(0.063095734448019303, pow(10.,0.4*G_star-6.)); // 0.06309...=10**(0.4*(12.0-15))
			// aux = z in www.cosmos.esa.int/web/gaia/science-performance#photometric performance
			// Including the is_selected in the power we avoid out-of-float range values when the extinction is extremely high
			aux =  sqrt(-1.631 + 680.766*aux + 32.732*aux*aux)*(0.986 + 0.014*VmIc_star)/1.2; // Here aux = sigma_pi for the considered star. The 20% of science margin is already included

			// Exclude stars whose relative parallax errors are above the selected threshold:
			is_selected = is_selected*(aux<=rel_sigma_cutoff_p);

			N_mi[star]*=is_selected; //Sometimes is_selected is a fraction

			Cum_N_mi += N_mi[star]; //Renormalisation of N_mi due to the out-of-G range stars. Cum_N_mi is the total number of stars observed (if Mass_p = 1)
			errorpi_RU += aux*N_mi[star]; // Add the contribution to the average


		}; // End of the star-loop

		// Save the number of stars observed for this particle
		nobstars_RU = Mass_p*Cum_N_mi;

		// Correction due to the G-band exclusion
		Cum_N_mi = Cum_N_mi -1.0*(Cum_N_mi<=0);//If no stars are observed, return a negative value of sigma_pi
		errorpi_RU = total_correction[particle]*errorpi_RU/Cum_N_mi; //Include the Calibration, the scanning law and the epoch corrections
		errorpi_RU = errorpi_RU*(Cum_N_mi>0.) - 99000.00*(Cum_N_mi<=0.);//If no stars are observed, return a negative value of sigma_pi


		/* ++++++++++++++++++++++++++++++++ LEFT UP POINT ++++++++++++++++++++++++++++++++++++++++*/

		//i_logAge as in the previous case, but i_Z decreases (or not)...
		i_Z = i_Z - 1*(i_Z>0);
		i_isochrone = i_logAge + Number_of_logAge*i_Z;

		//Weights:
		wDU = logAge_p - logAge_min - i_logAge*logAge_step; //For the Left-Up point of the grid

		// Read the following information about the selected isochrone		
		fseek( fichero_stat_bin, 40*i_isochrone+16, SEEK_SET);// 40 bytes = 4 bytes*10 columns
//		skip logmi_*
//		fread(&Logmi_MIN,4,1,fichero_stat_bin); // min of the initial mass (of the isochrone)
//		fread(&Logmi_MAX,4,1,fichero_stat_bin); // max of the initial mass (of the isochrone)

		fread(&starting_point_of_the_isochrone,4,1,fichero_stat_bin); //Starting position (inclusive)
		fread(&Number_of_logmasses_in_the_isochrone,4,1,fichero_stat_bin);// Rows (stars) that belong to this isochrone

		//Call to the Kroupa IMF function
		mL = pow(10., Logmi_MIN + dlogm*(Number_of_logmasses_in_the_isochrone-1));
		KroupaIMF( mi, Max_Number_of_logmasses_per_isochrone, mL, N_mi); //N_mi means number of masses between mi and mi+dm (egde effects included)

		// Initialize the Gaia error:
		aux = 0.; // Quantity used to estimate error_pi
		errorpi_LU = 0.; // Sigma_pi
		Cum_N_mi = 0.; //New norm of N_mi_p
		nobstars_LU = 0.;

		// Go to the desired isochrone
		fseek( fichero_isochr_bin, 8*starting_point_of_the_isochrone, SEEK_SET); //8=2*4 bytes = np.float32 bits*2 columns


		// Load V and V-Ic from the isochrone given by (Z, logAge), and finish P(m_i)
		for(star=0;star<Number_of_logmasses_in_the_isochrone;star++){
			// Get the absolute magnitudes
			fread(&V_star,4,1, fichero_isochr_bin); // Absolute V
			fread(&VmIc_star,4,1, fichero_isochr_bin); // Absolute V-Ic

			// From absolute to apparent
			V_star = V_star + distance_correction + ex31_p;
			VmIc_star = VmIc_star + 355./900.0*ex31_p; // 355/900 = 0.394444... = 1.962/3.24 (ratio between V and Ic extinction corrections)

			// Estimate the G band
			G_star = V_star-0.01746+0.008092*VmIc_star-0.2810*VmIc_star*VmIc_star+0.03655*VmIc_star*VmIc_star*VmIc_star; // G band (Tony Brown's formula)
		
			// Would this star be observed with Gaia?
			is_selected = GaiaSelectionFunction(G_star, G_limits[0],G_limits[1]); // 1= Yes, 0=No
			is_selected *= IndicatorFunction( VmIc_star, VmIc_cutoffmin, VmIc_cutoffmax); // 1= Yes, 0=No
			if(!is_selected) continue; // Go to the next star

			// Calculate the error for this star and add it to the particle contribution:
			aux = max(0.063095734448019303, pow(10.,0.4*G_star-6.)); // 0.06309...=10**(0.4*(12.0-15))
			// aux = z in www.cosmos.esa.int/web/gaia/science-performance#photometric performance
			// Including the is_selected in the power we avoid out-of-float range values when the extinction is extremely high
			aux =  sqrt(-1.631 + 680.766*aux + 32.732*aux*aux)*(0.986 + 0.014*VmIc_star)/1.2; // Here aux = sigma_pi for the considered star. The 20% of science margin is already included

			// Exclude stars whose relative parallax errors are above the selected threshold:
			is_selected = is_selected*(aux<=rel_sigma_cutoff_p);

			N_mi[star]*=is_selected; //Sometimes is_selected is a fraction

			Cum_N_mi += N_mi[star]; //Renormalisation of N_mi due to the out-of-G range stars. Cum_N_mi is the total number of stars observed (if Mass_p = 1)
			errorpi_LU += aux*N_mi[star]; // Add the contribution to the average


		}; // End of the star-loop

		// Save the number of stars observed for this particle
		nobstars_LU = Mass_p*Cum_N_mi;

		// Correction due to the G-band exclusion
		Cum_N_mi = Cum_N_mi -1.0*(Cum_N_mi<=0);//If no stars are observed, return a negative value of sigma_pi
		errorpi_LU = total_correction[particle]*errorpi_LU/Cum_N_mi; //Include the Calibration, the scanning law and the epoch corrections
		errorpi_LU = errorpi_LU*(Cum_N_mi>0.) - 99000.00*(Cum_N_mi<=0.);//If no stars are observed, return a negative value of sigma_pi

		/* ++++++++++++++++++++++++++++++++ WEIGHTING ++++++++++++++++++++++++++++++++++++++++*/
		if(wLR==-wRL){
			wLR = 1.;
			wRL = 0.;
		}else{
			wLR = wRL/(wLR+wRL);
			wRL = 1.-wLR;
		};

		if(wUD==-wDU){
			wUD = 1.;
			wDU = 0.;
		}else{
			wUD = wDU/(wUD+wDU);
			wDU = 1.-wUD;
		};

		error_pi[particle] = wDU*wLR*errorpi_LD + wDU*wRL*errorpi_RD + wUD*wRL*errorpi_RU +wUD*wLR*errorpi_LU;
		error_pi[particle] *= 0.001; //From uas to mas
		nobstars[particle] =  wDU*wLR*nobstars_LD + wDU*wRL*nobstars_RD + wUD*wRL*nobstars_RU +wUD*wLR*nobstars_LU;

		if(particle%10000==0) fprintf(stderr,"Particle %i of %i has finished (%.3f/100 completed)\n",particle, Nparticles, particle*100./Nparticles);

	}; // End of the particle

	fclose( fichero_stat_bin );
	fclose( fichero_isochr_bin );

}//End of SigmaPiGaiaKroupa_4Points
//-----------------------------------------------------------------------------------------------------------------//





//-----------------------------------------------------------------------------------------------------------------//
/* PhotometryGaia_Kroupa */
void PhotometryGaia_Kroupa(float *Z, float *logAge, float *ParticleMass, float *dist, float *ex31, float *mean_G, float *mean_BP, float *mean_RP, float *error_G, float *error_BP, float *error_RP, float *nobstars, int Nparticles, float *G_limits){
/* Since ex31 is provided, lon and lat are not needed*/
// UNITS: Z is a fraction, Ages in yrs, distances in kpc, ex31 in mag, errors and magnitudes in mag
	int n, particle, star; // Counters
	float Z_set[34]; // Set of metallicities (the grid is not regular in Z)
	float aux; //auxiliary variable
	int is_selected; // 1 if the G mag is in the Gaia range, 0 otherwise

	// Values of the fitting parameters for the photometric errors:
	double aBP, bBP, cBP;
	double aRP, bRP, cRP;

	/* Values for each particle */
	int i_Z, i_logAge, i_isochrone; //Index of the metallicity, logAge, isochrone
	float Z_p, logAge_p, Mass_p, ex31_p; // Values for one selected particle
	float distance_correction; // 10+5*log10(d_kpc)

	/* Values for each star */
	float V_star, G_star, VmIc_star, VmIc_star2; //Apparent V mag, G mag and colour difference (**2)
	float BP_star, RP_star;

	/* Values for each isochrone */
	int starting_point_of_the_isochrone, Number_of_logmasses_in_the_isochrone;

	/* IMF */
	const float dlogm = (Logmi_MAX-Logmi_MIN)/(1.0*Max_Number_of_logmasses_per_isochrone); // bin size
	float mi[Max_Number_of_logmasses_per_isochrone], N_mi[Max_Number_of_logmasses_per_isochrone]; //Masses and global IMF
	float Cum_N_mi;
	float mL; //Maximum initial mass


	// Files:
	FILE *fichero_ASCII; // ASCII file with the information about the isochrones
	FILE *fichero_stat_bin; //Binary file with the statistics of all the isochrones
	FILE *fichero_isochr_bin; //Binary file with all the isochroness

	// Always in the Gaia range
	G_limits[0] = max(G_limits[0], Gmin);
	G_limits[1] = min(G_limits[1], Gmax);

	/* Read the metallicity values of the grid */
	fichero_ASCII = fopen( ISONO,"r"); //Open IsoNo.txt
	for (n=0; n<Number_of_Z;n++){
		fscanf( fichero_ASCII, "%f\n", &Z_set[n]); // Read the decimal part of the metallicity 
		Z_set[n] = Z_set[n]*1E-6; // Convert to true Z [met]

	} // Metallicity values read!
	fclose( fichero_ASCII ); //Close the ASCII file


	fichero_isochr_bin = fopen( BIN3,"rb"); //Open All_the_isochrones3.bin
	fichero_stat_bin = fopen( EXPL3,"rb"); // Open the statistics of all the isochr.

	//Since logmi_mi and Logmi_MAX are equal for all of the isochrones, the IMF would be the same:
	/* Probability given the Kroupa IMF */
	for(star=0; star<Max_Number_of_logmasses_per_isochrone;star++){
		mi[star] = pow(10., Logmi_MIN + dlogm*star);
	};
//fprintf(stderr,"%f %f %f \n", Logmi_MIN, Logmi_MAX, dlogm);


// Loop over the particles
	for(particle=0;particle<Nparticles;particle++){

		// Store the parameters of the considered particle:
		Z_p = Z[particle];
		logAge_p = logAge[particle];
		ex31_p = ex31[particle];
		distance_correction = 10.+5.*log10(dist[particle]);
		Mass_p = ParticleMass[particle];
		

		// Avoid out of range values:
		logAge_p = (logAge_p<logAge_min? logAge_min: logAge_p);
		logAge_p = (logAge_p>logAge_max? logAge_max: logAge_p);

		Z_p = max(Z_p, Z_set[0]);
		Z_p = min(Z_p,Z_set[Number_of_Z-1]);


		// Get the index of that metallicity value (the Z sampling is not homogeneous):
		i_Z = -1; //Initial value
		do{
			i_Z++;
		}while( Z_set[i_Z]<=Z_p );
		i_Z--;
		i_Z = min(i_Z, Number_of_Z-1);
		i_Z = max(i_Z, 0);


		//Assume that the logAge grid is regular (from logAge_min to logAge_max with step logAge_step)
		i_logAge = (int)((Number_of_logAge-1)*(logAge_p-logAge_min)/(logAge_max-logAge_min));
		i_isochrone = i_logAge + (Number_of_logAge)*i_Z;

		fseek( fichero_stat_bin, 40*i_isochrone+16, SEEK_SET);// 40 bytes = 4 bytes*10 columns

		fread(&starting_point_of_the_isochrone,4,1,fichero_stat_bin); //Starting position (inclusive)
		fread(&Number_of_logmasses_in_the_isochrone,4,1,fichero_stat_bin);// Rows (stars) that belong to this isochrone


		//Call to the Kroupa IMF function
		mL = pow(10., Logmi_MIN + dlogm*(Number_of_logmasses_in_the_isochrone-1));

		KroupaIMF( mi, Max_Number_of_logmasses_per_isochrone, mL, N_mi); //N_mi means number of masses between mi and mi+dm (egde effects included)

		// Initialize the variables:
		aux = 0.; // Quantity used to estimate the errors
		Cum_N_mi = 0.; //New norm of N_mi_p
		error_G[particle] = 0.; // sigma_G
		error_BP[particle] = 0.; // sigma_BP
		error_RP[particle] = 0.; // sigma_RP
		mean_G[particle] = 0.; // mean_G
		mean_BP[particle] = 0.; // mean_BP
		mean_RP[particle] = 0.; // mean_RP
		nobstars[particle] = 0.;

		// Go to the desired isochrone
		fseek( fichero_isochr_bin, 8*starting_point_of_the_isochrone, SEEK_SET); //8=2*4 bytes = np.float32 bits*2 columns


		// Load V and V-Ic from the isochrone given by (Z, logAge), and finish P(m_i)
		for(star=0;star<Number_of_logmasses_in_the_isochrone;star++){
			// Get the absolute magnitudes
			fread(&V_star,4,1, fichero_isochr_bin); // Absolute V
			fread(&VmIc_star,4,1, fichero_isochr_bin); // Absolute V-Ic

			// From absolute to apparent
			V_star = V_star + distance_correction + ex31_p;
			VmIc_star = VmIc_star + 355./900.0*ex31_p; // 355/900 = 0.394444... = 1.962/3.24 (ratio between V and Ic extinction corrections)

			// Estimate the G band
			G_star = V_star-0.01746+0.008092*VmIc_star-0.2810*VmIc_star*VmIc_star + 0.03655*VmIc_star*VmIc_star*VmIc_star; // G band (Jordi et al. +10 formula)
		
			// Would this star be observed with Gaia?
			is_selected = GaiaSelectionFunction(G_star, G_limits[0],G_limits[1]); // 1= Yes, 0=No
			is_selected *= IndicatorFunction( VmIc_star, VmIc_cutoffmin, VmIc_cutoffmax); // 1= Yes, 0=No
			if(!is_selected) continue; // Go to the next star
			N_mi[star]*=is_selected; //Sometimes is_selected is a fraction

			// Estimate the BP/RP bands
			BP_star = V_star -0.05204+.4830*VmIc_star-0.2001*VmIc_star*VmIc_star+0.02186*VmIc_star*VmIc_star*VmIc_star; // G_BP band
			RP_star = V_star +0.0002428-0.8675*VmIc_star-0.02866*VmIc_star*VmIc_star+0.0000*VmIc_star*VmIc_star*VmIc_star; // G_RP band


			// Error for G:
			aux = max(0.0630957344480193, pow(10.,0.4*G_star-6.)); // 0.06309...=10**(0.4*(12.0-15))
			aux = 0.04895*aux*aux + 1.8633*aux + 0.0001985;
			aux = 0.001*sqrt(aux/70.);
			error_G[particle] += aux*N_mi[star]; // Add the contribution to the particle
			mean_G[particle] += G_star*N_mi[star];

			// Error for G_BP
			aux = max(0.025118864315095794, pow(10.,0.4*G_star-6.)); // 0.06309...=10**(0.4*(11.0-15))

			aBP = 0.355123*VmIc_star + 1.043270;
			bBP = 0.195768*VmIc_star + 1.465592;
			cBP = - 0.205807*VmIc_star - 1.866968;
			aRP = - 0.636628*VmIc_star + 1.615927;
			bRP = - 0.318499*VmIc_star + 1.783906;
			cRP = - 0.091569*VmIc_star - 3.042268;

			VmIc_star2 = VmIc_star*VmIc_star;

			aBP += 0.044390*VmIc_star2;
			bBP += 0.018878*VmIc_star2;
			cBP += 0.060769*VmIc_star2;
			aRP += 0.114126*VmIc_star2;
			bRP += 0.057112*VmIc_star2;
			cRP += 0.027352*VmIc_star2;

			VmIc_star2 = VmIc_star2*VmIc_star; //Now VmIc_star2 stands for (VmIc_star)**3

			aBP += -0.000562*VmIc_star2;
			bBP += -0.000400*VmIc_star2;
			cBP += +0.000262*VmIc_star2;
			aRP += -0.007597*VmIc_star2;
			bRP += -0.003803*VmIc_star2;
			cRP += -0.001923*VmIc_star2;

			// Use VmIc_star2 as an auxiliar variable
			VmIc_star2 = aux;

			// First for BP:
			aux = pow(10., aBP)*aux*aux + pow(10., bBP)*aux + pow(10., cBP);
			aux = 0.001*sqrt(aux/70.);
			error_BP[particle] += aux*N_mi[star]; // Add the contribution to the particle
			mean_BP[particle] += BP_star*N_mi[star];

			// Then for RP:
			aux = VmIc_star2;
			aux = pow(10., aRP)*aux*aux + pow(10., bRP)*aux + pow(10., cRP);
			aux = 0.001*sqrt(aux/70.);
			error_RP[particle] += aux*N_mi[star]; // Add the contribution to the particle
			mean_RP[particle] += RP_star*N_mi[star];

			Cum_N_mi += N_mi[star]; //Renormalisation of N_mi due to the out-of-G range stars. Cum_N_mi is the total number of stars observed (if Mass_p = 1)

		}; // End of the star-loop

		// Save the number of stars observed for this particle
		nobstars[particle] = Mass_p*Cum_N_mi;

		// Correction due to the G-band exclusion
		if(Cum_N_mi<=0.){
			//Bad values when no stars are observed
			mean_G[particle] = INFINITY; // mean_G
			mean_BP[particle] = INFINITY; // mean_BP
			mean_RP[particle] = INFINITY; // mean_RP
			error_G[particle] = -INFINITY; // sigma_G
			error_BP[particle] = -INFINITY; // sigma_BP
			error_RP[particle] = -INFINITY; // sigma_RP
		}else{
			mean_G[particle] = mean_G[particle]/Cum_N_mi; // mean_G
			mean_BP[particle] = mean_BP[particle]/Cum_N_mi; // mean_BP
			mean_RP[particle] = mean_RP[particle]/Cum_N_mi; // mean_RP
			error_G[particle] = error_G[particle]/Cum_N_mi; // sigma_G
			error_BP[particle] = error_BP[particle]/Cum_N_mi; // sigma_BP
			error_RP[particle] = error_RP[particle]/Cum_N_mi; // sigma_RP
		};

	
	}; // End of the particle

	fclose( fichero_stat_bin );
	fclose( fichero_isochr_bin );

}//End of PhotometryGaia_Kroupa
//-----------------------------------------------------------------------------------------------------------------//





//---------------------------------------PhotometryGaiaKroupa_4Points --------------------------------//
void PhotometryGaiaKroupa_4Points(float *Z, float *logAge, float *ParticleMass, float *dist, float *ex31, float *mean_G, float *mean_BP, float *mean_RP, float *error_G, float *error_BP, float *error_RP, float *nobstars, int Nparticles, float *G_limits){
/* Since ex31 is provided, lon and lat are not needed*/
// UNITS: Z is a fraction, Ages in yrs, distances in kpc, ex31 in mag, errors and magnitudes in mag
	int n, particle, star; // Counters
	float Z_set[34]; // Set of metallicities (the grid is not regular in Z)
	float aux; //auxiliary variable
	int is_selected; // 1 if the G mag is in the Gaia range, 0 otherwise

	// Values of the fitting parameters for the photometric errors:
	double aBP, bBP, cBP;
	double aRP, bRP, cRP;

	/* Values for each particle */
	int i_Z, i_logAge, i_isochrone; //Index of the metallicity, logAge, isochrone
	float Z_p, logAge_p, Mass_p, ex31_p; // Values for one selected particle
	float distance_correction; // 10+5*log10(d_kpc)

	/* Values for each star */
	float V_star, G_star, VmIc_star, VmIc_star2; //Apparent V mag, G mag and colour difference (**2)
	float BP_star, RP_star;

	/* Values for each isochrone */
	int starting_point_of_the_isochrone, Number_of_logmasses_in_the_isochrone;

	/* IMF */
	const float dlogm = (Logmi_MAX-Logmi_MIN)/(1.0*Max_Number_of_logmasses_per_isochrone); // bin size
	float mi[Max_Number_of_logmasses_per_isochrone], N_mi[Max_Number_of_logmasses_per_isochrone]; //Masses and global IMF
	float Cum_N_mi;
	float mL; //Maximum initial mass

	/* Weights and temporal errors */
	float nobstars_LD, nobstars_RD, nobstars_RU, nobstars_LU;
	float errorG_LD, errorG_RD, errorG_RU, errorG_LU;
	float errorBP_LD, errorBP_RD, errorBP_RU, errorBP_LU;
	float errorRP_LD, errorRP_RD, errorRP_RU, errorRP_LU;
	float meanG_LD, meanG_RD, meanG_RU, meanG_LU;
	float meanBP_LD, meanBP_RD, meanBP_RU, meanBP_LU;
	float meanRP_LD, meanRP_RD, meanRP_RU, meanRP_LU;
	float wLR, wRL, wDU, wUD; // wLR+wRL = wDU+wUD = 1.


	// Files:
	FILE *fichero_ASCII; // ASCII file with the information about the isochrones
	FILE *fichero_stat_bin; //Binary file with the statistics of all the isochrones
	FILE *fichero_isochr_bin; //Binary file with all the isochroness

	// Always in the Gaia range
	G_limits[0] = max(G_limits[0], Gmin);
	G_limits[1] = min(G_limits[1], Gmax);


	/* Read the metallicity values of the grid */
	fichero_ASCII = fopen( ISONO,"r"); //Open ISONO
	for (n=0; n<Number_of_Z;n++){
		fscanf( fichero_ASCII, "%f\n", &Z_set[n]); // Read the decimal part of the metallicity 
		Z_set[n] = Z_set[n]*1E-6; // Convert to true Z [met]

	} // Metallicity values read!
	fclose( fichero_ASCII ); //Close the ASCII file


	fichero_isochr_bin = fopen( BIN3,"rb"); //Open All_the_isochrones2.bin
	fichero_stat_bin = fopen( EXPL3,"rb"); // Open the statistics of all the isochr.

	//Since logmi_mi and Logmi_MAX are equal for all of the isochrones, the IMF would be the same:
	/* Probability given the Kroupa IMF */
	for(star=0; star<Max_Number_of_logmasses_per_isochrone;star++){
		mi[star] = pow(10., Logmi_MIN + dlogm*star);
	};


// Loop over the particles
	for(particle=0;particle<Nparticles;particle++){

		// Store the parameters of the considered particle:
		Z_p = Z[particle];
		logAge_p = logAge[particle];
		Mass_p = ParticleMass[particle];
		ex31_p = ex31[particle];
		distance_correction = 10.+5.*log10(dist[particle]);
		

		// Avoid out of range values:
		logAge_p = (logAge_p<logAge_min? logAge_min: logAge_p);
		logAge_p = (logAge_p>logAge_max? logAge_max: logAge_p);

		Z_p = max(Z_p, Z_set[0]);
		Z_p = min(Z_p,Z_set[Number_of_Z-1]);


		// Get the index of that metallicity value (the Z sampling is not homogeneous):
		i_Z = -1; //Initial value
		do{
			i_Z++;
		}while( Z_set[i_Z]<=Z_p );
		i_Z--;
		i_Z = min(i_Z, Number_of_Z-1);
		i_Z = max(i_Z, 0);

		/* ++++++++++++++++++++++++++++++++ LEFT DOWN POINT ++++++++++++++++++++++++++++++++++++++++*/

		//Assume that the logAge grid is regular (from logAge_min to logAge_max with step logAge_step)
		i_logAge = (int)((Number_of_logAge-1)*(logAge_p-logAge_min)/(logAge_max-logAge_min));
		i_isochrone = i_logAge + Number_of_logAge*i_Z;

		//Weights:
		wLR = Z_p-Z_set[i_Z]; //For the Left-Down point of the grid


		// Read the following information about the selected isochrone		
		fseek( fichero_stat_bin, 40*i_isochrone+16, SEEK_SET);// 40 bytes = 4 bytes*10 columns

		fread(&starting_point_of_the_isochrone,4,1,fichero_stat_bin); //Starting position (inclusive)
		fread(&Number_of_logmasses_in_the_isochrone,4,1,fichero_stat_bin);// Rows (stars) that belong to this isochrone


		//Call to the Kroupa IMF function
		mL = pow(10., Logmi_MIN + dlogm*(Number_of_logmasses_in_the_isochrone-1));

		KroupaIMF( mi, Max_Number_of_logmasses_per_isochrone, mL, N_mi); //N_mi means number of masses between mi and mi+dm (egde effects included)

		// Initialize the variables:
		aux = 0.; // Quantity used to estimate the errors
		Cum_N_mi = 0.; //New norm of N_mi_p
		errorG_LD = 0.; // sigma_G
		errorBP_LD = 0.; // sigma_BP
		errorRP_LD = 0.; // sigma_RP
		meanG_LD = 0.; // mean_G
		meanBP_LD = 0.; // mean_BP
		meanRP_LD = 0.; // mean_RP
		nobstars_LD = 0.;

		// Go to the desired isochrone
		fseek( fichero_isochr_bin, 8*starting_point_of_the_isochrone, SEEK_SET); //8=2*4 bytes = np.float32 bits*2 columns


		// Load V and V-Ic from the isochrone given by (Z, logAge), and finish P(m_i)
		for(star=0;star<Number_of_logmasses_in_the_isochrone;star++){
			// Get the absolute magnitudes
			fread(&V_star,4,1, fichero_isochr_bin); // Absolute V
			fread(&VmIc_star,4,1, fichero_isochr_bin); // Absolute V-Ic

			// From absolute to apparent
			V_star = V_star + distance_correction + ex31_p;
			VmIc_star = VmIc_star + 355./900.0*ex31_p; // 355/900 = 0.394444... = 1.962/3.24 (ratio between V and Ic extinction corrections)

			// Estimate the G band
			G_star = V_star-0.01746+0.008092*VmIc_star-0.2810*VmIc_star*VmIc_star + 0.03655*VmIc_star*VmIc_star*VmIc_star; // G band (Jordi et al. +10 formula)
		
			// Would this star be observed with Gaia?
			is_selected = GaiaSelectionFunction(G_star, G_limits[0],G_limits[1]); // 1= Yes, 0=No
			is_selected *= IndicatorFunction( VmIc_star, VmIc_cutoffmin, VmIc_cutoffmax); // 1= Yes, 0=No
			if(!is_selected) continue; // Go to the next star
			N_mi[star]*=is_selected; //Sometimes is_selected is a fraction

			// Estimate the BP/RP bands
			BP_star = V_star -0.05204+.4830*VmIc_star-0.2001*VmIc_star*VmIc_star+0.02186*VmIc_star*VmIc_star*VmIc_star; // G_BP band
			RP_star = V_star +0.0002428-0.8675*VmIc_star-0.02866*VmIc_star*VmIc_star+0.0000*VmIc_star*VmIc_star*VmIc_star; // G_RP band


			// Error for G:
			aux = max(0.0630957344480193, pow(10.,0.4*G_star-6.)); // 0.06309...=10**(0.4*(12.0-15))
			aux = 0.04895*aux*aux + 1.8633*aux + 0.0001985;
			aux = 0.001*sqrt(aux/70.);
			errorG_LD += aux*N_mi[star]; // Add the contribution to the particle
			meanG_LD += G_star*N_mi[star];

			// Error for G_BP
			aux = max(0.025118864315095794, pow(10.,0.4*G_star-6.)); // 0.06309...=10**(0.4*(11.0-15))

			aBP = 0.355123*VmIc_star + 1.043270;
			bBP = 0.195768*VmIc_star + 1.465592;
			cBP = - 0.205807*VmIc_star - 1.866968;
			aRP = - 0.636628*VmIc_star + 1.615927;
			bRP = - 0.318499*VmIc_star + 1.783906;
			cRP = - 0.091569*VmIc_star - 3.042268;

			VmIc_star2 = VmIc_star*VmIc_star;

			aBP += 0.044390*VmIc_star2;
			bBP += 0.018878*VmIc_star2;
			cBP += 0.060769*VmIc_star2;
			aRP += 0.114126*VmIc_star2;
			bRP += 0.057112*VmIc_star2;
			cRP += 0.027352*VmIc_star2;

			VmIc_star2 = VmIc_star2*VmIc_star; //Now VmIc_star2 stands for (VmIc_star)**3

			aBP += -0.000562*VmIc_star2;
			bBP += -0.000400*VmIc_star2;
			cBP += +0.000262*VmIc_star2;
			aRP += -0.007597*VmIc_star2;
			bRP += -0.003803*VmIc_star2;
			cRP += -0.001923*VmIc_star2;

			// Use VmIc_star2 as an auxiliar variable
			VmIc_star2 = aux;

			// First for BP:
			aux = pow(10., aBP)*aux*aux + pow(10., bBP)*aux + pow(10., cBP);
			aux = 0.001*sqrt(aux/70.);
			errorBP_LD += aux*N_mi[star]; // Add the contribution to the particle
			meanBP_LD += BP_star*N_mi[star];

			// Then for RP:
			aux = VmIc_star2;
			aux = pow(10., aRP)*aux*aux + pow(10., bRP)*aux + pow(10., cRP);
			aux = 0.001*sqrt(aux/70.);
			errorRP_LD += aux*N_mi[star]; // Add the contribution to the particle
			meanRP_LD += RP_star*N_mi[star];

			Cum_N_mi += N_mi[star]; //Renormalisation of N_mi due to the out-of-G range stars. Cum_N_mi is the total number of stars observed (if Mass_p = 1)

		}; // End of the star-loop

		// Save the number of stars observed for this particle
		nobstars_LD = Mass_p*Cum_N_mi;

		// Correction due to the G-band exclusion
		if(Cum_N_mi<=0.){
			//Bad values when no stars are observed
			meanG_LD = INFINITY; // mean_G
			meanBP_LD = INFINITY; // mean_BP
			meanRP_LD = INFINITY; // mean_RP
			errorG_LD = -INFINITY; // sigma_G
			errorBP_LD = -INFINITY; // sigma_BP
			errorRP_LD = -INFINITY; // sigma_RP
		}else{
			meanG_LD = meanG_LD/Cum_N_mi; // mean_G
			meanBP_LD = meanBP_LD/Cum_N_mi; // mean_BP
			meanRP_LD = meanRP_LD/Cum_N_mi; // mean_RP
			errorG_LD = errorG_LD/Cum_N_mi; // sigma_G
			errorBP_LD = errorBP_LD/Cum_N_mi; // sigma_BP
			errorRP_LD = errorRP_LD/Cum_N_mi; // sigma_RP
		};


	
		/* ++++++++++++++++++++++++++++++++ RIGHT DOWN POINT ++++++++++++++++++++++++++++++++++++++++*/

		//i_logAge does not change but i_Z...
		i_Z = i_Z + 1*(i_Z!=(Number_of_Z-1));
		
		i_isochrone = i_logAge + Number_of_logAge*i_Z;

		//Weights:
		wRL = Z_set[i_Z]-Z_p; //For the Right-Down point of the grid

		// Read the following information about the selected isochrone		
		fseek( fichero_stat_bin, 40*i_isochrone+16, SEEK_SET);// 40 bytes = 4 bytes*10 columns

		fread(&starting_point_of_the_isochrone,4,1,fichero_stat_bin); //Starting position (inclusive)
		fread(&Number_of_logmasses_in_the_isochrone,4,1,fichero_stat_bin);// Rows (stars) that belong to this isochrone


		//Call to the Kroupa IMF function
		mL = pow(10., Logmi_MIN + dlogm*(Number_of_logmasses_in_the_isochrone-1));

		KroupaIMF( mi, Max_Number_of_logmasses_per_isochrone, mL, N_mi); //N_mi means number of masses between mi and mi+dm (egde effects included)

		// Initialize the variables:
		aux = 0.; // Quantity used to estimate the errors
		Cum_N_mi = 0.; //New norm of N_mi_p
		errorG_RD = 0.; // sigma_G
		errorBP_RD = 0.; // sigma_BP
		errorRP_RD = 0.; // sigma_RP
		meanG_RD = 0.; // mean_G
		meanBP_RD = 0.; // mean_BP
		meanRP_RD = 0.; // mean_RP
		nobstars_RD = 0.;

		// Go to the desired isochrone
		fseek( fichero_isochr_bin, 8*starting_point_of_the_isochrone, SEEK_SET); //8=2*4 bytes = np.float32 bits*2 columns


		// Load V and V-Ic from the isochrone given by (Z, logAge), and finish P(m_i)
		for(star=0;star<Number_of_logmasses_in_the_isochrone;star++){
			// Get the absolute magnitudes
			fread(&V_star,4,1, fichero_isochr_bin); // Absolute V
			fread(&VmIc_star,4,1, fichero_isochr_bin); // Absolute V-Ic

			// From absolute to apparent
			V_star = V_star + distance_correction + ex31_p;
			VmIc_star = VmIc_star + 355./900.0*ex31_p; // 355/900 = 0.394444... = 1.962/3.24 (ratio between V and Ic extinction corrections)

			// Estimate the G band
			G_star = V_star-0.01746+0.008092*VmIc_star-0.2810*VmIc_star*VmIc_star + 0.03655*VmIc_star*VmIc_star*VmIc_star; // G band (Jordi et al. +10 formula)
		
			// Would this star be observed with Gaia?
			is_selected = GaiaSelectionFunction(G_star, G_limits[0],G_limits[1]); // 1= Yes, 0=No
			is_selected *= IndicatorFunction( VmIc_star, VmIc_cutoffmin, VmIc_cutoffmax); // 1= Yes, 0=No
			if(!is_selected) continue; // Go to the next star
			N_mi[star]*=is_selected; //Sometimes is_selected is a fraction

			// Estimate the BP/RP bands
			BP_star = V_star -0.05204+.4830*VmIc_star-0.2001*VmIc_star*VmIc_star+0.02186*VmIc_star*VmIc_star*VmIc_star; // G_BP band
			RP_star = V_star +0.0002428-0.8675*VmIc_star-0.02866*VmIc_star*VmIc_star+0.0000*VmIc_star*VmIc_star*VmIc_star; // G_RP band


			// Error for G:
			aux = max(0.0630957344480193, pow(10.,0.4*G_star-6.)); // 0.06309...=10**(0.4*(12.0-15))
			aux = 0.04895*aux*aux + 1.8633*aux + 0.0001985;
			aux = 0.001*sqrt(aux/70.);
			errorG_RD += aux*N_mi[star]; // Add the contribution to the particle
			meanG_RD += G_star*N_mi[star];

			// Error for G_BP
			aux = max(0.025118864315095794, pow(10.,0.4*G_star-6.)); // 0.06309...=10**(0.4*(11.0-15))

			aBP = 0.355123*VmIc_star + 1.043270;
			bBP = 0.195768*VmIc_star + 1.465592;
			cBP = - 0.205807*VmIc_star - 1.866968;
			aRP = - 0.636628*VmIc_star + 1.615927;
			bRP = - 0.318499*VmIc_star + 1.783906;
			cRP = - 0.091569*VmIc_star - 3.042268;

			VmIc_star2 = VmIc_star*VmIc_star;

			aBP += 0.044390*VmIc_star2;
			bBP += 0.018878*VmIc_star2;
			cBP += 0.060769*VmIc_star2;
			aRP += 0.114126*VmIc_star2;
			bRP += 0.057112*VmIc_star2;
			cRP += 0.027352*VmIc_star2;

			VmIc_star2 = VmIc_star2*VmIc_star; //Now VmIc_star2 stands for (VmIc_star)**3

			aBP += -0.000562*VmIc_star2;
			bBP += -0.000400*VmIc_star2;
			cBP += +0.000262*VmIc_star2;
			aRP += -0.007597*VmIc_star2;
			bRP += -0.003803*VmIc_star2;
			cRP += -0.001923*VmIc_star2;

			// Use VmIc_star2 as an auxiliar variable
			VmIc_star2 = aux;

			// First for BP:
			aux = pow(10., aBP)*aux*aux + pow(10., bBP)*aux + pow(10., cBP);
			aux = 0.001*sqrt(aux/70.);
			errorBP_RD += aux*N_mi[star]; // Add the contribution to the particle
			meanBP_RD += BP_star*N_mi[star];

			// Then for RP:
			aux = VmIc_star2;
			aux = pow(10., aRP)*aux*aux + pow(10., bRP)*aux + pow(10., cRP);
			aux = 0.001*sqrt(aux/70.);
			errorRP_RD += aux*N_mi[star]; // Add the contribution to the particle
			meanRP_RD += RP_star*N_mi[star];

			Cum_N_mi += N_mi[star]; //Renormalisation of N_mi due to the out-of-G range stars. Cum_N_mi is the total number of stars observed (if Mass_p = 1)

		}; // End of the star-loop

		// Save the number of stars observed for this particle
		nobstars_RD = Mass_p*Cum_N_mi;

		// Correction due to the G-band exclusion
		if(Cum_N_mi<=0.){
			//Bad values when no stars are observed
			meanG_RD = INFINITY; // mean_G
			meanBP_RD = INFINITY; // mean_BP
			meanRP_RD = INFINITY; // mean_RP
			errorG_RD = -INFINITY; // sigma_G
			errorBP_RD = -INFINITY; // sigma_BP
			errorRP_RD = -INFINITY; // sigma_RP
		}else{
			meanG_RD = meanG_RD/Cum_N_mi; // mean_G
			meanBP_RD = meanBP_RD/Cum_N_mi; // mean_BP
			meanRP_RD = meanRP_RD/Cum_N_mi; // mean_RP
			errorG_RD = errorG_RD/Cum_N_mi; // sigma_G
			errorBP_RD = errorBP_RD/Cum_N_mi; // sigma_BP
			errorRP_RD = errorRP_RD/Cum_N_mi; // sigma_RP
		};


		/* ++++++++++++++++++++++++++++++++ RIGHT UP POINT ++++++++++++++++++++++++++++++++++++++++*/

		//i_Z as in the previous case, but i_logAge has increase (or not)...
		i_logAge = i_logAge + 1*(i_logAge!=(Number_of_logAge-1));
		i_isochrone = i_logAge + Number_of_logAge*i_Z;

		//Weights:
		wUD = logAge_min - i_logAge*logAge_step - logAge_p; //For the Right-Up point of the grid

		// Read the following information about the selected isochrone		
		fseek( fichero_stat_bin, 40*i_isochrone+16, SEEK_SET);// 40 bytes = 4 bytes*10 columns

		fread(&starting_point_of_the_isochrone,4,1,fichero_stat_bin); //Starting position (inclusive)
		fread(&Number_of_logmasses_in_the_isochrone,4,1,fichero_stat_bin);// Rows (stars) that belong to this isochrone


		//Call to the Kroupa IMF function
		mL = pow(10., Logmi_MIN + dlogm*(Number_of_logmasses_in_the_isochrone-1));

		KroupaIMF( mi, Max_Number_of_logmasses_per_isochrone, mL, N_mi); //N_mi means number of masses between mi and mi+dm (egde effects included)

		// Initialize the variables:
		aux = 0.; // Quantity used to estimate the errors
		Cum_N_mi = 0.; //New norm of N_mi_p
		errorG_RU = 0.; // sigma_G
		errorBP_RU = 0.; // sigma_BP
		errorRP_RU = 0.; // sigma_RP
		meanG_RU = 0.; // mean_G
		meanBP_RU = 0.; // mean_BP
		meanRP_RU = 0.; // mean_RP
		nobstars_RU = 0.;

		// Go to the desired isochrone
		fseek( fichero_isochr_bin, 8*starting_point_of_the_isochrone, SEEK_SET); //8=2*4 bytes = np.float32 bits*2 columns


		// Load V and V-Ic from the isochrone given by (Z, logAge), and finish P(m_i)
		for(star=0;star<Number_of_logmasses_in_the_isochrone;star++){
			// Get the absolute magnitudes
			fread(&V_star,4,1, fichero_isochr_bin); // Absolute V
			fread(&VmIc_star,4,1, fichero_isochr_bin); // Absolute V-Ic

			// From absolute to apparent
			V_star = V_star + distance_correction + ex31_p;
			VmIc_star = VmIc_star + 355./900.0*ex31_p; // 355/900 = 0.394444... = 1.962/3.24 (ratio between V and Ic extinction corrections)

			// Estimate the G band
			G_star = V_star-0.01746+0.008092*VmIc_star-0.2810*VmIc_star*VmIc_star + 0.03655*VmIc_star*VmIc_star*VmIc_star; // G band (Jordi et al. +10 formula)
		
			// Would this star be observed with Gaia?
			is_selected = GaiaSelectionFunction(G_star, G_limits[0],G_limits[1]); // 1= Yes, 0=No
			is_selected *= IndicatorFunction( VmIc_star, VmIc_cutoffmin, VmIc_cutoffmax); // 1= Yes, 0=No
			if(!is_selected) continue; // Go to the next star
			N_mi[star]*=is_selected; //Sometimes is_selected is a fraction

			// Estimate the BP/RP bands
			BP_star = V_star -0.05204+.4830*VmIc_star-0.2001*VmIc_star*VmIc_star+0.02186*VmIc_star*VmIc_star*VmIc_star; // G_BP band
			RP_star = V_star +0.0002428-0.8675*VmIc_star-0.02866*VmIc_star*VmIc_star+0.0000*VmIc_star*VmIc_star*VmIc_star; // G_RP band


			// Error for G:
			aux = max(0.0630957344480193, pow(10.,0.4*G_star-6.)); // 0.06309...=10**(0.4*(12.0-15))
			aux = 0.04895*aux*aux + 1.8633*aux + 0.0001985;
			aux = 0.001*sqrt(aux/70.);
			errorG_RU += aux*N_mi[star]; // Add the contribution to the particle
			meanG_RU += G_star*N_mi[star];

			// Error for G_BP
			aux = max(0.025118864315095794, pow(10.,0.4*G_star-6.)); // 0.06309...=10**(0.4*(11.0-15))

			aBP = 0.355123*VmIc_star + 1.043270;
			bBP = 0.195768*VmIc_star + 1.465592;
			cBP = - 0.205807*VmIc_star - 1.866968;
			aRP = - 0.636628*VmIc_star + 1.615927;
			bRP = - 0.318499*VmIc_star + 1.783906;
			cRP = - 0.091569*VmIc_star - 3.042268;

			VmIc_star2 = VmIc_star*VmIc_star;

			aBP += 0.044390*VmIc_star2;
			bBP += 0.018878*VmIc_star2;
			cBP += 0.060769*VmIc_star2;
			aRP += 0.114126*VmIc_star2;
			bRP += 0.057112*VmIc_star2;
			cRP += 0.027352*VmIc_star2;

			VmIc_star2 = VmIc_star2*VmIc_star; //Now VmIc_star2 stands for (VmIc_star)**3

			aBP += -0.000562*VmIc_star2;
			bBP += -0.000400*VmIc_star2;
			cBP += +0.000262*VmIc_star2;
			aRP += -0.007597*VmIc_star2;
			bRP += -0.003803*VmIc_star2;
			cRP += -0.001923*VmIc_star2;

			// Use VmIc_star2 as an auxiliar variable
			VmIc_star2 = aux;

			// First for BP:
			aux = pow(10., aBP)*aux*aux + pow(10., bBP)*aux + pow(10., cBP);
			aux = 0.001*sqrt(aux/70.);
			errorBP_RU += aux*N_mi[star]; // Add the contribution to the particle
			meanBP_RU += BP_star*N_mi[star];

			// Then for RP:
			aux = VmIc_star2;
			aux = pow(10., aRP)*aux*aux + pow(10., bRP)*aux + pow(10., cRP);
			aux = 0.001*sqrt(aux/70.);
			errorRP_RU += aux*N_mi[star]; // Add the contribution to the particle
			meanRP_RU += RP_star*N_mi[star];

			Cum_N_mi += N_mi[star]; //Renormalisation of N_mi due to the out-of-G range stars. Cum_N_mi is the total number of stars observed (if Mass_p = 1)

		}; // End of the star-loop

		// Save the number of stars observed for this particle
		nobstars_RU = Mass_p*Cum_N_mi;

		// Correction due to the G-band exclusion
		if(Cum_N_mi<=0.){
			//Bad values when no stars are observed
			meanG_RU = INFINITY; // mean_G
			meanBP_RU = INFINITY; // mean_BP
			meanRP_RU = INFINITY; // mean_RP
			errorG_RU = -INFINITY; // sigma_G
			errorBP_RU = -INFINITY; // sigma_BP
			errorRP_RU = -INFINITY; // sigma_RP
		}else{
			meanG_RU = meanG_RU/Cum_N_mi; // mean_G
			meanBP_RU = meanBP_RU/Cum_N_mi; // mean_BP
			meanRP_RU = meanRP_RU/Cum_N_mi; // mean_RP
			errorG_RU = errorG_RU/Cum_N_mi; // sigma_G
			errorBP_RU = errorBP_RU/Cum_N_mi; // sigma_BP
			errorRP_RU = errorRP_RU/Cum_N_mi; // sigma_RP
		};


		/* ++++++++++++++++++++++++++++++++ LEFT UP POINT ++++++++++++++++++++++++++++++++++++++++*/

		//i_logAge as in the previous case, but i_Z decreases (or not)...
		i_Z = i_Z - 1*(i_Z>0);
		i_isochrone = i_logAge + Number_of_logAge*i_Z;

		//Weights:
		wDU = logAge_p - logAge_min - i_logAge*logAge_step; //For the Left-Up point of the grid

		// Read the following information about the selected isochrone		
		fseek( fichero_stat_bin, 40*i_isochrone+16, SEEK_SET);// 40 bytes = 4 bytes*10 columns

		fread(&starting_point_of_the_isochrone,4,1,fichero_stat_bin); //Starting position (inclusive)
		fread(&Number_of_logmasses_in_the_isochrone,4,1,fichero_stat_bin);// Rows (stars) that belong to this isochrone


		//Call to the Kroupa IMF function
		mL = pow(10., Logmi_MIN + dlogm*(Number_of_logmasses_in_the_isochrone-1));

		KroupaIMF( mi, Max_Number_of_logmasses_per_isochrone, mL, N_mi); //N_mi means number of masses between mi and mi+dm (egde effects included)

		// Initialize the variables:
		aux = 0.; // Quantity used to estimate the errors
		Cum_N_mi = 0.; //New norm of N_mi_p
		errorG_LU = 0.; // sigma_G
		errorBP_LU = 0.; // sigma_BP
		errorRP_LU = 0.; // sigma_RP
		meanG_LU = 0.; // mean_G
		meanBP_LU = 0.; // mean_BP
		meanRP_LU = 0.; // mean_RP
		nobstars_LU = 0.;

		// Go to the desired isochrone
		fseek( fichero_isochr_bin, 8*starting_point_of_the_isochrone, SEEK_SET); //8=2*4 bytes = np.float32 bits*2 columns


		// Load V and V-Ic from the isochrone given by (Z, logAge), and finish P(m_i)
		for(star=0;star<Number_of_logmasses_in_the_isochrone;star++){
			// Get the absolute magnitudes
			fread(&V_star,4,1, fichero_isochr_bin); // Absolute V
			fread(&VmIc_star,4,1, fichero_isochr_bin); // Absolute V-Ic

			// From absolute to apparent
			V_star = V_star + distance_correction + ex31_p;
			VmIc_star = VmIc_star + 355./900.0*ex31_p; // 355/900 = 0.394444... = 1.962/3.24 (ratio between V and Ic extinction corrections)

			// Estimate the G band
			G_star = V_star-0.01746+0.008092*VmIc_star-0.2810*VmIc_star*VmIc_star + 0.03655*VmIc_star*VmIc_star*VmIc_star; // G band (Jordi et al. +10 formula)
		
			// Would this star be observed with Gaia?
			is_selected = GaiaSelectionFunction(G_star, G_limits[0],G_limits[1]); // 1= Yes, 0=No
			is_selected *= IndicatorFunction( VmIc_star, VmIc_cutoffmin, VmIc_cutoffmax); // 1= Yes, 0=No
			if(!is_selected) continue; // Go to the next star
			N_mi[star]*=is_selected; //Sometimes is_selected is a fraction

			// Estimate the BP/RP bands
			BP_star = V_star -0.05204+.4830*VmIc_star-0.2001*VmIc_star*VmIc_star+0.02186*VmIc_star*VmIc_star*VmIc_star; // G_BP band
			RP_star = V_star +0.0002428-0.8675*VmIc_star-0.02866*VmIc_star*VmIc_star+0.0000*VmIc_star*VmIc_star*VmIc_star; // G_RP band


			// Error for G:
			aux = max(0.0630957344480193, pow(10.,0.4*G_star-6.)); // 0.06309...=10**(0.4*(12.0-15))
			aux = 0.04895*aux*aux + 1.8633*aux + 0.0001985;
			aux = 0.001*sqrt(aux/70.);
			errorG_LU += aux*N_mi[star]; // Add the contribution to the particle
			meanG_LU += G_star*N_mi[star];

			// Error for G_BP
			aux = max(0.025118864315095794, pow(10.,0.4*G_star-6.)); // 0.06309...=10**(0.4*(11.0-15))

			aBP = 0.355123*VmIc_star + 1.043270;
			bBP = 0.195768*VmIc_star + 1.465592;
			cBP = - 0.205807*VmIc_star - 1.866968;
			aRP = - 0.636628*VmIc_star + 1.615927;
			bRP = - 0.318499*VmIc_star + 1.783906;
			cRP = - 0.091569*VmIc_star - 3.042268;

			VmIc_star2 = VmIc_star*VmIc_star;

			aBP += 0.044390*VmIc_star2;
			bBP += 0.018878*VmIc_star2;
			cBP += 0.060769*VmIc_star2;
			aRP += 0.114126*VmIc_star2;
			bRP += 0.057112*VmIc_star2;
			cRP += 0.027352*VmIc_star2;

			VmIc_star2 = VmIc_star2*VmIc_star; //Now VmIc_star2 stands for (VmIc_star)**3

			aBP += -0.000562*VmIc_star2;
			bBP += -0.000400*VmIc_star2;
			cBP += +0.000262*VmIc_star2;
			aRP += -0.007597*VmIc_star2;
			bRP += -0.003803*VmIc_star2;
			cRP += -0.001923*VmIc_star2;

			// Use VmIc_star2 as an auxiliar variable
			VmIc_star2 = aux;

			// First for BP:
			aux = pow(10., aBP)*aux*aux + pow(10., bBP)*aux + pow(10., cBP);
			aux = 0.001*sqrt(aux/70.);
			errorBP_LU += aux*N_mi[star]; // Add the contribution to the particle
			meanBP_LU += BP_star*N_mi[star];

			// Then for RP:
			aux = VmIc_star2;
			aux = pow(10., aRP)*aux*aux + pow(10., bRP)*aux + pow(10., cRP);
			aux = 0.001*sqrt(aux/70.);
			errorRP_LU += aux*N_mi[star]; // Add the contribution to the particle
			meanRP_LU += RP_star*N_mi[star];

			Cum_N_mi += N_mi[star]; //Renormalisation of N_mi due to the out-of-G range stars. Cum_N_mi is the total number of stars observed (if Mass_p = 1)

		}; // End of the star-loop

		// Save the number of stars observed for this particle
		nobstars_LU = Mass_p*Cum_N_mi;

		// Correction due to the G-band exclusion
		if(Cum_N_mi<=0.){
			//Bad values when no stars are observed
			meanG_LU = INFINITY; // mean_G
			meanBP_LU = INFINITY; // mean_BP
			meanRP_LU = INFINITY; // mean_RP
			errorG_LU = -INFINITY; // sigma_G
			errorBP_LU = -INFINITY; // sigma_BP
			errorRP_LU = -INFINITY; // sigma_RP
		}else{
			meanG_LU = meanG_LU/Cum_N_mi; // mean_G
			meanBP_LU = meanBP_LU/Cum_N_mi; // mean_BP
			meanRP_LU = meanRP_LU/Cum_N_mi; // mean_RP
			errorG_LU = errorG_LU/Cum_N_mi; // sigma_G
			errorBP_LU = errorBP_LU/Cum_N_mi; // sigma_BP
			errorRP_LU = errorRP_LU/Cum_N_mi; // sigma_RP
		};


		/* ++++++++++++++++++++++++++++++++ WEIGHTING ++++++++++++++++++++++++++++++++++++++++*/
		if(wLR==-wRL){
			wLR = 1.;
			wRL = 0.;
		}else{
			wLR = wRL/(wLR+wRL);
			wRL = 1.-wLR;
		};

		if(wUD==-wDU){
			wUD = 1.;
			wDU = 0.;
		}else{
			wUD = wDU/(wUD+wDU);
			wDU = 1.-wUD;
		};

		mean_G[particle] = wDU*wLR*meanG_LD + wDU*wRL*meanG_RD + wUD*wRL*meanG_RU +wUD*wLR*meanG_LU;
		mean_BP[particle] = wDU*wLR*meanBP_LD + wDU*wRL*meanBP_RD + wUD*wRL*meanBP_RU +wUD*wLR*meanBP_LU;
		mean_RP[particle] = wDU*wLR*meanRP_LD + wDU*wRL*meanRP_RD + wUD*wRL*meanRP_RU +wUD*wLR*meanRP_LU;

		error_G[particle] = wDU*wLR*errorG_LD + wDU*wRL*errorG_RD + wUD*wRL*errorG_RU +wUD*wLR*errorG_LU;
		error_BP[particle] = wDU*wLR*errorBP_LD + wDU*wRL*errorBP_RD + wUD*wRL*errorBP_RU +wUD*wLR*errorBP_LU;
		error_RP[particle] = wDU*wLR*errorRP_LD + wDU*wRL*errorRP_RD + wUD*wRL*errorRP_RU +wUD*wLR*errorRP_LU;

		nobstars[particle] =  wDU*wLR*nobstars_LD + wDU*wRL*nobstars_RD + wUD*wRL*nobstars_RU +wUD*wLR*nobstars_LU;

		if(particle%10000==0) fprintf(stderr,"Particle %i of %i has finished (%.3f/100 completed)\n",particle, Nparticles, particle*100./Nparticles);

	}; // End of the particle

	fclose( fichero_stat_bin );
	fclose( fichero_isochr_bin );

}//End of PhotometryGaiaKroupa_4Points
//-----------------------------------------------------------------------------------------------------------------//





//-----------------------------------------------------------------------------------------------------------------//
/* SigmaVrGaia_Kroupa */
void SigmaVrGaia_Kroupa(float *Z, float *logAge, float *ParticleMass, float *dist, float *ex31, float *error_Vr, float *nobstars, int Nparticles, float *G_limits){
/* Since ex31 is provided, lon and lat are not needed*/
// G_limits has four elements: [G_min, G_max, G_RVS_min, G_RVS_max]
// UNITS: Z is a fraction, Ages in yrs, distances in kpc, ex31 in mag, error_Vr in km/s
	int n, particle, star; // Counters
	float Z_set[34]; // Set of metallicities (the grid is not regular in Z)
	float aux; //auxiliary variable
	int is_selected; // 1 if the G mag is in the Gaia range, 0 otherwise

	/* Values for each particle */
	int i_Z, i_logAge, i_isochrone; //Index of the metallicity, logAge, isochrone
	float Z_p, logAge_p, Mass_p, ex31_p; // Values for one selected particle
	float distance_correction; // 10+5*log10(d_kpc)

	/* Values for each star */
	float V_star, G_star, GRVS_star, VmIc_star; //Apparent V mag, G & GRVS mag and colour difference

	/* Values for each isochrone */
	int starting_point_of_the_isochrone, Number_of_logmasses_in_the_isochrone;

	/* IMF */
	const float dlogm = (Logmi_MAX-Logmi_MIN)/(1.0*Max_Number_of_logmasses_per_isochrone); // bin size
	float mi[Max_Number_of_logmasses_per_isochrone], N_mi[Max_Number_of_logmasses_per_isochrone]; //Masses and global IMF
	float Cum_N_mi;
	float mL; //Maximum initial mass

	/* Interpolation nodes*/
	float VmIc_data[Number_of_Nodes_Vr_table], b_data[Number_of_Nodes_Vr_table];
	float a_photom, b_photom;

	// Files:
	FILE *fichero_Vr; // ASCII file with the interpolation nodes of sigma_Vr = sigma_FLOOR + b*exp(a*(V-12.7));
	FILE *fichero_ASCII; // ASCII file with the information about the isochrones
	FILE *fichero_stat_bin; //Binary file with the statistics of all the isochrones
	FILE *fichero_isochr_bin; //Binary file with all the isochroness

	// Always in the Gaia range
	G_limits[0] = max(G_limits[0], Gmin);
	G_limits[1] = min(G_limits[1], Gmax);

	/* Read the metallicity values of the grid */
	fichero_ASCII = fopen( ISONO,"r"); //Open IsoNo.txt
	for (n=0; n<Number_of_Z;n++){
		fscanf( fichero_ASCII, "%f\n", &Z_set[n]); // Read the decimal part of the metallicity 
		Z_set[n] = Z_set[n]*1E-6; // Convert to true Z [met]

	} // Metallicity values read!
	fclose( fichero_ASCII ); //Close the ASCII file

	fichero_isochr_bin = fopen( BIN3,"rb"); //Open All_the_isochrones3.bin
	fichero_stat_bin = fopen( EXPL3,"rb"); // Open the statistics of all the isochr.

	//Since logmi_mi and Logmi_MAX are equal for all of the isochrones, the IMF would be the same:
	for(star=0; star<Max_Number_of_logmasses_per_isochrone;star++){
		mi[star] = pow(10., Logmi_MIN + dlogm*star);
	};

	/* Read the interpolation nodes */
	fichero_Vr = fopen( TABLE_VR,"r");
	if(!fichero_Vr) fprintf(stderr, "WARN: File %s not found\n",TABLE_VR);
	fscanf(fichero_Vr, "#(V-I)c    a      b\n");
	for(n=0;n<Number_of_Nodes_Vr_table;n++){
		fscanf(fichero_Vr, "%f\t%f\t%f\n",&VmIc_data[n], &Z_p, &b_data[n]); // Z_P is an auxliar variable here
		//fprintf(stderr,"%f\t%f\t%f\n",VmIc_data[n], Z_p, b_data[n]);
	};
	fclose(fichero_Vr);

// Loop over the particles
	for(particle=0;particle<Nparticles;particle++){

		// Store the parameters of the considered particle:
		Z_p = Z[particle];
		logAge_p = logAge[particle];
		ex31_p = ex31[particle];
		distance_correction = 10.+5.*log10(dist[particle]);
		Mass_p = ParticleMass[particle];
		

		// Avoid out of range values:
		logAge_p = (logAge_p<logAge_min? logAge_min: logAge_p);
		logAge_p = (logAge_p>logAge_max? logAge_max: logAge_p);

		Z_p = max(Z_p, Z_set[0]);
		Z_p = min(Z_p,Z_set[Number_of_Z-1]);


		// Get the index of that metallicity value (the Z sampling is not homogeneous):
		i_Z = -1; //Initial value
		do{
			i_Z++;
		}while( Z_set[i_Z]<=Z_p );
		i_Z--;
		i_Z = min(i_Z, Number_of_Z-1);
		i_Z = max(i_Z, 0);


		//Assume that the logAge grid is regular (from logAge_min to logAge_max with step logAge_step)
		i_logAge = (int)((Number_of_logAge-1)*(logAge_p-logAge_min)/(logAge_max-logAge_min));
		i_isochrone = i_logAge + (Number_of_logAge)*i_Z;
		
		fseek( fichero_stat_bin, 40*i_isochrone+16, SEEK_SET);// 40 bytes = 4 bytes*10 columns

		fread(&starting_point_of_the_isochrone,4,1,fichero_stat_bin); //Starting position (inclusive)
		fread(&Number_of_logmasses_in_the_isochrone,4,1,fichero_stat_bin);// Rows (stars) that belong to this isochrone

		//Call to the Kroupa IMF function
		mL = pow(10., Logmi_MIN + dlogm*(Number_of_logmasses_in_the_isochrone-1));

		KroupaIMF( mi, Max_Number_of_logmasses_per_isochrone, mL, N_mi); //N_mi means number of masses between mi and mi+dm (egde effects included)

		// Initialize the variables:
		aux = 0.; // Quantity used to estimate error_Vr
		error_Vr[particle] = 0.; // Sigma_Vr
		Cum_N_mi = 0.; //New norm of N_mi_p
		nobstars[particle] = 0.;

		// Go to the desired isochrone
		fseek( fichero_isochr_bin, 8*starting_point_of_the_isochrone, SEEK_SET); //8=2*4 bytes = np.float32 bits*2 columns


		// Load V and V-Ic from the isochrone given by (Z, logAge), and finish P(m_i)
		for(star=0;star<Number_of_logmasses_in_the_isochrone;star++){
			// Get the absolute magnitudes
			fread(&V_star,4,1, fichero_isochr_bin); // Absolute V
			fread(&VmIc_star,4,1, fichero_isochr_bin); // Absolute V-Ic

			// From absolute to apparent
			V_star = V_star + distance_correction + ex31_p;
			VmIc_star = VmIc_star + 355./900.0*ex31_p; // 355/900 = 0.394444... = 1.962/3.24 (ratio between V and Ic extinction corrections)

			// Estimate the G band
			G_star = V_star-0.01746+0.008092*VmIc_star-0.2810*VmIc_star*VmIc_star+0.03655*VmIc_star*VmIc_star*VmIc_star; // G band
			// Estimate the G_RVS band
			GRVS_star = V_star-0.0119-1.2092*VmIc_star+0.0188*VmIc_star*VmIc_star+0.0005*VmIc_star*VmIc_star*VmIc_star; // G_RVS band 
		
			// Would this star be observed with Gaia? With enough accuracy?
			is_selected = GaiaSelectionFunction(G_star, G_limits[0],G_limits[1]); // 1= Yes, 0=No
			is_selected *= IndicatorFunction( VmIc_star, VmIc_cutoffmin, VmIc_cutoffmax); // 1= Yes, 0=No
			is_selected*= GaiaSelectionFunction_GRVS(GRVS_star, G_limits[2], G_limits[3]);
			if(!is_selected) continue; // Go to the next star
			N_mi[star]*=is_selected;

			// Calculate the error for this star and add it to the particle contribution:
			a_photom = Spectr_parameter_a(VmIc_star);			
			b_photom = LinInterp(VmIc_star, VmIc_data, b_data, Number_of_Nodes_Vr_table );
			// see www.cosmos.esa.int/web/gaia/science-performance#photometric performance
			aux = SIGMA_FLOOR + b_photom*exp(a_photom*(V_star-12.7));

			Cum_N_mi += N_mi[star]; //Renormalisation of N_mi due to the out-of-G range stars. Cum_N_mi is the total number of stars observed (if Mass_p = 1)
			error_Vr[particle] += aux*N_mi[star]; // Add the contribution to the average


		}; // End of the star-loop

		// Save the number of stars observed for this particle
		nobstars[particle] = Mass_p*Cum_N_mi;

		// Correction due to the G-band exclusion
		Cum_N_mi = Cum_N_mi -1.0*(Cum_N_mi<=0);//If no stars are observed, return a negative value of sigma_Vr
		error_Vr[particle] = error_Vr[particle]/Cum_N_mi; //Include the Calibration, the scanning law and the epoch corrections
		error_Vr[particle] = error_Vr[particle]*(Cum_N_mi>0.) - 99000.00*(Cum_N_mi<=0.);//If no stars are observed, return a negative value of sigma_Vr

	
	}; // End of the particle

	fclose( fichero_stat_bin );
	fclose( fichero_isochr_bin );

}//End of SigmaVrGaia_Kroupa
//-----------------------------------------------------------------------------------------------------------------//





//---------------------------------------SigmaVrGaiaKroupa_4Points ------------------------------------------//
void SigmaVrGaiaKroupa_4Points(float *Z, float *logAge, float *ParticleMass, float *dist, float *ex31, float *error_Vr, float *nobstars, int Nparticles, float *G_limits){
/* Since ex31 is provided, lon and lat are not needed. Interpolates in the (Z, logAge) grid*/
// UNITS: Z (is a fraction)
//	  logAge (ages in yrs)
//	  distances in kpc
//	  ex31 in mag
//	  error_Vr in km/s
// Correction includes Sky Scanning Correction + Calibration factor + epoch (sqrt(60/month))
	int n, particle, star; // Counters
	float Z_set[34]; // Set of metallicities (the grid is not regular in Z)
	float aux; //auxiliary variable
	int is_selected; // 1 if the G mag is in the Gaia range, 0 otherwise

	/* Values for each particle */
	int i_Z, i_logAge, i_isochrone; //Index of the metallicity, logAge, isochrone
	float Z_p, logAge_p, Mass_p, ex31_p; // Values for one selected particle
	float distance_correction; // 10+5*log10(d_kpc)

	/* Values for each star */
	float V_star, G_star, GRVS_star, VmIc_star; //Apparent V mag, G & GRVS mag and colour difference

	/* Values for each isochrone */
//	float V_min, V_max, VmIc_min, VmIc_max;//Parameters to read in Explanation3.bin
	int starting_point_of_the_isochrone, Number_of_logmasses_in_the_isochrone;

	/* IMF */
	const float dlogm = (Logmi_MAX-Logmi_MIN)/(1.0*Max_Number_of_logmasses_per_isochrone); // bin size
	float mi[Max_Number_of_logmasses_per_isochrone], N_mi[Max_Number_of_logmasses_per_isochrone]; //Masses and global IMF
	float Cum_N_mi;
	float mL;

	/* Weights and temporal errors */
	float nobstars_LD, nobstars_RD, nobstars_RU, nobstars_LU;
	float errorVr_LD, errorVr_RD, errorVr_RU, errorVr_LU;
	float wLR, wRL, wDU, wUD; // wLR+wRL = wDU+wUD = 1.

	/* Interpolation nodes*/
	float VmIc_data[Number_of_Nodes_Vr_table], b_data[Number_of_Nodes_Vr_table];
	float a_photom, b_photom;

	// Files:
	FILE *fichero_Vr; // ASCII file with the interpolation nodes of sigma_Vr = sigma_FLOOR + b*exp(a*(V-12.7));
	FILE *fichero_ASCII; // ASCII file with the information about the isochrones
	FILE *fichero_stat_bin; //Binary file with the statistics of all the isochrones
	FILE *fichero_isochr_bin; //Binary file with all the isochroness

	// Always in the Gaia range
	G_limits[0] = max(G_limits[0], Gmin);
	G_limits[1] = min(G_limits[1], Gmax);


	/* Read the metallicity values of the grid */
	fichero_ASCII = fopen( ISONO,"r"); //Open ISONO
	for (n=0; n<Number_of_Z;n++){
		fscanf( fichero_ASCII, "%f\n", &Z_set[n]); // Read the decimal part of the metallicity 
		Z_set[n] = Z_set[n]*1E-6; // Convert to true Z [met]

	} // Metallicity values read!
	fclose( fichero_ASCII ); //Close the ASCII file


	fichero_isochr_bin = fopen( BIN3,"rb"); //Open All_the_isochrones2.bin
	fichero_stat_bin = fopen( EXPL3,"rb"); // Open the statistics of all the isochr.

	//Since logmi_mi and Logmi_MAX are equal for all of the isochrones, the IMF would be the same:
	for(star=0; star<Max_Number_of_logmasses_per_isochrone;star++){
		mi[star] = pow(10., Logmi_MIN + dlogm*star);
	};


	/* Read the interpolation nodes */
	fichero_Vr = fopen( TABLE_VR,"r");
	if(!fichero_Vr) fprintf(stderr, "WARN: File %s not found\n",TABLE_VR);
	fscanf(fichero_Vr, "#(V-I)c    a      b\n");
	for(n=0;n<Number_of_Nodes_Vr_table;n++){
		fscanf(fichero_Vr, "%f\t%f\t%f\n",&VmIc_data[n], &Z_p, &b_data[n]); // Z_P is an auxliar variable here
	};
	fclose(fichero_Vr);


// Loop over the particles
	for(particle=0;particle<Nparticles;particle++){
		// Store the parameters of the considered particle:
		Z_p = Z[particle];
		logAge_p = logAge[particle];
		Mass_p = ParticleMass[particle];
		ex31_p = ex31[particle];
		distance_correction = 10.+5.*log10(dist[particle]);
		

		// Avoid out of range values:
		logAge_p = (logAge_p<logAge_min? logAge_min: logAge_p);
		logAge_p = (logAge_p>logAge_max? logAge_max: logAge_p);

		Z_p = max(Z_p, Z_set[0]);
		Z_p = min(Z_p,Z_set[Number_of_Z-1]);


		// Get the index of that metallicity value (the Z sampling is not homogeneous):
		i_Z = -1; //Initial value
		do{
			i_Z++;
		}while( Z_set[i_Z]<=Z_p );
		i_Z--;
		i_Z = min(i_Z, Number_of_Z-1);
		i_Z = max(i_Z, 0);

		/* ++++++++++++++++++++++++++++++++ LEFT DOWN POINT ++++++++++++++++++++++++++++++++++++++++*/

		//Assume that the logAge grid is regular (from logAge_min to logAge_max with step logAge_step)
		i_logAge = (int)((Number_of_logAge-1)*(logAge_p-logAge_min)/(logAge_max-logAge_min));
		i_isochrone = i_logAge + Number_of_logAge*i_Z;

		//Weights:
		wLR = Z_p-Z_set[i_Z]; //For the Left-Down point of the grid


		// Read the following information about the selected isochrone		
		fseek( fichero_stat_bin, 40*i_isochrone+16, SEEK_SET);// 40 bytes = 4 bytes*10 columns

		fread(&starting_point_of_the_isochrone,4,1,fichero_stat_bin); //Starting position (inclusive)
		fread(&Number_of_logmasses_in_the_isochrone,4,1,fichero_stat_bin);// Rows (stars) that belong to this isochrone

		//Call to the Kroupa IMF function
		mL = pow(10., Logmi_MIN + dlogm*(Number_of_logmasses_in_the_isochrone-1));
		KroupaIMF( mi, Max_Number_of_logmasses_per_isochrone, mL, N_mi); //N_mi means number of masses between mi and mi+dm (egde effects included)


		// Initialize the Gaia error:
		aux = 0.; // Quantity used to estimate error_Vr
		errorVr_LD = 0.; // Sigma_Vr
		Cum_N_mi = 0.; //New norm of N_mi_p
		nobstars_LD = 0.;

		// Go to the desired isochrone
		fseek( fichero_isochr_bin, 8*starting_point_of_the_isochrone, SEEK_SET); //8=2*4 bytes = np.float32 bits*2 columns


		// Load V and V-Ic from the isochrone given by (Z, logAge), and finish P(m_i)
		for(star=0;star<Number_of_logmasses_in_the_isochrone;star++){

			// Get the absolute magnitudes
			fread(&V_star,4,1, fichero_isochr_bin); // Absolute V
			fread(&VmIc_star,4,1, fichero_isochr_bin); // Absolute V-Ic

			// From absolute to apparent
			V_star = V_star + distance_correction + ex31_p;
			VmIc_star = VmIc_star + 355./900.0*ex31_p; // 355/900 = 0.394444... = 1.962/3.24 (ratio between V and Ic extinction corrections)

			// Estimate the G band
			G_star = V_star-0.01746+0.008092*VmIc_star-0.2810*VmIc_star*VmIc_star+0.03655*VmIc_star*VmIc_star*VmIc_star; // G band
			// Estimate the G_RVS band
			GRVS_star = V_star-0.0119-1.2092*VmIc_star+0.0188*VmIc_star*VmIc_star+0.0005*VmIc_star*VmIc_star*VmIc_star; // G_RVS band 
		
			// Would this star be observed with Gaia? With enough accuracy?
			is_selected = GaiaSelectionFunction(G_star, G_limits[0],G_limits[1]); // 1= Yes, 0=No
			is_selected *= IndicatorFunction( VmIc_star, VmIc_cutoffmin, VmIc_cutoffmax); // 1= Yes, 0=No
			is_selected*= GaiaSelectionFunction_GRVS(GRVS_star, G_limits[2], G_limits[3]);
			if(!is_selected) continue; // Go to the next star
			N_mi[star]*=is_selected;

			// Calculate the error for this star and add it to the particle contribution:
			a_photom = Spectr_parameter_a(VmIc_star);			
			b_photom = LinInterp(VmIc_star, VmIc_data, b_data, Number_of_Nodes_Vr_table );
			// see www.cosmos.esa.int/web/gaia/science-performance#photometric performance
			aux = SIGMA_FLOOR + b_photom*exp(a_photom*(V_star-12.7));

			Cum_N_mi += N_mi[star]; //Renormalisation of N_mi due to the out-of-G range stars. Cum_N_mi is the total number of stars observed (if Mass_p = 1)
			errorVr_LD += aux*N_mi[star]; // Add the contribution to the average


		}; // End of the star-loop

		// Save the number of stars observed for this particle
		nobstars_LD = Mass_p*Cum_N_mi;

		// Correction due to the G-band exclusion
		Cum_N_mi = Cum_N_mi -1.0*(Cum_N_mi<=0);//If no stars are observed, return a negative value of sigma_Vr
		errorVr_LD = errorVr_LD/Cum_N_mi; //Include the Calibration, the scanning law and the epoch corrections
		errorVr_LD = errorVr_LD*(Cum_N_mi>0.) - 99000.00*(Cum_N_mi<=0.);//If no stars are observed, return a negative value of sigma_Vr

	
		/* ++++++++++++++++++++++++++++++++ RIGHT DOWN POINT ++++++++++++++++++++++++++++++++++++++++*/

		//i_logAge does not change but i_Z...
		i_Z = i_Z + 1*(i_Z!=(Number_of_Z-1));
		
		i_isochrone = i_logAge + Number_of_logAge*i_Z;

		//Weights:
		wRL = Z_set[i_Z]-Z_p; //For the Right-Down point of the grid

		// Read the following information about the selected isochrone		
		fseek( fichero_stat_bin, 40*i_isochrone+16, SEEK_SET);// 40 bytes = 4 bytes*10 columns

		fread(&starting_point_of_the_isochrone,4,1,fichero_stat_bin); //Starting position (inclusive)
		fread(&Number_of_logmasses_in_the_isochrone,4,1,fichero_stat_bin);// Rows (stars) that belong to this isochrone

		//Call to the Kroupa IMF function
		mL = pow(10., Logmi_MIN + dlogm*(Number_of_logmasses_in_the_isochrone-1));
		KroupaIMF( mi, Max_Number_of_logmasses_per_isochrone, mL, N_mi); //N_mi means number of masses between mi and mi+dm (egde effects included)

		// Initialize the Gaia error:
		aux = 0.; // Quantity used to estimate error_Vr
		errorVr_RD = 0.; // Sigma_Vr
		Cum_N_mi = 0.; //New norm of N_mi_p
		nobstars_RD = 0.;

		// Go to the desired isochrone
		fseek( fichero_isochr_bin, 8*starting_point_of_the_isochrone, SEEK_SET); //8=2*4 bytes = np.float32 bits*2 columns


		// Load V and V-Ic from the isochrone given by (Z, logAge), and finish P(m_i)
		for(star=0;star<Number_of_logmasses_in_the_isochrone;star++){
			// Get the absolute magnitudes
			fread(&V_star,4,1, fichero_isochr_bin); // Absolute V
			fread(&VmIc_star,4,1, fichero_isochr_bin); // Absolute V-Ic

			// From absolute to apparent
			V_star = V_star + distance_correction + ex31_p;
			VmIc_star = VmIc_star + 355./900.0*ex31_p; // 355/900 = 0.394444... = 1.962/3.24 (ratio between V and Ic extinction corrections)

			// Estimate the G band
			G_star = V_star-0.01746+0.008092*VmIc_star-0.2810*VmIc_star*VmIc_star+0.03655*VmIc_star*VmIc_star*VmIc_star; // G band
			// Estimate the G_RVS band
			GRVS_star = V_star-0.0119-1.2092*VmIc_star+0.0188*VmIc_star*VmIc_star+0.0005*VmIc_star*VmIc_star*VmIc_star; // G_RVS band 
		
			// Would this star be observed with Gaia? With enough accuracy?
			is_selected = GaiaSelectionFunction(G_star, G_limits[0],G_limits[1]); // 1= Yes, 0=No
			is_selected *= IndicatorFunction( VmIc_star, VmIc_cutoffmin, VmIc_cutoffmax); // 1= Yes, 0=No
			is_selected*= GaiaSelectionFunction_GRVS(GRVS_star, G_limits[2], G_limits[3]);
			if(!is_selected) continue; // Go to the next star
			N_mi[star]*=is_selected;

			// Calculate the error for this star and add it to the particle contribution:
			a_photom = Spectr_parameter_a(VmIc_star);			
			b_photom = LinInterp(VmIc_star, VmIc_data, b_data, Number_of_Nodes_Vr_table );
			// see www.cosmos.esa.int/web/gaia/science-performance#photometric performance
			aux = SIGMA_FLOOR + b_photom*exp(a_photom*(V_star-12.7));

			Cum_N_mi += N_mi[star]; //Renormalisation of N_mi due to the out-of-G range stars. Cum_N_mi is the total number of stars observed (if Mass_p = 1)
			errorVr_RD += aux*N_mi[star]; // Add the contribution to the average


		}; // End of the star-loop

		// Save the number of stars observed for this particle
		nobstars_RD = Mass_p*Cum_N_mi;

		// Correction due to the G-band exclusion
		Cum_N_mi = Cum_N_mi -1.0*(Cum_N_mi<=0);//If no stars are observed, return a negative value of sigma_Vr
		errorVr_RD = errorVr_RD/Cum_N_mi; //Include the Calibration, the scanning law and the epoch corrections
		errorVr_RD = errorVr_RD*(Cum_N_mi>0.) - 99000.00*(Cum_N_mi<=0.);//If no stars are observed, return a negative value of sigma_Vr


		/* ++++++++++++++++++++++++++++++++ RIGHT UP POINT ++++++++++++++++++++++++++++++++++++++++*/

		//i_Z as in the previous case, but i_logAge has increase (or not)...
		i_logAge = i_logAge + 1*(i_logAge!=(Number_of_logAge-1));
		i_isochrone = i_logAge + Number_of_logAge*i_Z;

		//Weights:
		wUD = logAge_min - i_logAge*logAge_step - logAge_p; //For the Right-Up point of the grid

		// Read the following information about the selected isochrone		
		fseek( fichero_stat_bin, 40*i_isochrone+16, SEEK_SET);// 40 bytes = 4 bytes*10 columns

		fread(&starting_point_of_the_isochrone,4,1,fichero_stat_bin); //Starting position (inclusive)
		fread(&Number_of_logmasses_in_the_isochrone,4,1,fichero_stat_bin);// Rows (stars) that belong to this isochrone

		//Call to the Kroupa IMF function
		mL = pow(10., Logmi_MIN + dlogm*(Number_of_logmasses_in_the_isochrone-1));
		KroupaIMF( mi, Max_Number_of_logmasses_per_isochrone, mL, N_mi); //N_mi means number of masses between mi and mi+dm (egde effects included)

		// Initialize the Gaia error:
		aux = 0.; // Quantity used to estimate error_Vr
		errorVr_RU = 0.; // Sigma_Vr
		Cum_N_mi = 0.; //New norm of N_mi_p
		nobstars_RU = 0.;

		// Go to the desired isochrone
		fseek( fichero_isochr_bin, 8*starting_point_of_the_isochrone, SEEK_SET); //8=2*4 bytes = np.float32 bits*2 columns


		// Load V and V-Ic from the isochrone given by (Z, logAge), and finish P(m_i)
		for(star=0;star<Number_of_logmasses_in_the_isochrone;star++){
			// Get the absolute magnitudes
			fread(&V_star,4,1, fichero_isochr_bin); // Absolute V
			fread(&VmIc_star,4,1, fichero_isochr_bin); // Absolute V-Ic

			// From absolute to apparent
			V_star = V_star + distance_correction + ex31_p;
			VmIc_star = VmIc_star + 355./900.0*ex31_p; // 355/900 = 0.394444... = 1.962/3.24 (ratio between V and Ic extinction corrections)

			// Estimate the G band
			G_star = V_star-0.01746+0.008092*VmIc_star-0.2810*VmIc_star*VmIc_star+0.03655*VmIc_star*VmIc_star*VmIc_star; // G band
			// Estimate the G_RVS band
			GRVS_star = V_star-0.0119-1.2092*VmIc_star+0.0188*VmIc_star*VmIc_star+0.0005*VmIc_star*VmIc_star*VmIc_star; // G_RVS band 
		
			// Would this star be observed with Gaia? With enough accuracy?
			is_selected = GaiaSelectionFunction(G_star, G_limits[0],G_limits[1]); // 1= Yes, 0=No
			is_selected *= IndicatorFunction( VmIc_star, VmIc_cutoffmin, VmIc_cutoffmax); // 1= Yes, 0=No
			is_selected*= GaiaSelectionFunction_GRVS(GRVS_star, G_limits[2], G_limits[3]);
			if(!is_selected) continue; // Go to the next star
			N_mi[star]*=is_selected;

			// Calculate the error for this star and add it to the particle contribution:
			a_photom = Spectr_parameter_a(VmIc_star);			
			b_photom = LinInterp(VmIc_star, VmIc_data, b_data, Number_of_Nodes_Vr_table );
			// see www.cosmos.esa.int/web/gaia/science-performance#photometric performance
			aux = SIGMA_FLOOR + b_photom*exp(a_photom*(V_star-12.7));

			Cum_N_mi += N_mi[star]; //Renormalisation of N_mi due to the out-of-G range stars. Cum_N_mi is the total number of stars observed (if Mass_p = 1)
			errorVr_RU += aux*N_mi[star]; // Add the contribution to the average


		}; // End of the star-loop

		// Save the number of stars observed for this particle
		nobstars_RU = Mass_p*Cum_N_mi;

		// Correction due to the G-band exclusion
		Cum_N_mi = Cum_N_mi -1.0*(Cum_N_mi<=0);//If no stars are observed, return a negative value of sigma_Vr
		errorVr_RU = errorVr_RU/Cum_N_mi; //Include the Calibration, the scanning law and the epoch corrections
		errorVr_RU = errorVr_RU*(Cum_N_mi>0.) - 99000.00*(Cum_N_mi<=0.);//If no stars are observed, return a negative value of sigma_Vr


		/* ++++++++++++++++++++++++++++++++ LEFT UP POINT ++++++++++++++++++++++++++++++++++++++++*/

		//i_logAge as in the previous case, but i_Z decreases (or not)...
		i_Z = i_Z - 1*(i_Z>0);
		i_isochrone = i_logAge + Number_of_logAge*i_Z;

		//Weights:
		wDU = logAge_p - logAge_min - i_logAge*logAge_step; //For the Left-Up point of the grid

		// Read the following information about the selected isochrone		
		fseek( fichero_stat_bin, 40*i_isochrone+16, SEEK_SET);// 40 bytes = 4 bytes*10 columns

		fread(&starting_point_of_the_isochrone,4,1,fichero_stat_bin); //Starting position (inclusive)
		fread(&Number_of_logmasses_in_the_isochrone,4,1,fichero_stat_bin);// Rows (stars) that belong to this isochrone

		//Call to the Kroupa IMF function
		mL = pow(10., Logmi_MIN + dlogm*(Number_of_logmasses_in_the_isochrone-1));
		KroupaIMF( mi, Max_Number_of_logmasses_per_isochrone, mL, N_mi); //N_mi means number of masses between mi and mi+dm (egde effects included)

		// Initialize the Gaia error:
		aux = 0.; // Quantity used to estimate error_Vr
		errorVr_LU = 0.; // Sigma_Vr
		Cum_N_mi = 0.; //New norm of N_mi_p
		nobstars_LU = 0.;

		// Go to the desired isochrone
		fseek( fichero_isochr_bin, 8*starting_point_of_the_isochrone, SEEK_SET); //8=2*4 bytes = np.float32 bits*2 columns

		// Load V and V-Ic from the isochrone given by (Z, logAge), and finish P(m_i)
		for(star=0;star<Number_of_logmasses_in_the_isochrone;star++){
			// Get the absolute magnitudes
			fread(&V_star,4,1, fichero_isochr_bin); // Absolute V
			fread(&VmIc_star,4,1, fichero_isochr_bin); // Absolute V-Ic

			// From absolute to apparent
			V_star = V_star + distance_correction + ex31_p;
			VmIc_star = VmIc_star + 355./900.0*ex31_p; // 355/900 = 0.394444... = 1.962/3.24 (ratio between V and Ic extinction corrections)

			// Estimate the G band
			G_star = V_star-0.01746+0.008092*VmIc_star-0.2810*VmIc_star*VmIc_star+0.03655*VmIc_star*VmIc_star*VmIc_star; // G band
			// Estimate the G_RVS band
			GRVS_star = V_star-0.0119-1.2092*VmIc_star+0.0188*VmIc_star*VmIc_star+0.0005*VmIc_star*VmIc_star*VmIc_star; // G_RVS band 
		
			// Would this star be observed with Gaia? With enough accuracy?
			is_selected = GaiaSelectionFunction(G_star, G_limits[0],G_limits[1]); // 1= Yes, 0=No
			is_selected *= IndicatorFunction( VmIc_star, VmIc_cutoffmin, VmIc_cutoffmax); // 1= Yes, 0=No
			is_selected*= GaiaSelectionFunction_GRVS(GRVS_star, G_limits[2], G_limits[3]);
			if(!is_selected) continue; // Go to the next star
			N_mi[star]*=is_selected;

			// Calculate the error for this star and add it to the particle contribution:
			a_photom = Spectr_parameter_a(VmIc_star);			
			b_photom = LinInterp(VmIc_star, VmIc_data, b_data, Number_of_Nodes_Vr_table );
			// see www.cosmos.esa.int/web/gaia/science-performance#photometric performance
			aux = SIGMA_FLOOR + b_photom*exp(a_photom*(V_star-12.7));

			Cum_N_mi += N_mi[star]; //Renormalisation of N_mi due to the out-of-G range stars. Cum_N_mi is the total number of stars observed (if Mass_p = 1)
			errorVr_LU += aux*N_mi[star]; // Add the contribution to the average


		}; // End of the star-loop

		// Save the number of stars observed for this particle
		nobstars_LU = Mass_p*Cum_N_mi;

		// Correction due to the G-band exclusion
		Cum_N_mi = Cum_N_mi -1.0*(Cum_N_mi<=0);//If no stars are observed, return a negative value of sigma_Vr
		errorVr_LU = errorVr_LU/Cum_N_mi; //Include the Calibration, the scanning law and the epoch corrections
		errorVr_LU = errorVr_LU*(Cum_N_mi>0.) - 99000.00*(Cum_N_mi<=0.);//If no stars are observed, return a negative value of sigma_Vr

		/* ++++++++++++++++++++++++++++++++ WEIGHTING ++++++++++++++++++++++++++++++++++++++++*/
		if(wLR==-wRL){
			wLR = 1.;
			wRL = 0.;
		}else{
			wLR = wRL/(wLR+wRL);
			wRL = 1.-wLR;
		};

		if(wUD==-wDU){
			wUD = 1.;
			wDU = 0.;
		}else{
			wUD = wDU/(wUD+wDU);
			wDU = 1.-wUD;
		};

		error_Vr[particle] = wDU*wLR*errorVr_LD + wDU*wRL*errorVr_RD + wUD*wRL*errorVr_RU +wUD*wLR*errorVr_LU;
		nobstars[particle] =  wDU*wLR*nobstars_LD + wDU*wRL*nobstars_RD + wUD*wRL*nobstars_RU +wUD*wLR*nobstars_LU;

		if(particle%10000==0) fprintf(stderr,"Particle %i of %i has finished (%.3f/100 completed)\n",particle, Nparticles, particle*100./Nparticles);

	}; // End of the particle

	fclose( fichero_stat_bin );
	fclose( fichero_isochr_bin );

}//End of SigmaVrGaiaKroupa_4Points
//-----------------------------------------------------------------------------------------------------------------//
