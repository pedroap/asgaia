#define PATH "/home/username/your_path/"
#define GaiaSelectionFunction(x,y,z) ((y)<=(x))*((x)<(z))
#define GaiaSelectionFunction_GRVS(x,y,z) ((y)<=(x))*((x)<(z))
#define IndicatorFunction(x,y,z) ((y)<=(x))*((x)<(z)) //Today IndicatorFunction == GaiaSelectionFunction, but in future in may change

// Interval in V-Ic in which G(V, V_I) is valid (the same stands for G_BP/RP)
#define VmIc_cutoffmin -0.3
#define VmIc_cutoffmax 2.7

#define Number_of_Z 34
#define logAge_min 6.6
#define logAge_max 10.22
#define logAge_step 0.02
#define Number_of_logAge 182
#define Ngfactors 20 //rows in the table of gfactor-Jun2013.dat
#define Number_of_Nodes_Vr_table 11 //rows in the table TableVr-Jun2015.dat

#ifndef Max_Number_of_masses_per_isochrone 
#define mi_MIN 0.15
#define mi_MAX 77.6
#define Max_Number_of_masses_per_isochrone 760
#endif


#ifndef Max_Number_of_logmasses_per_isochrone
#define Logmi_MIN -0.825
#define Logmi_MAX 1.890
#define Max_Number_of_logmasses_per_isochrone 543
#endif

#define Total_Number_of_isochrones 1792619

#define max(x,y) (x)>(y)?(x):(y)
#define min(x,y) (x)>(y)?(y):(x)

#ifndef pow2
#define pow2(x) ((x)*(x))
#endif

#ifndef pow3
#define pow3(x) ((x)*(x)*(x))
#endif

#define MNSc 4096
#define MN3D 1024

//Salpeter constants
#define simf_a 2.35
#define simf_m0 0.1

// Kroupa constants
#define kimf_m0 0.1
#define kimf_m1 0.25
#define kimf_m2 0.5
#define kimf_a0 1.3
#define kimf_a1 1.3
#define kimf_a2 2.3

// Bimodal constants
#define bimf_m0 0.1
#define bimf_m1 0.2
#define bimf_m2 0.6

// Hunt constants
#define himf_m0 0.1
#define himf_m2 0.5
#define himf_a1 1.3
#define himf_a2 2.3

//Gaia Limits
#define Gmin 3.
#define Gmax 20.

#define SIGMA_FLOOR 0.5 //See the spectroscopic performance of Gaia

// Files: Isocrones
#define ISONO PATH "IsoNo.txt"
#define EXPL3 PATH "Explanation3.bin"
#define BIN3 PATH "All_the_isochrones3.bin"

//Files: Extinction and geometric correction
#define ScEx1 PATH "ScEx1.dat"
#define Sc3Dcorr PATH "3dEx1.dat"
#define GFACTOR_FILE PATH "gfactor-Jun2013.dat"

//File: Nodes in sigma_Vr
#define TABLE_VR PATH "TableVr-Jun2015.dat"

// Absolute mag files. logm files
#define EXPL_U PATH "Bands/logm/Explanation_U_band.bin"
#define EXPL_B PATH "Bands/logm/Explanation_B_band.bin"
#define EXPL_V PATH "Bands/logm/Explanation_V_band.bin"
#define EXPL_R PATH "Bands/logm/Explanation_R_band.bin"
#define EXPL_I PATH "Bands/logm/Explanation_I_band.bin"
#define EXPL_J PATH "Bands/logm/Explanation_J_band.bin"
#define EXPL_H PATH "Bands/logm/Explanation_H_band.bin"
#define EXPL_K PATH "Bands/logm/Explanation_K_band.bin"

#define BIN_U PATH "Bands/logm/Allisoc_U_band.bin"
#define BIN_B PATH "Bands/logm/Allisoc_B_band.bin"
#define BIN_V PATH "Bands/logm/Allisoc_V_band.bin"
#define BIN_R PATH "Bands/logm/Allisoc_R_band.bin"
#define BIN_I PATH "Bands/logm/Allisoc_I_band.bin"
#define BIN_J PATH "Bands/logm/Allisoc_J_band.bin"
#define BIN_H PATH "Bands/logm/Allisoc_H_band.bin"
#define BIN_K PATH "Bands/logm/Allisoc_K_band.bin"


// Stellar parameters mag files. logm files
#define EXPL_logTe PATH "Bands/logm/Explanation_logTe.bin"
#define EXPL_logG PATH "Bands/logm/Explanation_logG.bin"
#define EXPL_mbol PATH "Bands/logm/Explanation_mbol.bin"

#define BIN_logTe PATH "Bands/logm/Allisoc_logTe.bin"
#define BIN_logG PATH "Bands/logm/Allisoc_logG.bin"
#define BIN_mbol PATH "Bands/logm/Allisoc_mbol.bin"

// Metallicity and Ages:
#define BIN_metZ PATH "Bands/logm/Allisoc_metZ.bin"
#define BIN_logAge PATH "Bands/logm/Allisoc_logAge.bin"



// Absolute mag files
#define EXPL_U_lin PATH "Bands/linm/Explanation_U_band_lin.bin"
#define EXPL_B_lin PATH "Bands/linm/Explanation_B_band_lin.bin"
#define EXPL_V_lin PATH "Bands/linm/Explanation_V_band_lin.bin"
#define EXPL_R_lin PATH "Bands/linm/Explanation_R_band_lin.bin"
#define EXPL_I_lin PATH "Bands/linm/Explanation_I_band_lin.bin"
#define EXPL_J_lin PATH "Bands/linm/Explanation_J_band_lin.bin"
#define EXPL_H_lin PATH "Bands/linm/Explanation_H_band_lin.bin"
#define EXPL_K_lin PATH "Bands/linm/Explanation_K_band_lin.bin"

#define BIN_U_lin PATH "Bands/linm/Allisoc_U_band_lin.bin"
#define BIN_B_lin PATH "Bands/linm/Allisoc_B_band_lin.bin"
#define BIN_V_lin PATH "Bands/linm/Allisoc_V_band_lin.bin"
#define BIN_R_lin PATH "Bands/linm/Allisoc_R_band_lin.bin"
#define BIN_I_lin PATH "Bands/linm/Allisoc_I_band_lin.bin"
#define BIN_J_lin PATH "Bands/linm/Allisoc_J_band_lin.bin"
#define BIN_H_lin PATH "Bands/linm/Allisoc_H_band_lin.bin"
#define BIN_K_lin PATH "Bands/linm/Allisoc_K_band_lin.bin"


// Stellar parameters mag files. lin files
#define EXPL_logTe_lin PATH "Bands/linm/Explanation_logTe_lin.bin"
#define EXPL_logG_lin PATH "Bands/linm/Explanation_logG_lin.bin"
#define EXPL_mbol_lin PATH "Bands/linm/Explanation_mbol_lin.bin"

#define BIN_logTe_lin PATH "Bands/linm/Allisoc_logTe_lin.bin"
#define BIN_logG_lin PATH "Bands/linm/Allisoc_logG_lin.bin"
#define BIN_mbol_lin PATH "Bands/linm/Allisoc_mbol_lin.bin"


/* Created 20/09/2017
	This .h file contains functions with general purposes
	These functions estimate the Schlegel extinction and the 3D corrections.

		ColourExcessMap(float* l_p, float* b_p, int N, float* exS);
		ColourExcess3DMap(float* l_p, float* b_p, float *d_p, int N, char* filename, float* ex3);
		SharmaCorrection(float *ex, int N);
		geometric_correction(float *lon, float *lat, int N, int flag, float *correction );
		ConversionSigmas(float *lon, float *lat, float *sigmapi, int N, int flag, float *out_alphastar, float *out_delta, float *out_muas, float *out_mudelta);

		StellarParameters_logm(float Z_p, float logAge_p, float *logmasses, float *logTe, float *mbol, float *logG )
		StellarAbsMagnitude_logm(float Z_p, float logAge_p, float *logmasses, float *M_V, int band_id);

		StellarParameters_linm(float Z_p, float logAge_p, float *logmasses, float *logTe, float *mbol, float *logG )
		StellarAbsMagnitude_linm(float Z_p, float logAge_p, float *logmasses, float *M_V, int band_id);


*/

void checkfopen(FILE *fichero){
	if(!fichero){
		fprintf(stderr, "\tWarning: Impossible to open a file\n");
	};
};

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//		                        COMMON FUNCTIONS

// +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/* Schlegel map */
int ColourExcessMap(float* l_p, float* b_p, int N, float* exS){
	// Calculates the value of the Schlegel extinction for all the N elements of the l_p, b_p arrays
	// l_p, b_p, exS are numpy.float32 arrays
	// UNITS: l_p, b_p are in radians, exS in mag
	// Uses the file ScEx1.dat"
		int i; // Counter
		int il,ib; // Index of the (l,b) map
		int ie1,ie2,ie3,ie4; // Indexes of the Schelegel array
		float dl,db,wl1,wl2,wb1,wb2;
		float leS[MNSc],beS[MNSc];
		float datS1, datS2, datS3, datS4;
		float aux_l; //This longitude is always positive
		FILE *fhandle;

//		Open the file
		fhandle = fopen(ScEx1,"rb");
		if(!fhandle){
			fprintf(stderr, " WARNING. The file:\n%s\n has not been found\n", ScEx1);
			return 0; // Indicator of failure
		}

//		Bin sizes
		dl = (2.0*M_PI)/(MNSc-1.); 
		db = M_PI/(MNSc-1.); 
//		fprintf(stdout,"Schlegel extinction array Read\n" );
		

//		 *** extinction (CTIO) ***
//		 *** Make grids l,b,d ***
		for(i=0; i<MNSc; i++){
			leS[i]=i*dl;
			beS[i]=i*db-0.5*M_PI;
		}


//		*** Find il,ib for particles ***
		for(i=0;i<N;i++){
			aux_l = l_p[i] + 2.*M_PI*(l_p[i]<0.);
			il = (int)(aux_l/dl);
			ib = (int)((b_p[i]+.5*M_PI)/db);

			// Find weighting
			wl2 = (aux_l-leS[il])/dl;
			wl1 = 1.-wl2;
			wb2 = (b_p[i]-beS[ib])/db;
			wb1 = 1.-wb2;

			//Find position in ScEx array
			ie1 = ib*4096+il;
			ie2 = (ib+1)*4096+il;
			ie3 = ib*4096+il+1;
			ie4 = (ib+1)*4096+il+1;

			// Schlegel extinction value
			//rewind( fhandle);
			fseek(fhandle, 4*ie1, SEEK_SET);
			fread(&datS1,4,1,fhandle); // datS[ie1]
			
			fseek(fhandle, 4*ie2, SEEK_SET);
			fread(&datS2,4,1,fhandle); // datS[ie2]

			fseek(fhandle, 4*ie3, SEEK_SET);
			fread(&datS3,4,1,fhandle); // datS[ie3]

			fseek(fhandle, 4*ie4, SEEK_SET);
			fread(&datS4,4,1,fhandle); // datS[ie4]
			
		        exS[i] = (float)(datS1*wl1*wb1+datS2*wl1*wb2+datS3*wl2*wb1+datS4*wl2*wb2);
		}; // End of the "for" loop
		fclose( fhandle ); //Close the file
//		fprintf(stderr,"%f\n%f\n%f\n%f\n",datS1,datS2,datS3,datS4);

return 1;
}// End of ColourExcessMap
//-----------------------------------------------------------------------------------------------------------------//



//-----------------------------------------------------------------------------------------------------------------//
/* 3DCorrection map */
int ColourExcess3DMap(float* l_p, float* b_p, float *d_p, int N, float* ex3){
	// Calculates the value of the 3D correction for the extinction for all the N elements of the l_p, b_p, d_p arrays
	// (l_p, b_p) -> Galactic coordinates
	// d_p -> heliocentric distance
	// l_p, b_p, d_p and ex3 are numpy.float32 arrays
	// UNITS: l_p, b_p are in radians, d_p in kpc, ex3 in mag
	// Uses the file 3dEx1.dat"
		int i; // Counter
		int ie1,ie2,ie3,ie4,ie5,ie6,ie7,ie8;
		int il3,ib3,id3;
		float wl1,wl2,wb1,wb2,wd1,wd2;
		float dl3,db3,dd3;
		float le3[MN3D], be3[MN3D];
		float de3[100];
		float dat3_1, dat3_2, dat3_3, dat3_4, dat3_5, dat3_6, dat3_7, dat3_8;
		float aux_l; //This longitude is always positive
		FILE *fhandle;

//		Open the file
		fhandle = fopen(Sc3Dcorr,"rb");
		if(!fhandle){
			fprintf(stderr, " WARNING: The file\n %s\n has not been found\n", Sc3Dcorr);
			return 0; // Indicator of failure
		}

//		Bin sizes
		dl3 = (2.0*M_PI)/1023.0;
		db3 = M_PI/1023.0;    
		dd3 = 3.47712123/99.0;		


//		 *** extinction (CTIO) ***
//		 *** Make grids l,b,d ***
		for(i=0;i<MN3D;i++){
			le3[i] = i*dl3;
			be3[i] = i*db3-0.5*M_PI;
		}
		for(i=0; i<100; i++){
			de3[i] = i*dd3-2.0;
		}


//		*** Find il,ib,id for particles ***
		for(i=0;i<N;i++){
			aux_l = l_p[i] + 2.*M_PI*(l_p[i]<0.);
			il3 = (int)(aux_l/dl3);
			ib3 = (int)((b_p[i] + .5*M_PI)/db3);
			id3 = (int)((log10(d_p[i])+2.0)/dd3);

			id3 = id3 + (99-id3)*(id3>99) -id3*(id3<0);

			// Find weighting
			wl2 = (aux_l-le3[il3])/dl3;
			wl1 = 1.-wl2;
			wb2 = (b_p[i]-be3[ib3])/db3;
			wb1 = 1.-wb2;
			wd2 = (log10(d_p[i])-de3[id3])/dd3;
			wd1 = 1.-wd2;

			// Find position in 3dEx1 array
			ie1 = (1048576*id3)+(1024*ib3)+il3;
			ie2 = (1048576*(id3+1))+(1024*ib3)+il3;
			ie3 = (1048576*(id3+1))+(1024*ib3+1)+il3;
			ie4 = (1048576*(id3+1))+(1024*(ib3+1))+il3+1;
			ie5 = (1048576*(id3+1))+(1024*ib3)+il3+1;
			ie6 = (1048576*id3)+(1024*(ib3+1))+il3;
			ie7 = (1048576*id3)+(1024*(ib3+1))+il3+1;
			ie8 = (1048576*id3)+(1024*ib3)+il3+1;

			// 3D correction value
			fseek( fhandle, 4*ie1, SEEK_SET);
			fread(&dat3_1,4,1,fhandle); // datS[ie1]
			
			fseek( fhandle, 4*ie2, SEEK_SET);
			fread(&dat3_2,4,1,fhandle); // datS[ie2]

			fseek( fhandle, 4*ie3, SEEK_SET);
			fread(&dat3_3,4,1,fhandle); // datS[ie3]

			fseek( fhandle, 4*ie4, SEEK_SET);
			fread(&dat3_4,4,1,fhandle); // datS[ie4]

			fseek( fhandle, 4*ie5, SEEK_SET);
			fread(&dat3_5,4,1,fhandle); // datS[ie5]
			
			fseek( fhandle, 4*ie6, SEEK_SET);
			fread(&dat3_6,4,1,fhandle); // datS[ie6]

			fseek( fhandle, 4*ie7, SEEK_SET);
			fread(&dat3_7,4,1,fhandle); // datS[ie7]

			fseek( fhandle, 4*ie8, SEEK_SET);
			fread(&dat3_8,4,1,fhandle); // datS[ie8]

			// 3D correction factor
			ex3[i] = dat3_1*wl1*wb1*wd1+dat3_2*wl1*wb1*wd2+dat3_3*wl1*wb2*wd2;
			ex3[i]+= dat3_4*wl2*wb2*wd2+dat3_5*wl2*wb1*wd2+dat3_6*wl1*wb2*wd1;
			ex3[i]+= dat3_7*wl2*wb2*wd1+dat3_8*wl2*wb1*wd1;
		}; // End of the "for" loop

		fclose( fhandle ); //Close the file
//		fprintf(stderr,"%f\n%f\n%f\n%f\n%f\n%f\n%f\n%f\n",dat3_1,dat3_2,dat3_3,dat3_4,dat3_5,dat3_6,dat3_7,dat3_8);
return 1;
}// End of ColourExcess3DMap
//-----------------------------------------------------------------------------------------------------------------//



//-----------------------------------------------------------------------------------------------------------------//
/* SharmaCorrection */
void SharmaCorrection(float *ex, int N){
	// Apply the Sharma et al. 14 correction to the extincion.
	// This should be used after ColourExcessMap and ColourExcess3DMap
	// ex: np.float32 array with N elements
	// UNITS: exS, ex3 in mag
	int n; //Counter
	for(n=0;n<N;n++){
		ex[n] = ex[n]*(0.6+0.2*(1.0-tanh((10*ex[n]-1.5))));
	}
} //End of SharmaCorrection
//-----------------------------------------------------------------------------------------------------------------//



//-----------------------------------------------------------------------------------------------------------------//
/* geometric_correction */
void geometric_correction(float *lon, float *lat, int N, int flag, float *correction ){
// Calculates the corrections to the errors depending on the scanning law
// (number of passages and the geometrical factor)
// UNITS: lon, lat are in radians
// 	Based on the Gaia_errors routine (Gaia-errors.F), created by M. Romero-Gomez: https://github.com/mromerog/Gaia-errors

	int k,n; //Counters
	float sinbeta;

	// Interpolation variables
	float xsb[Ngfactors], xga[Ngfactors], xgd[Ngfactors], xgpi[Ngfactors], xgmua[Ngfactors], xgmud[Ngfactors];
	float x0, x1, y0, y1;

	// Files:
	FILE *gfactorfile;

	gfactorfile = fopen( GFACTOR_FILE, "r");
	checkfopen(gfactorfile);

	// Read and store the table:
	fseek( gfactorfile, 67, SEEK_SET); //Jump the first line
	for(k=0;k<Ngfactors;k++){
		fscanf( gfactorfile,"%f           %f     %f    %f     %f        %f\n",&xsb[k], &xga[k], &xgd[k], &xgpi[k], &xgmua[k], &xgmud[k]);
	}// Store the interpolation values

	if(flag==-1){
	// Use the interpolated values
		//For each particle...
		for(n=0; n<N; n++){
			sinbeta = 0.4971*sin(lat[n]) + 0.8677*cos(lat[n])*sin(lon[n]-0.11135200627723822); //...sin(lon-6º38)
			// Correction factor
			correction[n] = xgpi[Ngfactors-1]*(sinbeta>xsb[Ngfactors-1])+ xgpi[0]*(sinbeta<xsb[0]); //sinbeta greater (lower) that the maximum (minimum);
			if((xsb[0]<sinbeta) && (sinbeta<xsb[Ngfactors-1])){
				k=0;
				do{
					k++;
				}while(xsb[k]<sinbeta);
				k--;
				// Values to interpolate
				x0 = xsb[k];
				x1 = xsb[k+1];
				y0 = xgpi[k];
				y1 = xgpi[k+1];
				// Correction factor
				correction[n] = ((sinbeta-x0)/(x1-x0))*y1+((x1-sinbeta)/(x1-x0))*y0;
			};// End of if
		}// End of the n-loop
	}else{
		// Use the mean values -> read them!:
		fscanf( gfactorfile, "Mean            %f     %f    %f     %f        %f",&xga[0], &xgd[0], &xgpi[0], &xgmua[0], &xgmud[0]); //Store the values in the first element
		for(n=0; n<N; n++){
			correction[n] = xgpi[0]; // Use the mean correction
		}; //Loop over all the particles
	};//End of "if"

	fclose( gfactorfile ); // Close the file
}// End of geometric_correction
//-----------------------------------------------------------------------------------------------------------------//



//-----------------------------------------------------------------------------------------------------------------//
/* ConversionSigmas */
void ConversionSigmas(float *lon, float *lat, float *sigmapi, int N, int flag, float *out_alphastar, float *out_delta, float *out_muas, float *out_mudelta){
// Once the error of the parallax is known, estimate the errors in:
// alpha*, delta, mu_alpha*, mu_delta 
// If flag == -1, use the interpolated values; otherwise use the mean values:
// This code makes use of the file:
// Uses the file gfactor-Jun2013.dat
// UNITS: lon, lat are in radians, if sigmapi is in mas, the outputs are in mas/yr or mas.

	int k,n; //Counters
	float sinbeta;

	// Interpolation variables
	float xsb[Ngfactors], xga[Ngfactors], xgd[Ngfactors], xgpi[Ngfactors], xgmua[Ngfactors], xgmud[Ngfactors];
	float x0, x1, y0, y1;
	float correction; // The correction that geometric_correction() applied to sigma_pi must be removed

	// Files:
	FILE *gfactorfile;

	gfactorfile = fopen( GFACTOR_FILE, "r");

	// Read and store the table:
	fseek( gfactorfile, 67, SEEK_SET); //Jump the first line
	for(k=0;k<Ngfactors;k++){
		fscanf( gfactorfile,"%f           %f     %f    %f     %f        %f\n",&xsb[k], &xga[k], &xgd[k], &xgpi[k], &xgmua[k], &xgmud[k]);
	}// Store the interpolation values


	if(flag==-1){
	// Use the interpolated values
		//For each particle...
		for(n=0; n<N; n++){
			sinbeta = 0.4971*sin(lat[n]) + 0.8677*cos(lat[n])*sin(lon[n]-0.11135200627723822); //...sin(lon-6º38)
			// Correction factors (sinbeta out of range)
			out_alphastar[n] = xga[Ngfactors-1]*(sinbeta>xsb[Ngfactors-1])+ xga[0]*(sinbeta<xsb[0]); //sinbeta greater (lower) that the maximum (minimum);
			out_delta[n] = xgd[Ngfactors-1]*(sinbeta>xsb[Ngfactors-1])+ xgd[0]*(sinbeta<xsb[0]); //sinbeta greater (lower) that the maximum (minimum);
			correction = xgpi[Ngfactors-1]*(sinbeta>xsb[Ngfactors-1])+ xgpi[0]*(sinbeta<xsb[0]); //sinbeta greater (lower) that the maximum (minimum);
			out_muas[n] = xgmua[Ngfactors-1]*(sinbeta>xsb[Ngfactors-1])+ xgmua[0]*(sinbeta<xsb[0]); //sinbeta greater (lower) that the maximum (minimum);
			out_mudelta[n] = xgmud[Ngfactors-1]*(sinbeta>xsb[Ngfactors-1])+ xgmud[0]*(sinbeta<xsb[0]); //sinbeta greater (lower) that the maximum (minimum);
			if((xsb[0]<sinbeta) && (sinbeta<xsb[Ngfactors-1])){
				k=0;
				do{
					k++;
				}while(xsb[k]<sinbeta);
				k--;
				// Values to interpolate
				x0 = xsb[k];
				x1 = xsb[k+1];
				// For the alpha* correction
				y0 = xga[k];
				y1 = xga[k+1];				
				out_alphastar[n] = ((sinbeta-x0)/(x1-x0))*y1+((x1-sinbeta)/(x1-x0))*y0;// Correction factor for alpha*
				// For the delta correction
				y0 = xgd[k];
				y1 = xgd[k+1];				
				out_delta[n] = ((sinbeta-x0)/(x1-x0))*y1+((x1-sinbeta)/(x1-x0))*y0;// Correction factor for delta
				// Extract the applied correction for the parallax error
				y0 = xgpi[k];
				y1 = xgpi[k+1];	
				correction = ((sinbeta-x0)/(x1-x0))*y1+((x1-sinbeta)/(x1-x0))*y0;// Correction factor for the parallax
				// For the mu_alpha* correction
				y0 = xgmua[k];
				y1 = xgmua[k+1];				
				out_muas[n] = ((sinbeta-x0)/(x1-x0))*y1+((x1-sinbeta)/(x1-x0))*y0;// Correction factor for mu_alpha*
				// For the mu_delta correction
				y0 = xgmud[k];
				y1 = xgmud[k+1];				
				out_mudelta[n] = ((sinbeta-x0)/(x1-x0))*y1+((x1-sinbeta)/(x1-x0))*y0;// Correction factor for mu_delta
			};// End of if
			//Multiply by sigma_pi and divide by correction:
			out_alphastar[n] = out_alphastar[n]*sigmapi[n]/correction;
			out_delta[n] = out_delta[n]*sigmapi[n]/correction; 
			out_muas[n] = out_muas[n]*sigmapi[n]/correction; 
			out_mudelta[n] = out_mudelta[n]*sigmapi[n]/correction; 

		}// End of the n-loop
	}else{
		// Use the mean values -> read them!:
		fscanf( gfactorfile, "Mean            %f     %f    %f     %f        %f",&xga[0], &xgd[0], &xgpi[0], &xgmua[0], &xgmud[0]); //Store the values in the first element
		// Here xgpi[0] is equivalent to "correction"
		// Renormalize the corrections (divide by the sigma_pi factor):
		xga[0] = xga[0]/xgpi[0]; // alpha*
		xgd[0] = xgd[0]/xgpi[0]; //delta
		xgmua[0] = xgmua[0]/xgpi[0];//mu_a*
		xgmud[0] = xgmud[0]/xgpi[0];// mu_delta
		//Use the mean corrections:
		for(n=0; n<N; n++){
			out_alphastar[n] = xga[0]*sigmapi[n];
			out_delta[n] = xgd[0]*sigmapi[n]; 
			out_muas[n] = xgmua[0]*sigmapi[n]; 
			out_mudelta[n] = xgmud[0]*sigmapi[n]; 
		}; //Loop over all the particles
	};//End of "if"

	fclose( gfactorfile ); // Close the file


}; //End of ConversionSigmas
//------------------------------------------------------------------------------------------------------------------//



//-----------------------------------------------------------------------------------------------------------------//
/* StellarParameters_logm */
void StellarParameters_logm(float Z_p, float logAge_p, float *logmasses, float *logTe, float *logG, float *mbol ){
/* Returns logTe, logg and the bolometric mag. for the initial stellar logmasses "logmasses" given the isochrone (Z_p, logAge_p)*/
/* Max_Number_of_logmasses_per_isochrone is the length of the outputs*/
// UNITS: Z_p is a fraction, Ages in years, masses in solar masses, mbol in mags
	int n, star; // Counters
	float Z_set[34]; // Set of metallicities (the grid is not regular in Z)

	/* Values for the particle */
	int i_Z, i_logAge, i_isochrone; //Index of the metallicity, logAge, isochrone

	/* Values for each isochrone */
	int starting_point_of_the_isochrone, Number_of_logmasses_in_the_isochrone;

	/* array of logmasses */
	const float dlogm = (Logmi_MAX-Logmi_MIN)/(1.0*Max_Number_of_logmasses_per_isochrone); // bin size

	// Files:
	FILE *fichero_ASCII; // ASCII file with the information about the isochrones
	FILE *fichero_stat_bin; //Binary file with the statistics of all the isochrones
	FILE *fichero_isochr_bin; //Binary file with all the isochrones


	/* Read the metallicity values of the grid */
	fichero_ASCII = fopen( ISONO,"r"); //Open All_the_isochrones3.bin
	for (n=0; n<Number_of_Z;n++){
		fscanf( fichero_ASCII, "%f\n", &Z_set[n]); // Read the decimal part of the metallicity 
		Z_set[n] = Z_set[n]*1E-6; // Convert to true Z [met]

	} // Metallicity values read!
	fclose( fichero_ASCII ); //Close the ASCII file

	// Avoid out of range values:
	logAge_p = (logAge_p<logAge_min? logAge_min: logAge_p);
	logAge_p = (logAge_p>logAge_max? logAge_max: logAge_p);

	Z_p = (Z_p<Z_set[0]? Z_set[0]: Z_p);
	Z_p = (Z_p>Z_set[Number_of_Z-1]? Z_set[Number_of_Z-1]: Z_p);



	//Fill the array of logmasses:
	for(star=0; star<Max_Number_of_logmasses_per_isochrone;star++){
		logmasses[star] = Logmi_MIN + dlogm*star;
	};

	// Get the index of that metallicity value (the Z sampling is not homogeneous):
	i_Z = -1; //Initial value
	do{
		i_Z++;
	}while( Z_set[i_Z]<=Z_p );
	i_Z--;

	//Assume that the logAge grid is regular (from logAge_min to logAge_max with step logAge_step)
	i_logAge = (int)((Number_of_logAge-1)*(logAge_p-logAge_min)/(logAge_max-logAge_min));
	i_isochrone = i_logAge + (Number_of_logAge)*i_Z;

	//++++++++++++++++++++++++++++ First: logTe ++++++++++++++++++++++++++++++++++++++++++++++++++++++//

	// Open the isochrone file:
	fichero_isochr_bin = fopen( BIN_logTe,"rb"); //Open the list of logTeff
	fichero_stat_bin = fopen( EXPL_logTe,"rb"); // Open the statistics of logTeff


	// Read the following information about the selected isochrone: [starting_point of the isochrone, Nmasses]
	fseek( fichero_stat_bin, 32*i_isochrone+16, SEEK_SET);// 4 bytes = 4 bytes*1columns columns

	fread(&starting_point_of_the_isochrone,4,1,fichero_stat_bin); //Starting position (inclusive)
	fread(&Number_of_logmasses_in_the_isochrone,4,1,fichero_stat_bin);// Rows (stars) that belong to this isochrone


	// Go to the desired isochrone
	fseek( fichero_isochr_bin, 4*starting_point_of_the_isochrone, SEEK_SET); //1=1*4 bytes = np.float32 bits*1 column

	// Load the absolute magnitude in the selected band from the isochrone given by (Z, logAge),
	for(star=0;star<Number_of_logmasses_in_the_isochrone;star++){
		// Get the absolute magnitudes
		fread(&logTe[star],4,1, fichero_isochr_bin); // Absolute magnitude

	}; // End of the star-loop

	for(star=Number_of_logmasses_in_the_isochrone; star<Max_Number_of_logmasses_per_isochrone; star++){
		// Return -inf for death stars
		logTe[star]=-INFINITY; // Absolute magnitude
	}; // End of the star-loop

	fclose( fichero_stat_bin );
	fclose( fichero_isochr_bin );

	//++++++++++++++++++++++++++++ Second: logG ++++++++++++++++++++++++++++++++++++++++++++++++++++++//

	// Open the isochrone file:
	fichero_isochr_bin = fopen( BIN_logG,"rb"); //Open the list of logg
	fichero_stat_bin = fopen( EXPL_logG,"rb"); // Open the statistics of logg


	// Read the following information about the selected isochrone: [starting_point of the isochrone, Nmasses]
	fseek( fichero_stat_bin, 32*i_isochrone+16, SEEK_SET);// 32 bytes = 4 bytes*8 columns

	fread(&starting_point_of_the_isochrone,4,1,fichero_stat_bin); //Starting position (inclusive)
	fread(&Number_of_logmasses_in_the_isochrone,4,1,fichero_stat_bin);// Rows (stars) that belong to this isochrone


	// Go to the desired isochrone
	fseek( fichero_isochr_bin, 4*starting_point_of_the_isochrone, SEEK_SET); //1=1*4 bytes = np.float32 bits*1 column

	// Load the absolute magnitude in the selected band from the isochrone given by (Z, logAge),
	for(star=0;star<Number_of_logmasses_in_the_isochrone;star++){
		// Get the absolute magnitudes
		fread(&logG[star],4,1, fichero_isochr_bin); // Absolute magnitude
	}; // End of the star-loop

	for(star=Number_of_logmasses_in_the_isochrone; star<Max_Number_of_logmasses_per_isochrone; star++){
		// Return -inf for death stars
		logG[star]=-INFINITY; // Absolute magnitude
	}; // End of the star-loop

	fclose( fichero_stat_bin );
	fclose( fichero_isochr_bin );

	//++++++++++++++++++++++++++++ Third: mbol ++++++++++++++++++++++++++++++++++++++++++++++++++++++//

	// Open the isochrone file:
	fichero_isochr_bin = fopen( BIN_mbol,"rb"); //Open the list of mbol
	fichero_stat_bin = fopen( EXPL_mbol,"rb"); // Open the statistics of mbol


	// Read the following information about the selected isochrone: [starting_point of the isochrone, Nmasses]
	fseek( fichero_stat_bin, 32*i_isochrone+16, SEEK_SET);// 32 bytes = 4 bytes*8 columns

	fread(&starting_point_of_the_isochrone,4,1,fichero_stat_bin); //Starting position (inclusive)
	fread(&Number_of_logmasses_in_the_isochrone,4,1,fichero_stat_bin);// Rows (stars) that belong to this isochrone


	// Go to the desired isochrone
	fseek( fichero_isochr_bin, 4*starting_point_of_the_isochrone, SEEK_SET); //1=1*4 bytes = np.float32 bits*1 column

	// Load the absolute magnitude in the selected band from the isochrone given by (Z, logAge),
	for(star=0;star<Number_of_logmasses_in_the_isochrone;star++){
		// Get the absolute magnitudes
		fread(&mbol[star],4,1, fichero_isochr_bin); // Absolute magnitude
	}; // End of the star-loop

	for(star=Number_of_logmasses_in_the_isochrone; star<Max_Number_of_logmasses_per_isochrone; star++){
		// Return -inf for death stars
		mbol[star]=-INFINITY; // Absolute magnitude
	}; // End of the star-loop

	fclose( fichero_stat_bin );
	fclose( fichero_isochr_bin );


}//End of StellarParameters_logm
//-----------------------------------------------------------------------------------------------------------------//




//-----------------------------------------------------------------------------------------------------------------//
/* StellarAbsMagnitude_logm */
void StellarAbsMagnitude_logm(float Z_p, float logAge_p, float *logmasses, float *AbsMag, int band_id ){
/* Returns the absolute V, Ic magnitudes for the initial stellar logmasses "logmasses" given the isochrone (Z_p, logAge_p)*/
/* Max_Number_of_logmasses_per_isochrone is the length of the outputs*/
// UNITS: Z_p is a fraction, Ages in years, masses in solar masses, AbsMag in mags
	int n, star; // Counters
	float Z_set[34]; // Set of metallicities (the grid is not regular in Z)

	/* Values for the particle */
	int i_Z, i_logAge, i_isochrone; //Index of the metallicity, logAge, isochrone

	/* Values for each isochrone */
	int starting_point_of_the_isochrone, Number_of_logmasses_in_the_isochrone;

	/* array of logmasses */
	const float dlogm = (Logmi_MAX-Logmi_MIN)/(1.0*Max_Number_of_logmasses_per_isochrone); // bin size

	// Files:
	FILE *fichero_ASCII; // ASCII file with the information about the isochrones
	FILE *fichero_stat_bin; //Binary file with the statistics of all the isochrones
	FILE *fichero_isochr_bin; //Binary file with all the isochrones


	/* Read the metallicity values of the grid */
	fichero_ASCII = fopen( ISONO,"r"); //Open All_the_isochrones3.bin
	for (n=0; n<Number_of_Z;n++){
		fscanf( fichero_ASCII, "%f\n", &Z_set[n]); // Read the decimal part of the metallicity 
		Z_set[n] = Z_set[n]*1E-6; // Convert to true Z [met]

	} // Metallicity values read!
	fclose( fichero_ASCII ); //Close the ASCII file

	// Avoid out of range values:
	logAge_p = (logAge_p<logAge_min? logAge_min: logAge_p);
	logAge_p = (logAge_p>logAge_max? logAge_max: logAge_p);

	Z_p = (Z_p<Z_set[0]? Z_set[0]: Z_p);
	Z_p = (Z_p>Z_set[Number_of_Z-1]? Z_set[Number_of_Z-1]: Z_p);



	// Open the isochrone file:
	switch(band_id){
		case 0:
			fichero_isochr_bin = fopen( BIN_U,"rb"); //Open the list of M_U magnitudes
			fichero_stat_bin = fopen( EXPL_U,"rb"); // Open the statistics of the M_U magnitudes
			break;
		case 1:
			fichero_isochr_bin = fopen( BIN_B,"rb"); //Open the list of M_B magnitudes
			fichero_stat_bin = fopen( EXPL_B,"rb"); // Open the statistics of the M_B magnitudes
			break;
		case 2:
			fichero_isochr_bin = fopen( BIN_V,"rb"); //Open the list of M_V magnitudes
			fichero_stat_bin = fopen( EXPL_V,"rb"); // Open the statistics of the M_V magnitudes
			break;
		case 3:
			fichero_isochr_bin = fopen( BIN_R,"rb"); //Open the list of M_R magnitudes
			fichero_stat_bin = fopen( EXPL_R,"rb"); // Open the statistics of the M_R magnitudes
			break;
		case 4:
			fichero_isochr_bin = fopen( BIN_I,"rb"); //Open the list of M_I magnitudes
			fichero_stat_bin = fopen( EXPL_I,"rb"); // Open the statistics of the M_I magnitudes
			break;
		case 5:
			fichero_isochr_bin = fopen( BIN_J,"rb"); //Open the list of M_J magnitudes
			fichero_stat_bin = fopen( EXPL_J,"rb"); // Open the statistics of the M_J magnitudes
			break;
		case 6:
			fichero_isochr_bin = fopen( BIN_H,"rb"); //Open the list of M_H magnitudes
			fichero_stat_bin = fopen( EXPL_H,"rb"); // Open the statistics of the M_H magnitudes
			break;
		case 7:
			fichero_isochr_bin = fopen( BIN_K,"rb"); //Open the list of M_K magnitudes
			fichero_stat_bin = fopen( EXPL_K,"rb"); // Open the statistics of the M_K magnitudes
			break;
		default:
			fprintf(stderr, "StellarPopulation: Unknown filter. Johnson V band will be used instead\n");
			fichero_isochr_bin = fopen( BIN_V,"rb"); //Open the list of M_V magnitudes
			fichero_stat_bin = fopen( EXPL_V,"rb"); // Open the statistics of the M_V magnitudes

	};



	//Create the array of logmasses:
	for(star=0; star<Max_Number_of_logmasses_per_isochrone;star++){
		logmasses[star] = Logmi_MIN + dlogm*star;
	};


	// Get the index of that metallicity value (the Z sampling is not homogeneous):
	i_Z = -1; //Initial value
	do{
		i_Z++;
	}while( Z_set[i_Z]<=Z_p );
	i_Z--;


	//Assume that the logAge grid is regular (from logAge_min to logAge_max with step logAge_step)
	i_logAge = (int)((Number_of_logAge-1)*(logAge_p-logAge_min)/(logAge_max-logAge_min));
	i_isochrone = i_logAge + (Number_of_logAge)*i_Z;


	// Read the following information about the selected isochrone: [starting_point of the isochrone, Nmasses]
	fseek( fichero_stat_bin, 32*i_isochrone+16, SEEK_SET);// 32 bytes = 4 bytes*8 columns

	fread(&starting_point_of_the_isochrone,4,1,fichero_stat_bin); //Starting position (inclusive)
	fread(&Number_of_logmasses_in_the_isochrone,4,1,fichero_stat_bin);// Rows (stars) that belong to this isochrone


	// Go to the desired isochrone
	fseek( fichero_isochr_bin, 4*starting_point_of_the_isochrone, SEEK_SET); //1=1*4 bytes = np.float32 bits*1 column

	// Load the absolute magnitude in the selected band from the isochrone given by (Z, logAge),
	for(star=0;star<Number_of_logmasses_in_the_isochrone;star++){
		// Get the absolute magnitudes
		fread(&AbsMag[star],4,1, fichero_isochr_bin); // Absolute magnitude
	}; // End of the star-loop

	for(star=Number_of_logmasses_in_the_isochrone;star<Max_Number_of_logmasses_per_isochrone;star++){
		// Return -inf for death stars
		AbsMag[star]=-INFINITY; // Absolute magnitude
	}; // End of the star-loop


	fclose( fichero_stat_bin );
	fclose( fichero_isochr_bin );

}//End of StellarAbsMagnitude_logm
//-----------------------------------------------------------------------------------------------------------------//



//-----------------------------------------------------------------------------------------------------------------//
/* StellarParameters_linm */
void StellarParameters_linm(float Z_p, float logAge_p, float *masses, float *logTe, float *logG, float *mbol ){
/* Returns logTe, logg and the bolometric mag. for the initial stellar masses "masses" given the isochrone (Z_p, logAge_p)*/
/* Max_Number_of_masses_per_isochrone is the length of the outputs*/
// UNITS: Z_p is a fraction, Ages in years, masses in solar masses, mbol in mags
	int n, star; // Counters
	float Z_set[34]; // Set of metallicities (the grid is not regular in Z)

	/* Values for the particle */
	int i_Z, i_logAge, i_isochrone; //Index of the metallicity, logAge, isochrone

	/* Values for each isochrone */
	int starting_point_of_the_isochrone, Number_of_masses_in_the_isochrone;

	/* array of masses */
	const float dm = (mi_MAX-mi_MIN)/(1.0*Max_Number_of_masses_per_isochrone); // bin size

	// Files:
	FILE *fichero_ASCII; // ASCII file with the information about the isochrones
	FILE *fichero_stat_bin; //Binary file with the statistics of all the isochrones
	FILE *fichero_isochr_bin; //Binary file with all the isochrones


	/* Read the metallicity values of the grid */
	fichero_ASCII = fopen( ISONO,"r"); //Open All_the_isochrones3.bin
	for (n=0; n<Number_of_Z;n++){
		fscanf( fichero_ASCII, "%f\n", &Z_set[n]); // Read the decimal part of the metallicity 
		Z_set[n] = Z_set[n]*1E-6; // Convert to true Z [met]

	} // Metallicity values read!
	fclose( fichero_ASCII ); //Close the ASCII file

	// Avoid out of range values:
	logAge_p = (logAge_p<logAge_min? logAge_min: logAge_p);
	logAge_p = (logAge_p>logAge_max? logAge_max: logAge_p);

	Z_p = (Z_p<Z_set[0]? Z_set[0]: Z_p);
	Z_p = (Z_p>Z_set[Number_of_Z-1]? Z_set[Number_of_Z-1]: Z_p);



	//Fill the array of masses:
	for(star=0; star<Max_Number_of_masses_per_isochrone;star++){
		masses[star] = mi_MIN + dm*star;
	};

	// Get the index of that metallicity value (the Z sampling is not homogeneous):
	i_Z = -1; //Initial value
	do{
		i_Z++;
	}while( Z_set[i_Z]<=Z_p );
	i_Z--;

	//Assume that the logAge grid is regular (from logAge_min to logAge_max with step logAge_step)
	i_logAge = (int)((Number_of_logAge-1)*(logAge_p-logAge_min)/(logAge_max-logAge_min));
	i_isochrone = i_logAge + (Number_of_logAge)*i_Z;

	//++++++++++++++++++++++++++++ First: logTe ++++++++++++++++++++++++++++++++++++++++++++++++++++++//

	// Open the isochrone file:
	fichero_isochr_bin = fopen( BIN_logTe_lin,"rb"); //Open the list of logTeff
	fichero_stat_bin = fopen( EXPL_logTe_lin,"rb"); // Open the statistics of logTeff


	// Read the following information about the selected isochrone: [starting_point of the isochrone, Nmasses]
	fseek( fichero_stat_bin, 32*i_isochrone+16, SEEK_SET);// 32 bytes = 4 bytes*8 columns

	fread(&starting_point_of_the_isochrone,4,1,fichero_stat_bin); //Starting position (inclusive)
	fread(&Number_of_masses_in_the_isochrone,4,1,fichero_stat_bin);// Rows (stars) that belong to this isochrone


	// Go to the desired isochrone
	fseek( fichero_isochr_bin, 4*starting_point_of_the_isochrone, SEEK_SET); //1=1*4 bytes = np.float32 bits*1 column

	// Load the absolute magnitude in the selected band from the isochrone given by (Z, logAge),
	for(star=0;star<Number_of_masses_in_the_isochrone;star++){
		// Get the absolute magnitudes
		fread(&logTe[star],4,1, fichero_isochr_bin); // Absolute magnitude
	}; // End of the star-loop

	for(star=Number_of_masses_in_the_isochrone; star<Max_Number_of_masses_per_isochrone; star++){
		// Return -inf for death stars
		logTe[star]=-INFINITY; // Absolute magnitude
	}; // End of the star-loop

	fclose( fichero_stat_bin );
	fclose( fichero_isochr_bin );

	//++++++++++++++++++++++++++++ Second: logG ++++++++++++++++++++++++++++++++++++++++++++++++++++++//

	// Open the isochrone file:
	fichero_isochr_bin = fopen( BIN_logG_lin,"rb"); //Open the list of logg
	fichero_stat_bin = fopen( EXPL_logG_lin,"rb"); // Open the statistics of logg


	// Read the following information about the selected isochrone: [starting_point of the isochrone, Nmasses]
	fseek( fichero_stat_bin, 32*i_isochrone+16, SEEK_SET);// 32 bytes = 4 bytes*8 columns

	fread(&starting_point_of_the_isochrone,4,1,fichero_stat_bin); //Starting position (inclusive)
	fread(&Number_of_masses_in_the_isochrone,4,1,fichero_stat_bin);// Rows (stars) that belong to this isochrone


	// Go to the desired isochrone
	fseek( fichero_isochr_bin, 4*starting_point_of_the_isochrone, SEEK_SET); //1=1*4 bytes = np.float32 bits*1 column

	// Load the absolute magnitude in the selected band from the isochrone given by (Z, logAge),
	for(star=0;star<Number_of_masses_in_the_isochrone;star++){
		// Get the absolute magnitudes
		fread(&logG[star],4,1, fichero_isochr_bin); // Absolute magnitude
	}; // End of the star-loop

	for(star=Number_of_masses_in_the_isochrone; star<Max_Number_of_masses_per_isochrone; star++){
		// Return -inf for death stars
		logG[star]=-INFINITY; // Absolute magnitude
	}; // End of the star-loop

	fclose( fichero_stat_bin );
	fclose( fichero_isochr_bin );

	//++++++++++++++++++++++++++++ Third: mbol ++++++++++++++++++++++++++++++++++++++++++++++++++++++//

	// Open the isochrone file:
	fichero_isochr_bin = fopen( BIN_mbol_lin,"rb"); //Open the list of mbol
	fichero_stat_bin = fopen( EXPL_mbol_lin,"rb"); // Open the statistics of mbol


	// Read the following information about the selected isochrone: [starting_point of the isochrone, Nmasses]
	fseek( fichero_stat_bin, 32*i_isochrone+16, SEEK_SET);// 32 bytes = 4 bytes*8 columns

	fread(&starting_point_of_the_isochrone,4,1,fichero_stat_bin); //Starting position (inclusive)
	fread(&Number_of_masses_in_the_isochrone,4,1,fichero_stat_bin);// Rows (stars) that belong to this isochrone


	// Go to the desired isochrone
	fseek( fichero_isochr_bin, 4*starting_point_of_the_isochrone, SEEK_SET); //1=1*4 bytes = np.float32 bits*1 column

	// Load the absolute magnitude in the selected band from the isochrone given by (Z, logAge),
	for(star=0;star<Number_of_masses_in_the_isochrone;star++){
		// Get the absolute magnitudes
		fread(&mbol[star],4,1, fichero_isochr_bin); // Absolute magnitude
	}; // End of the star-loop

	for(star=Number_of_masses_in_the_isochrone; star<Max_Number_of_masses_per_isochrone; star++){
		// Return -inf for death stars
		mbol[star]=-INFINITY; // Absolute magnitude
	}; // End of the star-loop

	fclose( fichero_stat_bin );
	fclose( fichero_isochr_bin );


}//End of StellarParameters_linm
//-----------------------------------------------------------------------------------------------------------------//




//-----------------------------------------------------------------------------------------------------------------//
/* StellarAbsMagnitude_linm */
void StellarAbsMagnitude_linm(float Z_p, float logAge_p, float *masses, float *AbsMag, int band_id ){
/* Returns the absolute V, Ic magnitudes for the initial stellar masses "masses" given the isochrone (Z_p, logAge_p)*/
/* Max_Number_of_masses_per_isochrone is the length of the outputs*/
// UNITS: Z_p is a fraction, Ages in years, masses in solar masses, AbsMag in mags
	int n, star; // Counters
	float Z_set[34]; // Set of metallicities (the grid is not regular in Z)

	/* Values for the particle */
	int i_Z, i_logAge, i_isochrone; //Index of the metallicity, logAge, isochrone

	/* Values for each isochrone */
	int starting_point_of_the_isochrone, Number_of_masses_in_the_isochrone;

	/* array of masses */
	const float dm = (mi_MAX-mi_MIN)/(1.0*Max_Number_of_masses_per_isochrone); // bin size

	// Files:
	FILE *fichero_ASCII; // ASCII file with the information about the isochrones
	FILE *fichero_stat_bin; //Binary file with the statistics of all the isochrones
	FILE *fichero_isochr_bin; //Binary file with all the isochrones


	/* Read the metallicity values of the grid */
	fichero_ASCII = fopen( ISONO,"r"); //Open ISONo file

	for (n=0; n<Number_of_Z;n++){
		fscanf( fichero_ASCII, "%f\n", &Z_set[n]); // Read the decimal part of the metallicity 
		Z_set[n] = Z_set[n]*1E-6; // Convert to true Z [met]

	} // Metallicity values read!
	fclose( fichero_ASCII ); //Close the ASCII file

	// Avoid out of range values:
	logAge_p = (logAge_p<logAge_min? logAge_min: logAge_p);
	logAge_p = (logAge_p>logAge_max? logAge_max: logAge_p);

	Z_p = (Z_p<Z_set[0]? Z_set[0]: Z_p);
	Z_p = (Z_p>Z_set[Number_of_Z-1]? Z_set[Number_of_Z-1]: Z_p);



	// Open the isochrone file:
	switch(band_id){
		case 0:
			fichero_isochr_bin = fopen( BIN_U_lin,"rb"); //Open the list of M_U magnitudes
			fichero_stat_bin = fopen( EXPL_U_lin,"rb"); // Open the statistics of the M_U magnitudes
			break;
		case 1:
			fichero_isochr_bin = fopen( BIN_B_lin,"rb"); //Open the list of M_B magnitudes
			fichero_stat_bin = fopen( EXPL_B_lin,"rb"); // Open the statistics of the M_B magnitudes
			break;
		case 2:
			fichero_isochr_bin = fopen( BIN_V_lin,"rb"); //Open the list of M_V magnitudes
			fichero_stat_bin = fopen( EXPL_V_lin,"rb"); // Open the statistics of the M_V magnitudes
			break;
		case 3:
			fichero_isochr_bin = fopen( BIN_R_lin,"rb"); //Open the list of M_R magnitudes
			fichero_stat_bin = fopen( EXPL_R_lin,"rb"); // Open the statistics of the M_R magnitudes
			break;
		case 4:
			fichero_isochr_bin = fopen( BIN_I_lin,"rb"); //Open the list of M_I magnitudes
			fichero_stat_bin = fopen( EXPL_I_lin,"rb"); // Open the statistics of the M_I magnitudes
			break;
		case 5:
			fichero_isochr_bin = fopen( BIN_J_lin,"rb"); //Open the list of M_J magnitudes
			fichero_stat_bin = fopen( EXPL_J_lin,"rb"); // Open the statistics of the M_J magnitudes
			break;
		case 6:
			fichero_isochr_bin = fopen( BIN_H_lin,"rb"); //Open the list of M_H magnitudes
			fichero_stat_bin = fopen( EXPL_H_lin,"rb"); // Open the statistics of the M_H magnitudes
			break;
		case 7:
			fichero_isochr_bin = fopen( BIN_K_lin,"rb"); //Open the list of M_K magnitudes
			fichero_stat_bin = fopen( EXPL_K_lin,"rb"); // Open the statistics of the M_K magnitudes
			break;
		default:
			fprintf(stderr, "StellarAbsMagnitude: Unknown filter. Johnson V band will be used instead\n");
			fichero_isochr_bin = fopen( BIN_V_lin,"rb"); //Open the list of M_V magnitudes
			fichero_stat_bin = fopen( EXPL_V_lin,"rb"); // Open the statistics of the M_V magnitudes

	};



	//Create the array of masses:
	for(star=0; star<Max_Number_of_masses_per_isochrone;star++){
		masses[star] = mi_MIN + dm*star;
	};


	// Get the index of that metallicity value (the Z sampling is not homogeneous):
	i_Z = -1; //Initial value
	do{
		i_Z++;
	}while( Z_set[i_Z]<=Z_p );
	i_Z--;


	//Assume that the logAge grid is regular (from logAge_min to logAge_max with step logAge_step)
	i_logAge = (int)((Number_of_logAge-1)*(logAge_p-logAge_min)/(logAge_max-logAge_min));
	i_isochrone = i_logAge + (Number_of_logAge)*i_Z;


	// Read the following information about the selected isochrone: [starting_point of the isochrone, Nmasses]
	fseek( fichero_stat_bin, 32*i_isochrone+16, SEEK_SET);// 32 bytes = 4 bytes*8 columns

	fread(&starting_point_of_the_isochrone,4,1,fichero_stat_bin); //Starting position (inclusive)
	fread(&Number_of_masses_in_the_isochrone,4,1,fichero_stat_bin);// Rows (stars) that belong to this isochrone


	// Go to the desired isochrone
	fseek( fichero_isochr_bin, 4*starting_point_of_the_isochrone, SEEK_SET); //1=1*4 bytes = np.float32 bits*1 column

	// Load the absolute magnitude in the selected band from the isochrone given by (Z, logAge),
	for(star=0;star<Number_of_masses_in_the_isochrone;star++){
		// Get the absolute magnitudes
		fread(&AbsMag[star],4,1, fichero_isochr_bin); // Absolute magnitude
	}; // End of the star-loop

	for(star=Number_of_masses_in_the_isochrone;star<Max_Number_of_masses_per_isochrone;star++){
		// Return -inf for death stars
		AbsMag[star]=-INFINITY; // Absolute magnitude
	}; // End of the star-loop


	fclose( fichero_stat_bin );
	fclose( fichero_isochr_bin );

}//End of StellarAbsMagnitude_linm
//-----------------------------------------------------------------------------------------------------------------//








//-----------------------------------------------------------------------------------------------------------------//
/* GetAPOGEEdistances*/
void GetAPOGEEdistances(float *lon, float*lat, float *Z, float *logTe, float *logg, float *J, float* H, float *K, float *eZ, float *elogTe, float *elogg, float *eJ, float *eH, float *eK, float *distestim, int *info_flag, int Nstars){
	float Z_i, logTe_i, logg_i, J_i, H_i, K_i;
	float Z_r, logTe_r, logg_r, J_r, H_r, K_r;
	float eZ_r, elogTe_r, elogg_r, eJ_r, eH_r, eK_r;
	float costJ, costJ_min;
	int realstar, isocstar; // Counters
	float aux[100], EBV[100]; //Ancillary variable and color excess

	float lon_arr[100], lat_arr[100], dist[100];
	int n; // Counter

	// Files:
	FILE *fichero_isochr_bin_metZ; //Binary file with the metallicity of all the isochroness
	FILE *fichero_isochr_bin_logTe; //Binary file with the logTeff of all the isochroness
	FILE *fichero_isochr_bin_logg; //Binary file with the logg of all the isochroness
	FILE *fichero_isochr_bin_J; //Binary file with the J magnitudes of all the isochroness
	FILE *fichero_isochr_bin_H; //Binary file with the J magnitudes of all the isochroness
	FILE *fichero_isochr_bin_K; //Binary file with the J magnitudes of all the isochroness


	// Open the files:
	fichero_isochr_bin_metZ = fopen(BIN_metZ,"rb");
	fichero_isochr_bin_logTe = fopen(BIN_logTe,"rb");
	fichero_isochr_bin_logg = fopen(BIN_logG,"rb");

	fichero_isochr_bin_J = fopen(BIN_J,"rb");
	fichero_isochr_bin_H = fopen(BIN_H,"rb");
	fichero_isochr_bin_K = fopen(BIN_K,"rb");

	//Loop over the observed stars
	for( realstar = 0; realstar<Nstars; realstar++){
		// Parameters of the real star
		Z_r = Z[realstar];
		logTe_r = logTe[realstar];
		logg_r = logg[realstar];
		eZ_r = eZ[realstar];
		elogTe_r = elogTe[realstar];
		elogg_r = elogg[realstar];

		J_r = J[realstar];
		H_r = H[realstar];
		K_r = K[realstar];

		eJ_r = eJ[realstar];
		eH_r = eH[realstar];
		eK_r = eK[realstar];

		// Initialize cost function
		costJ_min = INFINITY;

		// Reset the file position
		fseek( fichero_isochr_bin_metZ, 0, SEEK_SET); // 4 = 4 bytes * 1 columns
		fseek( fichero_isochr_bin_logTe, 0, SEEK_SET); // 4 = 4 bytes * 1 columns
		fseek( fichero_isochr_bin_logg, 0, SEEK_SET); // 4 = 4 bytes * 1 columns


		// Loop over the isochrones
		for(isocstar=0; isocstar<Total_Number_of_isochrones; isocstar++){
			fread(&Z_i,4,1,fichero_isochr_bin_metZ);// Rows (stars) that belong to this isochrone
			fread(&logTe_i,4,1,fichero_isochr_bin_logTe);
			fread(&logg_i,4,1,fichero_isochr_bin_logg);

			//if(realstar==0){
			//	fprintf(stderr, "%i %f %f | %f %f | %f %f\n",isocstar, Z_i, Z_r, logTe_i, logTe_r, logg_i, logg_r);
			//};

			// Cost function
			costJ = 0.;
			costJ = pow2( (Z_i-Z_r)/eZ_r);
			costJ += pow2( (logTe_i-logTe_r)/elogTe_r);
			costJ += pow2( (logg_i-logg_r)/elogg_r );

			info_flag[realstar] = info_flag[realstar]*(costJ>costJ_min) + isocstar*(costJ<=costJ_min);
			costJ_min = min(costJ, costJ_min);
		};// End of loop over the isochrones


		// Go to the best matching isochrone star:
		fseek( fichero_isochr_bin_metZ, info_flag[realstar]*4, SEEK_SET); // 4 = 4 bytes * 1 columns
		fseek( fichero_isochr_bin_logTe, info_flag[realstar]*4, SEEK_SET); // 4 = 4 bytes * 1 columns
		fseek( fichero_isochr_bin_logg, info_flag[realstar]*4, SEEK_SET); // 4 = 4 bytes * 1 columns
		fseek( fichero_isochr_bin_J, info_flag[realstar]*4, SEEK_SET); // 4 = 4 bytes * 1 columns
		fseek( fichero_isochr_bin_H, info_flag[realstar]*4, SEEK_SET); // 4 = 4 bytes * 1 columns
		fseek( fichero_isochr_bin_K, info_flag[realstar]*4, SEEK_SET); // 4 = 4 bytes * 1 columns

		// Reads (Z, logTe, logg)
		fread(&Z_i,4,1,fichero_isochr_bin_metZ);// Rows (stars) that belong to this isochrone
		fread(&logTe_i,4,1,fichero_isochr_bin_logTe);
		fread(&logg_i,4,1,fichero_isochr_bin_logg);
		
		fread(&J_i,4,1,fichero_isochr_bin_J);
		fread(&H_i,4,1,fichero_isochr_bin_H);
		fread(&K_i,4,1,fichero_isochr_bin_K);

		// Get the extinction
		lon_arr[0] = lon[realstar];
		lat_arr[0] = lat[realstar];
		dist[0] = .02;
		for(n = 1; n<100; n++){
			lon_arr[n] = lon_arr[n-1];
			lat_arr[n] = lat_arr[n-1];
			dist[n] = dist[n-1] + 0.2; // up to 10 kpc
		};

		ColourExcessMap(lon_arr, lat_arr, 100, EBV);
		ColourExcess3DMap(lon_arr, lat_arr, dist, 100, aux);
		// Sharma, 3D correction and log10(d) correction
		for(n=0;n<100;n++){
			EBV[n] = EBV[n]*aux[n]*(0.6+0.2*(1.0-tanh((10*EBV[n]*aux[n]-1.5))));
		};

		// Now get the minimum cost:
		costJ_min = INFINITY;
		for(n=0;n<100;n++){
			costJ =  pow2( (J_r-J_i-10. - 5.*log10(dist[n]) - 0.9536616*EBV[n])/eJ_r );
			costJ += pow2( (H_r-H_i-10. - 5.*log10(dist[n]) -0.5873472*EBV[n])/eH_r );
			costJ += pow2( (K_r-K_i-10. - 5.*log10(dist[n]) -0.367*EBV[n])/eK_r );

			costJ_min = min(costJ_min, costJ);
			distestim[realstar] = dist[n]*(costJ<=costJ_min) + distestim[realstar]*(costJ>costJ_min);
		};



	}; //En of loop over the observed stars

	//Close binary files
	fclose( fichero_isochr_bin_metZ );
	fclose( fichero_isochr_bin_logTe );
	fclose( fichero_isochr_bin_logg );
	fclose( fichero_isochr_bin_J );
	fclose( fichero_isochr_bin_H );
	fclose( fichero_isochr_bin_K );


}//GetAPOGEEdistances
//-----------------------------------------------------------------------------------------------------------------//




// ------------------------------------- Some ancillary functions --------------------------------------------------------
float Spectr_parameter_a(float VmIc){
	//Instead of interpolating, we model a(V-Ic) as a piecewise function.
	// This function implicitly uses TableVr-jun2015
	float a;

	//If (V-Ic)< -0.08:
	a = 0.9*(VmIc<-.08);
	//If -0.08<= (V-Ic)< 0.16:
	a+= (0.9+.25*(VmIc+0.08)/.24)*(-0.08<=VmIc)*(VmIc<.16);
	//If (V-Ic)< 0.16:
	a+= 1.15*(0.16<=VmIc);
return a;}


float LinInterp(float x_input, float *x_data, float *y_data, int Ndata){
	// Linear interpolation	
	int k; //Counter
	float x0, x1, y0, y1;
	float y_output;
	
	x_input = x_data[Ndata-1]*(x_input>=x_data[Ndata-1]) + x_data[0]*(x_input<=x_data[0])+x_input*(x_input<x_data[Ndata-1])*(x_input>x_data[0]);

	k = 0;
	while(x_data[k]<x_input){
		k++;
	}
	k--;
	
	x0 = x_data[k];
	x1 = x_data[k+1];
	y0 = y_data[k];
	y1 = y_data[k+1];
	y_output = ((x_input-x0)/(x1-x0))*y1+((x1-x_input)/(x1-x0))*y0;

return y_output;}
