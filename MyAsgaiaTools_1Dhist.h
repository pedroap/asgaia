#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#ifndef kimf_m0
#include "AsgaiaConstants.h"
#endif



//Note: M_PI is defined in math.h

/* Created 13/10/2017
   Modified 02/03/2018
  This .h file contains the versions in C of the Fortran code PopSynth.F (Snapdragons)

	SigmaPiGaia_IMGhist_Salpeter(float *Z, float *logAge, float *dist, float *ex31, float *correction, float *error_pi, float *ini_mass_histogram, float *Gband_histogram, int Nparticles);
	SigmaPiGaia_IMGhist_Kroupa(float *Z, float *logAge, float *dist, float *ex31, float *correction, float *error_pi, float *ini_mass_histogram, float *Gband_histogram, int Nparticles);

	Mag_distr_Salpeter(float *Z, float *logAge, float *ParticleMass, float *dist, float *extinction, float *magnitude_histogram, float *Mfcis, int Nparticles, int bins_in_mag, float *mag_axis, int band_id );
	Mag_distr_Kroupa(float *Z, float *logAge, float *ParticleMass, float *dist, float *extinction, float *magnitude_histogram, float *Mfcis, int Nparticles, int bins_in_mag, float *mag_axis, int band_id );

	CMD_Salpeter(float *Z, float *logAge, float *ParticleMass, float *dist, float *A_V, float *cmd_map, float *Mfcis, int Nparticles, int *CMD_dim, float *mag_axis, int *bands_id );
	CMD_Salpeter_4Points(float *Z, float *logAge, float *ParticleMass, float *dist, float *A_V, float *cmd_map, float *Mfcis, int Nparticles, int *CMD_dim, float *mag_axis, int *bands_id );
	CMD_Kroupa(float *Z, float *logAge, float *ParticleMass, float *dist, float *A_V, float *cmd_map, float *Mfcis, int Nparticles, int *CMD_dim, float *mag_axis, int *bands_id );
	CMD_Kroupa_4Points(float *Z, float *logAge, float *ParticleMass, float *dist, float *A_V, float *cmd_map, float *Mfcis, int Nparticles, int *CMD_dim, float *mag_axis, int *bands_id );

	HR_Salpeter(float *Z, float *logAge, float *ParticleMass, float *dist, float *A_V, float *hr_map, float *Mfcis, int Nparticles, int *HR_dim, float *hr_axis, int *bands_id );
	HR_Salpeter_4Points(float *Z, float *logAge, float *ParticleMass, float *dist, float *A_V, float *hr_map, float *Mfcis, int Nparticles, int *HR_dim, float *hr_axis, int *bands_id );
	HR_Kroupa(float *Z, float *logAge, float *ParticleMass, float *dist, float *A_V, float *hr_map, float *Mfcis, int Nparticles, int *HR_dim, float *hr_axis, int *bands_id );
	HR_Kroupa_4Points(float *Z, float *logAge, float *ParticleMass, float *dist, float *A_V, float *hr_map, float *Mfcis, int Nparticles, int *HR_dim, float *hr_axis, int *bands_id );
*/

// +++++++++++++++++++++++++++++++++++++ 1D Histograms +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//



//-----------------------------------------------------------------------------------------------------------------//
/* SigmaPiGaia_IMGhist_Salpeter */
void SigmaPiGaia_IMGhist_Salpeter(float *Z, float *logAge, float *ParticleMass, float *dist, float *ex31, float *total_correction, float *error_pi, float *ini_mass_histogram, float *Gband_histogram, float *nobstars, int Nparticles, int Nbins_G, float *G_histo_axis){
/* Since ex31 is provided, lon and lat are not needed*/
// ini_mass_histogram must have Max_Number_of_logmasses_per_isocrone elements
// Histogram of the initial masses: ini_mass_histogram
// Histogram of the G band: Gband_histogram
// UNITS: Z is a fraction, Ages in yrs, distances in kpc, ex31 in mag, error_pi in mas
	int n, particle, star, Gbin; // Counters
	float Z_set[34]; // Set of metallicities (the grid is not regular in Z)
	float aux; //auxiliary variable
	int is_selected; // 1 if the G mag is in the Gaia range, 0 otherwise

	/* Values for each particle */
	int i_Z, i_logAge, i_isocrone; //Index of the metallicity, logAge, isocrone
	float Z_p, logAge_p, Mass_p, ex31_p; // Values for one selected particle
	float distance_correction; // 10+5*log10(d_kpc)

	/* Values for each star */
	float V_star, G_star, VmIc_star; //Apparent V mag, G mag and colour difference

	/* Values for each isocrone */
//	float V_min, V_max, VmIc_min, VmIc_max;//Parameters to read in Explanation3.bin
	int starting_point_of_the_isocrone, Number_of_logmasses_in_the_isocrone;

	/* IMF */
	const float dlogm = (Logmi_MAX-Logmi_MIN)/(1.0*Max_Number_of_logmasses_per_isocrone); // bin size
	float mi[Max_Number_of_logmasses_per_isocrone], N_mi[Max_Number_of_logmasses_per_isocrone]; //Masses and global IMF
	float N_mi_p[Max_Number_of_logmasses_per_isocrone]; //Masses distribution for a particular particle -> Gaia selection affected
	float Cum_N_mi;

	// Histogram limits:
	float G_histo_min = max(G_histo_axis[0], Gmin);
	float G_histo_max = min(G_histo_axis[1], Gmax);

	// Files:
	FILE *fichero_ASCII; // ASCII file with the information about the isocrones
	FILE *fichero_stat_bin; //Binary file with the statistics of all the isocrones
	FILE *fichero_isocr_bin; //Binary file with all the isocroness

	// Always in the Gaia range
	G_histo_axis[0] = G_histo_min;
	G_histo_axis[1] = G_histo_max;


	/* Read the metallicity values of the grid */
	fichero_ASCII = fopen( ISONO,"r"); //Open All_the_isocrones2.bin
	for (n=0; n<Number_of_Z;n++){
		fscanf( fichero_ASCII, "%f\n", &Z_set[n]); // Read the decimal part of the metallicity 
		Z_set[n] = Z_set[n]*1E-6; // Convert to true Z [met]

	} // Metallicity values read!
	fclose( fichero_ASCII ); //Close the ASCII file


	fichero_isocr_bin = fopen( BIN3,"rb"); //Open All_the_isocrones2.bin
	fichero_stat_bin = fopen( EXPL3,"rb"); // Open the statistics of all the isocr.

	//Since logmi_mi and Logmi_MAX are equal for all of the isocrones, the IMF would be the same:
	/* Probability given the Kroupa IMF */
	for(star=0; star<Max_Number_of_logmasses_per_isocrone;star++){
		mi[star] = pow(10., Logmi_MIN + dlogm*star);
	};

	//Call to the Salpeter IMF function
	SalpeterIMF( mi, Max_Number_of_logmasses_per_isocrone, INFINITY, N_mi); //N_mi means number of masses between mi and mi+dm (egde effects included)

	//Initiliaze the histogram of the initial masses:
	for(star=0; star<Max_Number_of_logmasses_per_isocrone; star++){
		ini_mass_histogram[star] = 0.0; //Take advantage of the for loop and initialize
	};
	//Initiliaze the histogram of the G band:
	for(Gbin=0;Gbin<Nbins_G;Gbin++){
		Gband_histogram[Gbin]=0.0;
	};

// Loop over the particles
	for(particle=0;particle<Nparticles;particle++){

		// Store the parameters of the considered particle:
		Z_p = Z[particle];
		logAge_p = logAge[particle];
		Mass_p = ParticleMass[particle];
		ex31_p = ex31[particle];
		distance_correction = 10.+5.*log10(dist[particle]);
		

		// Avoid out of range values:
		logAge_p = (logAge_p<logAge_min? logAge_min: logAge_p);
		logAge_p = (logAge_p>logAge_max? logAge_max: logAge_p);

		Z_p = (Z_p<Z_set[0]? Z_set[0]: Z_p);
		Z_p = (Z_p>Z_set[Number_of_Z-1]? Z_set[Number_of_Z-1]: Z_p);


		// Get the index of that metallicity value (the Z sampling is not homogeneous):
		i_Z = -1; //Initial value
		do{
			i_Z++;
		}while( Z_set[i_Z]<=Z_p );
		i_Z--;


		//Assume that the logAge grid is regular (from logAge_min to logAge_max with step logAge_step)
		i_logAge = (int)((Number_of_logAge-1)*(logAge_p-logAge_min)/(logAge_max-logAge_min));
		i_isocrone = i_logAge + (Number_of_logAge)*i_Z;


		// Read the following information about the selected isocrone: [Z, logAge, m_ini.min, m_ini.max, Nasses, V.min, V.max, (V-Ic).min, (V-Ic).max]
//		fseek( fichero_stat_bin, 40*i_isocrone, SEEK_SET);// 40 bytes = 4 bytes*10 columns
//		fread(&Z_p,4,1,fichero_stat_bin); // Metallicity of the isocrone
//		fread(&logAge_p,4,1,fichero_stat_bin); // logAge of the isocrone
		
		fseek( fichero_stat_bin, 40*i_isocrone+16, SEEK_SET);// 40 bytes = 4 bytes*10 columns
//		skip logmi_*
//		fread(&Logmi_MIN,4,1,fichero_stat_bin); // min of the initial mass (of the isocrone)
//		fread(&Logmi_MAX,4,1,fichero_stat_bin); // max of the initial mass (of the isocrone)

		fread(&starting_point_of_the_isocrone,4,1,fichero_stat_bin); //Starting position (inclusive)
		fread(&Number_of_logmasses_in_the_isocrone,4,1,fichero_stat_bin);// Rows (stars) that belong to this isocrone

//		fread(&V_min,4,1,fichero_stat_bin); // min of the V band (of the isocrone)
//		fread(&V_max,4,1,fichero_stat_bin); // max of the V band (of the isocrone)
//		fread(&VmIc_min,4,1,fichero_stat_bin); // min of the V band (of the isocrone)
//		fread(&VmIc_max,4,1,fichero_stat_bin); // max of the V band (of the isocrone)


		// Initialize the variables:
		aux = 0.; // Quantity used to estimate error_pi
		error_pi[particle] = 0.; // Sigma_pi
		Cum_N_mi = 0.; //New norm of N_mi_p
		nobstars[particle] = 0.;

		// Go to the desired isocrone
		fseek( fichero_isocr_bin, 8*starting_point_of_the_isocrone, SEEK_SET); //8=2*4 bytes = np.float32 bits*2 columns


		//Correct the Particle Mass to exclude stellar masses larger than mi[ Number_of_logmasses_in_the_isocrone-1]
		Mass_p = Mass_p/(1.-pow(mi[Number_of_logmasses_in_the_isocrone-1]/simf_m0, 2.-simf_a)); // Finite max mass correction


		// Load V and V-Ic from the isocrone given by (Z, logAge), and finish P(m_i)
		for(star=0;star<Number_of_logmasses_in_the_isocrone;star++){
			// Get the absolute magnitudes
			fread(&V_star,4,1, fichero_isocr_bin); // Absolute V
			fread(&VmIc_star,4,1, fichero_isocr_bin); // Absolute V-Ic

			// From absolute to apparent
			V_star = V_star + distance_correction + ex31_p;
			VmIc_star = VmIc_star + 355./900.0*ex31_p; // 355/900 = 0.394444... = 1.962/3.24 (ratio between V and Ic extinction corrections)

			// Estimate the G band
			G_star = V_star-0.0257-0.0924*VmIc_star-0.1623*VmIc_star*VmIc_star+0.0090*VmIc_star*VmIc_star*VmIc_star; // G band (Tony Brown's formula)
		
			// Would this star be observed?
			is_selected = (G_histo_min<=G_star)*(G_star<G_histo_max);// 1= Yes, 0=No

			// Calculate the error for this star and add it to the particle contribution:
			aux = max(0.063095734448019303, pow(10.,0.4*G_star*is_selected-6.)); // 0.06309...=10**(0.4*(12.0-15))
			// aux = z in www.cosmos.esa.int/web/gaia/science-performance#photometric performance
			// Including the is_selected in the power we avoid out-of-float range values when the extinction is extremely high
			aux =  sqrt(-1.631 + 680.766*aux + 32.732*aux*aux)*(0.986 + 0.014*VmIc_star); // Here aux = sigma_pi for the considered star
			aux = aux*is_selected; // Exclude the contribution of the brightest/faintest stars

			N_mi_p[star] = N_mi[star]*is_selected; // Make N_mi=0 if the star is not detected
			Cum_N_mi += N_mi_p[star]; //Renormalisation of N_mi due to the out-of-G range stars. Cum_N_mi is the total number of stars observed (if Mass_p = 1)
			error_pi[particle] += aux*N_mi_p[star]; // Add the contribution to the average

			ini_mass_histogram[star] += Mass_p*N_mi_p[star]; //Add the contribution to the histogram

			//Histogram of the G-band
			Gbin = (int)(Nbins_G*(G_star-G_histo_min)/(G_histo_max-G_histo_min));
			if((G_histo_min<=G_star)*(G_star<G_histo_max)){
				Gband_histogram[Gbin] += Mass_p*N_mi[star];//Add the contribution to the histogram
			};

		}; // End of the star-loop

		// Save the number of stars observed for this particle
		nobstars[particle] = Mass_p*Cum_N_mi;

		// Correction due to the G-band exclusion
		Cum_N_mi = Cum_N_mi -1.0*(Cum_N_mi<=0);//If no stars are observed, return a negative value of sigma_pi
		error_pi[particle] = total_correction[particle]*error_pi[particle]/Cum_N_mi; //Include the Calibration, the scanning law and the epoch corrections
		error_pi[particle] = error_pi[particle]*(Cum_N_mi>0.) - 99000.00*(Cum_N_mi<=0.);//If no stars are observed, return a negative value of sigma_pi
		error_pi[particle] *= 0.001; //From uas to mas

	
	}; // End of the particle

	fclose( fichero_stat_bin );
	fclose( fichero_isocr_bin );

}//End of SigmaPiGaia_IMGhist_Salpeter
//-----------------------------------------------------------------------------------------------------------------//




//-----------------------------------------------------------------------------------------------------------------//
/* SigmaPiGaia_IMGhist_Kroupa */
void SigmaPiGaia_IMGhist_Kroupa(float *Z, float *logAge, float *ParticleMass, float *dist, float *ex31, float *total_correction, float *error_pi, float *ini_mass_histogram, float *Gband_histogram, float *nobstars, int Nparticles, int Nbins_G, float *G_histo_axis ){
/* Since ex31 is provided, lon and lat are not needed*/
// ini_mass_histogram must have Max_Number_of_logmasses_per_isocrone elements
// Histogram of the initial masses: ini_mass_histogram
// Histogram of the G band: Gband_histogram
// UNITS: Z is a fraction, Ages in yrs, distances in kpc, ex31 in mag, error_pi in mas
	int n, particle, star, Gbin; // Counters
	float Z_set[34]; // Set of metallicities (the grid is not regular in Z)
	float aux; //auxiliary variable
	int is_selected; // 1 if the G mag is in the Gaia range, 0 otherwise

	/* Values for each particle */
	int i_Z, i_logAge, i_isocrone; //Index of the metallicity, logAge, isocrone
	float Z_p, logAge_p, Mass_p, ex31_p; // Values for one selected particle
	float distance_correction; // 10+5*log10(d_kpc)

	/* Values for each star */
	float V_star, G_star, VmIc_star; //Apparent V mag, G mag and colour difference

	/* Values for each isocrone */
//	float V_min, V_max, VmIc_min, VmIc_max;//Parameters to read in Explanation2.bin
	int starting_point_of_the_isocrone, Number_of_logmasses_in_the_isocrone;

	/* IMF */
	const float dlogm = (Logmi_MAX-Logmi_MIN)/(1.0*Max_Number_of_logmasses_per_isocrone); // bin size
	float mi[Max_Number_of_logmasses_per_isocrone], N_mi[Max_Number_of_logmasses_per_isocrone]; //Masses and global IMF
	float N_mi_p[Max_Number_of_logmasses_per_isocrone]; //Masses distribution for a particular particle -> Gaia selection affected
	float Cum_N_mi;
	float mL; //Maximum initial mass

	// Histogram limits:
	float G_histo_min = max(G_histo_axis[0], Gmin);
	float G_histo_max = min(G_histo_axis[1], Gmax);

	// Files:
	FILE *fichero_ASCII; // ASCII file with the information about the isocrones
	FILE *fichero_stat_bin; //Binary file with the statistics of all the isocrones
	FILE *fichero_isocr_bin; //Binary file with all the isocroness

	// Always in the Gaia range
	G_histo_axis[0] = G_histo_min;
	G_histo_axis[1] = G_histo_max;

	/* Read the metallicity values of the grid */
	fichero_ASCII = fopen( ISONO,"r"); //Open IsoNo.txt
	for (n=0; n<Number_of_Z;n++){
		fscanf( fichero_ASCII, "%f\n", &Z_set[n]); // Read the decimal part of the metallicity 
		Z_set[n] = Z_set[n]*1E-6; // Convert to true Z [met]

	} // Metallicity values read!
	fclose( fichero_ASCII ); //Close the ASCII file


	fichero_isocr_bin = fopen( BIN3,"rb"); //Open All_the_isocrones3.bin
	fichero_stat_bin = fopen( EXPL3,"rb"); // Open the statistics of all the isocr.

	//Since logmi_mi and Logmi_MAX are equal for all of the isocrones, the IMF would be the same:
	/* Probability given the Kroupa IMF */
	for(star=0; star<Max_Number_of_logmasses_per_isocrone;star++){
		mi[star] = pow(10., Logmi_MIN + dlogm*star);
	};
//fprintf(stderr,"%f %f %f \n", Logmi_MIN, Logmi_MAX, dlogm);

	//Initiliaze the histogram:
	for(star=0; star<Max_Number_of_logmasses_per_isocrone; star++){
		ini_mass_histogram[star] = 0.0; //Take advantage of the for loop and initialize
	};

	//Initiliaze the histogram of the G band:
	for(Gbin=0;Gbin<Nbins_G;Gbin++){
		Gband_histogram[Gbin]=0.0;
	};

// Loop over the particles
	for(particle=0;particle<Nparticles;particle++){

		// Store the parameters of the considered particle:
		Z_p = Z[particle];
		logAge_p = logAge[particle];
		Mass_p = ParticleMass[particle];
		ex31_p = ex31[particle];
		distance_correction = 10.+5.*log10(dist[particle]);
		

		// Avoid out of range values:
		logAge_p = (logAge_p<logAge_min? logAge_min: logAge_p);
		logAge_p = (logAge_p>logAge_max? logAge_max: logAge_p);

		Z_p = (Z_p<Z_set[0]? Z_set[0]: Z_p);
		Z_p = (Z_p>Z_set[Number_of_Z-1]? Z_set[Number_of_Z-1]: Z_p);


		// Get the index of that metallicity value (the Z sampling is not homogeneous):
		i_Z = -1; //Initial value
		do{
			i_Z++;
		}while( Z_set[i_Z]<=Z_p );
		i_Z--;


		//Assume that the logAge grid is regular (from logAge_min to logAge_max with step logAge_step)
		i_logAge = (int)((Number_of_logAge-1)*(logAge_p-logAge_min)/(logAge_max-logAge_min));
		i_isocrone = i_logAge + (Number_of_logAge)*i_Z;


		// Read the following information about the selected isocrone: [Z, logAge, m_ini.min, m_ini.max, Nasses, V.min, V.max, (V-Ic).min, (V-Ic).max]
//		fseek( fichero_stat_bin, 40*i_isocrone, SEEK_SET);// 40 bytes = 4 bytes*10 columns
//		fread(&Z_p,4,1,fichero_stat_bin); // Metallicity of the isocrone
//		fread(&logAge_p,4,1,fichero_stat_bin); // logAge of the isocrone
		
		fseek( fichero_stat_bin, 40*i_isocrone+16, SEEK_SET);// 40 bytes = 4 bytes*10 columns
//		skip logmi_*
//		fread(&Logmi_MIN,4,1,fichero_stat_bin); // min of the initial mass (of the isocrone)
//		fread(&Logmi_MAX,4,1,fichero_stat_bin); // max of the initial mass (of the isocrone)

		fread(&starting_point_of_the_isocrone,4,1,fichero_stat_bin); //Starting position (inclusive)
		fread(&Number_of_logmasses_in_the_isocrone,4,1,fichero_stat_bin);// Rows (stars) that belong to this isocrone

//		fread(&V_min,4,1,fichero_stat_bin); // min of the V band (of the isocrone)
//		fread(&V_max,4,1,fichero_stat_bin); // max of the V band (of the isocrone)
//		fread(&VmIc_min,4,1,fichero_stat_bin); // min of the V band (of the isocrone)
//		fread(&VmIc_max,4,1,fichero_stat_bin); // max of the V band (of the isocrone)

		//Call to the Kroupa IMF function
		mL = pow(10., Logmi_MIN + dlogm*(Number_of_logmasses_in_the_isocrone-1));

		KroupaIMF( mi, Max_Number_of_logmasses_per_isocrone, mL, N_mi); //N_mi means number of masses between mi and mi+dm (egde effects included)

		// Initialize the variables:
		aux = 0.; // Quantity used to estimate error_pi
		error_pi[particle] = 0.; // Sigma_pi
		Cum_N_mi = 0.; //New norm of N_mi_p
		nobstars[particle] = 0.;

		// Go to the desired isocrone
		fseek( fichero_isocr_bin, 8*starting_point_of_the_isocrone, SEEK_SET); //8=2*4 bytes = np.float32 bits*2 columns


		// Load V and V-Ic from the isocrone given by (Z, logAge), and finish P(m_i)
		for(star=0;star<Number_of_logmasses_in_the_isocrone;star++){
			// Get the absolute magnitudes
			fread(&V_star,4,1, fichero_isocr_bin); // Absolute V
			fread(&VmIc_star,4,1, fichero_isocr_bin); // Absolute V-Ic

			// From absolute to apparent
			V_star = V_star + distance_correction + ex31_p;
			VmIc_star = VmIc_star + 355./900.0*ex31_p; // 355/900 = 0.394444... = 1.962/3.24 (ratio between V and Ic extinction corrections)

			// Estimate the G band
			G_star = V_star-0.0257-0.0924*VmIc_star-0.1623*VmIc_star*VmIc_star+0.0090*VmIc_star*VmIc_star*VmIc_star; // G band (Tony Brown's formula)
		
			// Would this star be observed with Gaia?
			is_selected = (G_histo_min<=G_star)*(G_star<G_histo_max);// 1= Yes, 0=No

			// Calculate the error for this star and add it to the particle contribution:
			aux = max(0.063095734448019303, pow(10.,0.4*G_star*is_selected-6.)); // 0.06309...=10**(0.4*(12.0-15))
			// aux = z in www.cosmos.esa.int/web/gaia/science-performance#photometric performance
			// Including the is_selected in the power we avoid out-of-float range values when the extinction is extremely high
			aux =  sqrt(-1.631 + 680.766*aux + 32.732*aux*aux)*(0.986 + 0.014*VmIc_star); // Here aux = sigma_pi for the considered star
			aux = aux*is_selected; // Exclude the contribution of the brightest/faintest stars

			N_mi_p[star] = N_mi[star]*is_selected; // Make N_mi=0 if the star is not detected
			Cum_N_mi += N_mi_p[star]; //Renormalisation of N_mi due to the out-of-G range stars. Cum_N_mi is the total number of stars observed (if Mass_p = 1)
			error_pi[particle] += aux*N_mi_p[star]; // Add the contribution to the average

			ini_mass_histogram[star] += Mass_p*N_mi_p[star]; //Add the contribution to the histogram

			//Histogram of the G-band
			Gbin = (int)(Nbins_G*(G_star-G_histo_min)/(G_histo_max-G_histo_min));
			if((G_star>=G_histo_min)*(G_star<G_histo_max)){
				Gband_histogram[Gbin] += Mass_p*N_mi[star];//Add the contribution to the histogram
			};

		}; // End of the star-loop

		// Save the number of stars observed for this particle
		nobstars[particle] = Mass_p*Cum_N_mi;

		// Correction due to the G-band exclusion
		Cum_N_mi = Cum_N_mi -1.0*(Cum_N_mi<=0);//If no stars are observed, return a negative value of sigma_pi
		error_pi[particle] = total_correction[particle]*error_pi[particle]/Cum_N_mi; //Include the Calibration, the scanning law and the epoch corrections
		error_pi[particle] = error_pi[particle]*(Cum_N_mi>0.) - 99000.00*(Cum_N_mi<=0.);//If no stars are observed, return a negative value of sigma_pi
		error_pi[particle] *= 0.001; //From uas to mas

	
	}; // End of the particle

	fclose( fichero_stat_bin );
	fclose( fichero_isocr_bin );

}//End of SigmaPiGaia_IMGhist_Kroupa
//-----------------------------------------------------------------------------------------------------------------//




//-----------------------------------------------------------------------------------------------------------------//
/* Mag_distr_Salpeter */
void Mag_distr_Salpeter(float *Z, float *logAge, float *ParticleMass, float *dist, float *extinction, float *magnitude_histogram, float *nobstars, float *Mfcis, int Nparticles, int bins_in_mag, float *mag_axis, int band_id ){
/* Since extinction is provided, lon and lat are not needed*/
// magnitude_histogram is a 1D array with bins_in_mag bins between [mag_axis[0], mag_axis[1])
// Mfcis means Mass fraction converted to stars. It has Nparticles elements
// extinction: in mag, must be given for the CONSIDERED band
// UNITS: Z is a fraction, Ages in yrs, distances in kpc, extinction in mag
//	total_correction is dimensionless
// Output: magnitude_histogram (bins_in_mag elements) and Mfcis (Nparticles- values in an array)
	int n, particle, star, mag_bin; // Counters
	float Z_set[34]; // Set of metallicities (the grid is not regular in Z)
	int is_selected; // 1 if the magnitude is in [mag_min, mag_max)
	float mag_min, mag_max; //Histogram limits

	/* Values for each particle */
	int i_Z, i_logAge, i_isocrone; //Index of the metallicity, logAge, isocrone
	float Z_p, logAge_p, Mass_p, extinction_p; // Values for one selected particle
	float distance_correction; // 10+5*log10(d_kpc)

	/* Values for each star */
	float magnitude; //Apparent magnitude

	/* Values for each isocrone */
//	float V_min, V_max, VmIc_min, VmIc_max;//Parameters to read in Explanation3.bin
	int starting_point_of_the_isocrone, Number_of_logmasses_in_the_isocrone;

	/* IMF */
	const float dlogm = (Logmi_MAX-Logmi_MIN)/(1.0*Max_Number_of_logmasses_per_isocrone); // bin size
	float mi[Max_Number_of_logmasses_per_isocrone], N_mi[Max_Number_of_logmasses_per_isocrone]; //Masses and global IMF

	// Files:
	FILE *fichero_ASCII; // ASCII file with the information about the isocrones
	FILE *fichero_stat_bin; //Binary file with the statistics of all the isocrones
	FILE *fichero_isocr_bin; //Binary file with all the isocroness

	mag_min = mag_axis[0]; //Lower limit of the histogram
	mag_max = mag_axis[1]; //Upper limit of the histogram

	/* Read the metallicity values of the grid */
	fichero_ASCII = fopen( ISONO,"r"); //Open All_the_isocrones3.bin
	for (n=0; n<Number_of_Z;n++){
		fscanf( fichero_ASCII, "%f\n", &Z_set[n]); // Read the decimal part of the metallicity 
		Z_set[n] = Z_set[n]*1E-6; // Convert to true Z [met]

	} // Metallicity values read!
	fclose( fichero_ASCII ); //Close the ASCII file

	// Open the correspondent file:
	switch(band_id){
		case 0:
			fichero_isocr_bin = fopen( BIN_U,"rb"); //Open the list of M_U magnitudes
			fichero_stat_bin = fopen( EXPL_U,"rb"); // Open the statistics of the M_U magnitudes
			break;
		case 1:
			fichero_isocr_bin = fopen( BIN_B,"rb"); //Open the list of M_B magnitudes
			fichero_stat_bin = fopen( EXPL_B,"rb"); // Open the statistics of the M_B magnitudes
			break;
		case 2:
			fichero_isocr_bin = fopen( BIN_V,"rb"); //Open the list of M_V magnitudes
			fichero_stat_bin = fopen( EXPL_V,"rb"); // Open the statistics of the M_V magnitudes
			break;
		case 3:
			fichero_isocr_bin = fopen( BIN_R,"rb"); //Open the list of M_R magnitudes
			fichero_stat_bin = fopen( EXPL_R,"rb"); // Open the statistics of the M_R magnitudes
			break;
		case 4:
			fichero_isocr_bin = fopen( BIN_I,"rb"); //Open the list of M_I magnitudes
			fichero_stat_bin = fopen( EXPL_I,"rb"); // Open the statistics of the M_I magnitudes
			break;
		case 5:
			fichero_isocr_bin = fopen( BIN_J,"rb"); //Open the list of M_J magnitudes
			fichero_stat_bin = fopen( EXPL_J,"rb"); // Open the statistics of the M_J magnitudes
			break;
		case 6:
			fichero_isocr_bin = fopen( BIN_H,"rb"); //Open the list of M_H magnitudes
			fichero_stat_bin = fopen( EXPL_H,"rb"); // Open the statistics of the M_H magnitudes
			break;
		case 7:
			fichero_isocr_bin = fopen( BIN_K,"rb"); //Open the list of M_K magnitudes
			fichero_stat_bin = fopen( EXPL_K,"rb"); // Open the statistics of the M_K magnitudes
			break;
		default:
			fprintf(stderr, "Unknown filter. Johnson V band will be used instead\n");
			fichero_isocr_bin = fopen( BIN_V,"rb"); //Open the list of M_V magnitudes
			fichero_stat_bin = fopen( EXPL_V,"rb"); // Open the statistics of the M_V magnitudes
	};


	//Since logmi_mi and Logmi_MAX are equal for all of the isocrones, the IMF would be the same:
	/* Probability given the Kroupa IMF */
	for(star=0; star<Max_Number_of_logmasses_per_isocrone;star++){
		mi[star] = pow(10., Logmi_MIN + dlogm*star);
	};

	//Call to the Salpeter IMF function
	SalpeterIMF( mi, Max_Number_of_logmasses_per_isocrone, INFINITY, N_mi); //N_mi means number of masses between mi and mi+dm (egde effects included)

	//Initiliaze the histogram of the G band:
	for(mag_bin=0;mag_bin<bins_in_mag;mag_bin++){
		magnitude_histogram[mag_bin]=0.0;
	};

// Loop over the particles
	for(particle=0;particle<Nparticles;particle++){

		// Store the parameters of the considered particle:
		Z_p = Z[particle];
		logAge_p = logAge[particle];
		Mass_p = ParticleMass[particle];
		extinction_p = extinction[particle];
		distance_correction = 10.+5.*log10(dist[particle]);
		nobstars[particle] = 0.; //Initial value
		

		// Avoid out of range values:
		logAge_p = (logAge_p<logAge_min? logAge_min: logAge_p);
		logAge_p = (logAge_p>logAge_max? logAge_max: logAge_p);

		Z_p = (Z_p<Z_set[0]? Z_set[0]: Z_p);
		Z_p = (Z_p>Z_set[Number_of_Z-1]? Z_set[Number_of_Z-1]: Z_p);


		// Get the index of that metallicity value (the Z sampling is not homogeneous):
		i_Z = -1; //Initial value
		do{
			i_Z++;
		}while( Z_set[i_Z]<=Z_p );
		i_Z--;


		//Assume that the logAge grid is regular (from logAge_min to logAge_max with step logAge_step)
		i_logAge = (int)((Number_of_logAge-1)*(logAge_p-logAge_min)/(logAge_max-logAge_min));
		i_isocrone = i_logAge + (Number_of_logAge)*i_Z;


		// Read the following information about the selected isocrone: [Z, logAge, m_ini.min, m_ini.max, Nasses]
//		fseek( fichero_stat_bin, 32*i_isocrone, SEEK_SET);// 32 bytes = 4 bytes*8 columns

//		fread(&Z_p,4,1,fichero_stat_bin); // Metallicity of the isocrone
//		fread(&logAge_p,4,1,fichero_stat_bin); // logAge of the isocrone
		
		fseek( fichero_stat_bin, 32*i_isocrone+16, SEEK_SET);// 32 bytes = 4 bytes*8 columns
//		+16 = skip logmi_*


		fread(&starting_point_of_the_isocrone,4,1,fichero_stat_bin); //Starting position (inclusive)
		fread(&Number_of_logmasses_in_the_isocrone,4,1,fichero_stat_bin);// Rows (stars) that belong to this isocrone


		// Initialize the Mfcis:
		Mfcis[particle] = 0.0; // Mass fraction converted to stars

		// Go to the desired isocrone
		fseek( fichero_isocr_bin, 4*starting_point_of_the_isocrone, SEEK_SET); //1=1*4 bytes = np.float32 bits*1 column


		//Correct the Particle Mass to exclude stellar masses larger than mi[ Number_of_logmasses_in_the_isocrone-1]
		Mass_p = Mass_p/(1.-pow(mi[Number_of_logmasses_in_the_isocrone-1]/simf_m0, 2.-simf_a)); // Finite max mass correction


		// Load V and V-Ic from the isocrone given by (Z, logAge), and finish P(m_i)
		for(star=0;star<Number_of_logmasses_in_the_isocrone;star++){
			// Get the absolute magnitudes
			fread(&magnitude,4,1, fichero_isocr_bin); // Absolute magnitude

			// From absolute to apparent
			magnitude = magnitude + distance_correction + extinction_p;
					
			// Would this star be included in the histogram?
			is_selected = (mag_min<=magnitude)*(magnitude<mag_max);// 1= Yes, 0=No

			// Mass fraction converted to stars
			Mfcis[particle] += mi[star]*N_mi[star]*is_selected; //Add the contribution

			// Number of observed stars:
			nobstars[particle] += Mass_p*N_mi[star]*is_selected; //Add the contribution

			//Histogram of the G-band
			mag_bin = (int)(bins_in_mag*(magnitude-mag_min)/(mag_max-mag_min));
			if(is_selected){
				magnitude_histogram[mag_bin] += Mass_p*N_mi[star];//Add the contribution to the histogram
			};

		}; // End of the star-loop

	}; // End of the particle

	fclose( fichero_stat_bin );
	fclose( fichero_isocr_bin );

}//End of Mag_distr_Salpeter
//-----------------------------------------------------------------------------------------------------------------//






//-----------------------------------------------------------------------------------------------------------------//
/* Mag_distr_Kroupa */
void Mag_distr_Kroupa(float *Z, float *logAge, float *ParticleMass, float *dist, float *extinction, float *magnitude_histogram, float *nobstars, float *Mfcis, int Nparticles, int bins_in_mag, float *mag_axis, int band_id ){
/* Since extinction is provided, lon and lat are not needed*/
// magnitude_histogram is a 1D array with bins_in_mag bins between [mag_axis[0], mag_axis[1])
// Mfcis means Mass fraction converted to stars. It has Nparticles elements
// extinction: in mag, must be given for the CONSIDERED band
// UNITS: Z is a fraction, Ages in yrs, distances in kpc, extinction in mag
//	total_correction is dimensionless
// Output: magnitude_histogram (bins_in_mag elements) and Mfcis (Nparticles- values in an array)
	int n, particle, star, mag_bin; // Counters
	float Z_set[34]; // Set of metallicities (the grid is not regular in Z)
	int is_selected; // 1 if the magnitude is in [mag_min, mag_max)
	float mag_min, mag_max; //Histogram limits

	/* Values for each particle */
	int i_Z, i_logAge, i_isocrone; //Index of the metallicity, logAge, isocrone
	float Z_p, logAge_p, Mass_p, extinction_p; // Values for one selected particle
	float distance_correction; // 10+5*log10(d_kpc)

	/* Values for each star */
	float magnitude; //Apparent magnitude

	/* Values for each isocrone */
//	float V_min, V_max, VmIc_min, VmIc_max;//Parameters to read in Explanation3.bin
	int starting_point_of_the_isocrone, Number_of_logmasses_in_the_isocrone;

	/* IMF */
	const float dlogm = (Logmi_MAX-Logmi_MIN)/(1.0*Max_Number_of_logmasses_per_isocrone); // bin size
	float mi[Max_Number_of_logmasses_per_isocrone], N_mi[Max_Number_of_logmasses_per_isocrone]; //Masses and global IMF

	// Files:
	FILE *fichero_ASCII; // ASCII file with the information about the isocrones
	FILE *fichero_stat_bin; //Binary file with the statistics of all the isocrones
	FILE *fichero_isocr_bin; //Binary file with all the isocroness

	mag_min = mag_axis[0]; //Lower limit of the histogram
	mag_max = mag_axis[1]; //Upper limit of the histogram

	/* Read the metallicity values of the grid */
	fichero_ASCII = fopen( ISONO,"r"); //Open All_the_isocrones3.bin
	for (n=0; n<Number_of_Z;n++){
		fscanf( fichero_ASCII, "%f\n", &Z_set[n]); // Read the decimal part of the metallicity 
		Z_set[n] = Z_set[n]*1E-6; // Convert to true Z [met]

	} // Metallicity values read!
	fclose( fichero_ASCII ); //Close the ASCII file

	// Open the correspondent file:
	switch(band_id){
		case 0:
			fichero_isocr_bin = fopen( BIN_U,"rb"); //Open the list of M_U magnitudes
			fichero_stat_bin = fopen( EXPL_U,"rb"); // Open the statistics of the M_U magnitudes
			break;
		case 1:
			fichero_isocr_bin = fopen( BIN_B,"rb"); //Open the list of M_B magnitudes
			fichero_stat_bin = fopen( EXPL_B,"rb"); // Open the statistics of the M_B magnitudes
			break;
		case 2:
			fichero_isocr_bin = fopen( BIN_V,"rb"); //Open the list of M_V magnitudes
			fichero_stat_bin = fopen( EXPL_V,"rb"); // Open the statistics of the M_V magnitudes
			break;
		case 3:
			fichero_isocr_bin = fopen( BIN_R,"rb"); //Open the list of M_R magnitudes
			fichero_stat_bin = fopen( EXPL_R,"rb"); // Open the statistics of the M_R magnitudes
			break;
		case 4:
			fichero_isocr_bin = fopen( BIN_I,"rb"); //Open the list of M_I magnitudes
			fichero_stat_bin = fopen( EXPL_I,"rb"); // Open the statistics of the M_I magnitudes
			break;
		case 5:
			fichero_isocr_bin = fopen( BIN_J,"rb"); //Open the list of M_J magnitudes
			fichero_stat_bin = fopen( EXPL_J,"rb"); // Open the statistics of the M_J magnitudes
			break;
		case 6:
			fichero_isocr_bin = fopen( BIN_H,"rb"); //Open the list of M_H magnitudes
			fichero_stat_bin = fopen( EXPL_H,"rb"); // Open the statistics of the M_H magnitudes
			break;
		case 7:
			fichero_isocr_bin = fopen( BIN_K,"rb"); //Open the list of M_K magnitudes
			fichero_stat_bin = fopen( EXPL_K,"rb"); // Open the statistics of the M_K magnitudes
			break;
		default:
			fprintf(stderr, "Unknown filter. Johnson V band will be used instead\n");
			fichero_isocr_bin = fopen( BIN_V,"rb"); //Open the list of M_V magnitudes
			fichero_stat_bin = fopen( EXPL_V,"rb"); // Open the statistics of the M_V magnitudes
	};


	//Since logmi_mi and Logmi_MAX are equal for all of the isocrones, the IMF would be the same:
	/* Probability given the Kroupa IMF */
	for(star=0; star<Max_Number_of_logmasses_per_isocrone;star++){
		mi[star] = pow(10., Logmi_MIN + dlogm*star);
	};

	//WARNING! This line is different from the Salpeter's version

	//Initiliaze the histogram of the G band:
	for(mag_bin=0;mag_bin<bins_in_mag;mag_bin++){
		magnitude_histogram[mag_bin]=0.0;
	};

// Loop over the particles
	for(particle=0;particle<Nparticles;particle++){

		// Store the parameters of the considered particle:
		Z_p = Z[particle];
		logAge_p = logAge[particle];
		Mass_p = ParticleMass[particle];
		extinction_p = extinction[particle];
		distance_correction = 10.+5.*log10(dist[particle]);
		nobstars[particle] = 0.;
		

		// Avoid out of range values:
		logAge_p = (logAge_p<logAge_min? logAge_min: logAge_p);
		logAge_p = (logAge_p>logAge_max? logAge_max: logAge_p);

		Z_p = (Z_p<Z_set[0]? Z_set[0]: Z_p);
		Z_p = (Z_p>Z_set[Number_of_Z-1]? Z_set[Number_of_Z-1]: Z_p);


		// Get the index of that metallicity value (the Z sampling is not homogeneous):
		i_Z = -1; //Initial value
		do{
			i_Z++;
		}while( Z_set[i_Z]<=Z_p );
		i_Z--;


		//Assume that the logAge grid is regular (from logAge_min to logAge_max with step logAge_step)
		i_logAge = (int)((Number_of_logAge-1)*(logAge_p-logAge_min)/(logAge_max-logAge_min));
		i_isocrone = i_logAge + (Number_of_logAge)*i_Z;


		// Read the following information about the selected isocrone: [Z, logAge, m_ini.min, m_ini.max, Nasses]
//		fseek( fichero_stat_bin, 32*i_isocrone, SEEK_SET);// 32 bytes = 4 bytes*8 columns

//		fread(&Z_p,4,1,fichero_stat_bin); // Metallicity of the isocrone
//		fread(&logAge_p,4,1,fichero_stat_bin); // logAge of the isocrone
		
		fseek( fichero_stat_bin, 32*i_isocrone+16, SEEK_SET);// 32 bytes = 4 bytes*8 columns
//		+16 = skip logmi_*


		fread(&starting_point_of_the_isocrone,4,1,fichero_stat_bin); //Starting position (inclusive)
		fread(&Number_of_logmasses_in_the_isocrone,4,1,fichero_stat_bin);// Rows (stars) that belong to this isocrone


		// Initialize the Mfcis:
		Mfcis[particle] = 0.0; // Mass fraction converted to stars

		// Go to the desired isocrone
		fseek( fichero_isocr_bin, 4*starting_point_of_the_isocrone, SEEK_SET); //1=1*4 bytes = np.float32 bits*1 column


		//WARNING!: This line is different from the Salpeter's version!

		//Call to the Kroupa IMF function
		KroupaIMF( mi, Max_Number_of_logmasses_per_isocrone, mi[Number_of_logmasses_in_the_isocrone-1], N_mi); //N_mi means number of masses between mi and mi+dm (egde effects included)


		// Load V and V-Ic from the isocrone given by (Z, logAge), and finish P(m_i)
		for(star=0;star<Number_of_logmasses_in_the_isocrone;star++){
			// Get the absolute magnitudes
			fread(&magnitude,4,1, fichero_isocr_bin); // Absolute magnitude

			// From absolute to apparent
			magnitude = magnitude + distance_correction + extinction_p;
					
			// Would this star be included in the histogram?
			is_selected = (mag_min<=magnitude)*(magnitude<mag_max);// 1= Yes, 0=No

			// Mass fraction converted into stars
			Mfcis[particle] += mi[star]*N_mi[star]*is_selected; //Add the contribution

			// Number of OBSERVED stars for this particular particle
			nobstars[particle] += Mass_p*N_mi[star]*is_selected; //Add the contribution

			//Histogram of the G-band
			mag_bin = (int)(bins_in_mag*(magnitude-mag_min)/(mag_max-mag_min));
			if(is_selected){
				magnitude_histogram[mag_bin] += Mass_p*N_mi[star];//Add the contribution to the histogram (counts)
			};

		}; // End of the star-loop

	}; // End of the particle

	fclose( fichero_stat_bin );
	fclose( fichero_isocr_bin );

}//End of Mag_distr_Kroupa
//-----------------------------------------------------------------------------------------------------------------//






//-----------------------------------------------------------------------------------------------------------------//
/* CMD_Salpeter */
void CMD_Salpeter(float *Z, float *logAge, float *ParticleMass, float *dist, float *A_V, float *cmd_map, float *nobstars, float *Mfcis, int Nparticles, int *CMD_dim, float *mag_axis, int *bands_id ){
/* Since extinction is provided, lon and lat are not needed*/
// Color magnitude histogram is a 2D array (but vectorized) of size CMD_dim[1]*CMD_dim[0]
// Mfcis means Mass fraction converted to stars. It has Nparticles elements
// A_V: in mag, must be given for the Johnsons V band
// UNITS: Z is a fraction, Ages in yrs, distances in kpc, extinction in mag
//	total_correction is dimensionless
// Output: CMD (CMD_dim[0]*CMD_dim[1] elements) and Mfcis (Nparticles- values in an array)
	int n, particle, star, cmd_bin, bin_x, bin_y; // Counters
	float Z_set[34]; // Set of metallicities (the grid is not regular in Z)
	int is_selected; // 1 if the magnitude is in [mag_min, mag_max)
	/* factor = A_lambda/A_V*/
	float factor_x, factor_y;
	int sign=1; // The horizontal axis of the CMD goes from the Bluest to the reddest (B-V) and not (V-B)

	/* Values for each particle */
	int i_Z, i_logAge, i_isocrone; //Index of the metallicity, logAge, isocrone
	float Z_p, logAge_p, Mass_p, AV_p; // Values for one selected particle
	float distance_correction; // 10+5*log10(d_kpc)

	/* Values for each star */
	float magnitude_x, magnitude_y; //Apparent magnitude

	/* Values for each isocrone */
	int starting_point_of_the_isocrone, Number_of_logmasses_in_the_isocrone;

	/* IMF */
	const float dlogm = (Logmi_MAX-Logmi_MIN)/(1.0*Max_Number_of_logmasses_per_isocrone); // bin size
	float mi[Max_Number_of_logmasses_per_isocrone], N_mi[Max_Number_of_logmasses_per_isocrone]; //Masses and global IMF

	// Files:
	FILE *fichero_ASCII; // ASCII file with the information about the isocrones
	FILE *fichero_stat_bin_x, *fichero_stat_bin_y; //Binary file with the statistics of all the isocrones
	FILE *fichero_isocr_bin_x, *fichero_isocr_bin_y; //Binary file with all the isocrones


	/* Read the metallicity values of the grid */
	fichero_ASCII = fopen( ISONO,"r"); //Open All_the_isocrones3.bin
	for (n=0; n<Number_of_Z;n++){
		fscanf( fichero_ASCII, "%f\n", &Z_set[n]); // Read the decimal part of the metallicity 
		Z_set[n] = Z_set[n]*1E-6; // Convert to true Z [met]

	} // Metallicity values read!
	fclose( fichero_ASCII ); //Close the ASCII file

	// Open the correspondent file:
	switch(bands_id[0]){
		case 0:
			fichero_isocr_bin_x = fopen( BIN_U,"rb"); //Open the list of M_U magnitudes
			fichero_stat_bin_x = fopen( EXPL_U,"rb"); // Open the statistics of the M_U magnitudes
			factor_x = 4.968/3.24;
			break;
		case 1:
			fichero_isocr_bin_x = fopen( BIN_B,"rb"); //Open the list of M_B magnitudes
			fichero_stat_bin_x = fopen( EXPL_B,"rb"); // Open the statistics of the M_B magnitudes
			factor_x = 4.325/3.24;
			break;
		case 2:
			fichero_isocr_bin_x = fopen( BIN_V,"rb"); //Open the list of M_V magnitudes
			fichero_stat_bin_x = fopen( EXPL_V,"rb"); // Open the statistics of the M_V magnitudes
			factor_x = 1.;
			break;
		case 3:
			fichero_isocr_bin_x = fopen( BIN_R,"rb"); //Open the list of M_R magnitudes
			fichero_stat_bin_x = fopen( EXPL_R,"rb"); // Open the statistics of the M_R magnitudes
			factor_x = 2.634/3.24;
			break;
		case 4:
			fichero_isocr_bin_x = fopen( BIN_I,"rb"); //Open the list of M_I magnitudes
			fichero_stat_bin_x = fopen( EXPL_I,"rb"); // Open the statistics of the M_I magnitudes
			factor_x = 1.962/3.24;
			break;
		case 5:
			fichero_isocr_bin_x = fopen( BIN_J,"rb"); //Open the list of M_J magnitudes
			fichero_stat_bin_x = fopen( EXPL_J,"rb"); // Open the statistics of the M_J magnitudes
			factor_x = 0.9536616/3.24;
			break;
		case 6:
			fichero_isocr_bin_x = fopen( BIN_H,"rb"); //Open the list of M_H magnitudes
			fichero_stat_bin_x = fopen( EXPL_H,"rb"); // Open the statistics of the M_H magnitudes
			factor_x = 0.5873472/3.24;
			break;
		case 7:
			fichero_isocr_bin_x = fopen( BIN_K,"rb"); //Open the list of M_K magnitudes
			fichero_stat_bin_x = fopen( EXPL_K,"rb"); // Open the statistics of the M_K magnitudes
			factor_x = 0.367/3.24;
			break;
		default:
			fprintf(stderr, "Unknown first filter. Johnson V band will be used instead\n");
			fichero_isocr_bin_x = fopen( BIN_V,"rb"); //Open the list of M_V magnitudes
			fichero_stat_bin_x = fopen( EXPL_V,"rb"); // Open the statistics of the M_V magnitudes
			factor_x = 1.;
	};

	// Open the correspondent file:
	switch(bands_id[1]){
		case 0:
			fichero_isocr_bin_y = fopen( BIN_U,"rb"); //Open the list of M_U magnitudes
			fichero_stat_bin_y = fopen( EXPL_U,"rb"); // Open the statistics of the M_U magnitudes
			factor_y = 4.968/3.24;
			break;
		case 1:
			fichero_isocr_bin_y = fopen( BIN_B,"rb"); //Open the list of M_B magnitudes
			fichero_stat_bin_y = fopen( EXPL_B,"rb"); // Open the statistics of the M_B magnitudes
			factor_y = 4.325/3.24;
			break;
		case 2:
			fichero_isocr_bin_y = fopen( BIN_V,"rb"); //Open the list of M_V magnitudes
			fichero_stat_bin_y = fopen( EXPL_V,"rb"); // Open the statistics of the M_V magnitudes
			factor_y = 1.;
			break;
		case 3:
			fichero_isocr_bin_y = fopen( BIN_R,"rb"); //Open the list of M_R magnitudes
			fichero_stat_bin_y = fopen( EXPL_R,"rb"); // Open the statistics of the M_R magnitudes
			factor_y = 2.634/3.24;
			break;
		case 4:
			fichero_isocr_bin_y = fopen( BIN_I,"rb"); //Open the list of M_I magnitudes
			fichero_stat_bin_y = fopen( EXPL_I,"rb"); // Open the statistics of the M_I magnitudes
			factor_y = 1.962/3.24;
			break;
		case 5:
			fichero_isocr_bin_y = fopen( BIN_J,"rb"); //Open the list of M_J magnitudes
			fichero_stat_bin_y = fopen( EXPL_J,"rb"); // Open the statistics of the M_J magnitudes
			factor_y = 0.9536616/3.24;
			break;
		case 6:
			fichero_isocr_bin_y = fopen( BIN_H,"rb"); //Open the list of M_H magnitudes
			fichero_stat_bin_y = fopen( EXPL_H,"rb"); // Open the statistics of the M_H magnitudes
			factor_y = 0.5873472/3.24;
			break;
		case 7:
			fichero_isocr_bin_y = fopen( BIN_K,"rb"); //Open the list of M_K magnitudes
			fichero_stat_bin_y = fopen( EXPL_K,"rb"); // Open the statistics of the M_K magnitudes
			factor_y = 0.367/3.24;
			break;
		default:
			fprintf(stderr, "Unknown second filter. Johnson V band will be used instead\n");
			fichero_isocr_bin_y = fopen( BIN_V,"rb"); //Open the list of M_V magnitudes
			fichero_stat_bin_y = fopen( EXPL_V,"rb"); // Open the statistics of the M_V magnitudes
			factor_y = 1.;
	};

	//X axis orientation;
	sign = (bands_id[1]>bands_id[0]?1:-1);

	//Since logmi_mi and Logmi_MAX are equal for all of the isocrones, the IMF would be the same:
	/* Probability given the Kroupa IMF */
	for(star=0; star<Max_Number_of_logmasses_per_isocrone;star++){
		mi[star] = pow(10., Logmi_MIN + dlogm*star);
	};

	//Call to the Salpeter IMF function
	SalpeterIMF( mi, Max_Number_of_logmasses_per_isocrone, INFINITY, N_mi); //N_mi means number of masses between mi and mi+dm (egde effects included)

	//Initiliaze the CMD:
	for(cmd_bin=0;cmd_bin<CMD_dim[0]*CMD_dim[1];cmd_bin++){
		cmd_map[cmd_bin]=0.0;
	};

// Loop over the particles
	for(particle=0;particle<Nparticles;particle++){

		// Store the parameters of the considered particle:
		Z_p = Z[particle];
		logAge_p = logAge[particle];
		Mass_p = ParticleMass[particle];
		AV_p = A_V[particle];
		distance_correction = 10.+5.*log10(dist[particle]);
		nobstars[particle] = 0.;
		

		// Avoid out of range values:
		logAge_p = (logAge_p<logAge_min? logAge_min: logAge_p);
		logAge_p = (logAge_p>logAge_max? logAge_max: logAge_p);

		Z_p = (Z_p<Z_set[0]? Z_set[0]: Z_p);
		Z_p = (Z_p>Z_set[Number_of_Z-1]? Z_set[Number_of_Z-1]: Z_p);


		// Get the index of that metallicity value (the Z sampling is not homogeneous):
		i_Z = -1; //Initial value
		do{
			i_Z++;
		}while( Z_set[i_Z]<=Z_p );
		i_Z--;


		//Assume that the logAge grid is regular (from logAge_min to logAge_max with step logAge_step)
		i_logAge = (int)((Number_of_logAge-1)*(logAge_p-logAge_min)/(logAge_max-logAge_min));
		i_isocrone = i_logAge + (Number_of_logAge)*i_Z;


		// Read the following information about the selected isocrone: [starting_point of the isocrone, Nasses]
		fseek( fichero_stat_bin_x, 32*i_isocrone+16, SEEK_SET);// 32 bytes = 4 bytes*8 columns
		fseek( fichero_stat_bin_y, 32*i_isocrone+16, SEEK_SET);// 32 bytes = 4 bytes*8 columns

		fread(&starting_point_of_the_isocrone,4,1,fichero_stat_bin_x); //Starting position (inclusive)(common for both magnitudes)
		fread(&Number_of_logmasses_in_the_isocrone,4,1,fichero_stat_bin_x);// Rows (stars) that belong to this isocrone(common for both magnitudes)


		// Initialize the Mfcis:
		Mfcis[particle] = 0.0; // Mass fraction converted to stars

		// Go to the desired isocrone
		fseek( fichero_isocr_bin_x, 4*starting_point_of_the_isocrone, SEEK_SET); //1=1*4 bytes = np.float32 bits*1 column
		fseek( fichero_isocr_bin_y, 4*starting_point_of_the_isocrone, SEEK_SET); //1=1*4 bytes = np.float32 bits*1 column


		//Correct the Particle Mass to exclude stellar masses larger than mi[ Number_of_logmasses_in_the_isocrone-1]
		Mass_p = Mass_p/(1.-pow(mi[Number_of_logmasses_in_the_isocrone-1]/simf_m0, 2.-simf_a)); // Finite max mass correction


		// Load V and V-Ic from the isocrone given by (Z, logAge), and finish P(m_i)
		for(star=0;star<Number_of_logmasses_in_the_isocrone;star++){
			// Get the absolute magnitudes
			fread(&magnitude_x,4,1, fichero_isocr_bin_x); // Absolute magnitude
			fread(&magnitude_y,4,1, fichero_isocr_bin_y); // Absolute magnitude

			// From absolute to apparent
			magnitude_x = magnitude_x + distance_correction + AV_p*factor_x;
			magnitude_y = magnitude_y + distance_correction + AV_p*factor_y;

		
			//Colour difference:
			magnitude_x = magnitude_x - magnitude_y;
			magnitude_x *=sign; //Orientation

			
			// Would this star be included in the histogram?
			is_selected = (mag_axis[0]<=magnitude_x)*(magnitude_x<mag_axis[1]);// 1= Yes, 0=No
			is_selected = is_selected*(mag_axis[2]<=magnitude_y)*(magnitude_y<mag_axis[3]);// 1= Yes, 0=No

			// Mass fraction converted to stars
			Mfcis[particle] += mi[star]*N_mi[star]*is_selected; //Add the contribution

			// Number of OBSERVED stars for this particular particle
			nobstars[particle] += Mass_p*N_mi[star]*is_selected; //Add the contribution

			//CMD
			bin_y = (int)(CMD_dim[1]*(magnitude_y-mag_axis[2])/(mag_axis[3]-mag_axis[2]));// Row
			bin_x = (int)(CMD_dim[0]*(magnitude_x-mag_axis[0])/(mag_axis[1]-mag_axis[0]));// Column
			cmd_bin = bin_x + CMD_dim[0]*bin_y;

			if(is_selected){
				cmd_map[cmd_bin] += Mass_p*N_mi[star];//Add the contribution to the histogram
			};

		}; // End of the star-loop

	}; // End of the particle

	fclose( fichero_stat_bin_x );
	fclose( fichero_isocr_bin_x );
	fclose( fichero_stat_bin_y );
	fclose( fichero_isocr_bin_y );

}//End of CMD_Salpeter
//-----------------------------------------------------------------------------------------------------------------//




//-----------------------------------------------------------------------------------------------------------------//
/* CMD_Salpeter_4Points */
void CMD_Salpeter_4Points(float *Z, float *logAge, float *ParticleMass, float *dist, float *A_V, float *cmd_map, float *nobstars, float *Mfcis, int Nparticles, int *CMD_dim, float *mag_axis, int *bands_id ){
/* Since extinction is provided, lon and lat are not needed*/
// Color magnitude histogram is a 2D array (but vectorized) of size CMD_dim[1]*CMD_dim[0]
// Mfcis means Mass fraction converted to stars. It has Nparticles elements
// A_V: in mag, must be given for the Johnsons V band
// UNITS: Z is a fraction, Ages in yrs, distances in kpc, extinction in mag
//	total_correction is dimensionless
// Output: CMD (CMD_dim[0]*CMD_dim[1] elements) and Mfcis (Nparticles- values in an array)
	int n, particle, star, cmd_bin, bin_x, bin_y; // Counters
	float Z_set[34]; // Set of metallicities (the grid is not regular in Z)
	int is_selected; // 1 if the magnitude is in [mag_min, mag_max)
	/* factor = A_lambda/A_V*/
	float factor_x, factor_y;
	int sign=1; // The horizontal axis of the CMD goes from the Bluest to the reddest (B-V) and not (V-B)

	/* Values for each particle */
	int i_Z, i_logAge, i_isocrone; //Index of the metallicity, logAge, isocrone
	float Z_p, logAge_p, Mass_p, AV_p; // Values for one selected particle
	float distance_correction; // 10+5*log10(d_kpc)

	/* Values for each star */
	float magnitude_x, magnitude_y; //Apparent magnitude

	/* Values for each isocrone */
	int starting_point_of_the_isocrone, Number_of_logmasses_in_the_isocrone;

	/* IMF */
	const float dlogm = (Logmi_MAX-Logmi_MIN)/(1.0*Max_Number_of_logmasses_per_isocrone); // bin size
	float mi[Max_Number_of_logmasses_per_isocrone], N_mi[Max_Number_of_logmasses_per_isocrone]; //Masses and global IMF

	/* Weights and temporal outputs */
	float wLR, wRL, wDU, wUD; // wLR+wRL = wDU+wUD = 1.

	// Files:
	FILE *fichero_ASCII; // ASCII file with the information about the isocrones
	FILE *fichero_stat_bin_x, *fichero_stat_bin_y; //Binary file with the statistics of all the isocrones
	FILE *fichero_isocr_bin_x, *fichero_isocr_bin_y; //Binary file with all the isocrones


	/* Read the metallicity values of the grid */
	fichero_ASCII = fopen( ISONO,"r"); //Open All_the_isocrones3.bin
	for (n=0; n<Number_of_Z;n++){
		fscanf( fichero_ASCII, "%f\n", &Z_set[n]); // Read the decimal part of the metallicity 
		Z_set[n] = Z_set[n]*1E-6; // Convert to true Z [met]

	} // Metallicity values read!
	fclose( fichero_ASCII ); //Close the ASCII file

	// Open the correspondent file:
	switch(bands_id[0]){
		case 0:
			fichero_isocr_bin_x = fopen( BIN_U,"rb"); //Open the list of M_U magnitudes
			fichero_stat_bin_x = fopen( EXPL_U,"rb"); // Open the statistics of the M_U magnitudes
			factor_x = 4.968/3.24;
			break;
		case 1:
			fichero_isocr_bin_x = fopen( BIN_B,"rb"); //Open the list of M_B magnitudes
			fichero_stat_bin_x = fopen( EXPL_B,"rb"); // Open the statistics of the M_B magnitudes
			factor_x = 4.325/3.24;
			break;
		case 2:
			fichero_isocr_bin_x = fopen( BIN_V,"rb"); //Open the list of M_V magnitudes
			fichero_stat_bin_x = fopen( EXPL_V,"rb"); // Open the statistics of the M_V magnitudes
			factor_x = 1.;
			break;
		case 3:
			fichero_isocr_bin_x = fopen( BIN_R,"rb"); //Open the list of M_R magnitudes
			fichero_stat_bin_x = fopen( EXPL_R,"rb"); // Open the statistics of the M_R magnitudes
			factor_x = 2.634/3.24;
			break;
		case 4:
			fichero_isocr_bin_x = fopen( BIN_I,"rb"); //Open the list of M_I magnitudes
			fichero_stat_bin_x = fopen( EXPL_I,"rb"); // Open the statistics of the M_I magnitudes
			factor_x = 1.962/3.24;
			break;
		case 5:
			fichero_isocr_bin_x = fopen( BIN_J,"rb"); //Open the list of M_J magnitudes
			fichero_stat_bin_x = fopen( EXPL_J,"rb"); // Open the statistics of the M_J magnitudes
			factor_x = 0.9536616/3.24;
			break;
		case 6:
			fichero_isocr_bin_x = fopen( BIN_H,"rb"); //Open the list of M_H magnitudes
			fichero_stat_bin_x = fopen( EXPL_H,"rb"); // Open the statistics of the M_H magnitudes
			factor_x = 0.5873472/3.24;
			break;
		case 7:
			fichero_isocr_bin_x = fopen( BIN_K,"rb"); //Open the list of M_K magnitudes
			fichero_stat_bin_x = fopen( EXPL_K,"rb"); // Open the statistics of the M_K magnitudes
			factor_x = 0.367/3.24;
			break;
		default:
			fprintf(stderr, "Unknown first filter. Johnson V band will be used instead\n");
			fichero_isocr_bin_x = fopen( BIN_V,"rb"); //Open the list of M_V magnitudes
			fichero_stat_bin_x = fopen( EXPL_V,"rb"); // Open the statistics of the M_V magnitudes
			factor_x = 1.;
	};

	// Open the correspondent file:
	switch(bands_id[1]){
		case 0:
			fichero_isocr_bin_y = fopen( BIN_U,"rb"); //Open the list of M_U magnitudes
			fichero_stat_bin_y = fopen( EXPL_U,"rb"); // Open the statistics of the M_U magnitudes
			factor_y = 4.968/3.24;
			break;
		case 1:
			fichero_isocr_bin_y = fopen( BIN_B,"rb"); //Open the list of M_B magnitudes
			fichero_stat_bin_y = fopen( EXPL_B,"rb"); // Open the statistics of the M_B magnitudes
			factor_y = 4.325/3.24;
			break;
		case 2:
			fichero_isocr_bin_y = fopen( BIN_V,"rb"); //Open the list of M_V magnitudes
			fichero_stat_bin_y = fopen( EXPL_V,"rb"); // Open the statistics of the M_V magnitudes
			factor_y = 1.;
			break;
		case 3:
			fichero_isocr_bin_y = fopen( BIN_R,"rb"); //Open the list of M_R magnitudes
			fichero_stat_bin_y = fopen( EXPL_R,"rb"); // Open the statistics of the M_R magnitudes
			factor_y = 2.634/3.24;
			break;
		case 4:
			fichero_isocr_bin_y = fopen( BIN_I,"rb"); //Open the list of M_I magnitudes
			fichero_stat_bin_y = fopen( EXPL_I,"rb"); // Open the statistics of the M_I magnitudes
			factor_y = 1.962/3.24;
			break;
		case 5:
			fichero_isocr_bin_y = fopen( BIN_J,"rb"); //Open the list of M_J magnitudes
			fichero_stat_bin_y = fopen( EXPL_J,"rb"); // Open the statistics of the M_J magnitudes
			factor_y = 0.9536616/3.24;
			break;
		case 6:
			fichero_isocr_bin_y = fopen( BIN_H,"rb"); //Open the list of M_H magnitudes
			fichero_stat_bin_y = fopen( EXPL_H,"rb"); // Open the statistics of the M_H magnitudes
			factor_y = 0.5873472/3.24;
			break;
		case 7:
			fichero_isocr_bin_y = fopen( BIN_K,"rb"); //Open the list of M_K magnitudes
			fichero_stat_bin_y = fopen( EXPL_K,"rb"); // Open the statistics of the M_K magnitudes
			factor_y = 0.367/3.24;
			break;
		default:
			fprintf(stderr, "Unknown second filter. Johnson V band will be used instead\n");
			fichero_isocr_bin_y = fopen( BIN_V,"rb"); //Open the list of M_V magnitudes
			fichero_stat_bin_y = fopen( EXPL_V,"rb"); // Open the statistics of the M_V magnitudes
			factor_y = 1.;
	};

	//X axis orientation;
	sign = (bands_id[1]>bands_id[0]?1:-1);

	//Since logmi_mi and Logmi_MAX are equal for all of the isocrones, the initial mass array would be the same:
	/* Probability given the Salpeter IMF */
	for(star=0; star<Max_Number_of_logmasses_per_isocrone;star++){
		mi[star] = pow(10., Logmi_MIN + dlogm*star);
	};

	// WARNING! This line is different from the Salpeter's version!

	//Initiliaze the CMD:
	for(cmd_bin=0;cmd_bin<CMD_dim[0]*CMD_dim[1];cmd_bin++){
		cmd_map[cmd_bin]=0.0;
	};

// Loop over the particles
	for(particle=0;particle<Nparticles;particle++){

		// Store the parameters of the considered particle:
		Z_p = Z[particle];
		logAge_p = logAge[particle];
		Mass_p = ParticleMass[particle];
		AV_p = A_V[particle];
		distance_correction = 10.+5.*log10(dist[particle]);
		nobstars[particle] = 0.;
		// Initialize the Mfcis:
		Mfcis[particle] = 0.0; // Mass fraction converted to stars
		

		// Avoid out of range values:
		logAge_p = (logAge_p<logAge_min? logAge_min: logAge_p);
		logAge_p = (logAge_p>logAge_max? logAge_max: logAge_p);

		Z_p = (Z_p<Z_set[0]? Z_set[0]: Z_p);
		Z_p = (Z_p>Z_set[Number_of_Z-1]? Z_set[Number_of_Z-1]: Z_p);



		/* ++++++++++++++++++++++++++++++++ ESTIMATION OF THE WEIGHTS ARE DONE FIRSTLY ++++++++++++++++++++++++++++++++++++*/
		// By calculating the weights first, we avoid the use of huge cmd_maps temporaly variables

			//......................................... LEFT DOWN POINT ..........................................
			// Get the index of that metallicity value (the Z sampling is not homogeneous):
			i_Z = -1; //Initial value
			do{
				i_Z++;
			}while( Z_set[i_Z]<=Z_p );
			i_Z--;
			//Assume that the logAge grid is regular (from logAge_min to logAge_max with step logAge_step)
			i_logAge = (int)((Number_of_logAge-1)*(logAge_p-logAge_min)/(logAge_max-logAge_min));

			//Weights:
			wLR = Z_p-Z_set[i_Z]; //For the Left-Down point of the grid


			//......................................... RIGHT DOWN POINT .........................................

			//i_logAge does not change but i_Z...
			i_Z = i_Z + 1*(i_Z!=(Number_of_Z-1));

			//Weights:
			wRL = Z_set[i_Z]-Z_p; //For the Right-Down point of the grid

			// ......................................... RIGHT UP POINT.........................................
			//i_Z as in the previous case, but i_logAge has increase (or not)...
			i_logAge = i_logAge + 1*(i_logAge!=(Number_of_logAge-1));
			//Weights:
			wUD = logAge_min - i_logAge*logAge_step - logAge_p; //For the Right-Up point of the grid

			// .........................................  LEFT UP POINT.........................................

			//i_logAge as in the previous case, but i_Z decreases (or not)...
			i_Z = i_Z - 1*(i_Z>0);
			//Weights:
			wDU = logAge_p - logAge_min - i_logAge*logAge_step; //For the Left-Up point of the grid

			// Be sure they are normalized...

			if(wLR==-wRL){
				wLR = 1.;
				wRL = 0.;
			}else{
				wLR = wRL/(wLR+wRL);
				wRL = 1.-wLR;
			};

			if(wUD==-wDU){
				wUD = 1.;
				wDU = 0.;
			}else{
				wUD = wDU/(wUD+wDU);
				wDU = 1.-wUD;
			};



		//-----------------------------------------------------------------------------------------------------
		// From now on, proceed as usual

		/* ++++++++++++++++++++++++++++++++ LEFT DOWN POINT ++++++++++++++++++++++++++++++++++++++++*/

		// Get the index of that metallicity value (the Z sampling is not homogeneous):
		i_Z = -1; //Initial value
		do{
			i_Z++;
		}while( Z_set[i_Z]<=Z_p );
		i_Z--;


		//Assume that the logAge grid is regular (from logAge_min to logAge_max with step logAge_step)
		i_logAge = (int)((Number_of_logAge-1)*(logAge_p-logAge_min)/(logAge_max-logAge_min));
		i_isocrone = i_logAge + (Number_of_logAge)*i_Z;


		// Read the following information about the selected isocrone: [starting_point of the isocrone, Nasses]
		fseek( fichero_stat_bin_x, 32*i_isocrone+16, SEEK_SET);// 32 bytes = 4 bytes*8 columns
		fseek( fichero_stat_bin_y, 32*i_isocrone+16, SEEK_SET);// 32 bytes = 4 bytes*8 columns

		fread(&starting_point_of_the_isocrone,4,1,fichero_stat_bin_x); //Starting position (inclusive)(common for both magnitudes)
		fread(&Number_of_logmasses_in_the_isocrone,4,1,fichero_stat_bin_x);// Rows (stars) that belong to this isocrone(common for both magnitudes)


		// Go to the desired isocrone
		fseek( fichero_isocr_bin_x, 4*starting_point_of_the_isocrone, SEEK_SET); //1=1*4 bytes = np.float32 bits*1 column
		fseek( fichero_isocr_bin_y, 4*starting_point_of_the_isocrone, SEEK_SET); //1=1*4 bytes = np.float32 bits*1 column

		// WARNING! This line is different from the Salpeter's version:
		// Call to the Salpeter IMF function
		SalpeterIMF( mi, Max_Number_of_logmasses_per_isocrone, mi[Number_of_logmasses_in_the_isocrone-1], N_mi); //N_mi means number of masses between mi and mi+dm (egde effects included)


		// Load V and V-Ic from the isocrone given by (Z, logAge), and finish P(m_i)
		for(star=0;star<Number_of_logmasses_in_the_isocrone;star++){
			// Get the absolute magnitudes
			fread(&magnitude_x,4,1, fichero_isocr_bin_x); // Absolute magnitude
			fread(&magnitude_y,4,1, fichero_isocr_bin_y); // Absolute magnitude

			// From absolute to apparent
			magnitude_x = magnitude_x + distance_correction + AV_p*factor_x;
			magnitude_y = magnitude_y + distance_correction + AV_p*factor_y;

		
			//Colour difference:
			magnitude_x = magnitude_x - magnitude_y;
			magnitude_x *=sign; //Orientation

			
			// Would this star be included in the histogram?
			is_selected = (mag_axis[0]<=magnitude_x)*(magnitude_x<mag_axis[1]);// 1= Yes, 0=No
			is_selected = is_selected*(mag_axis[2]<=magnitude_y)*(magnitude_y<mag_axis[3]);// 1= Yes, 0=No

			// Make N_mi[star] == 0 if the star is out of range...
			N_mi[star] = wDU*wLR*N_mi[star]*is_selected;//...and apply the weighting

			// Mass fraction converted into observed stars
			Mfcis[particle] += mi[star]*N_mi[star]; //Add the contribution

			// Number of OBSERVED stars for this particular particle
			nobstars[particle] += Mass_p*N_mi[star]; //Add the contribution

			//CMD
			bin_y = (int)(CMD_dim[1]*(magnitude_y-mag_axis[2])/(mag_axis[3]-mag_axis[2]));// Row
			bin_x = (int)(CMD_dim[0]*(magnitude_x-mag_axis[0])/(mag_axis[1]-mag_axis[0]));// Column
			cmd_bin = bin_x + CMD_dim[0]*bin_y;

			if(is_selected){
				cmd_map[cmd_bin] += Mass_p*N_mi[star];//Add the contribution to the histogram
			};

		}; // End of the star-loop


		/* ++++++++++++++++++++++++++++++++ RIGHT DOWN POINT ++++++++++++++++++++++++++++++++++++++++*/

		//i_logAge does not change but i_Z...
		i_Z = i_Z + 1*(i_Z!=(Number_of_Z-1));
		
		i_isocrone = i_logAge + Number_of_logAge*i_Z;

		// Read the following information about the selected isocrone: [starting_point of the isocrone, Nasses]
		fseek( fichero_stat_bin_x, 32*i_isocrone+16, SEEK_SET);// 32 bytes = 4 bytes*8 columns
		fseek( fichero_stat_bin_y, 32*i_isocrone+16, SEEK_SET);// 32 bytes = 4 bytes*8 columns

		fread(&starting_point_of_the_isocrone,4,1,fichero_stat_bin_x); //Starting position (inclusive)(common for both magnitudes)
		fread(&Number_of_logmasses_in_the_isocrone,4,1,fichero_stat_bin_x);// Rows (stars) that belong to this isocrone(common for both magnitudes)


		// Go to the desired isocrone
		fseek( fichero_isocr_bin_x, 4*starting_point_of_the_isocrone, SEEK_SET); //1=1*4 bytes = np.float32 bits*1 column
		fseek( fichero_isocr_bin_y, 4*starting_point_of_the_isocrone, SEEK_SET); //1=1*4 bytes = np.float32 bits*1 column

		// WARNING! This line is different from the Salpeter's version:
		// Call to the Salpeter IMF function
		SalpeterIMF( mi, Max_Number_of_logmasses_per_isocrone, mi[Number_of_logmasses_in_the_isocrone-1], N_mi); //N_mi means number of masses between mi and mi+dm (egde effects included)

		// Load V and V-Ic from the isocrone given by (Z, logAge), and finish P(m_i)
		for(star=0;star<Number_of_logmasses_in_the_isocrone;star++){
			// Get the absolute magnitudes
			fread(&magnitude_x,4,1, fichero_isocr_bin_x); // Absolute magnitude
			fread(&magnitude_y,4,1, fichero_isocr_bin_y); // Absolute magnitude

			// From absolute to apparent
			magnitude_x = magnitude_x + distance_correction + AV_p*factor_x;
			magnitude_y = magnitude_y + distance_correction + AV_p*factor_y;

		
			//Colour difference:
			magnitude_x = magnitude_x - magnitude_y;
			magnitude_x *=sign; //Orientation

			
			// Would this star be included in the histogram?
			is_selected = (mag_axis[0]<=magnitude_x)*(magnitude_x<mag_axis[1]);// 1= Yes, 0=No
			is_selected = is_selected*(mag_axis[2]<=magnitude_y)*(magnitude_y<mag_axis[3]);// 1= Yes, 0=No

			// Make N_mi[star] == 0 if the star is out of range...
			N_mi[star] = wDU*wRL*N_mi[star]*is_selected;//...and apply the weighting

			// Mass fraction converted into observed stars
			Mfcis[particle] += mi[star]*N_mi[star]; //Add the contribution

			// Number of OBSERVED stars for this particular particle
			nobstars[particle] += Mass_p*N_mi[star]; //Add the contribution

			//CMD
			bin_y = (int)(CMD_dim[1]*(magnitude_y-mag_axis[2])/(mag_axis[3]-mag_axis[2]));// Row
			bin_x = (int)(CMD_dim[0]*(magnitude_x-mag_axis[0])/(mag_axis[1]-mag_axis[0]));// Column
			cmd_bin = bin_x + CMD_dim[0]*bin_y;

			if(is_selected){
				cmd_map[cmd_bin] += Mass_p*N_mi[star];//Add the contribution to the histogram
			};

		}; // End of the star-loop


		/* ++++++++++++++++++++++++++++++++ RIGHT UP POINT ++++++++++++++++++++++++++++++++++++++++*/

		//i_Z as in the previous case, but i_logAge has increase (or not)...
		i_logAge = i_logAge + 1*(i_logAge!=(Number_of_logAge-1));
		i_isocrone = i_logAge + Number_of_logAge*i_Z;

		// Read the following information about the selected isocrone: [starting_point of the isocrone, Nasses]
		fseek( fichero_stat_bin_x, 32*i_isocrone+16, SEEK_SET);// 32 bytes = 4 bytes*8 columns
		fseek( fichero_stat_bin_y, 32*i_isocrone+16, SEEK_SET);// 32 bytes = 4 bytes*8 columns

		fread(&starting_point_of_the_isocrone,4,1,fichero_stat_bin_x); //Starting position (inclusive)(common for both magnitudes)
		fread(&Number_of_logmasses_in_the_isocrone,4,1,fichero_stat_bin_x);// Rows (stars) that belong to this isocrone(common for both magnitudes)


		// Go to the desired isocrone
		fseek( fichero_isocr_bin_x, 4*starting_point_of_the_isocrone, SEEK_SET); //1=1*4 bytes = np.float32 bits*1 column
		fseek( fichero_isocr_bin_y, 4*starting_point_of_the_isocrone, SEEK_SET); //1=1*4 bytes = np.float32 bits*1 column

		// WARNING! This line is different from the Salpeter's version:
		// Call to the Salpeter IMF function
		SalpeterIMF( mi, Max_Number_of_logmasses_per_isocrone, mi[Number_of_logmasses_in_the_isocrone-1], N_mi); //N_mi means number of masses between mi and mi+dm (egde effects included)

		// Load V and V-Ic from the isocrone given by (Z, logAge), and finish P(m_i)
		for(star=0;star<Number_of_logmasses_in_the_isocrone;star++){
			// Get the absolute magnitudes
			fread(&magnitude_x,4,1, fichero_isocr_bin_x); // Absolute magnitude
			fread(&magnitude_y,4,1, fichero_isocr_bin_y); // Absolute magnitude

			// From absolute to apparent
			magnitude_x = magnitude_x + distance_correction + AV_p*factor_x;
			magnitude_y = magnitude_y + distance_correction + AV_p*factor_y;

		
			//Colour difference:
			magnitude_x = magnitude_x - magnitude_y;
			magnitude_x *=sign; //Orientation

			
			// Would this star be included in the histogram?
			is_selected = (mag_axis[0]<=magnitude_x)*(magnitude_x<mag_axis[1]);// 1= Yes, 0=No
			is_selected = is_selected*(mag_axis[2]<=magnitude_y)*(magnitude_y<mag_axis[3]);// 1= Yes, 0=No

			// Make N_mi[star] == 0 if the star is out of range...
			N_mi[star] = wUD*wRL*N_mi[star]*is_selected;//...and apply the weighting

			// Mass fraction converted into observed stars
			Mfcis[particle] += mi[star]*N_mi[star]; //Add the contribution

			// Number of OBSERVED stars for this particular particle
			nobstars[particle] += Mass_p*N_mi[star]; //Add the contribution

			//CMD
			bin_y = (int)(CMD_dim[1]*(magnitude_y-mag_axis[2])/(mag_axis[3]-mag_axis[2]));// Row
			bin_x = (int)(CMD_dim[0]*(magnitude_x-mag_axis[0])/(mag_axis[1]-mag_axis[0]));// Column
			cmd_bin = bin_x + CMD_dim[0]*bin_y;

			if(is_selected){
				cmd_map[cmd_bin] += Mass_p*N_mi[star];//Add the contribution to the histogram
			};

		}; // End of the star-loop

		/* ++++++++++++++++++++++++++++++++ LEFT UP POINT ++++++++++++++++++++++++++++++++++++++++*/

		//i_logAge as in the previous case, but i_Z decreases (or not)...
		i_Z = i_Z - 1*(i_Z>0);
		i_isocrone = i_logAge + Number_of_logAge*i_Z;

		// Read the following information about the selected isocrone: [starting_point of the isocrone, Nasses]
		fseek( fichero_stat_bin_x, 32*i_isocrone+16, SEEK_SET);// 32 bytes = 4 bytes*8 columns
		fseek( fichero_stat_bin_y, 32*i_isocrone+16, SEEK_SET);// 32 bytes = 4 bytes*8 columns

		fread(&starting_point_of_the_isocrone,4,1,fichero_stat_bin_x); //Starting position (inclusive)(common for both magnitudes)
		fread(&Number_of_logmasses_in_the_isocrone,4,1,fichero_stat_bin_x);// Rows (stars) that belong to this isocrone(common for both magnitudes)


		// Go to the desired isocrone
		fseek( fichero_isocr_bin_x, 4*starting_point_of_the_isocrone, SEEK_SET); //1=1*4 bytes = np.float32 bits*1 column
		fseek( fichero_isocr_bin_y, 4*starting_point_of_the_isocrone, SEEK_SET); //1=1*4 bytes = np.float32 bits*1 column

		// WARNING! This line is different from the Salpeter's version:
		// Call to the Salpeter IMF function
		SalpeterIMF( mi, Max_Number_of_logmasses_per_isocrone, mi[Number_of_logmasses_in_the_isocrone-1], N_mi); //N_mi means number of masses between mi and mi+dm (egde effects included)

		// Load V and V-Ic from the isocrone given by (Z, logAge), and finish P(m_i)
		for(star=0;star<Number_of_logmasses_in_the_isocrone;star++){
			// Get the absolute magnitudes
			fread(&magnitude_x,4,1, fichero_isocr_bin_x); // Absolute magnitude
			fread(&magnitude_y,4,1, fichero_isocr_bin_y); // Absolute magnitude

			// From absolute to apparent
			magnitude_x = magnitude_x + distance_correction + AV_p*factor_x;
			magnitude_y = magnitude_y + distance_correction + AV_p*factor_y;

		
			//Colour difference:
			magnitude_x = magnitude_x - magnitude_y;
			magnitude_x *=sign; //Orientation

			
			// Would this star be included in the histogram?
			is_selected = (mag_axis[0]<=magnitude_x)*(magnitude_x<mag_axis[1]);// 1= Yes, 0=No
			is_selected = is_selected*(mag_axis[2]<=magnitude_y)*(magnitude_y<mag_axis[3]);// 1= Yes, 0=No

			// Make N_mi[star] == 0 if the star is out of range...
			N_mi[star] = wUD*wLR*N_mi[star]*is_selected;//...and apply the weighting

			// Mass fraction converted into observed stars
			Mfcis[particle] += mi[star]*N_mi[star]; //Add the contribution

			// Number of OBSERVED stars for this particular particle
			nobstars[particle] += Mass_p*N_mi[star]; //Add the contribution

			//CMD
			bin_y = (int)(CMD_dim[1]*(magnitude_y-mag_axis[2])/(mag_axis[3]-mag_axis[2]));// Row
			bin_x = (int)(CMD_dim[0]*(magnitude_x-mag_axis[0])/(mag_axis[1]-mag_axis[0]));// Column
			cmd_bin = bin_x + CMD_dim[0]*bin_y;

			if(is_selected){
				cmd_map[cmd_bin] += Mass_p*N_mi[star];//Add the contribution to the histogram
			};

		}; // End of the star-loop

	}; // End of the particle

	fclose( fichero_stat_bin_x );
	fclose( fichero_isocr_bin_x );
	fclose( fichero_stat_bin_y );
	fclose( fichero_isocr_bin_y );

}//End of CMD_Salpeter_4Points
//-----------------------------------------------------------------------------------------------------------------//





//-----------------------------------------------------------------------------------------------------------------//
/* CMD_Kroupa */
void CMD_Kroupa(float *Z, float *logAge, float *ParticleMass, float *dist, float *A_V, float *cmd_map, float *nobstars, float *Mfcis, int Nparticles, int *CMD_dim, float *mag_axis, int *bands_id ){
/* Since extinction is provided, lon and lat are not needed*/
// Color magnitude histogram is a 2D array (but vectorized) of size CMD_dim[1]*CMD_dim[0]
// Mfcis means Mass fraction converted to stars. It has Nparticles elements
// A_V: in mag, must be given for the Johnsons V band
// UNITS: Z is a fraction, Ages in yrs, distances in kpc, extinction in mag
//	total_correction is dimensionless
// Output: CMD (CMD_dim[0]*CMD_dim[1] elements) and Mfcis (Nparticles- values in an array)
	int n, particle, star, cmd_bin, bin_x, bin_y; // Counters
	float Z_set[34]; // Set of metallicities (the grid is not regular in Z)
	int is_selected; // 1 if the magnitude is in [mag_min, mag_max)
	/* factor = A_lambda/A_V*/
	float factor_x, factor_y;
	int sign=1; // The horizontal axis of the CMD goes from the Bluest to the reddest (B-V) and not (V-B)

	/* Values for each particle */
	int i_Z, i_logAge, i_isocrone; //Index of the metallicity, logAge, isocrone
	float Z_p, logAge_p, Mass_p, AV_p; // Values for one selected particle
	float distance_correction; // 10+5*log10(d_kpc)

	/* Values for each star */
	float magnitude_x, magnitude_y; //Apparent magnitude

	/* Values for each isocrone */
	int starting_point_of_the_isocrone, Number_of_logmasses_in_the_isocrone;

	/* IMF */
	const float dlogm = (Logmi_MAX-Logmi_MIN)/(1.0*Max_Number_of_logmasses_per_isocrone); // bin size
	float mi[Max_Number_of_logmasses_per_isocrone], N_mi[Max_Number_of_logmasses_per_isocrone]; //Masses and global IMF

	// Files:
	FILE *fichero_ASCII; // ASCII file with the information about the isocrones
	FILE *fichero_stat_bin_x, *fichero_stat_bin_y; //Binary file with the statistics of all the isocrones
	FILE *fichero_isocr_bin_x, *fichero_isocr_bin_y; //Binary file with all the isocrones


	/* Read the metallicity values of the grid */
	fichero_ASCII = fopen( ISONO,"r"); //Open All_the_isocrones3.bin
	for (n=0; n<Number_of_Z;n++){
		fscanf( fichero_ASCII, "%f\n", &Z_set[n]); // Read the decimal part of the metallicity 
		Z_set[n] = Z_set[n]*1E-6; // Convert to true Z [met]

	} // Metallicity values read!
	fclose( fichero_ASCII ); //Close the ASCII file

	// Open the correspondent file:
	switch(bands_id[0]){
		case 0:
			fichero_isocr_bin_x = fopen( BIN_U,"rb"); //Open the list of M_U magnitudes
			fichero_stat_bin_x = fopen( EXPL_U,"rb"); // Open the statistics of the M_U magnitudes
			factor_x = 4.968/3.24;
			break;
		case 1:
			fichero_isocr_bin_x = fopen( BIN_B,"rb"); //Open the list of M_B magnitudes
			fichero_stat_bin_x = fopen( EXPL_B,"rb"); // Open the statistics of the M_B magnitudes
			factor_x = 4.325/3.24;
			break;
		case 2:
			fichero_isocr_bin_x = fopen( BIN_V,"rb"); //Open the list of M_V magnitudes
			fichero_stat_bin_x = fopen( EXPL_V,"rb"); // Open the statistics of the M_V magnitudes
			factor_x = 1.;
			break;
		case 3:
			fichero_isocr_bin_x = fopen( BIN_R,"rb"); //Open the list of M_R magnitudes
			fichero_stat_bin_x = fopen( EXPL_R,"rb"); // Open the statistics of the M_R magnitudes
			factor_x = 2.634/3.24;
			break;
		case 4:
			fichero_isocr_bin_x = fopen( BIN_I,"rb"); //Open the list of M_I magnitudes
			fichero_stat_bin_x = fopen( EXPL_I,"rb"); // Open the statistics of the M_I magnitudes
			factor_x = 1.962/3.24;
			break;
		case 5:
			fichero_isocr_bin_x = fopen( BIN_J,"rb"); //Open the list of M_J magnitudes
			fichero_stat_bin_x = fopen( EXPL_J,"rb"); // Open the statistics of the M_J magnitudes
			factor_x = 0.9536616/3.24;
			break;
		case 6:
			fichero_isocr_bin_x = fopen( BIN_H,"rb"); //Open the list of M_H magnitudes
			fichero_stat_bin_x = fopen( EXPL_H,"rb"); // Open the statistics of the M_H magnitudes
			factor_x = 0.5873472/3.24;
			break;
		case 7:
			fichero_isocr_bin_x = fopen( BIN_K,"rb"); //Open the list of M_K magnitudes
			fichero_stat_bin_x = fopen( EXPL_K,"rb"); // Open the statistics of the M_K magnitudes
			factor_x = 0.367/3.24;
			break;
		default:
			fprintf(stderr, "Unknown first filter. Johnson V band will be used instead\n");
			fichero_isocr_bin_x = fopen( BIN_V,"rb"); //Open the list of M_V magnitudes
			fichero_stat_bin_x = fopen( EXPL_V,"rb"); // Open the statistics of the M_V magnitudes
			factor_x = 1.;
	};

	// Open the correspondent file:
	switch(bands_id[1]){
		case 0:
			fichero_isocr_bin_y = fopen( BIN_U,"rb"); //Open the list of M_U magnitudes
			fichero_stat_bin_y = fopen( EXPL_U,"rb"); // Open the statistics of the M_U magnitudes
			factor_y = 4.968/3.24;
			break;
		case 1:
			fichero_isocr_bin_y = fopen( BIN_B,"rb"); //Open the list of M_B magnitudes
			fichero_stat_bin_y = fopen( EXPL_B,"rb"); // Open the statistics of the M_B magnitudes
			factor_y = 4.325/3.24;
			break;
		case 2:
			fichero_isocr_bin_y = fopen( BIN_V,"rb"); //Open the list of M_V magnitudes
			fichero_stat_bin_y = fopen( EXPL_V,"rb"); // Open the statistics of the M_V magnitudes
			factor_y = 1.;
			break;
		case 3:
			fichero_isocr_bin_y = fopen( BIN_R,"rb"); //Open the list of M_R magnitudes
			fichero_stat_bin_y = fopen( EXPL_R,"rb"); // Open the statistics of the M_R magnitudes
			factor_y = 2.634/3.24;
			break;
		case 4:
			fichero_isocr_bin_y = fopen( BIN_I,"rb"); //Open the list of M_I magnitudes
			fichero_stat_bin_y = fopen( EXPL_I,"rb"); // Open the statistics of the M_I magnitudes
			factor_y = 1.962/3.24;
			break;
		case 5:
			fichero_isocr_bin_y = fopen( BIN_J,"rb"); //Open the list of M_J magnitudes
			fichero_stat_bin_y = fopen( EXPL_J,"rb"); // Open the statistics of the M_J magnitudes
			factor_y = 0.9536616/3.24;
			break;
		case 6:
			fichero_isocr_bin_y = fopen( BIN_H,"rb"); //Open the list of M_H magnitudes
			fichero_stat_bin_y = fopen( EXPL_H,"rb"); // Open the statistics of the M_H magnitudes
			factor_y = 0.5873472/3.24;
			break;
		case 7:
			fichero_isocr_bin_y = fopen( BIN_K,"rb"); //Open the list of M_K magnitudes
			fichero_stat_bin_y = fopen( EXPL_K,"rb"); // Open the statistics of the M_K magnitudes
			factor_y = 0.367/3.24;
			break;
		default:
			fprintf(stderr, "Unknown second filter. Johnson V band will be used instead\n");
			fichero_isocr_bin_y = fopen( BIN_V,"rb"); //Open the list of M_V magnitudes
			fichero_stat_bin_y = fopen( EXPL_V,"rb"); // Open the statistics of the M_V magnitudes
			factor_y = 1.;
	};

	//X axis orientation;
	sign = (bands_id[1]>bands_id[0]?1:-1);

	//Since logmi_mi and Logmi_MAX are equal for all of the isocrones, the initial mass array would be the same:
	/* Probability given the Kroupa IMF */
	for(star=0; star<Max_Number_of_logmasses_per_isocrone;star++){
		mi[star] = pow(10., Logmi_MIN + dlogm*star);
	};

	// WARNING! This line is different from the Salpeter's version!

	//Initiliaze the CMD:
	for(cmd_bin=0;cmd_bin<CMD_dim[0]*CMD_dim[1];cmd_bin++){
		cmd_map[cmd_bin]=0.0;
	};

// Loop over the particles
	for(particle=0;particle<Nparticles;particle++){

		// Store the parameters of the considered particle:
		Z_p = Z[particle];
		logAge_p = logAge[particle];
		Mass_p = ParticleMass[particle];
		AV_p = A_V[particle];
		distance_correction = 10.+5.*log10(dist[particle]);
		nobstars[particle] = 0.;
		

		// Avoid out of range values:
		logAge_p = (logAge_p<logAge_min? logAge_min: logAge_p);
		logAge_p = (logAge_p>logAge_max? logAge_max: logAge_p);

		Z_p = (Z_p<Z_set[0]? Z_set[0]: Z_p);
		Z_p = (Z_p>Z_set[Number_of_Z-1]? Z_set[Number_of_Z-1]: Z_p);


		// Get the index of that metallicity value (the Z sampling is not homogeneous):
		i_Z = -1; //Initial value
		do{
			i_Z++;
		}while( Z_set[i_Z]<=Z_p );
		i_Z--;


		//Assume that the logAge grid is regular (from logAge_min to logAge_max with step logAge_step)
		i_logAge = (int)((Number_of_logAge-1)*(logAge_p-logAge_min)/(logAge_max-logAge_min));
		i_isocrone = i_logAge + (Number_of_logAge)*i_Z;


		// Read the following information about the selected isocrone: [starting_point of the isocrone, Nasses]
		fseek( fichero_stat_bin_x, 32*i_isocrone+16, SEEK_SET);// 32 bytes = 4 bytes*8 columns
		fseek( fichero_stat_bin_y, 32*i_isocrone+16, SEEK_SET);// 32 bytes = 4 bytes*8 columns

		fread(&starting_point_of_the_isocrone,4,1,fichero_stat_bin_x); //Starting position (inclusive)(common for both magnitudes)
		fread(&Number_of_logmasses_in_the_isocrone,4,1,fichero_stat_bin_x);// Rows (stars) that belong to this isocrone(common for both magnitudes)


		// Initialize the Mfcis:
		Mfcis[particle] = 0.0; // Mass fraction converted to stars

		// Go to the desired isocrone
		fseek( fichero_isocr_bin_x, 4*starting_point_of_the_isocrone, SEEK_SET); //1=1*4 bytes = np.float32 bits*1 column
		fseek( fichero_isocr_bin_y, 4*starting_point_of_the_isocrone, SEEK_SET); //1=1*4 bytes = np.float32 bits*1 column

		// WARNING! This line is different from the Salpeter's version:
		// Call to the Kroupa IMF function
		KroupaIMF( mi, Max_Number_of_logmasses_per_isocrone, mi[Number_of_logmasses_in_the_isocrone-1], N_mi); //N_mi means number of masses between mi and mi+dm (egde effects included)


		// Load V and V-Ic from the isocrone given by (Z, logAge), and finish P(m_i)
		for(star=0;star<Number_of_logmasses_in_the_isocrone;star++){
			// Get the absolute magnitudes
			fread(&magnitude_x,4,1, fichero_isocr_bin_x); // Absolute magnitude
			fread(&magnitude_y,4,1, fichero_isocr_bin_y); // Absolute magnitude

			// From absolute to apparent
			magnitude_x = magnitude_x + distance_correction + AV_p*factor_x;
			magnitude_y = magnitude_y + distance_correction + AV_p*factor_y;

		
			//Colour difference:
			magnitude_x = magnitude_x - magnitude_y;
			magnitude_x *=sign; //Orientation

			
			// Would this star be included in the histogram?
			is_selected = (mag_axis[0]<=magnitude_x)*(magnitude_x<mag_axis[1]);// 1= Yes, 0=No
			is_selected = is_selected*(mag_axis[2]<=magnitude_y)*(magnitude_y<mag_axis[3]);// 1= Yes, 0=No

			// Mass fraction converted into observed stars
			Mfcis[particle] += mi[star]*N_mi[star]*is_selected; //Add the contribution

			// Number of OBSERVED stars for this particular particle
			nobstars[particle] += Mass_p*N_mi[star]*is_selected; //Add the contribution

			//CMD
			bin_y = (int)(CMD_dim[1]*(magnitude_y-mag_axis[2])/(mag_axis[3]-mag_axis[2]));// Row
			bin_x = (int)(CMD_dim[0]*(magnitude_x-mag_axis[0])/(mag_axis[1]-mag_axis[0]));// Column
			cmd_bin = bin_x + CMD_dim[0]*bin_y;

			if(is_selected){
				cmd_map[cmd_bin] += Mass_p*N_mi[star];//Add the contribution to the histogram
			};

		}; // End of the star-loop

	}; // End of the particle

	fclose( fichero_stat_bin_x );
	fclose( fichero_isocr_bin_x );
	fclose( fichero_stat_bin_y );
	fclose( fichero_isocr_bin_y );

}//End of CMD_Kroupa
//-----------------------------------------------------------------------------------------------------------------//




//-----------------------------------------------------------------------------------------------------------------//
/* CMD_Kroupa_4Points */
void CMD_Kroupa_4Points(float *Z, float *logAge, float *ParticleMass, float *dist, float *A_V, float *cmd_map, float *nobstars, float *Mfcis, int Nparticles, int *CMD_dim, float *mag_axis, int *bands_id ){
/* Since extinction is provided, lon and lat are not needed*/
// Color magnitude histogram is a 2D array (but vectorized) of size CMD_dim[1]*CMD_dim[0]
// Mfcis means Mass fraction converted to stars. It has Nparticles elements
// A_V: in mag, must be given for the Johnsons V band
// UNITS: Z is a fraction, Ages in yrs, distances in kpc, extinction in mag
//	total_correction is dimensionless
// Output: CMD (CMD_dim[0]*CMD_dim[1] elements) and Mfcis (Nparticles- values in an array)
	int n, particle, star, cmd_bin, bin_x, bin_y; // Counters
	float Z_set[34]; // Set of metallicities (the grid is not regular in Z)
	int is_selected; // 1 if the magnitude is in [mag_min, mag_max)
	/* factor = A_lambda/A_V*/
	float factor_x, factor_y;
	int sign=1; // The horizontal axis of the CMD goes from the Bluest to the reddest (B-V) and not (V-B)

	/* Values for each particle */
	int i_Z, i_logAge, i_isocrone; //Index of the metallicity, logAge, isocrone
	float Z_p, logAge_p, Mass_p, AV_p; // Values for one selected particle
	float distance_correction; // 10+5*log10(d_kpc)

	/* Values for each star */
	float magnitude_x, magnitude_y; //Apparent magnitude

	/* Values for each isocrone */
	int starting_point_of_the_isocrone, Number_of_logmasses_in_the_isocrone;

	/* IMF */
	const float dlogm = (Logmi_MAX-Logmi_MIN)/(1.0*Max_Number_of_logmasses_per_isocrone); // bin size
	float mi[Max_Number_of_logmasses_per_isocrone], N_mi[Max_Number_of_logmasses_per_isocrone]; //Masses and global IMF

	/* Weights and temporal outputs */
	float wLR, wRL, wDU, wUD; // wLR+wRL = wDU+wUD = 1.

	// Files:
	FILE *fichero_ASCII; // ASCII file with the information about the isocrones
	FILE *fichero_stat_bin_x, *fichero_stat_bin_y; //Binary file with the statistics of all the isocrones
	FILE *fichero_isocr_bin_x, *fichero_isocr_bin_y; //Binary file with all the isocrones


	/* Read the metallicity values of the grid */
	fichero_ASCII = fopen( ISONO,"r"); //Open All_the_isocrones3.bin
	for (n=0; n<Number_of_Z;n++){
		fscanf( fichero_ASCII, "%f\n", &Z_set[n]); // Read the decimal part of the metallicity 
		Z_set[n] = Z_set[n]*1E-6; // Convert to true Z [met]

	} // Metallicity values read!
	fclose( fichero_ASCII ); //Close the ASCII file

	// Open the correspondent file:
	switch(bands_id[0]){
		case 0:
			fichero_isocr_bin_x = fopen( BIN_U,"rb"); //Open the list of M_U magnitudes
			fichero_stat_bin_x = fopen( EXPL_U,"rb"); // Open the statistics of the M_U magnitudes
			factor_x = 4.968/3.24;
			break;
		case 1:
			fichero_isocr_bin_x = fopen( BIN_B,"rb"); //Open the list of M_B magnitudes
			fichero_stat_bin_x = fopen( EXPL_B,"rb"); // Open the statistics of the M_B magnitudes
			factor_x = 4.325/3.24;
			break;
		case 2:
			fichero_isocr_bin_x = fopen( BIN_V,"rb"); //Open the list of M_V magnitudes
			fichero_stat_bin_x = fopen( EXPL_V,"rb"); // Open the statistics of the M_V magnitudes
			factor_x = 1.;
			break;
		case 3:
			fichero_isocr_bin_x = fopen( BIN_R,"rb"); //Open the list of M_R magnitudes
			fichero_stat_bin_x = fopen( EXPL_R,"rb"); // Open the statistics of the M_R magnitudes
			factor_x = 2.634/3.24;
			break;
		case 4:
			fichero_isocr_bin_x = fopen( BIN_I,"rb"); //Open the list of M_I magnitudes
			fichero_stat_bin_x = fopen( EXPL_I,"rb"); // Open the statistics of the M_I magnitudes
			factor_x = 1.962/3.24;
			break;
		case 5:
			fichero_isocr_bin_x = fopen( BIN_J,"rb"); //Open the list of M_J magnitudes
			fichero_stat_bin_x = fopen( EXPL_J,"rb"); // Open the statistics of the M_J magnitudes
			factor_x = 0.9536616/3.24;
			break;
		case 6:
			fichero_isocr_bin_x = fopen( BIN_H,"rb"); //Open the list of M_H magnitudes
			fichero_stat_bin_x = fopen( EXPL_H,"rb"); // Open the statistics of the M_H magnitudes
			factor_x = 0.5873472/3.24;
			break;
		case 7:
			fichero_isocr_bin_x = fopen( BIN_K,"rb"); //Open the list of M_K magnitudes
			fichero_stat_bin_x = fopen( EXPL_K,"rb"); // Open the statistics of the M_K magnitudes
			factor_x = 0.367/3.24;
			break;
		default:
			fprintf(stderr, "Unknown first filter. Johnson V band will be used instead\n");
			fichero_isocr_bin_x = fopen( BIN_V,"rb"); //Open the list of M_V magnitudes
			fichero_stat_bin_x = fopen( EXPL_V,"rb"); // Open the statistics of the M_V magnitudes
			factor_x = 1.;
	};

	// Open the correspondent file:
	switch(bands_id[1]){
		case 0:
			fichero_isocr_bin_y = fopen( BIN_U,"rb"); //Open the list of M_U magnitudes
			fichero_stat_bin_y = fopen( EXPL_U,"rb"); // Open the statistics of the M_U magnitudes
			factor_y = 4.968/3.24;
			break;
		case 1:
			fichero_isocr_bin_y = fopen( BIN_B,"rb"); //Open the list of M_B magnitudes
			fichero_stat_bin_y = fopen( EXPL_B,"rb"); // Open the statistics of the M_B magnitudes
			factor_y = 4.325/3.24;
			break;
		case 2:
			fichero_isocr_bin_y = fopen( BIN_V,"rb"); //Open the list of M_V magnitudes
			fichero_stat_bin_y = fopen( EXPL_V,"rb"); // Open the statistics of the M_V magnitudes
			factor_y = 1.;
			break;
		case 3:
			fichero_isocr_bin_y = fopen( BIN_R,"rb"); //Open the list of M_R magnitudes
			fichero_stat_bin_y = fopen( EXPL_R,"rb"); // Open the statistics of the M_R magnitudes
			factor_y = 2.634/3.24;
			break;
		case 4:
			fichero_isocr_bin_y = fopen( BIN_I,"rb"); //Open the list of M_I magnitudes
			fichero_stat_bin_y = fopen( EXPL_I,"rb"); // Open the statistics of the M_I magnitudes
			factor_y = 1.962/3.24;
			break;
		case 5:
			fichero_isocr_bin_y = fopen( BIN_J,"rb"); //Open the list of M_J magnitudes
			fichero_stat_bin_y = fopen( EXPL_J,"rb"); // Open the statistics of the M_J magnitudes
			factor_y = 0.9536616/3.24;
			break;
		case 6:
			fichero_isocr_bin_y = fopen( BIN_H,"rb"); //Open the list of M_H magnitudes
			fichero_stat_bin_y = fopen( EXPL_H,"rb"); // Open the statistics of the M_H magnitudes
			factor_y = 0.5873472/3.24;
			break;
		case 7:
			fichero_isocr_bin_y = fopen( BIN_K,"rb"); //Open the list of M_K magnitudes
			fichero_stat_bin_y = fopen( EXPL_K,"rb"); // Open the statistics of the M_K magnitudes
			factor_y = 0.367/3.24;
			break;
		default:
			fprintf(stderr, "Unknown second filter. Johnson V band will be used instead\n");
			fichero_isocr_bin_y = fopen( BIN_V,"rb"); //Open the list of M_V magnitudes
			fichero_stat_bin_y = fopen( EXPL_V,"rb"); // Open the statistics of the M_V magnitudes
			factor_y = 1.;
	};

	//X axis orientation;
	sign = (bands_id[1]>bands_id[0]?1:-1);

	//Since logmi_mi and Logmi_MAX are equal for all of the isocrones, the initial mass array would be the same:
	/* Probability given the Kroupa IMF */
	for(star=0; star<Max_Number_of_logmasses_per_isocrone;star++){
		mi[star] = pow(10., Logmi_MIN + dlogm*star);
	};

	// WARNING! This line is different from the Salpeter's version!

	//Initiliaze the CMD:
	for(cmd_bin=0;cmd_bin<CMD_dim[0]*CMD_dim[1];cmd_bin++){
		cmd_map[cmd_bin]=0.0;
	};

// Loop over the particles
	for(particle=0;particle<Nparticles;particle++){

		// Store the parameters of the considered particle:
		Z_p = Z[particle];
		logAge_p = logAge[particle];
		Mass_p = ParticleMass[particle];
		AV_p = A_V[particle];
		distance_correction = 10.+5.*log10(dist[particle]);
		nobstars[particle] = 0.;
		// Initialize the Mfcis:
		Mfcis[particle] = 0.0; // Mass fraction converted to stars
		

		// Avoid out of range values:
		logAge_p = (logAge_p<logAge_min? logAge_min: logAge_p);
		logAge_p = (logAge_p>logAge_max? logAge_max: logAge_p);

		Z_p = (Z_p<Z_set[0]? Z_set[0]: Z_p);
		Z_p = (Z_p>Z_set[Number_of_Z-1]? Z_set[Number_of_Z-1]: Z_p);



		/* ++++++++++++++++++++++++++++++++ ESTIMATION OF THE WEIGHTS ARE DONE FIRSTLY ++++++++++++++++++++++++++++++++++++*/
		// By calculating the weights first, we avoid the use of huge cmd_maps temporaly variables

			//......................................... LEFT DOWN POINT ..........................................
			// Get the index of that metallicity value (the Z sampling is not homogeneous):
			i_Z = -1; //Initial value
			do{
				i_Z++;
			}while( Z_set[i_Z]<=Z_p );
			i_Z--;

			//Assume that the logAge grid is regular (from logAge_min to logAge_max with step logAge_step)
			i_logAge = (int)((Number_of_logAge-1)*(logAge_p-logAge_min)/(logAge_max-logAge_min));

			//Weights:
			wLR = Z_p-Z_set[i_Z]; //For the Left-Down point of the grid


			//......................................... RIGHT DOWN POINT .........................................

			//i_logAge does not change but i_Z...
			i_Z = i_Z + 1*(i_Z!=(Number_of_Z-1));

			//Weights:
			wRL = Z_set[i_Z]-Z_p; //For the Right-Down point of the grid

			// ......................................... RIGHT UP POINT.........................................
			//i_Z as in the previous case, but i_logAge has increase (or not)...
			i_logAge = i_logAge + 1*(i_logAge!=(Number_of_logAge-1));
			//Weights:
			wUD = logAge_min - i_logAge*logAge_step - logAge_p; //For the Right-Up point of the grid

			// .........................................  LEFT UP POINT.........................................

			//i_logAge as in the previous case, but i_Z decreases (or not)...
			i_Z = i_Z - 1*(i_Z>0);
			//Weights:
			wDU = logAge_p - logAge_min - i_logAge*logAge_step; //For the Left-Up point of the grid

			// Be sure they are normalized...

			if(wLR==-wRL){
				wLR = 1.;
				wRL = 0.;
			}else{
				wLR = wRL/(wLR+wRL);
				wRL = 1.-wLR;
			};

			if(wUD==-wDU){
				wUD = 1.;
				wDU = 0.;
			}else{
				wUD = wDU/(wUD+wDU);
				wDU = 1.-wUD;
			};



		//-----------------------------------------------------------------------------------------------------
		// From now on, proceed as usual

		/* ++++++++++++++++++++++++++++++++ LEFT DOWN POINT ++++++++++++++++++++++++++++++++++++++++*/

		// Get the index of that metallicity value (the Z sampling is not homogeneous):
		i_Z = -1; //Initial value
		do{
			i_Z++;
		}while( Z_set[i_Z]<=Z_p );
		i_Z--;


		//Assume that the logAge grid is regular (from logAge_min to logAge_max with step logAge_step)
		i_logAge = (int)((Number_of_logAge-1)*(logAge_p-logAge_min)/(logAge_max-logAge_min));
		i_isocrone = i_logAge + (Number_of_logAge)*i_Z;


		// Read the following information about the selected isocrone: [starting_point of the isocrone, Nasses]
		fseek( fichero_stat_bin_x, 32*i_isocrone+16, SEEK_SET);// 32 bytes = 4 bytes*8 columns
		fseek( fichero_stat_bin_y, 32*i_isocrone+16, SEEK_SET);// 32 bytes = 4 bytes*8 columns

		fread(&starting_point_of_the_isocrone,4,1,fichero_stat_bin_x); //Starting position (inclusive)(common for both magnitudes)
		fread(&Number_of_logmasses_in_the_isocrone,4,1,fichero_stat_bin_x);// Rows (stars) that belong to this isocrone(common for both magnitudes)



		// Go to the desired isocrone
		fseek( fichero_isocr_bin_x, 4*starting_point_of_the_isocrone, SEEK_SET); //1=1*4 bytes = np.float32 bits*1 column
		fseek( fichero_isocr_bin_y, 4*starting_point_of_the_isocrone, SEEK_SET); //1=1*4 bytes = np.float32 bits*1 column

		// WARNING! This line is different from the Salpeter's version:
		// Call to the Kroupa IMF function
		KroupaIMF( mi, Max_Number_of_logmasses_per_isocrone, mi[Number_of_logmasses_in_the_isocrone-1], N_mi); //N_mi means number of masses between mi and mi+dm (egde effects included)


		// Load V and V-Ic from the isocrone given by (Z, logAge), and finish P(m_i)
		for(star=0;star<Number_of_logmasses_in_the_isocrone;star++){
			// Get the absolute magnitudes
			fread(&magnitude_x,4,1, fichero_isocr_bin_x); // Absolute magnitude
			fread(&magnitude_y,4,1, fichero_isocr_bin_y); // Absolute magnitude

			// From absolute to apparent
			magnitude_x = magnitude_x + distance_correction + AV_p*factor_x;
			magnitude_y = magnitude_y + distance_correction + AV_p*factor_y;

		
			//Colour difference:
			magnitude_x = magnitude_x - magnitude_y;
			magnitude_x *=sign; //Orientation

			
			// Would this star be included in the histogram?
			is_selected = (mag_axis[0]<=magnitude_x)*(magnitude_x<mag_axis[1]);// 1= Yes, 0=No
			is_selected = is_selected*(mag_axis[2]<=magnitude_y)*(magnitude_y<mag_axis[3]);// 1= Yes, 0=No

			// Make N_mi[star] == 0 if the star is out of range...
			N_mi[star] = wDU*wLR*N_mi[star]*is_selected;//...and apply the weighting

			// Mass fraction converted into observed stars
			Mfcis[particle] += mi[star]*N_mi[star]; //Add the contribution

			// Number of OBSERVED stars for this particular particle
			nobstars[particle] += Mass_p*N_mi[star]; //Add the contribution

			//CMD
			bin_y = (int)(CMD_dim[1]*(magnitude_y-mag_axis[2])/(mag_axis[3]-mag_axis[2]));// Row
			bin_x = (int)(CMD_dim[0]*(magnitude_x-mag_axis[0])/(mag_axis[1]-mag_axis[0]));// Column
			cmd_bin = bin_x + CMD_dim[0]*bin_y;

			if(is_selected){
				cmd_map[cmd_bin] += Mass_p*N_mi[star];//Add the contribution to the histogram
			};

		}; // End of the star-loop


		/* ++++++++++++++++++++++++++++++++ RIGHT DOWN POINT ++++++++++++++++++++++++++++++++++++++++*/

		//i_logAge does not change but i_Z...
		i_Z = i_Z + 1*(i_Z!=(Number_of_Z-1));
		
		i_isocrone = i_logAge + Number_of_logAge*i_Z;

		// Read the following information about the selected isocrone: [starting_point of the isocrone, Nasses]
		fseek( fichero_stat_bin_x, 32*i_isocrone+16, SEEK_SET);// 32 bytes = 4 bytes*8 columns
		fseek( fichero_stat_bin_y, 32*i_isocrone+16, SEEK_SET);// 32 bytes = 4 bytes*8 columns

		fread(&starting_point_of_the_isocrone,4,1,fichero_stat_bin_x); //Starting position (inclusive)(common for both magnitudes)
		fread(&Number_of_logmasses_in_the_isocrone,4,1,fichero_stat_bin_x);// Rows (stars) that belong to this isocrone(common for both magnitudes)



		// Go to the desired isocrone
		fseek( fichero_isocr_bin_x, 4*starting_point_of_the_isocrone, SEEK_SET); //1=1*4 bytes = np.float32 bits*1 column
		fseek( fichero_isocr_bin_y, 4*starting_point_of_the_isocrone, SEEK_SET); //1=1*4 bytes = np.float32 bits*1 column

		// WARNING! This line is different from the Salpeter's version:
		// Call to the Kroupa IMF function
		KroupaIMF( mi, Max_Number_of_logmasses_per_isocrone, mi[Number_of_logmasses_in_the_isocrone-1], N_mi); //N_mi means number of masses between mi and mi+dm (egde effects included)

		// Load V and V-Ic from the isocrone given by (Z, logAge), and finish P(m_i)
		for(star=0;star<Number_of_logmasses_in_the_isocrone;star++){
			// Get the absolute magnitudes
			fread(&magnitude_x,4,1, fichero_isocr_bin_x); // Absolute magnitude
			fread(&magnitude_y,4,1, fichero_isocr_bin_y); // Absolute magnitude

			// From absolute to apparent
			magnitude_x = magnitude_x + distance_correction + AV_p*factor_x;
			magnitude_y = magnitude_y + distance_correction + AV_p*factor_y;

		
			//Colour difference:
			magnitude_x = magnitude_x - magnitude_y;
			magnitude_x *=sign; //Orientation

			
			// Would this star be included in the histogram?
			is_selected = (mag_axis[0]<=magnitude_x)*(magnitude_x<mag_axis[1]);// 1= Yes, 0=No
			is_selected = is_selected*(mag_axis[2]<=magnitude_y)*(magnitude_y<mag_axis[3]);// 1= Yes, 0=No

			// Make N_mi[star] == 0 if the star is out of range...
			N_mi[star] = wDU*wRL*N_mi[star]*is_selected;//...and apply the weighting

			// Mass fraction converted into observed stars
			Mfcis[particle] += mi[star]*N_mi[star]; //Add the contribution

			// Number of OBSERVED stars for this particular particle
			nobstars[particle] += Mass_p*N_mi[star]; //Add the contribution

			//CMD
			bin_y = (int)(CMD_dim[1]*(magnitude_y-mag_axis[2])/(mag_axis[3]-mag_axis[2]));// Row
			bin_x = (int)(CMD_dim[0]*(magnitude_x-mag_axis[0])/(mag_axis[1]-mag_axis[0]));// Column
			cmd_bin = bin_x + CMD_dim[0]*bin_y;

			if(is_selected){
				cmd_map[cmd_bin] += Mass_p*N_mi[star];//Add the contribution to the histogram
			};

		}; // End of the star-loop


		/* ++++++++++++++++++++++++++++++++ RIGHT UP POINT ++++++++++++++++++++++++++++++++++++++++*/

		//i_Z as in the previous case, but i_logAge has increase (or not)...
		i_logAge = i_logAge + 1*(i_logAge!=(Number_of_logAge-1));
		i_isocrone = i_logAge + Number_of_logAge*i_Z;

		// Read the following information about the selected isocrone: [starting_point of the isocrone, Nasses]
		fseek( fichero_stat_bin_x, 32*i_isocrone+16, SEEK_SET);// 32 bytes = 4 bytes*8 columns
		fseek( fichero_stat_bin_y, 32*i_isocrone+16, SEEK_SET);// 32 bytes = 4 bytes*8 columns

		fread(&starting_point_of_the_isocrone,4,1,fichero_stat_bin_x); //Starting position (inclusive)(common for both magnitudes)
		fread(&Number_of_logmasses_in_the_isocrone,4,1,fichero_stat_bin_x);// Rows (stars) that belong to this isocrone(common for both magnitudes)



		// Go to the desired isocrone
		fseek( fichero_isocr_bin_x, 4*starting_point_of_the_isocrone, SEEK_SET); //1=1*4 bytes = np.float32 bits*1 column
		fseek( fichero_isocr_bin_y, 4*starting_point_of_the_isocrone, SEEK_SET); //1=1*4 bytes = np.float32 bits*1 column

		// WARNING! This line is different from the Salpeter's version:
		// Call to the Kroupa IMF function
		KroupaIMF( mi, Max_Number_of_logmasses_per_isocrone, mi[Number_of_logmasses_in_the_isocrone-1], N_mi); //N_mi means number of masses between mi and mi+dm (egde effects included)

		// Load V and V-Ic from the isocrone given by (Z, logAge), and finish P(m_i)
		for(star=0;star<Number_of_logmasses_in_the_isocrone;star++){
			// Get the absolute magnitudes
			fread(&magnitude_x,4,1, fichero_isocr_bin_x); // Absolute magnitude
			fread(&magnitude_y,4,1, fichero_isocr_bin_y); // Absolute magnitude

			// From absolute to apparent
			magnitude_x = magnitude_x + distance_correction + AV_p*factor_x;
			magnitude_y = magnitude_y + distance_correction + AV_p*factor_y;

		
			//Colour difference:
			magnitude_x = magnitude_x - magnitude_y;
			magnitude_x *=sign; //Orientation

			
			// Would this star be included in the histogram?
			is_selected = (mag_axis[0]<=magnitude_x)*(magnitude_x<mag_axis[1]);// 1= Yes, 0=No
			is_selected = is_selected*(mag_axis[2]<=magnitude_y)*(magnitude_y<mag_axis[3]);// 1= Yes, 0=No

			// Make N_mi[star] == 0 if the star is out of range...
			N_mi[star] = wUD*wRL*N_mi[star]*is_selected;//...and apply the weighting

			// Mass fraction converted into observed stars
			Mfcis[particle] += mi[star]*N_mi[star]; //Add the contribution

			// Number of OBSERVED stars for this particular particle
			nobstars[particle] += Mass_p*N_mi[star]; //Add the contribution

			//CMD
			bin_y = (int)(CMD_dim[1]*(magnitude_y-mag_axis[2])/(mag_axis[3]-mag_axis[2]));// Row
			bin_x = (int)(CMD_dim[0]*(magnitude_x-mag_axis[0])/(mag_axis[1]-mag_axis[0]));// Column
			cmd_bin = bin_x + CMD_dim[0]*bin_y;

			if(is_selected){
				cmd_map[cmd_bin] += Mass_p*N_mi[star];//Add the contribution to the histogram
			};

		}; // End of the star-loop

		/* ++++++++++++++++++++++++++++++++ LEFT UP POINT ++++++++++++++++++++++++++++++++++++++++*/

		//i_logAge as in the previous case, but i_Z decreases (or not)...
		i_Z = i_Z - 1*(i_Z>0);
		i_isocrone = i_logAge + Number_of_logAge*i_Z;

		// Read the following information about the selected isocrone: [starting_point of the isocrone, Nasses]
		fseek( fichero_stat_bin_x, 32*i_isocrone+16, SEEK_SET);// 32 bytes = 4 bytes*8 columns
		fseek( fichero_stat_bin_y, 32*i_isocrone+16, SEEK_SET);// 32 bytes = 4 bytes*8 columns

		fread(&starting_point_of_the_isocrone,4,1,fichero_stat_bin_x); //Starting position (inclusive)(common for both magnitudes)
		fread(&Number_of_logmasses_in_the_isocrone,4,1,fichero_stat_bin_x);// Rows (stars) that belong to this isocrone(common for both magnitudes)


		// Go to the desired isocrone
		fseek( fichero_isocr_bin_x, 4*starting_point_of_the_isocrone, SEEK_SET); //1=1*4 bytes = np.float32 bits*1 column
		fseek( fichero_isocr_bin_y, 4*starting_point_of_the_isocrone, SEEK_SET); //1=1*4 bytes = np.float32 bits*1 column

		// WARNING! This line is different from the Salpeter's version:
		// Call to the Kroupa IMF function
		KroupaIMF( mi, Max_Number_of_logmasses_per_isocrone, mi[Number_of_logmasses_in_the_isocrone-1], N_mi); //N_mi means number of masses between mi and mi+dm (egde effects included)

		// Load V and V-Ic from the isocrone given by (Z, logAge), and finish P(m_i)
		for(star=0;star<Number_of_logmasses_in_the_isocrone;star++){
			// Get the absolute magnitudes
			fread(&magnitude_x,4,1, fichero_isocr_bin_x); // Absolute magnitude
			fread(&magnitude_y,4,1, fichero_isocr_bin_y); // Absolute magnitude

			// From absolute to apparent
			magnitude_x = magnitude_x + distance_correction + AV_p*factor_x;
			magnitude_y = magnitude_y + distance_correction + AV_p*factor_y;

		
			//Colour difference:
			magnitude_x = magnitude_x - magnitude_y;
			magnitude_x *=sign; //Orientation

			
			// Would this star be included in the histogram?
			is_selected = (mag_axis[0]<=magnitude_x)*(magnitude_x<mag_axis[1]);// 1= Yes, 0=No
			is_selected = is_selected*(mag_axis[2]<=magnitude_y)*(magnitude_y<mag_axis[3]);// 1= Yes, 0=No

			// Make N_mi[star] == 0 if the star is out of range...
			N_mi[star] = wUD*wLR*N_mi[star]*is_selected;//...and apply the weighting

			// Mass fraction converted into observed stars
			Mfcis[particle] += mi[star]*N_mi[star]; //Add the contribution

			// Number of OBSERVED stars for this particular particle
			nobstars[particle] += Mass_p*N_mi[star]; //Add the contribution

			//CMD
			bin_y = (int)(CMD_dim[1]*(magnitude_y-mag_axis[2])/(mag_axis[3]-mag_axis[2]));// Row
			bin_x = (int)(CMD_dim[0]*(magnitude_x-mag_axis[0])/(mag_axis[1]-mag_axis[0]));// Column
			cmd_bin = bin_x + CMD_dim[0]*bin_y;

			if(is_selected){
				cmd_map[cmd_bin] += Mass_p*N_mi[star];//Add the contribution to the histogram
			};

		}; // End of the star-loop

	}; // End of the particle

	fclose( fichero_stat_bin_x );
	fclose( fichero_isocr_bin_x );
	fclose( fichero_stat_bin_y );
	fclose( fichero_isocr_bin_y );

}//End of CMD_Kroupa_4Points
//-----------------------------------------------------------------------------------------------------------------//




//-----------------------------------------------------------------------------------------------------------------//
/* HR_Salpeter */
void HR_Salpeter(float *Z, float *logAge, float *ParticleMass, float *dist, float *A_V, float *hr_map, float *nobstars, float *Mfcis, int Nparticles, int *HR_dim, float *hr_axis, int *bands_id ){
/* Since extinction is provided, lon and lat are not needed*/
// Hertzsprung Russell Diagram is a 2D array (but vectorized) of size HR_dim[1]*HR_dim[0]
// Mfcis means Mass fraction converted to stars. It has Nparticles elements
// A_V: in mag, must be given for the Johnsons V band
// UNITS: Z is a fraction, Ages in yrs, distances in kpc, extinction in mag
//	total_correction is dimensionless
// Output: HR (HR_dim[0]*HR_dim[1] elements) and Mfcis (Nparticles- values in an array)
	int n, particle, star, hr_bin, bin_x, bin_y; // Counters
	float Z_set[34]; // Set of metallicities (the grid is not regular in Z)
	int is_selected; // 1 if the magnitude is in [mag_min, mag_max)
	/* factor = A_lambda/A_V*/
	float factor_x, factor_y;

	/* Values for each particle */
	int i_Z, i_logAge, i_isocrone; //Index of the metallicity, logAge, isocrone
	float Z_p, logAge_p, Mass_p, AV_p; // Values for one selected particle
	float distance_correction_x, distance_correction_y; // 10+5*log10(d_kpc)

	/* Values for each star */
	float magnitude_x, magnitude_y; //Apparent magnitude

	/* Values for each isocrone */
	int starting_point_of_the_isocrone, Number_of_logmasses_in_the_isocrone;

	/* IMF */
	const float dlogm = (Logmi_MAX-Logmi_MIN)/(1.0*Max_Number_of_logmasses_per_isocrone); // bin size
	float mi[Max_Number_of_logmasses_per_isocrone], N_mi[Max_Number_of_logmasses_per_isocrone]; //Masses and global IMF

	// Files:
	FILE *fichero_ASCII; // ASCII file with the information about the isocrones
	FILE *fichero_stat_bin_x, *fichero_stat_bin_y; //Binary file with the statistics of all the isocrones
	FILE *fichero_isocr_bin_x, *fichero_isocr_bin_y; //Binary file with all the isocrones


	/* Read the metallicity values of the grid */
	fichero_ASCII = fopen( ISONO,"r"); //Open All_the_isocrones3.bin
	for (n=0; n<Number_of_Z;n++){
		fscanf( fichero_ASCII, "%f\n", &Z_set[n]); // Read the decimal part of the metallicity 
		Z_set[n] = Z_set[n]*1E-6; // Convert to true Z [met]

	} // Metallicity values read!
	fclose( fichero_ASCII ); //Close the ASCII file

	// Open the correspondent file:
	switch(bands_id[0]){
		case -3:
			fichero_isocr_bin_x = fopen( BIN_mbol,"rb"); //Open the list of bolometric magnitudes
			fichero_stat_bin_x = fopen( EXPL_mbol,"rb"); // Open the statistics of the bolometric magnitudes
			factor_x = 0;
			break;
		case -2:
			fichero_isocr_bin_x = fopen( BIN_logG,"rb"); //Open the list of logg
			fichero_stat_bin_x = fopen( EXPL_logG,"rb"); // Open the statistics of the logg
			factor_x = 0.;
			break;
		case -1:
			fichero_isocr_bin_x = fopen( BIN_logTe,"rb"); //Open the list of logTe
			fichero_stat_bin_x = fopen( EXPL_logTe,"rb"); // Open the statistics of logTe
			factor_x = 0.;
			break;
		case 0:
			fichero_isocr_bin_x = fopen( BIN_U,"rb"); //Open the list of M_U magnitudes
			fichero_stat_bin_x = fopen( EXPL_U,"rb"); // Open the statistics of the M_U magnitudes
			factor_x = 4.968/3.24;
			break;
		case 1:
			fichero_isocr_bin_x = fopen( BIN_B,"rb"); //Open the list of M_B magnitudes
			fichero_stat_bin_x = fopen( EXPL_B,"rb"); // Open the statistics of the M_B magnitudes
			factor_x = 4.325/3.24;
			break;
		case 2:
			fichero_isocr_bin_x = fopen( BIN_V,"rb"); //Open the list of M_V magnitudes
			fichero_stat_bin_x = fopen( EXPL_V,"rb"); // Open the statistics of the M_V magnitudes
			factor_x = 1.;
			break;
		case 3:
			fichero_isocr_bin_x = fopen( BIN_R,"rb"); //Open the list of M_R magnitudes
			fichero_stat_bin_x = fopen( EXPL_R,"rb"); // Open the statistics of the M_R magnitudes
			factor_x = 2.634/3.24;
			break;
		case 4:
			fichero_isocr_bin_x = fopen( BIN_I,"rb"); //Open the list of M_I magnitudes
			fichero_stat_bin_x = fopen( EXPL_I,"rb"); // Open the statistics of the M_I magnitudes
			factor_x = 1.962/3.24;
			break;
		case 5:
			fichero_isocr_bin_x = fopen( BIN_J,"rb"); //Open the list of M_J magnitudes
			fichero_stat_bin_x = fopen( EXPL_J,"rb"); // Open the statistics of the M_J magnitudes
			factor_x = 0.9536616/3.24;
			break;
		case 6:
			fichero_isocr_bin_x = fopen( BIN_H,"rb"); //Open the list of M_H magnitudes
			fichero_stat_bin_x = fopen( EXPL_H,"rb"); // Open the statistics of the M_H magnitudes
			factor_x = 0.5873472/3.24;
			break;
		case 7:
			fichero_isocr_bin_x = fopen( BIN_K,"rb"); //Open the list of M_K magnitudes
			fichero_stat_bin_x = fopen( EXPL_K,"rb"); // Open the statistics of the M_K magnitudes
			factor_x = 0.367/3.24;
			break;
		default:
			fprintf(stderr, "Unknown first filter. Johnson V band will be used instead\n");
			fichero_isocr_bin_x = fopen( BIN_V,"rb"); //Open the list of M_V magnitudes
			fichero_stat_bin_x = fopen( EXPL_V,"rb"); // Open the statistics of the M_V magnitudes
			factor_x = 1.;
	};

	// Open the correspondent file:
	switch(bands_id[1]){
		case -3:
			fichero_isocr_bin_y = fopen( BIN_mbol,"rb"); //Open the list of bolometric magnitudes
			fichero_stat_bin_y = fopen( EXPL_mbol,"rb"); // Open the statistics of the bolometric magnitudes
			factor_y = 0.;
			break;
		case -2:
			fichero_isocr_bin_y = fopen( BIN_logG,"rb"); //Open the list of logg
			fichero_stat_bin_y = fopen( EXPL_logG,"rb"); // Open the statistics of the logg
			factor_y = 0.;
			break;
		case -1:
			fichero_isocr_bin_y = fopen( BIN_logTe,"rb"); //Open the list of logTe
			fichero_stat_bin_y = fopen( EXPL_logTe,"rb"); // Open the statistics of logTe
			factor_y = 0.;
			break;
		case 0:
			fichero_isocr_bin_y = fopen( BIN_U,"rb"); //Open the list of M_U magnitudes
			fichero_stat_bin_y = fopen( EXPL_U,"rb"); // Open the statistics of the M_U magnitudes
			factor_y = 4.968/3.24;
			break;
		case 1:
			fichero_isocr_bin_y = fopen( BIN_B,"rb"); //Open the list of M_B magnitudes
			fichero_stat_bin_y = fopen( EXPL_B,"rb"); // Open the statistics of the M_B magnitudes
			factor_y = 4.325/3.24;
			break;
		case 2:
			fichero_isocr_bin_y = fopen( BIN_V,"rb"); //Open the list of M_V magnitudes
			fichero_stat_bin_y = fopen( EXPL_V,"rb"); // Open the statistics of the M_V magnitudes
			factor_y = 1.;
			break;
		case 3:
			fichero_isocr_bin_y = fopen( BIN_R,"rb"); //Open the list of M_R magnitudes
			fichero_stat_bin_y = fopen( EXPL_R,"rb"); // Open the statistics of the M_R magnitudes
			factor_y = 2.634/3.24;
			break;
		case 4:
			fichero_isocr_bin_y = fopen( BIN_I,"rb"); //Open the list of M_I magnitudes
			fichero_stat_bin_y = fopen( EXPL_I,"rb"); // Open the statistics of the M_I magnitudes
			factor_y = 1.962/3.24;
			break;
		case 5:
			fichero_isocr_bin_y = fopen( BIN_J,"rb"); //Open the list of M_J magnitudes
			fichero_stat_bin_y = fopen( EXPL_J,"rb"); // Open the statistics of the M_J magnitudes
			factor_y = 0.9536616/3.24;
			break;
		case 6:
			fichero_isocr_bin_y = fopen( BIN_H,"rb"); //Open the list of M_H magnitudes
			fichero_stat_bin_y = fopen( EXPL_H,"rb"); // Open the statistics of the M_H magnitudes
			factor_y = 0.5873472/3.24;
			break;
		case 7:
			fichero_isocr_bin_y = fopen( BIN_K,"rb"); //Open the list of M_K magnitudes
			fichero_stat_bin_y = fopen( EXPL_K,"rb"); // Open the statistics of the M_K magnitudes
			factor_y = 0.367/3.24;
			break;
		default:
			fprintf(stderr, "Unknown second filter. Johnson V band will be used instead\n");
			fichero_isocr_bin_y = fopen( BIN_V,"rb"); //Open the list of M_V magnitudes
			fichero_stat_bin_y = fopen( EXPL_V,"rb"); // Open the statistics of the M_V magnitudes
			factor_y = 1.;
	};


	//Since logmi_mi and Logmi_MAX are equal for all of the isocrones, the IMF would be the same:
	/* Probability given the Kroupa IMF */
	for(star=0; star<Max_Number_of_logmasses_per_isocrone;star++){
		mi[star] = pow(10., Logmi_MIN + dlogm*star);
	};

	//Call to the Salpeter IMF function
	SalpeterIMF( mi, Max_Number_of_logmasses_per_isocrone, INFINITY, N_mi); //N_mi means number of masses between mi and mi+dm (egde effects included)

	//Initiliaze the HR:
	for(hr_bin=0;hr_bin<HR_dim[0]*HR_dim[1];hr_bin++){
		hr_map[hr_bin]=0.0;
	};

// Loop over the particles
	for(particle=0;particle<Nparticles;particle++){

		// Store the parameters of the considered particle:
		Z_p = Z[particle];
		logAge_p = logAge[particle];
		Mass_p = ParticleMass[particle];
		AV_p = A_V[particle];
		distance_correction_x = (10.+5.*log10(dist[particle]))*(bands_id[0]>=0);
		distance_correction_y = (10.+5.*log10(dist[particle]))*(bands_id[1]>=0);
		nobstars[particle] = 0.;
		

		// Avoid out of range values:
		logAge_p = (logAge_p<logAge_min? logAge_min: logAge_p);
		logAge_p = (logAge_p>logAge_max? logAge_max: logAge_p);

		Z_p = (Z_p<Z_set[0]? Z_set[0]: Z_p);
		Z_p = (Z_p>Z_set[Number_of_Z-1]? Z_set[Number_of_Z-1]: Z_p);


		// Get the index of that metallicity value (the Z sampling is not homogeneous):
		i_Z = -1; //Initial value
		do{
			i_Z++;
		}while( Z_set[i_Z]<=Z_p );
		i_Z--;


		//Assume that the logAge grid is regular (from logAge_min to logAge_max with step logAge_step)
		i_logAge = (int)((Number_of_logAge-1)*(logAge_p-logAge_min)/(logAge_max-logAge_min));
		i_isocrone = i_logAge + (Number_of_logAge)*i_Z;


		// Read the following information about the selected isocrone: [starting_point of the isocrone, Nasses]
		fseek( fichero_stat_bin_x, 32*i_isocrone+16, SEEK_SET);// 32 bytes = 4 bytes*8 columns
		fseek( fichero_stat_bin_y, 32*i_isocrone+16, SEEK_SET);// 32 bytes = 4 bytes*8 columns

		fread(&starting_point_of_the_isocrone,4,1,fichero_stat_bin_x); //Starting position (inclusive)(common for both magnitudes)
		fread(&Number_of_logmasses_in_the_isocrone,4,1,fichero_stat_bin_x);// Rows (stars) that belong to this isocrone(common for both magnitudes)


		// Initialize the Mfcis:
		Mfcis[particle] = 0.0; // Mass fraction converted to stars

		// Go to the desired isocrone
		fseek( fichero_isocr_bin_x, 4*starting_point_of_the_isocrone, SEEK_SET); //1=1*4 bytes = np.float32 bits*1 column
		fseek( fichero_isocr_bin_y, 4*starting_point_of_the_isocrone, SEEK_SET); //1=1*4 bytes = np.float32 bits*1 column


		//Correct the Particle Mass to exclude stellar masses larger than mi[ Number_of_logmasses_in_the_isocrone-1]
		Mass_p = Mass_p/(1.-pow(mi[Number_of_logmasses_in_the_isocrone-1]/simf_m0, 2.-simf_a)); // Finite max mass correction


		// Load V and V-Ic from the isocrone given by (Z, logAge), and finish P(m_i)
		for(star=0;star<Number_of_logmasses_in_the_isocrone;star++){
			// Get the absolute magnitudes
			fread(&magnitude_x,4,1, fichero_isocr_bin_x); // Absolute magnitude
			fread(&magnitude_y,4,1, fichero_isocr_bin_y); // Absolute magnitude

			// From absolute to apparent
			magnitude_x = magnitude_x + distance_correction_x + AV_p*factor_x;
			magnitude_y = magnitude_y + distance_correction_y + AV_p*factor_y;

			
			// Would this star be included in the histogram?
			is_selected = (hr_axis[0]<=magnitude_x)*(magnitude_x<hr_axis[1]);// 1= Yes, 0=No
			is_selected = is_selected*(hr_axis[2]<=magnitude_y)*(magnitude_y<hr_axis[3]);// 1= Yes, 0=No

			// Mass fraction converted to stars
			Mfcis[particle] += mi[star]*N_mi[star]*is_selected; //Add the contribution

			// Number of OBSERVED stars for this particular particle
			nobstars[particle] += Mass_p*N_mi[star]*is_selected; //Add the contribution

			//HR
			bin_y = (int)(HR_dim[1]*(magnitude_y-hr_axis[2])/(hr_axis[3]-hr_axis[2]));// Row
			bin_x = (int)(HR_dim[0]*(magnitude_x-hr_axis[0])/(hr_axis[1]-hr_axis[0]));// Column
			hr_bin = bin_x + HR_dim[0]*bin_y;

			if(is_selected){
				hr_map[hr_bin] += Mass_p*N_mi[star];//Add the contribution to the histogram
			};

		}; // End of the star-loop

	}; // End of the particle

	fclose( fichero_stat_bin_x );
	fclose( fichero_isocr_bin_x );
	fclose( fichero_stat_bin_y );
	fclose( fichero_isocr_bin_y );

}//End of HR_Salpeter
//-----------------------------------------------------------------------------------------------------------------//




//-----------------------------------------------------------------------------------------------------------------//
/* HR_Salpeter_4Points */
void HR_Salpeter_4Points(float *Z, float *logAge, float *ParticleMass, float *dist, float *A_V, float *hr_map, float *nobstars, float *Mfcis, int Nparticles, int *HR_dim, float *hr_axis, int *bands_id ){
/* Since extinction is provided, lon and lat are not needed*/
// Hertzsprung-Russell Diagram is a 2D array (but vectorized) of size HR_dim[1]*HR_dim[0]
// Mfcis means Mass fraction converted to stars. It has Nparticles elements
// A_V: in mag, must be given for the Johnsons V band
// UNITS: Z is a fraction, Ages in yrs, distances in kpc, extinction in mag
//	total_correction is dimensionless
// Output: HR (HR_dim[0]*HR_dim[1] elements) and Mfcis (Nparticles- values in an array)
	int n, particle, star, hr_bin, bin_x, bin_y; // Counters
	float Z_set[34]; // Set of metallicities (the grid is not regular in Z)
	int is_selected; // 1 if the magnitude is in [mag_min, mag_max)
	/* factor = A_lambda/A_V*/
	float factor_x, factor_y;

	/* Values for each particle */
	int i_Z, i_logAge, i_isocrone; //Index of the metallicity, logAge, isocrone
	float Z_p, logAge_p, Mass_p, AV_p; // Values for one selected particle
	float distance_correction_x, distance_correction_y; // 10+5*log10(d_kpc)

	/* Values for each star */
	float magnitude_x, magnitude_y; //Apparent magnitude

	/* Values for each isocrone */
	int starting_point_of_the_isocrone, Number_of_logmasses_in_the_isocrone;

	/* IMF */
	const float dlogm = (Logmi_MAX-Logmi_MIN)/(1.0*Max_Number_of_logmasses_per_isocrone); // bin size
	float mi[Max_Number_of_logmasses_per_isocrone], N_mi[Max_Number_of_logmasses_per_isocrone]; //Masses and global IMF

	/* Weights and temporal outputs */
	float wLR, wRL, wDU, wUD; // wLR+wRL = wDU+wUD = 1.

	// Files:
	FILE *fichero_ASCII; // ASCII file with the information about the isocrones
	FILE *fichero_stat_bin_x, *fichero_stat_bin_y; //Binary file with the statistics of all the isocrones
	FILE *fichero_isocr_bin_x, *fichero_isocr_bin_y; //Binary file with all the isocrones


	/* Read the metallicity values of the grid */
	fichero_ASCII = fopen( ISONO,"r"); //Open All_the_isocrones3.bin
	for (n=0; n<Number_of_Z;n++){
		fscanf( fichero_ASCII, "%f\n", &Z_set[n]); // Read the decimal part of the metallicity 
		Z_set[n] = Z_set[n]*1E-6; // Convert to true Z [met]

	} // Metallicity values read!
	fclose( fichero_ASCII ); //Close the ASCII file

	// Open the correspondent file:
	switch(bands_id[0]){
		case -3:
			fichero_isocr_bin_x = fopen( BIN_mbol,"rb"); //Open the list of bolometric magnitudes
			fichero_stat_bin_x = fopen( EXPL_mbol,"rb"); // Open the statistics of the bolometric magnitudes
			factor_x = 0;
			break;
		case -2:
			fichero_isocr_bin_x = fopen( BIN_logG,"rb"); //Open the list of logg
			fichero_stat_bin_x = fopen( EXPL_logG,"rb"); // Open the statistics of the logg
			factor_x = 0.;
			break;
		case -1:
			fichero_isocr_bin_x = fopen( BIN_logTe,"rb"); //Open the list of logTe
			fichero_stat_bin_x = fopen( EXPL_logTe,"rb"); // Open the statistics of logTe
			factor_x = 0.;
			break;
		case 0:
			fichero_isocr_bin_x = fopen( BIN_U,"rb"); //Open the list of M_U magnitudes
			fichero_stat_bin_x = fopen( EXPL_U,"rb"); // Open the statistics of the M_U magnitudes
			factor_x = 4.968/3.24;
			break;
		case 1:
			fichero_isocr_bin_x = fopen( BIN_B,"rb"); //Open the list of M_B magnitudes
			fichero_stat_bin_x = fopen( EXPL_B,"rb"); // Open the statistics of the M_B magnitudes
			factor_x = 4.325/3.24;
			break;
		case 2:
			fichero_isocr_bin_x = fopen( BIN_V,"rb"); //Open the list of M_V magnitudes
			fichero_stat_bin_x = fopen( EXPL_V,"rb"); // Open the statistics of the M_V magnitudes
			factor_x = 1.;
			break;
		case 3:
			fichero_isocr_bin_x = fopen( BIN_R,"rb"); //Open the list of M_R magnitudes
			fichero_stat_bin_x = fopen( EXPL_R,"rb"); // Open the statistics of the M_R magnitudes
			factor_x = 2.634/3.24;
			break;
		case 4:
			fichero_isocr_bin_x = fopen( BIN_I,"rb"); //Open the list of M_I magnitudes
			fichero_stat_bin_x = fopen( EXPL_I,"rb"); // Open the statistics of the M_I magnitudes
			factor_x = 1.962/3.24;
			break;
		case 5:
			fichero_isocr_bin_x = fopen( BIN_J,"rb"); //Open the list of M_J magnitudes
			fichero_stat_bin_x = fopen( EXPL_J,"rb"); // Open the statistics of the M_J magnitudes
			factor_x = 0.9536616/3.24;
			break;
		case 6:
			fichero_isocr_bin_x = fopen( BIN_H,"rb"); //Open the list of M_H magnitudes
			fichero_stat_bin_x = fopen( EXPL_H,"rb"); // Open the statistics of the M_H magnitudes
			factor_x = 0.5873472/3.24;
			break;
		case 7:
			fichero_isocr_bin_x = fopen( BIN_K,"rb"); //Open the list of M_K magnitudes
			fichero_stat_bin_x = fopen( EXPL_K,"rb"); // Open the statistics of the M_K magnitudes
			factor_x = 0.367/3.24;
			break;
		default:
			fprintf(stderr, "Unknown first filter. Johnson V band will be used instead\n");
			fichero_isocr_bin_x = fopen( BIN_V,"rb"); //Open the list of M_V magnitudes
			fichero_stat_bin_x = fopen( EXPL_V,"rb"); // Open the statistics of the M_V magnitudes
			factor_x = 1.;
	};

	// Open the correspondent file:
	switch(bands_id[1]){
		case -3:
			fichero_isocr_bin_y = fopen( BIN_mbol,"rb"); //Open the list of bolometric magnitudes
			fichero_stat_bin_y = fopen( EXPL_mbol,"rb"); // Open the statistics of the bolometric magnitudes
			factor_y = 0.;
			break;
		case -2:
			fichero_isocr_bin_y = fopen( BIN_logG,"rb"); //Open the list of logg
			fichero_stat_bin_y = fopen( EXPL_logG,"rb"); // Open the statistics of the logg
			factor_y = 0.;
			break;
		case -1:
			fichero_isocr_bin_y = fopen( BIN_logTe,"rb"); //Open the list of logTe
			fichero_stat_bin_y = fopen( EXPL_logTe,"rb"); // Open the statistics of logTe
			factor_y = 0.;
			break;
		case 0:
			fichero_isocr_bin_y = fopen( BIN_U,"rb"); //Open the list of M_U magnitudes
			fichero_stat_bin_y = fopen( EXPL_U,"rb"); // Open the statistics of the M_U magnitudes
			factor_y = 4.968/3.24;
			break;
		case 1:
			fichero_isocr_bin_y = fopen( BIN_B,"rb"); //Open the list of M_B magnitudes
			fichero_stat_bin_y = fopen( EXPL_B,"rb"); // Open the statistics of the M_B magnitudes
			factor_y = 4.325/3.24;
			break;
		case 2:
			fichero_isocr_bin_y = fopen( BIN_V,"rb"); //Open the list of M_V magnitudes
			fichero_stat_bin_y = fopen( EXPL_V,"rb"); // Open the statistics of the M_V magnitudes
			factor_y = 1.;
			break;
		case 3:
			fichero_isocr_bin_y = fopen( BIN_R,"rb"); //Open the list of M_R magnitudes
			fichero_stat_bin_y = fopen( EXPL_R,"rb"); // Open the statistics of the M_R magnitudes
			factor_y = 2.634/3.24;
			break;
		case 4:
			fichero_isocr_bin_y = fopen( BIN_I,"rb"); //Open the list of M_I magnitudes
			fichero_stat_bin_y = fopen( EXPL_I,"rb"); // Open the statistics of the M_I magnitudes
			factor_y = 1.962/3.24;
			break;
		case 5:
			fichero_isocr_bin_y = fopen( BIN_J,"rb"); //Open the list of M_J magnitudes
			fichero_stat_bin_y = fopen( EXPL_J,"rb"); // Open the statistics of the M_J magnitudes
			factor_y = 0.9536616/3.24;
			break;
		case 6:
			fichero_isocr_bin_y = fopen( BIN_H,"rb"); //Open the list of M_H magnitudes
			fichero_stat_bin_y = fopen( EXPL_H,"rb"); // Open the statistics of the M_H magnitudes
			factor_y = 0.5873472/3.24;
			break;
		case 7:
			fichero_isocr_bin_y = fopen( BIN_K,"rb"); //Open the list of M_K magnitudes
			fichero_stat_bin_y = fopen( EXPL_K,"rb"); // Open the statistics of the M_K magnitudes
			factor_y = 0.367/3.24;
			break;
		default:
			fprintf(stderr, "Unknown second filter. Johnson V band will be used instead\n");
			fichero_isocr_bin_y = fopen( BIN_V,"rb"); //Open the list of M_V magnitudes
			fichero_stat_bin_y = fopen( EXPL_V,"rb"); // Open the statistics of the M_V magnitudes
			factor_y = 1.;
	};

	//Since logmi_mi and Logmi_MAX are equal for all of the isocrones, the initial mass array would be the same:
	/* Probability given the Salpeter IMF */
	for(star=0; star<Max_Number_of_logmasses_per_isocrone;star++){
		mi[star] = pow(10., Logmi_MIN + dlogm*star);
	};

	// WARNING! This line is different from the Salpeter's version!

	//Initiliaze the HR:
	for(hr_bin=0;hr_bin<HR_dim[0]*HR_dim[1];hr_bin++){
		hr_map[hr_bin]=0.0;
	};

// Loop over the particles
	for(particle=0;particle<Nparticles;particle++){

		// Store the parameters of the considered particle:
		Z_p = Z[particle];
		logAge_p = logAge[particle];
		Mass_p = ParticleMass[particle];
		AV_p = A_V[particle];
		distance_correction_x = (10.+5.*log10(dist[particle]))*(bands_id[0]>=0);
		distance_correction_y = (10.+5.*log10(dist[particle]))*(bands_id[1]>=0);
		nobstars[particle] = 0.;
		// Initialize the Mfcis:
		Mfcis[particle] = 0.0; // Mass fraction converted to stars
		

		// Avoid out of range values:
		logAge_p = (logAge_p<logAge_min? logAge_min: logAge_p);
		logAge_p = (logAge_p>logAge_max? logAge_max: logAge_p);

		Z_p = (Z_p<Z_set[0]? Z_set[0]: Z_p);
		Z_p = (Z_p>Z_set[Number_of_Z-1]? Z_set[Number_of_Z-1]: Z_p);



		/* ++++++++++++++++++++++++++++++++ ESTIMATION OF THE WEIGHTS ARE DONE FIRSTLY ++++++++++++++++++++++++++++++++++++*/
		// By calculating the weights first, we avoid the use of huge hr_maps temporaly variables

			//......................................... LEFT DOWN POINT ..........................................
			// Get the index of that metallicity value (the Z sampling is not homogeneous):
			i_Z = -1; //Initial value
			do{
				i_Z++;
			}while( Z_set[i_Z]<=Z_p );
			i_Z--;
			//Assume that the logAge grid is regular (from logAge_min to logAge_max with step logAge_step)
			i_logAge = (int)((Number_of_logAge-1)*(logAge_p-logAge_min)/(logAge_max-logAge_min));

			//Weights:
			wLR = Z_p-Z_set[i_Z]; //For the Left-Down point of the grid


			//......................................... RIGHT DOWN POINT .........................................

			//i_logAge does not change but i_Z...
			i_Z = i_Z + 1*(i_Z!=(Number_of_Z-1));

			//Weights:
			wRL = Z_set[i_Z]-Z_p; //For the Right-Down point of the grid

			// ......................................... RIGHT UP POINT.........................................
			//i_Z as in the previous case, but i_logAge has increase (or not)...
			i_logAge = i_logAge + 1*(i_logAge!=(Number_of_logAge-1));
			//Weights:
			wUD = logAge_min - i_logAge*logAge_step - logAge_p; //For the Right-Up point of the grid

			// .........................................  LEFT UP POINT.........................................

			//i_logAge as in the previous case, but i_Z decreases (or not)...
			i_Z = i_Z - 1*(i_Z>0);
			//Weights:
			wDU = logAge_p - logAge_min - i_logAge*logAge_step; //For the Left-Up point of the grid

			// Be sure they are normalized...

			if(wLR==-wRL){
				wLR = 1.;
				wRL = 0.;
			}else{
				wLR = wRL/(wLR+wRL);
				wRL = 1.-wLR;
			};

			if(wUD==-wDU){
				wUD = 1.;
				wDU = 0.;
			}else{
				wUD = wDU/(wUD+wDU);
				wDU = 1.-wUD;
			};



		//-----------------------------------------------------------------------------------------------------
		// From now on, proceed as usual

		/* ++++++++++++++++++++++++++++++++ LEFT DOWN POINT ++++++++++++++++++++++++++++++++++++++++*/

		// Get the index of that metallicity value (the Z sampling is not homogeneous):
		i_Z = -1; //Initial value
		do{
			i_Z++;
		}while( Z_set[i_Z]<=Z_p );
		i_Z--;


		//Assume that the logAge grid is regular (from logAge_min to logAge_max with step logAge_step)
		i_logAge = (int)((Number_of_logAge-1)*(logAge_p-logAge_min)/(logAge_max-logAge_min));
		i_isocrone = i_logAge + (Number_of_logAge)*i_Z;


		// Read the following information about the selected isocrone: [starting_point of the isocrone, Nasses]
		fseek( fichero_stat_bin_x, 32*i_isocrone+16, SEEK_SET);// 32 bytes = 4 bytes*8 columns
		fseek( fichero_stat_bin_y, 32*i_isocrone+16, SEEK_SET);// 32 bytes = 4 bytes*8 columns

		fread(&starting_point_of_the_isocrone,4,1,fichero_stat_bin_x); //Starting position (inclusive)(common for both magnitudes)
		fread(&Number_of_logmasses_in_the_isocrone,4,1,fichero_stat_bin_x);// Rows (stars) that belong to this isocrone(common for both magnitudes)


		// Go to the desired isocrone
		fseek( fichero_isocr_bin_x, 4*starting_point_of_the_isocrone, SEEK_SET); //1=1*4 bytes = np.float32 bits*1 column
		fseek( fichero_isocr_bin_y, 4*starting_point_of_the_isocrone, SEEK_SET); //1=1*4 bytes = np.float32 bits*1 column

		// WARNING! This line is different from the Salpeter's version:
		// Call to the Salpeter IMF function
		SalpeterIMF( mi, Max_Number_of_logmasses_per_isocrone, mi[Number_of_logmasses_in_the_isocrone-1], N_mi); //N_mi means number of masses between mi and mi+dm (egde effects included)


		// Load V and V-Ic from the isocrone given by (Z, logAge), and finish P(m_i)
		for(star=0;star<Number_of_logmasses_in_the_isocrone;star++){
			// Get the absolute magnitudes
			fread(&magnitude_x,4,1, fichero_isocr_bin_x); // Absolute magnitude
			fread(&magnitude_y,4,1, fichero_isocr_bin_y); // Absolute magnitude

			// From absolute to apparent
			magnitude_x = magnitude_x + distance_correction_x + AV_p*factor_x;
			magnitude_y = magnitude_y + distance_correction_y + AV_p*factor_y;

			
			// Would this star be included in the histogram?
			is_selected = (hr_axis[0]<=magnitude_x)*(magnitude_x<hr_axis[1]);// 1= Yes, 0=No
			is_selected = is_selected*(hr_axis[2]<=magnitude_y)*(magnitude_y<hr_axis[3]);// 1= Yes, 0=No

			// Make N_mi[star] == 0 if the star is out of range...
			N_mi[star] = wDU*wLR*N_mi[star]*is_selected;//...and apply the weighting

			// Mass fraction converted into observed stars
			Mfcis[particle] += mi[star]*N_mi[star]; //Add the contribution

			// Number of OBSERVED stars for this particular particle
			nobstars[particle] += Mass_p*N_mi[star]; //Add the contribution

			//HR
			bin_y = (int)(HR_dim[1]*(magnitude_y-hr_axis[2])/(hr_axis[3]-hr_axis[2]));// Row
			bin_x = (int)(HR_dim[0]*(magnitude_x-hr_axis[0])/(hr_axis[1]-hr_axis[0]));// Column
			hr_bin = bin_x + HR_dim[0]*bin_y;

			if(is_selected){
				hr_map[hr_bin] += Mass_p*N_mi[star];//Add the contribution to the histogram
			};

		}; // End of the star-loop


		/* ++++++++++++++++++++++++++++++++ RIGHT DOWN POINT ++++++++++++++++++++++++++++++++++++++++*/

		//i_logAge does not change but i_Z...
		i_Z = i_Z + 1*(i_Z!=(Number_of_Z-1));
		
		i_isocrone = i_logAge + Number_of_logAge*i_Z;

		// Read the following information about the selected isocrone: [starting_point of the isocrone, Nasses]
		fseek( fichero_stat_bin_x, 32*i_isocrone+16, SEEK_SET);// 32 bytes = 4 bytes*8 columns
		fseek( fichero_stat_bin_y, 32*i_isocrone+16, SEEK_SET);// 32 bytes = 4 bytes*8 columns

		fread(&starting_point_of_the_isocrone,4,1,fichero_stat_bin_x); //Starting position (inclusive)(common for both magnitudes)
		fread(&Number_of_logmasses_in_the_isocrone,4,1,fichero_stat_bin_x);// Rows (stars) that belong to this isocrone(common for both magnitudes)


		// Go to the desired isocrone
		fseek( fichero_isocr_bin_x, 4*starting_point_of_the_isocrone, SEEK_SET); //1=1*4 bytes = np.float32 bits*1 column
		fseek( fichero_isocr_bin_y, 4*starting_point_of_the_isocrone, SEEK_SET); //1=1*4 bytes = np.float32 bits*1 column

		// WARNING! This line is different from the Salpeter's version:
		// Call to the Salpeter IMF function
		SalpeterIMF( mi, Max_Number_of_logmasses_per_isocrone, mi[Number_of_logmasses_in_the_isocrone-1], N_mi); //N_mi means number of masses between mi and mi+dm (egde effects included)

		// Load V and V-Ic from the isocrone given by (Z, logAge), and finish P(m_i)
		for(star=0;star<Number_of_logmasses_in_the_isocrone;star++){
			// Get the absolute magnitudes
			fread(&magnitude_x,4,1, fichero_isocr_bin_x); // Absolute magnitude
			fread(&magnitude_y,4,1, fichero_isocr_bin_y); // Absolute magnitude

			// From absolute to apparent
			magnitude_x = magnitude_x + distance_correction_x + AV_p*factor_x;
			magnitude_y = magnitude_y + distance_correction_y + AV_p*factor_y;

			
			// Would this star be included in the histogram?
			is_selected = (hr_axis[0]<=magnitude_x)*(magnitude_x<hr_axis[1]);// 1= Yes, 0=No
			is_selected = is_selected*(hr_axis[2]<=magnitude_y)*(magnitude_y<hr_axis[3]);// 1= Yes, 0=No

			// Make N_mi[star] == 0 if the star is out of range...
			N_mi[star] = wDU*wRL*N_mi[star]*is_selected;//...and apply the weighting

			// Mass fraction converted into observed stars
			Mfcis[particle] += mi[star]*N_mi[star]; //Add the contribution

			// Number of OBSERVED stars for this particular particle
			nobstars[particle] += Mass_p*N_mi[star]; //Add the contribution

			//HR
			bin_y = (int)(HR_dim[1]*(magnitude_y-hr_axis[2])/(hr_axis[3]-hr_axis[2]));// Row
			bin_x = (int)(HR_dim[0]*(magnitude_x-hr_axis[0])/(hr_axis[1]-hr_axis[0]));// Column
			hr_bin = bin_x + HR_dim[0]*bin_y;

			if(is_selected){
				hr_map[hr_bin] += Mass_p*N_mi[star];//Add the contribution to the histogram
			};

		}; // End of the star-loop


		/* ++++++++++++++++++++++++++++++++ RIGHT UP POINT ++++++++++++++++++++++++++++++++++++++++*/

		//i_Z as in the previous case, but i_logAge has increase (or not)...
		i_logAge = i_logAge + 1*(i_logAge!=(Number_of_logAge-1));
		i_isocrone = i_logAge + Number_of_logAge*i_Z;

		// Read the following information about the selected isocrone: [starting_point of the isocrone, Nasses]
		fseek( fichero_stat_bin_x, 32*i_isocrone+16, SEEK_SET);// 32 bytes = 4 bytes*8 columns
		fseek( fichero_stat_bin_y, 32*i_isocrone+16, SEEK_SET);// 32 bytes = 4 bytes*8 columns

		fread(&starting_point_of_the_isocrone,4,1,fichero_stat_bin_x); //Starting position (inclusive)(common for both magnitudes)
		fread(&Number_of_logmasses_in_the_isocrone,4,1,fichero_stat_bin_x);// Rows (stars) that belong to this isocrone(common for both magnitudes)


		// Go to the desired isocrone
		fseek( fichero_isocr_bin_x, 4*starting_point_of_the_isocrone, SEEK_SET); //1=1*4 bytes = np.float32 bits*1 column
		fseek( fichero_isocr_bin_y, 4*starting_point_of_the_isocrone, SEEK_SET); //1=1*4 bytes = np.float32 bits*1 column

		// WARNING! This line is different from the Salpeter's version:
		// Call to the Salpeter IMF function
		SalpeterIMF( mi, Max_Number_of_logmasses_per_isocrone, mi[Number_of_logmasses_in_the_isocrone-1], N_mi); //N_mi means number of masses between mi and mi+dm (egde effects included)

		// Load V and V-Ic from the isocrone given by (Z, logAge), and finish P(m_i)
		for(star=0;star<Number_of_logmasses_in_the_isocrone;star++){
			// Get the absolute magnitudes
			fread(&magnitude_x,4,1, fichero_isocr_bin_x); // Absolute magnitude
			fread(&magnitude_y,4,1, fichero_isocr_bin_y); // Absolute magnitude

			// From absolute to apparent
			magnitude_x = magnitude_x + distance_correction_x + AV_p*factor_x;
			magnitude_y = magnitude_y + distance_correction_y + AV_p*factor_y;

			
			// Would this star be included in the histogram?
			is_selected = (hr_axis[0]<=magnitude_x)*(magnitude_x<hr_axis[1]);// 1= Yes, 0=No
			is_selected = is_selected*(hr_axis[2]<=magnitude_y)*(magnitude_y<hr_axis[3]);// 1= Yes, 0=No

			// Make N_mi[star] == 0 if the star is out of range...
			N_mi[star] = wUD*wRL*N_mi[star]*is_selected;//...and apply the weighting

			// Mass fraction converted into observed stars
			Mfcis[particle] += mi[star]*N_mi[star]; //Add the contribution

			// Number of OBSERVED stars for this particular particle
			nobstars[particle] += Mass_p*N_mi[star]; //Add the contribution

			//HR
			bin_y = (int)(HR_dim[1]*(magnitude_y-hr_axis[2])/(hr_axis[3]-hr_axis[2]));// Row
			bin_x = (int)(HR_dim[0]*(magnitude_x-hr_axis[0])/(hr_axis[1]-hr_axis[0]));// Column
			hr_bin = bin_x + HR_dim[0]*bin_y;

			if(is_selected){
				hr_map[hr_bin] += Mass_p*N_mi[star];//Add the contribution to the histogram
			};

		}; // End of the star-loop

		/* ++++++++++++++++++++++++++++++++ LEFT UP POINT ++++++++++++++++++++++++++++++++++++++++*/

		//i_logAge as in the previous case, but i_Z decreases (or not)...
		i_Z = i_Z - 1*(i_Z>0);
		i_isocrone = i_logAge + Number_of_logAge*i_Z;

		// Read the following information about the selected isocrone: [starting_point of the isocrone, Nasses]
		fseek( fichero_stat_bin_x, 32*i_isocrone+16, SEEK_SET);// 32 bytes = 4 bytes*8 columns
		fseek( fichero_stat_bin_y, 32*i_isocrone+16, SEEK_SET);// 32 bytes = 4 bytes*8 columns

		fread(&starting_point_of_the_isocrone,4,1,fichero_stat_bin_x); //Starting position (inclusive)(common for both magnitudes)
		fread(&Number_of_logmasses_in_the_isocrone,4,1,fichero_stat_bin_x);// Rows (stars) that belong to this isocrone(common for both magnitudes)


		// Go to the desired isocrone
		fseek( fichero_isocr_bin_x, 4*starting_point_of_the_isocrone, SEEK_SET); //1=1*4 bytes = np.float32 bits*1 column
		fseek( fichero_isocr_bin_y, 4*starting_point_of_the_isocrone, SEEK_SET); //1=1*4 bytes = np.float32 bits*1 column

		// WARNING! This line is different from the Salpeter's version:
		// Call to the Salpeter IMF function
		SalpeterIMF( mi, Max_Number_of_logmasses_per_isocrone, mi[Number_of_logmasses_in_the_isocrone-1], N_mi); //N_mi means number of masses between mi and mi+dm (egde effects included)

		// Load V and V-Ic from the isocrone given by (Z, logAge), and finish P(m_i)
		for(star=0;star<Number_of_logmasses_in_the_isocrone;star++){
			// Get the absolute magnitudes
			fread(&magnitude_x,4,1, fichero_isocr_bin_x); // Absolute magnitude
			fread(&magnitude_y,4,1, fichero_isocr_bin_y); // Absolute magnitude

			// From absolute to apparent
			magnitude_x = magnitude_x + distance_correction_x + AV_p*factor_x;
			magnitude_y = magnitude_y + distance_correction_y + AV_p*factor_y;

			
			// Would this star be included in the histogram?
			is_selected = (hr_axis[0]<=magnitude_x)*(magnitude_x<hr_axis[1]);// 1= Yes, 0=No
			is_selected = is_selected*(hr_axis[2]<=magnitude_y)*(magnitude_y<hr_axis[3]);// 1= Yes, 0=No

			// Make N_mi[star] == 0 if the star is out of range...
			N_mi[star] = wUD*wLR*N_mi[star]*is_selected;//...and apply the weighting

			// Mass fraction converted into observed stars
			Mfcis[particle] += mi[star]*N_mi[star]; //Add the contribution

			// Number of OBSERVED stars for this particular particle
			nobstars[particle] += Mass_p*N_mi[star]; //Add the contribution

			//HR
			bin_y = (int)(HR_dim[1]*(magnitude_y-hr_axis[2])/(hr_axis[3]-hr_axis[2]));// Row
			bin_x = (int)(HR_dim[0]*(magnitude_x-hr_axis[0])/(hr_axis[1]-hr_axis[0]));// Column
			hr_bin = bin_x + HR_dim[0]*bin_y;

			if(is_selected){
				hr_map[hr_bin] += Mass_p*N_mi[star];//Add the contribution to the histogram
			};

		}; // End of the star-loop

	}; // End of the particle

	fclose( fichero_stat_bin_x );
	fclose( fichero_isocr_bin_x );
	fclose( fichero_stat_bin_y );
	fclose( fichero_isocr_bin_y );

}//End of HR_Salpeter_4Points
//-----------------------------------------------------------------------------------------------------------------//





//-----------------------------------------------------------------------------------------------------------------//
/* HR_Kroupa */
void HR_Kroupa(float *Z, float *logAge, float *ParticleMass, float *dist, float *A_V, float *hr_map, float *nobstars, float *Mfcis, int Nparticles, int *HR_dim, float *hr_axis, int *bands_id ){
/* Since extinction is provided, lon and lat are not needed*/
// Hertzsprung-Russell Diagram is a 2D array (but vectorized) of size HR_dim[1]*HR_dim[0]
// Mfcis means Mass fraction converted to stars. It has Nparticles elements
// A_V: in mag, must be given for the Johnsons V band
// UNITS: Z is a fraction, Ages in yrs, distances in kpc, extinction in mag
//	total_correction is dimensionless
// Output: HR (HR_dim[0]*HR_dim[1] elements) and Mfcis (Nparticles- values in an array)
	int n, particle, star, hr_bin, bin_x, bin_y; // Counters
	float Z_set[34]; // Set of metallicities (the grid is not regular in Z)
	int is_selected; // 1 if the magnitude is in [mag_min, mag_max)
	/* factor = A_lambda/A_V*/
	float factor_x, factor_y;

	/* Values for each particle */
	int i_Z, i_logAge, i_isocrone; //Index of the metallicity, logAge, isocrone
	float Z_p, logAge_p, Mass_p, AV_p; // Values for one selected particle
	float distance_correction_x, distance_correction_y; // 10+5*log10(d_kpc)

	/* Values for each star */
	float magnitude_x, magnitude_y; //Apparent magnitude

	/* Values for each isocrone */
	int starting_point_of_the_isocrone, Number_of_logmasses_in_the_isocrone;

	/* IMF */
	const float dlogm = (Logmi_MAX-Logmi_MIN)/(1.0*Max_Number_of_logmasses_per_isocrone); // bin size
	float mi[Max_Number_of_logmasses_per_isocrone], N_mi[Max_Number_of_logmasses_per_isocrone]; //Masses and global IMF

	// Files:
	FILE *fichero_ASCII; // ASCII file with the information about the isocrones
	FILE *fichero_stat_bin_x, *fichero_stat_bin_y; //Binary file with the statistics of all the isocrones
	FILE *fichero_isocr_bin_x, *fichero_isocr_bin_y; //Binary file with all the isocrones


	/* Read the metallicity values of the grid */
	fichero_ASCII = fopen( ISONO,"r"); //Open All_the_isocrones3.bin
	for (n=0; n<Number_of_Z;n++){
		fscanf( fichero_ASCII, "%f\n", &Z_set[n]); // Read the decimal part of the metallicity 
		Z_set[n] = Z_set[n]*1E-6; // Convert to true Z [met]

	} // Metallicity values read!
	fclose( fichero_ASCII ); //Close the ASCII file

	// Open the correspondent file:
	switch(bands_id[0]){
		case -3:
			fichero_isocr_bin_x = fopen( BIN_mbol,"rb"); //Open the list of bolometric magnitudes
			fichero_stat_bin_x = fopen( EXPL_mbol,"rb"); // Open the statistics of the bolometric magnitudes
			factor_x = 0;
			break;
		case -2:
			fichero_isocr_bin_x = fopen( BIN_logG,"rb"); //Open the list of logg
			fichero_stat_bin_x = fopen( EXPL_logG,"rb"); // Open the statistics of the logg
			factor_x = 0.;
			break;
		case -1:
			fichero_isocr_bin_x = fopen( BIN_logTe,"rb"); //Open the list of logTe
			fichero_stat_bin_x = fopen( EXPL_logTe,"rb"); // Open the statistics of logTe
			factor_x = 0.;
			break;
		case 0:
			fichero_isocr_bin_x = fopen( BIN_U,"rb"); //Open the list of M_U magnitudes
			fichero_stat_bin_x = fopen( EXPL_U,"rb"); // Open the statistics of the M_U magnitudes
			factor_x = 4.968/3.24;
			break;
		case 1:
			fichero_isocr_bin_x = fopen( BIN_B,"rb"); //Open the list of M_B magnitudes
			fichero_stat_bin_x = fopen( EXPL_B,"rb"); // Open the statistics of the M_B magnitudes
			factor_x = 4.325/3.24;
			break;
		case 2:
			fichero_isocr_bin_x = fopen( BIN_V,"rb"); //Open the list of M_V magnitudes
			fichero_stat_bin_x = fopen( EXPL_V,"rb"); // Open the statistics of the M_V magnitudes
			factor_x = 1.;
			break;
		case 3:
			fichero_isocr_bin_x = fopen( BIN_R,"rb"); //Open the list of M_R magnitudes
			fichero_stat_bin_x = fopen( EXPL_R,"rb"); // Open the statistics of the M_R magnitudes
			factor_x = 2.634/3.24;
			break;
		case 4:
			fichero_isocr_bin_x = fopen( BIN_I,"rb"); //Open the list of M_I magnitudes
			fichero_stat_bin_x = fopen( EXPL_I,"rb"); // Open the statistics of the M_I magnitudes
			factor_x = 1.962/3.24;
			break;
		case 5:
			fichero_isocr_bin_x = fopen( BIN_J,"rb"); //Open the list of M_J magnitudes
			fichero_stat_bin_x = fopen( EXPL_J,"rb"); // Open the statistics of the M_J magnitudes
			factor_x = 0.9536616/3.24;
			break;
		case 6:
			fichero_isocr_bin_x = fopen( BIN_H,"rb"); //Open the list of M_H magnitudes
			fichero_stat_bin_x = fopen( EXPL_H,"rb"); // Open the statistics of the M_H magnitudes
			factor_x = 0.5873472/3.24;
			break;
		case 7:
			fichero_isocr_bin_x = fopen( BIN_K,"rb"); //Open the list of M_K magnitudes
			fichero_stat_bin_x = fopen( EXPL_K,"rb"); // Open the statistics of the M_K magnitudes
			factor_x = 0.367/3.24;
			break;
		default:
			fprintf(stderr, "Unknown first filter. Johnson V band will be used instead\n");
			fichero_isocr_bin_x = fopen( BIN_V,"rb"); //Open the list of M_V magnitudes
			fichero_stat_bin_x = fopen( EXPL_V,"rb"); // Open the statistics of the M_V magnitudes
			factor_x = 1.;
	};

	// Open the correspondent file:
	switch(bands_id[1]){
		case -3:
			fichero_isocr_bin_y = fopen( BIN_mbol,"rb"); //Open the list of bolometric magnitudes
			fichero_stat_bin_y = fopen( EXPL_mbol,"rb"); // Open the statistics of the bolometric magnitudes
			factor_y = 0.;
			break;
		case -2:
			fichero_isocr_bin_y = fopen( BIN_logG,"rb"); //Open the list of logg
			fichero_stat_bin_y = fopen( EXPL_logG,"rb"); // Open the statistics of the logg
			factor_y = 0.;
			break;
		case -1:
			fichero_isocr_bin_y = fopen( BIN_logTe,"rb"); //Open the list of logTe
			fichero_stat_bin_y = fopen( EXPL_logTe,"rb"); // Open the statistics of logTe
			factor_y = 0.;
			break;
		case 0:
			fichero_isocr_bin_y = fopen( BIN_U,"rb"); //Open the list of M_U magnitudes
			fichero_stat_bin_y = fopen( EXPL_U,"rb"); // Open the statistics of the M_U magnitudes
			factor_y = 4.968/3.24;
			break;
		case 1:
			fichero_isocr_bin_y = fopen( BIN_B,"rb"); //Open the list of M_B magnitudes
			fichero_stat_bin_y = fopen( EXPL_B,"rb"); // Open the statistics of the M_B magnitudes
			factor_y = 4.325/3.24;
			break;
		case 2:
			fichero_isocr_bin_y = fopen( BIN_V,"rb"); //Open the list of M_V magnitudes
			fichero_stat_bin_y = fopen( EXPL_V,"rb"); // Open the statistics of the M_V magnitudes
			factor_y = 1.;
			break;
		case 3:
			fichero_isocr_bin_y = fopen( BIN_R,"rb"); //Open the list of M_R magnitudes
			fichero_stat_bin_y = fopen( EXPL_R,"rb"); // Open the statistics of the M_R magnitudes
			factor_y = 2.634/3.24;
			break;
		case 4:
			fichero_isocr_bin_y = fopen( BIN_I,"rb"); //Open the list of M_I magnitudes
			fichero_stat_bin_y = fopen( EXPL_I,"rb"); // Open the statistics of the M_I magnitudes
			factor_y = 1.962/3.24;
			break;
		case 5:
			fichero_isocr_bin_y = fopen( BIN_J,"rb"); //Open the list of M_J magnitudes
			fichero_stat_bin_y = fopen( EXPL_J,"rb"); // Open the statistics of the M_J magnitudes
			factor_y = 0.9536616/3.24;
			break;
		case 6:
			fichero_isocr_bin_y = fopen( BIN_H,"rb"); //Open the list of M_H magnitudes
			fichero_stat_bin_y = fopen( EXPL_H,"rb"); // Open the statistics of the M_H magnitudes
			factor_y = 0.5873472/3.24;
			break;
		case 7:
			fichero_isocr_bin_y = fopen( BIN_K,"rb"); //Open the list of M_K magnitudes
			fichero_stat_bin_y = fopen( EXPL_K,"rb"); // Open the statistics of the M_K magnitudes
			factor_y = 0.367/3.24;
			break;
		default:
			fprintf(stderr, "Unknown second filter. Johnson V band will be used instead\n");
			fichero_isocr_bin_y = fopen( BIN_V,"rb"); //Open the list of M_V magnitudes
			fichero_stat_bin_y = fopen( EXPL_V,"rb"); // Open the statistics of the M_V magnitudes
			factor_y = 1.;
	};

	//Since logmi_mi and Logmi_MAX are equal for all of the isocrones, the initial mass array would be the same:
	/* Probability given the Kroupa IMF */
	for(star=0; star<Max_Number_of_logmasses_per_isocrone;star++){
		mi[star] = pow(10., Logmi_MIN + dlogm*star);
	};

	// WARNING! This line is different from the Salpeter's version!

	//Initiliaze the HR:
	for(hr_bin=0;hr_bin<HR_dim[0]*HR_dim[1];hr_bin++){
		hr_map[hr_bin]=0.0;
	};

// Loop over the particles
	for(particle=0;particle<Nparticles;particle++){

		// Store the parameters of the considered particle:
		Z_p = Z[particle];
		logAge_p = logAge[particle];
		Mass_p = ParticleMass[particle];
		AV_p = A_V[particle];
		distance_correction_x = (10.+5.*log10(dist[particle]))*(bands_id[0]);
		distance_correction_y = (10.+5.*log10(dist[particle]))*(bands_id[1]);
		nobstars[particle] = 0.;
		

		// Avoid out of range values:
		logAge_p = (logAge_p<logAge_min? logAge_min: logAge_p);
		logAge_p = (logAge_p>logAge_max? logAge_max: logAge_p);

		Z_p = (Z_p<Z_set[0]? Z_set[0]: Z_p);
		Z_p = (Z_p>Z_set[Number_of_Z-1]? Z_set[Number_of_Z-1]: Z_p);


		// Get the index of that metallicity value (the Z sampling is not homogeneous):
		i_Z = -1; //Initial value
		do{
			i_Z++;
		}while( Z_set[i_Z]<=Z_p );
		i_Z--;


		//Assume that the logAge grid is regular (from logAge_min to logAge_max with step logAge_step)
		i_logAge = (int)((Number_of_logAge-1)*(logAge_p-logAge_min)/(logAge_max-logAge_min));
		i_isocrone = i_logAge + (Number_of_logAge)*i_Z;


		// Read the following information about the selected isocrone: [starting_point of the isocrone, Nasses]
		fseek( fichero_stat_bin_x, 32*i_isocrone+16, SEEK_SET);// 32 bytes = 4 bytes*8 columns
		fseek( fichero_stat_bin_y, 32*i_isocrone+16, SEEK_SET);// 32 bytes = 4 bytes*8 columns

		fread(&starting_point_of_the_isocrone,4,1,fichero_stat_bin_x); //Starting position (inclusive)(common for both magnitudes)
		fread(&Number_of_logmasses_in_the_isocrone,4,1,fichero_stat_bin_x);// Rows (stars) that belong to this isocrone(common for both magnitudes)


		// Initialize the Mfcis:
		Mfcis[particle] = 0.0; // Mass fraction converted to stars

		// Go to the desired isocrone
		fseek( fichero_isocr_bin_x, 4*starting_point_of_the_isocrone, SEEK_SET); //1=1*4 bytes = np.float32 bits*1 column
		fseek( fichero_isocr_bin_y, 4*starting_point_of_the_isocrone, SEEK_SET); //1=1*4 bytes = np.float32 bits*1 column

		// WARNING! This line is different from the Salpeter's version:
		// Call to the Kroupa IMF function
		KroupaIMF( mi, Max_Number_of_logmasses_per_isocrone, mi[Number_of_logmasses_in_the_isocrone-1], N_mi); //N_mi means number of masses between mi and mi+dm (egde effects included)


		// Load V and V-Ic from the isocrone given by (Z, logAge), and finish P(m_i)
		for(star=0;star<Number_of_logmasses_in_the_isocrone;star++){
			// Get the absolute magnitudes
			fread(&magnitude_x,4,1, fichero_isocr_bin_x); // Absolute magnitude
			fread(&magnitude_y,4,1, fichero_isocr_bin_y); // Absolute magnitude

			// From absolute to apparent
			magnitude_x = magnitude_x + distance_correction_x + AV_p*factor_x;
			magnitude_y = magnitude_y + distance_correction_y + AV_p*factor_y;
	
			// Would this star be included in the histogram?
			is_selected = (hr_axis[0]<=magnitude_x)*(magnitude_x<hr_axis[1]);// 1= Yes, 0=No
			is_selected = is_selected*(hr_axis[2]<=magnitude_y)*(magnitude_y<hr_axis[3]);// 1= Yes, 0=No

			// Mass fraction converted into observed stars
			Mfcis[particle] += mi[star]*N_mi[star]*is_selected; //Add the contribution

			// Number of OBSERVED stars for this particular particle
			nobstars[particle] += Mass_p*N_mi[star]*is_selected; //Add the contribution

			//HR
			bin_y = (int)(HR_dim[1]*(magnitude_y-hr_axis[2])/(hr_axis[3]-hr_axis[2]));// Row
			bin_x = (int)(HR_dim[0]*(magnitude_x-hr_axis[0])/(hr_axis[1]-hr_axis[0]));// Column
			hr_bin = bin_x + HR_dim[0]*bin_y;

			if(is_selected){
				hr_map[hr_bin] += Mass_p*N_mi[star];//Add the contribution to the histogram
			};

		}; // End of the star-loop

	}; // End of the particle

	fclose( fichero_stat_bin_x );
	fclose( fichero_isocr_bin_x );
	fclose( fichero_stat_bin_y );
	fclose( fichero_isocr_bin_y );

}//End of HR_Kroupa
//-----------------------------------------------------------------------------------------------------------------//






//-----------------------------------------------------------------------------------------------------------------//
/* HR_Kroupa_4Points */
void HR_Kroupa_4Points(float *Z, float *logAge, float *ParticleMass, float *dist, float *A_V, float *hr_map, float *nobstars, float *Mfcis, int Nparticles, int *HR_dim, float *hr_axis, int *bands_id ){
/* Since extinction is provided, lon and lat are not needed*/
// Hertzsprung-Russell Diagram is a 2D array (but vectorized) of size HR_dim[1]*HR_dim[0]
// Mfcis means Mass fraction converted to stars. It has Nparticles elements
// A_V: in mag, must be given for the Johnsons V band
// UNITS: Z is a fraction, Ages in yrs, distances in kpc, extinction in mag
//	total_correction is dimensionless
// Output: HR (HR_dim[0]*HR_dim[1] elements) and Mfcis (Nparticles- values in an array)
	int n, particle, star, hr_bin, bin_x, bin_y; // Counters
	float Z_set[34]; // Set of metallicities (the grid is not regular in Z)
	int is_selected; // 1 if the magnitude is in [mag_min, mag_max)
	/* factor = A_lambda/A_V*/
	float factor_x, factor_y;

	/* Values for each particle */
	int i_Z, i_logAge, i_isocrone; //Index of the metallicity, logAge, isocrone
	float Z_p, logAge_p, Mass_p, AV_p; // Values for one selected particle
	float distance_correction_x, distance_correction_y; // 10+5*log10(d_kpc)

	/* Values for each star */
	float magnitude_x, magnitude_y; //Apparent magnitude

	/* Values for each isocrone */
	int starting_point_of_the_isocrone, Number_of_logmasses_in_the_isocrone;

	/* IMF */
	const float dlogm = (Logmi_MAX-Logmi_MIN)/(1.0*Max_Number_of_logmasses_per_isocrone); // bin size
	float mi[Max_Number_of_logmasses_per_isocrone], N_mi[Max_Number_of_logmasses_per_isocrone]; //Masses and global IMF

	/* Weights and temporal outputs */
	float wLR, wRL, wDU, wUD; // wLR+wRL = wDU+wUD = 1.

	// Files:
	FILE *fichero_ASCII; // ASCII file with the information about the isocrones
	FILE *fichero_stat_bin_x, *fichero_stat_bin_y; //Binary file with the statistics of all the isocrones
	FILE *fichero_isocr_bin_x, *fichero_isocr_bin_y; //Binary file with all the isocrones


	/* Read the metallicity values of the grid */
	fichero_ASCII = fopen( ISONO,"r"); //Open All_the_isocrones3.bin
	for (n=0; n<Number_of_Z;n++){
		fscanf( fichero_ASCII, "%f\n", &Z_set[n]); // Read the decimal part of the metallicity 
		Z_set[n] = Z_set[n]*1E-6; // Convert to true Z [met]

	} // Metallicity values read!
	fclose( fichero_ASCII ); //Close the ASCII file

	// Open the correspondent file:
	switch(bands_id[0]){
		case -3:
			fichero_isocr_bin_x = fopen( BIN_mbol,"rb"); //Open the list of bolometric magnitudes
			fichero_stat_bin_x = fopen( EXPL_mbol,"rb"); // Open the statistics of the bolometric magnitudes
			factor_x = 0;
			break;
		case -2:
			fichero_isocr_bin_x = fopen( BIN_logG,"rb"); //Open the list of logg
			fichero_stat_bin_x = fopen( EXPL_logG,"rb"); // Open the statistics of the logg
			factor_x = 0.;
			break;
		case -1:
			fichero_isocr_bin_x = fopen( BIN_logTe,"rb"); //Open the list of logTe
			fichero_stat_bin_x = fopen( EXPL_logTe,"rb"); // Open the statistics of logTe
			factor_x = 0.;
			break;
		case 0:
			fichero_isocr_bin_x = fopen( BIN_U,"rb"); //Open the list of M_U magnitudes
			fichero_stat_bin_x = fopen( EXPL_U,"rb"); // Open the statistics of the M_U magnitudes
			factor_x = 4.968/3.24;
			break;
		case 1:
			fichero_isocr_bin_x = fopen( BIN_B,"rb"); //Open the list of M_B magnitudes
			fichero_stat_bin_x = fopen( EXPL_B,"rb"); // Open the statistics of the M_B magnitudes
			factor_x = 4.325/3.24;
			break;
		case 2:
			fichero_isocr_bin_x = fopen( BIN_V,"rb"); //Open the list of M_V magnitudes
			fichero_stat_bin_x = fopen( EXPL_V,"rb"); // Open the statistics of the M_V magnitudes
			factor_x = 1.;
			break;
		case 3:
			fichero_isocr_bin_x = fopen( BIN_R,"rb"); //Open the list of M_R magnitudes
			fichero_stat_bin_x = fopen( EXPL_R,"rb"); // Open the statistics of the M_R magnitudes
			factor_x = 2.634/3.24;
			break;
		case 4:
			fichero_isocr_bin_x = fopen( BIN_I,"rb"); //Open the list of M_I magnitudes
			fichero_stat_bin_x = fopen( EXPL_I,"rb"); // Open the statistics of the M_I magnitudes
			factor_x = 1.962/3.24;
			break;
		case 5:
			fichero_isocr_bin_x = fopen( BIN_J,"rb"); //Open the list of M_J magnitudes
			fichero_stat_bin_x = fopen( EXPL_J,"rb"); // Open the statistics of the M_J magnitudes
			factor_x = 0.9536616/3.24;
			break;
		case 6:
			fichero_isocr_bin_x = fopen( BIN_H,"rb"); //Open the list of M_H magnitudes
			fichero_stat_bin_x = fopen( EXPL_H,"rb"); // Open the statistics of the M_H magnitudes
			factor_x = 0.5873472/3.24;
			break;
		case 7:
			fichero_isocr_bin_x = fopen( BIN_K,"rb"); //Open the list of M_K magnitudes
			fichero_stat_bin_x = fopen( EXPL_K,"rb"); // Open the statistics of the M_K magnitudes
			factor_x = 0.367/3.24;
			break;
		default:
			fprintf(stderr, "Unknown first filter. Johnson V band will be used instead\n");
			fichero_isocr_bin_x = fopen( BIN_V,"rb"); //Open the list of M_V magnitudes
			fichero_stat_bin_x = fopen( EXPL_V,"rb"); // Open the statistics of the M_V magnitudes
			factor_x = 1.;
	};

	// Open the correspondent file:
	switch(bands_id[1]){
		case -3:
			fichero_isocr_bin_y = fopen( BIN_mbol,"rb"); //Open the list of bolometric magnitudes
			fichero_stat_bin_y = fopen( EXPL_mbol,"rb"); // Open the statistics of the bolometric magnitudes
			factor_y = 0.;
			break;
		case -2:
			fichero_isocr_bin_y = fopen( BIN_logG,"rb"); //Open the list of logg
			fichero_stat_bin_y = fopen( EXPL_logG,"rb"); // Open the statistics of the logg
			factor_y = 0.;
			break;
		case -1:
			fichero_isocr_bin_y = fopen( BIN_logTe,"rb"); //Open the list of logTe
			fichero_stat_bin_y = fopen( EXPL_logTe,"rb"); // Open the statistics of logTe
			factor_y = 0.;
			break;
		case 0:
			fichero_isocr_bin_y = fopen( BIN_U,"rb"); //Open the list of M_U magnitudes
			fichero_stat_bin_y = fopen( EXPL_U,"rb"); // Open the statistics of the M_U magnitudes
			factor_y = 4.968/3.24;
			break;
		case 1:
			fichero_isocr_bin_y = fopen( BIN_B,"rb"); //Open the list of M_B magnitudes
			fichero_stat_bin_y = fopen( EXPL_B,"rb"); // Open the statistics of the M_B magnitudes
			factor_y = 4.325/3.24;
			break;
		case 2:
			fichero_isocr_bin_y = fopen( BIN_V,"rb"); //Open the list of M_V magnitudes
			fichero_stat_bin_y = fopen( EXPL_V,"rb"); // Open the statistics of the M_V magnitudes
			factor_y = 1.;
			break;
		case 3:
			fichero_isocr_bin_y = fopen( BIN_R,"rb"); //Open the list of M_R magnitudes
			fichero_stat_bin_y = fopen( EXPL_R,"rb"); // Open the statistics of the M_R magnitudes
			factor_y = 2.634/3.24;
			break;
		case 4:
			fichero_isocr_bin_y = fopen( BIN_I,"rb"); //Open the list of M_I magnitudes
			fichero_stat_bin_y = fopen( EXPL_I,"rb"); // Open the statistics of the M_I magnitudes
			factor_y = 1.962/3.24;
			break;
		case 5:
			fichero_isocr_bin_y = fopen( BIN_J,"rb"); //Open the list of M_J magnitudes
			fichero_stat_bin_y = fopen( EXPL_J,"rb"); // Open the statistics of the M_J magnitudes
			factor_y = 0.9536616/3.24;
			break;
		case 6:
			fichero_isocr_bin_y = fopen( BIN_H,"rb"); //Open the list of M_H magnitudes
			fichero_stat_bin_y = fopen( EXPL_H,"rb"); // Open the statistics of the M_H magnitudes
			factor_y = 0.5873472/3.24;
			break;
		case 7:
			fichero_isocr_bin_y = fopen( BIN_K,"rb"); //Open the list of M_K magnitudes
			fichero_stat_bin_y = fopen( EXPL_K,"rb"); // Open the statistics of the M_K magnitudes
			factor_y = 0.367/3.24;
			break;
		default:
			fprintf(stderr, "Unknown second filter. Johnson V band will be used instead\n");
			fichero_isocr_bin_y = fopen( BIN_V,"rb"); //Open the list of M_V magnitudes
			fichero_stat_bin_y = fopen( EXPL_V,"rb"); // Open the statistics of the M_V magnitudes
			factor_y = 1.;
	};

	//Since logmi_mi and Logmi_MAX are equal for all of the isocrones, the initial mass array would be the same:
	/* Probability given the Kroupa IMF */
	for(star=0; star<Max_Number_of_logmasses_per_isocrone;star++){
		mi[star] = pow(10., Logmi_MIN + dlogm*star);
	};

	// WARNING! This line is different from the Salpeter's version!

	//Initiliaze the HR:
	for(hr_bin=0;hr_bin<HR_dim[0]*HR_dim[1];hr_bin++){
		hr_map[hr_bin]=0.0;
	};

// Loop over the particles
	for(particle=0;particle<Nparticles;particle++){

		// Store the parameters of the considered particle:
		Z_p = Z[particle];
		logAge_p = logAge[particle];
		Mass_p = ParticleMass[particle];
		AV_p = A_V[particle];
		distance_correction_x = (10.+5.*log10(dist[particle]))*(bands_id[0]>=0);
		distance_correction_y = (10.+5.*log10(dist[particle]))*(bands_id[1]>=0);
		nobstars[particle] = 0.;
		// Initialize the Mfcis:
		Mfcis[particle] = 0.0; // Mass fraction converted to stars
		

		// Avoid out of range values:
		logAge_p = (logAge_p<logAge_min? logAge_min: logAge_p);
		logAge_p = (logAge_p>logAge_max? logAge_max: logAge_p);

		Z_p = (Z_p<Z_set[0]? Z_set[0]: Z_p);
		Z_p = (Z_p>Z_set[Number_of_Z-1]? Z_set[Number_of_Z-1]: Z_p);



		/* ++++++++++++++++++++++++++++++++ ESTIMATION OF THE WEIGHTS ARE DONE FIRSTLY ++++++++++++++++++++++++++++++++++++*/
		// By calculating the weights first, we avoid the use of huge hr_maps temporaly variables

			//......................................... LEFT DOWN POINT ..........................................
			// Get the index of that metallicity value (the Z sampling is not homogeneous):
			i_Z = -1; //Initial value
			do{
				i_Z++;
			}while( Z_set[i_Z]<=Z_p );
			i_Z--;

			//Assume that the logAge grid is regular (from logAge_min to logAge_max with step logAge_step)
			i_logAge = (int)((Number_of_logAge-1)*(logAge_p-logAge_min)/(logAge_max-logAge_min));

			//Weights:
			wLR = Z_p-Z_set[i_Z]; //For the Left-Down point of the grid


			//......................................... RIGHT DOWN POINT .........................................

			//i_logAge does not change but i_Z...
			i_Z = i_Z + 1*(i_Z!=(Number_of_Z-1));

			//Weights:
			wRL = Z_set[i_Z]-Z_p; //For the Right-Down point of the grid

			// ......................................... RIGHT UP POINT.........................................
			//i_Z as in the previous case, but i_logAge has increase (or not)...
			i_logAge = i_logAge + 1*(i_logAge!=(Number_of_logAge-1));
			//Weights:
			wUD = logAge_min - i_logAge*logAge_step - logAge_p; //For the Right-Up point of the grid

			// .........................................  LEFT UP POINT.........................................

			//i_logAge as in the previous case, but i_Z decreases (or not)...
			i_Z = i_Z - 1*(i_Z>0);
			//Weights:
			wDU = logAge_p - logAge_min - i_logAge*logAge_step; //For the Left-Up point of the grid

			// Be sure they are normalized...

			if(wLR==-wRL){
				wLR = 1.;
				wRL = 0.;
			}else{
				wLR = wRL/(wLR+wRL);
				wRL = 1.-wLR;
			};

			if(wUD==-wDU){
				wUD = 1.;
				wDU = 0.;
			}else{
				wUD = wDU/(wUD+wDU);
				wDU = 1.-wUD;
			};



		//-----------------------------------------------------------------------------------------------------
		// From now on, proceed as usual

		/* ++++++++++++++++++++++++++++++++ LEFT DOWN POINT ++++++++++++++++++++++++++++++++++++++++*/

		// Get the index of that metallicity value (the Z sampling is not homogeneous):
		i_Z = -1; //Initial value
		do{
			i_Z++;
		}while( Z_set[i_Z]<=Z_p );
		i_Z--;


		//Assume that the logAge grid is regular (from logAge_min to logAge_max with step logAge_step)
		i_logAge = (int)((Number_of_logAge-1)*(logAge_p-logAge_min)/(logAge_max-logAge_min));
		i_isocrone = i_logAge + (Number_of_logAge)*i_Z;


		// Read the following information about the selected isocrone: [starting_point of the isocrone, Nasses]
		fseek( fichero_stat_bin_x, 32*i_isocrone+16, SEEK_SET);// 32 bytes = 4 bytes*8 columns
		fseek( fichero_stat_bin_y, 32*i_isocrone+16, SEEK_SET);// 32 bytes = 4 bytes*8 columns

		fread(&starting_point_of_the_isocrone,4,1,fichero_stat_bin_x); //Starting position (inclusive)(common for both magnitudes)
		fread(&Number_of_logmasses_in_the_isocrone,4,1,fichero_stat_bin_x);// Rows (stars) that belong to this isocrone(common for both magnitudes)



		// Go to the desired isocrone
		fseek( fichero_isocr_bin_x, 4*starting_point_of_the_isocrone, SEEK_SET); //1=1*4 bytes = np.float32 bits*1 column
		fseek( fichero_isocr_bin_y, 4*starting_point_of_the_isocrone, SEEK_SET); //1=1*4 bytes = np.float32 bits*1 column

		// WARNING! This line is different from the Salpeter's version:
		// Call to the Kroupa IMF function
		KroupaIMF( mi, Max_Number_of_logmasses_per_isocrone, mi[Number_of_logmasses_in_the_isocrone-1], N_mi); //N_mi means number of masses between mi and mi+dm (egde effects included)


		// Load V and V-Ic from the isocrone given by (Z, logAge), and finish P(m_i)
		for(star=0;star<Number_of_logmasses_in_the_isocrone;star++){
			// Get the absolute magnitudes
			fread(&magnitude_x,4,1, fichero_isocr_bin_x); // Absolute magnitude
			fread(&magnitude_y,4,1, fichero_isocr_bin_y); // Absolute magnitude

			// From absolute to apparent
			magnitude_x = magnitude_x + distance_correction_x + AV_p*factor_x;
			magnitude_y = magnitude_y + distance_correction_y + AV_p*factor_y;

			// Would this star be included in the histogram?
			is_selected = (hr_axis[0]<=magnitude_x)*(magnitude_x<hr_axis[1]);// 1= Yes, 0=No
			is_selected = is_selected*(hr_axis[2]<=magnitude_y)*(magnitude_y<hr_axis[3]);// 1= Yes, 0=No

			// Make N_mi[star] == 0 if the star is out of range...
			N_mi[star] = wDU*wLR*N_mi[star]*is_selected;//...and apply the weighting

			// Mass fraction converted into observed stars
			Mfcis[particle] += mi[star]*N_mi[star]; //Add the contribution

			// Number of OBSERVED stars for this particular particle
			nobstars[particle] += Mass_p*N_mi[star]; //Add the contribution

			//HR
			bin_y = (int)(HR_dim[1]*(magnitude_y-hr_axis[2])/(hr_axis[3]-hr_axis[2]));// Row
			bin_x = (int)(HR_dim[0]*(magnitude_x-hr_axis[0])/(hr_axis[1]-hr_axis[0]));// Column
			hr_bin = bin_x + HR_dim[0]*bin_y;

			if(is_selected){
				hr_map[hr_bin] += Mass_p*N_mi[star];//Add the contribution to the histogram
			};

		}; // End of the star-loop


		/* ++++++++++++++++++++++++++++++++ RIGHT DOWN POINT ++++++++++++++++++++++++++++++++++++++++*/

		//i_logAge does not change but i_Z...
		i_Z = i_Z + 1*(i_Z!=(Number_of_Z-1));
		
		i_isocrone = i_logAge + Number_of_logAge*i_Z;

		// Read the following information about the selected isocrone: [starting_point of the isocrone, Nasses]
		fseek( fichero_stat_bin_x, 32*i_isocrone+16, SEEK_SET);// 32 bytes = 4 bytes*8 columns
		fseek( fichero_stat_bin_y, 32*i_isocrone+16, SEEK_SET);// 32 bytes = 4 bytes*8 columns

		fread(&starting_point_of_the_isocrone,4,1,fichero_stat_bin_x); //Starting position (inclusive)(common for both magnitudes)
		fread(&Number_of_logmasses_in_the_isocrone,4,1,fichero_stat_bin_x);// Rows (stars) that belong to this isocrone(common for both magnitudes)



		// Go to the desired isocrone
		fseek( fichero_isocr_bin_x, 4*starting_point_of_the_isocrone, SEEK_SET); //1=1*4 bytes = np.float32 bits*1 column
		fseek( fichero_isocr_bin_y, 4*starting_point_of_the_isocrone, SEEK_SET); //1=1*4 bytes = np.float32 bits*1 column

		// WARNING! This line is different from the Salpeter's version:
		// Call to the Kroupa IMF function
		KroupaIMF( mi, Max_Number_of_logmasses_per_isocrone, mi[Number_of_logmasses_in_the_isocrone-1], N_mi); //N_mi means number of masses between mi and mi+dm (egde effects included)

		// Load V and V-Ic from the isocrone given by (Z, logAge), and finish P(m_i)
		for(star=0;star<Number_of_logmasses_in_the_isocrone;star++){
			// Get the absolute magnitudes
			fread(&magnitude_x,4,1, fichero_isocr_bin_x); // Absolute magnitude
			fread(&magnitude_y,4,1, fichero_isocr_bin_y); // Absolute magnitude

			// From absolute to apparent
			magnitude_x = magnitude_x + distance_correction_x + AV_p*factor_x;
			magnitude_y = magnitude_y + distance_correction_y + AV_p*factor_y;

			
			// Would this star be included in the histogram?
			is_selected = (hr_axis[0]<=magnitude_x)*(magnitude_x<hr_axis[1]);// 1= Yes, 0=No
			is_selected = is_selected*(hr_axis[2]<=magnitude_y)*(magnitude_y<hr_axis[3]);// 1= Yes, 0=No

			// Make N_mi[star] == 0 if the star is out of range...
			N_mi[star] = wDU*wRL*N_mi[star]*is_selected;//...and apply the weighting

			// Mass fraction converted into observed stars
			Mfcis[particle] += mi[star]*N_mi[star]; //Add the contribution

			// Number of OBSERVED stars for this particular particle
			nobstars[particle] += Mass_p*N_mi[star]; //Add the contribution

			//HR
			bin_y = (int)(HR_dim[1]*(magnitude_y-hr_axis[2])/(hr_axis[3]-hr_axis[2]));// Row
			bin_x = (int)(HR_dim[0]*(magnitude_x-hr_axis[0])/(hr_axis[1]-hr_axis[0]));// Column
			hr_bin = bin_x + HR_dim[0]*bin_y;

			if(is_selected){
				hr_map[hr_bin] += Mass_p*N_mi[star];//Add the contribution to the histogram
			};

		}; // End of the star-loop


		/* ++++++++++++++++++++++++++++++++ RIGHT UP POINT ++++++++++++++++++++++++++++++++++++++++*/

		//i_Z as in the previous case, but i_logAge has increase (or not)...
		i_logAge = i_logAge + 1*(i_logAge!=(Number_of_logAge-1));
		i_isocrone = i_logAge + Number_of_logAge*i_Z;

		// Read the following information about the selected isocrone: [starting_point of the isocrone, Nasses]
		fseek( fichero_stat_bin_x, 32*i_isocrone+16, SEEK_SET);// 32 bytes = 4 bytes*8 columns
		fseek( fichero_stat_bin_y, 32*i_isocrone+16, SEEK_SET);// 32 bytes = 4 bytes*8 columns

		fread(&starting_point_of_the_isocrone,4,1,fichero_stat_bin_x); //Starting position (inclusive)(common for both magnitudes)
		fread(&Number_of_logmasses_in_the_isocrone,4,1,fichero_stat_bin_x);// Rows (stars) that belong to this isocrone(common for both magnitudes)



		// Go to the desired isocrone
		fseek( fichero_isocr_bin_x, 4*starting_point_of_the_isocrone, SEEK_SET); //1=1*4 bytes = np.float32 bits*1 column
		fseek( fichero_isocr_bin_y, 4*starting_point_of_the_isocrone, SEEK_SET); //1=1*4 bytes = np.float32 bits*1 column

		// WARNING! This line is different from the Salpeter's version:
		// Call to the Kroupa IMF function
		KroupaIMF( mi, Max_Number_of_logmasses_per_isocrone, mi[Number_of_logmasses_in_the_isocrone-1], N_mi); //N_mi means number of masses between mi and mi+dm (egde effects included)

		// Load V and V-Ic from the isocrone given by (Z, logAge), and finish P(m_i)
		for(star=0;star<Number_of_logmasses_in_the_isocrone;star++){
			// Get the absolute magnitudes
			fread(&magnitude_x,4,1, fichero_isocr_bin_x); // Absolute magnitude
			fread(&magnitude_y,4,1, fichero_isocr_bin_y); // Absolute magnitude

			// From absolute to apparent
			magnitude_x = magnitude_x + distance_correction_x + AV_p*factor_x;
			magnitude_y = magnitude_y + distance_correction_y + AV_p*factor_y;

			
			// Would this star be included in the histogram?
			is_selected = (hr_axis[0]<=magnitude_x)*(magnitude_x<hr_axis[1]);// 1= Yes, 0=No
			is_selected = is_selected*(hr_axis[2]<=magnitude_y)*(magnitude_y<hr_axis[3]);// 1= Yes, 0=No

			// Make N_mi[star] == 0 if the star is out of range...
			N_mi[star] = wUD*wRL*N_mi[star]*is_selected;//...and apply the weighting

			// Mass fraction converted into observed stars
			Mfcis[particle] += mi[star]*N_mi[star]; //Add the contribution

			// Number of OBSERVED stars for this particular particle
			nobstars[particle] += Mass_p*N_mi[star]; //Add the contribution

			//HR
			bin_y = (int)(HR_dim[1]*(magnitude_y-hr_axis[2])/(hr_axis[3]-hr_axis[2]));// Row
			bin_x = (int)(HR_dim[0]*(magnitude_x-hr_axis[0])/(hr_axis[1]-hr_axis[0]));// Column
			hr_bin = bin_x + HR_dim[0]*bin_y;

			if(is_selected){
				hr_map[hr_bin] += Mass_p*N_mi[star];//Add the contribution to the histogram
			};

		}; // End of the star-loop

		/* ++++++++++++++++++++++++++++++++ LEFT UP POINT ++++++++++++++++++++++++++++++++++++++++*/

		//i_logAge as in the previous case, but i_Z decreases (or not)...
		i_Z = i_Z - 1*(i_Z>0);
		i_isocrone = i_logAge + Number_of_logAge*i_Z;

		// Read the following information about the selected isocrone: [starting_point of the isocrone, Nasses]
		fseek( fichero_stat_bin_x, 32*i_isocrone+16, SEEK_SET);// 32 bytes = 4 bytes*8 columns
		fseek( fichero_stat_bin_y, 32*i_isocrone+16, SEEK_SET);// 32 bytes = 4 bytes*8 columns

		fread(&starting_point_of_the_isocrone,4,1,fichero_stat_bin_x); //Starting position (inclusive)(common for both magnitudes)
		fread(&Number_of_logmasses_in_the_isocrone,4,1,fichero_stat_bin_x);// Rows (stars) that belong to this isocrone(common for both magnitudes)


		// Go to the desired isocrone
		fseek( fichero_isocr_bin_x, 4*starting_point_of_the_isocrone, SEEK_SET); //1=1*4 bytes = np.float32 bits*1 column
		fseek( fichero_isocr_bin_y, 4*starting_point_of_the_isocrone, SEEK_SET); //1=1*4 bytes = np.float32 bits*1 column

		// WARNING! This line is different from the Salpeter's version:
		// Call to the Kroupa IMF function
		KroupaIMF( mi, Max_Number_of_logmasses_per_isocrone, mi[Number_of_logmasses_in_the_isocrone-1], N_mi); //N_mi means number of masses between mi and mi+dm (egde effects included)

		// Load V and V-Ic from the isocrone given by (Z, logAge), and finish P(m_i)
		for(star=0;star<Number_of_logmasses_in_the_isocrone;star++){
			// Get the absolute magnitudes
			fread(&magnitude_x,4,1, fichero_isocr_bin_x); // Absolute magnitude
			fread(&magnitude_y,4,1, fichero_isocr_bin_y); // Absolute magnitude

			// From absolute to apparent
			magnitude_x = magnitude_x + distance_correction_x + AV_p*factor_x;
			magnitude_y = magnitude_y + distance_correction_y + AV_p*factor_y;

	
			// Would this star be included in the histogram?
			is_selected = (hr_axis[0]<=magnitude_x)*(magnitude_x<hr_axis[1]);// 1= Yes, 0=No
			is_selected = is_selected*(hr_axis[2]<=magnitude_y)*(magnitude_y<hr_axis[3]);// 1= Yes, 0=No

			// Make N_mi[star] == 0 if the star is out of range...
			N_mi[star] = wUD*wLR*N_mi[star]*is_selected;//...and apply the weighting

			// Mass fraction converted into observed stars
			Mfcis[particle] += mi[star]*N_mi[star]; //Add the contribution

			// Number of OBSERVED stars for this particular particle
			nobstars[particle] += Mass_p*N_mi[star]; //Add the contribution

			//HR
			bin_y = (int)(HR_dim[1]*(magnitude_y-hr_axis[2])/(hr_axis[3]-hr_axis[2]));// Row
			bin_x = (int)(HR_dim[0]*(magnitude_x-hr_axis[0])/(hr_axis[1]-hr_axis[0]));// Column
			hr_bin = bin_x + HR_dim[0]*bin_y;

			if(is_selected){
				hr_map[hr_bin] += Mass_p*N_mi[star];//Add the contribution to the histogram
			};

		}; // End of the star-loop

	}; // End of the particle

	fclose( fichero_stat_bin_x );
	fclose( fichero_isocr_bin_x );
	fclose( fichero_stat_bin_y );
	fclose( fichero_isocr_bin_y );

}//End of HR_Kroupa_4Points
//-----------------------------------------------------------------------------------------------------------------//
