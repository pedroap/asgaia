#	What is Asgaia?
=================================================================================

Asgaia is a code that allows us to estimate statistical quantities, such as mean values or distributions, about the stellar population contained in numerical simulation particles, without creating a mock catalogue. Asgaia makes use of a grid of PARSEC isochrones (http://stev.oapd.inaf.it/cgi-bin/cmd) to extract the stellar populations, and includes the observational effects of Gaia described in the ESA's webpage (https://www.cosmos.esa.int/web/gaia/science-performance).

Asgaia is written in C, but it is implemented through the Python module **AsgaiaTools**.

For more information, please do not hesitate to contact me at: pedroap@iac.es

#	How to install AsgaiaTools
=================================================================================

1). Download the files found in the repository:

2). Download the .zip file:
https://www.dropbox.com/s/arx2j9ud35jlhff/Data_files.zip?dl=0

3). Select the directory where you want to install Asgaia. We recommend you to create a new folder named "Asgaia" in the directory you usually store your python libraries.

4). Move the files downloaded in 1) to that directory.

5). Inside this folder, create a sub-folder with the name "Data_files"

6). Unzip the file downloaded in 2) an put all the extracted files in the sub-folder you have just created. From now on, we do not need to care about this sub-folder.

7). Edit the first line of **AsgaiaConstants.h** with your path to the directory you have selected in 3). and save the changes:
~~~
#define PATH "(edit_this_path_to_the_selected_folder)/Asgaia/Data_files/"
~~~
(please note the "/" at the end).

8). Open a terminal in this directory.

9). Type:
~~~
python setupAsgaia.py install (press enter)
~~~
10). You may obtain several warnings like:

			 #warning "Using deprecated NumPy API, disable it by " \
			  ^
			CAsgaiaTools.c: In function 'CAsgaiaTools_IMFdistr':
			CAsgaiaTools.c:44:122: warning: 'return' with no value, in function returning non-void [-Wreturn-type]
			  import_array();// needed command

but they are not important.

11). If this setup was correct, you would find a new folder named "build" which contains two sub-folders. Inside one of them, there is a .so file named "CAsgaiaTools.so", which has to be copied/moved to the selected folder (the one you chose in 3). After that, you can delete "build".

12). If you are a Python expert you will know how to make your scripts use "AsgaiaTools". Otherwise, I strongly recommend you to use the following optional steps:

13). Make a link of AsgaiaTools.py and CAsgaiaTools.so and move them to the directory (or directories) where you usually run your python scripts.

14). From now on, include the following command in the head of your scripts:
~~~
import AsgaiaTools
~~~
or any other valid command.

15). Use the AsgaiaTools functions as it is usual in Python.

Caveat: In reality, AsgaiaTools is just friendly module that allows us to use the functions of CAsgaiaTools in a easier way. The computationally hard tasks are always performed by CAsgaiaTools, but this module is more confusing to use due to some C-language restrictions. You can work directly with CAsgaiaTools under your responsability.


#	Practical Examples
================================================================================

To check if the installation was correct, put the following files in the same directory:

* Example*.ipynb
* CAsgaiaTools.so (or a link to the original file)
* AsgaiaTools.py (or a link to the original file)
* Particle_list.dat

Open **jupyter** or **ipython** and run the examples. You can remove the .ipynb and the .dat files after these tests.

##	Examples description:
 * Example.ipynb: Computes the Color-Magnitude diagram (CMD) and the Hertzsprung-Russell (HR) diagram of a set of particles. First, it computes the CMD and the HR diagram using absolute magnitudes, since no distances or positions are provided. Then, calculates the apparent magnitudes.

 * Example2.ipynb: Illustrative example on how to obtain the mean parallax error for the stars contained in the particles. Two methods are described: the first one computes each quantity separately and allows to introduce your own extinction values A_V, while the second assumes you want to use the Schlegel maps.

 * Example3.ipynb: This exercise titled "The role of the IMF in the G-band magnitude distribution" illustrates how to set the differents Initial Mass Functions allowed by **Asgaia**. This algorithm computes the distribution of the G band magnitudes of the set of particles Particle_list.dat.

 * Example4.ipynb: The same as the example 3 but in other bands: UBVRIJHK.

 * Example5.ipynb: Another example similar to Example2.ipynb.


#	About the Asgaia functions
================================================================================
##	1.1 Isochrones
**Asgaia** uses the same set of isochrones as GALAXIA [(Sharma et al. 2011)](http://adsabs.harvard.edu/abs/2011ApJ...730....3S) and Snapdragons ([Hunt et al. 2015](http://adsabs.harvard.edu/abs/2015MNRAS.450.2132H)):

 * log(Age/1 yr): from 6.6 to 10.22 with step 0.02.
 * Metallicity:
	+ from Z=0.00010 to Z=0.00100 with step 0.0001, without Z=0.0003
	+ from Z=0.00100 to Z=0.00260 with step 0.0002.
	+ from Z=0.00300 to Z=0.01000 with step 0.0010, with the inclusion of Z=0.00340
	+ from Z=0.01000 to Z=0.03000 with step 0.0020, without Z=0.02200 and Z=0.02600

These PARSEC isochrones provide the absolute magnitude in the U, B, V, R, I (also named as Ic), J, H and K bands, and the stellar parameters logTe, logg and mbol (bolometric absolute magnitude). The apparent magnitudes in the Gaia bands (G_BP, G, G_RP and G_RVS) are estimated using V and V-Ic (also apparent magnitudes, see [Evans et al. 2018](http://adsabs.harvard.edu/abs/2018arXiv180409368E) and [Jordi et al. 2010](http://adsabs.harvard.edu/abs/2010A%26A...523A..48J)). Although some functions of Asgaia return absolute magnitudes in the Gaia bands, you should keep in mind that these luminosities may not be accurate enough because they are calculated from the estimation of the apparent luminosities. In other words, absolute magnitudes in G_BP, G, G_RP and G_RVS bands are not provided by the isocrones, but extrapolated from the apparent magnitudes estimated with the fitting of [Jordi et al. 2010](http://adsabs.harvard.edu/abs/2010A%26A...523A..48J).

If you want to use another set of isochrones, please contact me at pedroap@iac.es to know more about the data files structure. 

##	1.2 Asgaia useful information

 * The age of the particles younger than 10^6.6 yrs will be redefined to 10^6.6 yrs.
 * The age of the particles older than 10^10.22 yrs will be redefined to 10^22 yrs.
 * Metallicities below this range will be set to Z=0.001.
 * Metallicities above this range will be set to Z=0.03.
 * No differences in chemical abundances were considered.
 * Each particle is modelled as by a single stellar population: no second-generation stars were considered.
 * The mass-loss is not included, because it is relevant only for high mass and very young stars.
 * No extinction correction for sources beyond 30 kpc or closer than 10 pc, 
 * The absorption in the G_RVS band is assumed to be the same as in the G_RP band, because no official value was found. This overestimates the extinction in G_RVS.
 * The absolute magnitudes in the Gaia bands are estimated using the absolute values of V and Ic, so they are not as good as their apparent counterparts.


##	1.3 Output
The functions that estimate errors will return a negative value (typically -99.0) when a particle do not provide stars observable by Gaia, while other functions return -INFINITY or INFINITY. The code you include after the function call must account for it.

##	1.4 Precision and types
The functions of **CAsgaiaTools** usually consider float variables instead of doubles.
**AsgaiaTools** converts the array inputs to np.float32 arrays automatically. If you prefer to use **CAsgaiaTools** directly, you **must** convert the input arrays to np.float32.


##	2. Initial Mass Functions
--------------------------------------------------------------------------------
**AsgaiaTools** has two main functions related to the IMFs (**IMFdistr** and **IMF**), as well as many other which use an IMF. All of these functions allow us to choose among three different types of IMF:

 * [Salpeter](http://adsabs.harvard.edu/abs/1955ApJ...121..161S) ('s').
 * [Kroupa](http://adsabs.harvard.edu/abs/2002Sci...295...82K) ('k').
 * [Bimodal](http://adsabs.harvard.edu/abs/1996ApJS..106..307V) ('b').


###    2.1 Salpeter IMF
The *Salpeter IMF* is defined as:

k·m^{-2.35} if m>=m0

and 0 otherwise, with m0=0.1 M0.

The normalisation parameter k is chosen such that the total mass of the stellar population equals 1 M0. 

###    2.2 Kroupa IMF
The *Kroupa IMF* is divided in three different intervals:

k·m^{-0.3} for m <\ m0

k·m^{-1.3} for m0<= m <\ m1

k·m^{-2.3} for m1 <\ m

in which m0 = 0.08 M0, m1 = 0.5 M0.
The normalisation parameter k is chosen such that the total mass of the stellar population equals 1 M0.

###    2.3 Bimodal IMF
The *bimodal IMF* is defined as:

 0 if m<\m0
 
 k·(m12)^{-u} if m0 <= m <\ m1
 
 k·spline(m) if m1 <= m <\ m2
 
 k·m^{-u}  if m2 <= m

Where m0 = 0.1 M_0, m1 = 0.2 M_0, m2 = 0.6 M_0. In the original paper of [Vazdekis et al. 1996](http://adsabs.harvard.edu/abs/1996ApJS..106..307V) m12 = 0.4 M_0, but we have adopted the more flexible definition of

 m12 = (m1+m2)/2

so that m12=0.4 M0 when the standard values for m1 and m2 are assumed.
spline(m) is a third order polynomial which makes the IMF continuous and differenciable. The four coefficients of this polynomial are not trivial, but they can be calculated solving a 4x4 linear equation system.
The term "u" is the free parameter of the bimodal IMF. In the **Asgaia** functions, the default value assumed for "u" is 2.35, in order to match the Salpeter IMF for high stellar masses.

The normalisation parameter k is chosen such that the total mass of the stellar population equals 1 M0.

###    2.4 Customized IMF
In case a new IMF is required, you can edit one of the previous IMFs to implement the desired one. All the values assumed for the parameters of the different IMFs can be found in **AsgaiaConstants.h** (just search for *Salpeter constants* in your text editor). These constants or *MACROS* are named in the following way:

 * The first character refers to the IMF type: 's' for Salpeter, 'k' for Kroupa and 'b' for bimodal.
 * Then "imf_" is added.
 * If the constant refers to a mass (an edge of the different intervals that define the IMFs), "m"+number is used. Similarly, the exponents are denoted by "a"+number.

Please, remember that the masses must be sorted to be consistent, and that an IMF which decays as m^-u with u\<\2 leads to an increasing cumulative distribution of masses.
Providing that these caveats are taken into account, you do have to worry about neither the continuity of the piecewise IMFs nor the normalisation constants k. The Kroupa IMF, moreover, allows to introduce an additional domain through *kimf_m2* and *kimf_a2*.

Once the custom IMF is created, repeat the steps described in the *"How to install AsgaiaTools"* section.


##	3. Extinction map
--------------------------------------------------------------------------------
###	3.1 Functions description and implementation
There are five functions in **AsgaiaTools** involved in the modelling of the extinction:

 * ColourExcessMap(lon, lat, degrees = False);
 * ColourExcess3DMap(lon, lat, dist, degrees=False);
 * SharmaCorrection(exS, ex3, band = 'V');
 * GetAV(lon, lat, dist_kpc);
 * ExtinctionConversion( band, band_ini = 'V');

The first function (*ColourExcessMap*) returns the colour excess E(B-V) provided by the Schlegel maps for a very far source at the (lon,lat) sky position. For nearby sources (i.e. for sources at finite distance from the Sun), this colour excess is corrected according to the method described in Bland-Hawthorn et al. (2010). Thus, the output of *ColourExcessMap()* must be multiplied by the output of the *ColourExcess3DMap()* function. After that, we suggest to also apply the correction proposed by Sharma et al. (2013) through *SharmaCorrection()*. Finally, to convert the Colour Excess E(B-V) to the extinction in the *V* band (A_V), just multiply E(B-V) by 3.24:

	A_V = 3.24·E(B-V)

All of these steps can be directly implemented using *GetAV(lon, lat, dist_kpc)* (see Section 1).
In case you are interested in the extinction for other bands, you can just use *ExtinctionConversion()*. For example, the absorption in the R band can be calculated from A_V as follows:

	A_R = A_V*ExtinctionConversion('R');

In the same way, the conversion from A_R to A_B is:

	A_B = A_R*ExtinctionConversion('B','R');

###	3.2 Using other extinction maps
Many functions of **AsgaiaTools** uses the extinction in the visual band A_V as input, which value is computed from the E(B-V) Schlegel maps using certain functions (see Section 3.1). In case you are interested in other extinction maps (for example those of [Green et al. 2015](http://adsabs.harvard.edu/abs/2015arXiv150701005G) or [Green et al. 2018](http://adsabs.harvard.edu/abs/2018arXiv180103555G)), just use the A_V arrays provided by these maps (you may be also interested in [this tool](http://dustmaps.readthedocs.io/en/latest/)).


##	4. Names of the functions
--------------------------------------------------------------------------------

All the AsgaiaTools functions have additional documentation about how to use them. To read that information, just add "?" at the end of the function's name in your Python IDE. You can also use the help() command. 
Usually, all the AsgaiaTools functions whose name stars with the word "Get" imply some handly simplifications; i.e., they combine several **AsgaiaTools** or **CAsgaiaTools** functions to reduce the number of lines of code. For example, GetSigmaPiGaia combines:

 * ColourExcessMap
 * ColourExcess3DMap
 * geometric_correction
 * SigmaPiGaia_


